<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/* Multisite */
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/dev/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+}+6^z~xt6X!|x,=:?;:NC6&WkL5eV0sN/p:T>j-E~%[)(a*++<l3J9|2}?j|{&5');
define('SECURE_AUTH_KEY',  '&Uo58Nd;<)Ru|,U} |k*}HJHpfo1u1~PS]d:N *8^0kfY$TD*Q* 6($sk|e7 dME');
define('LOGGED_IN_KEY',    'c+^uOgQkFo-^mG|lb(y<VUf?^9I~-v[X74P=l%s38L-~U-WIZ@W_]v6lVb--)_]i');
define('NONCE_KEY',        'L]%,U=;Bfq2=[RS<$IjAkG)gNEk~sr04-N#nN)Bm:&tG3sY4:H!|u%bsu8u&)6UM');
define('AUTH_SALT',        '8Ge06)~<5XeP]Y/N,A]u;J(j,dd{<W)JT![*[*,_C/&Ri>vj-}k#*YU^7`]J7uiO');
define('SECURE_AUTH_SALT', 'tQk(mPSiXmg}33hmAX@)Z`V%6a.znhvnB#+KNJd6ic&WgF=Y@?+1(Zkb5]3Z10L^');
define('LOGGED_IN_SALT',   'ir/UDI>7j</1ht/$n`/MF;jvc V*!V_vZO)ggne-:g&.^ZH~enKD%p4UwFA7nQP=');
define('NONCE_SALT',       'Bf+0zxeoGTxhjTmQ0Mt7t;K0JJKe~x<o4#U,Lk$&<7msf SovNU[-5{*1SRmV_-(');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG',true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
