# Copyright (C) 2014 System Plugin
# This file is distributed under the same license as the System Plugin package.
msgid ""
msgstr ""
"Project-Id-Version: System Plugin\n"
"POT-Creation-Date: 2016-02-02 09:56-0600\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2016-02-15 09:11-0600\n"
"Last-Translator: \n"
"X-Generator: Poedit 1.8.7\n"
"X-Poedit-Basepath: ../..\n"
"X-Poedit-KeywordsList: __;_e;__ngettext;_n;__ngettext_noop;_n_noop;_x;_nx;_nx_noop;_ex;esc_attr__;"
"esc_attr_e;esc_attr_x;esc_html__;esc_html_e;esc_html_x;_c;_nc\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Language-Team: \n"
"Language: fi\n"
"X-Poedit-SearchPath-0: gd-system-plugin.php\n"
"X-Poedit-SearchPath-1: gd-system-plugin\n"
"X-Poedit-SearchPathExcluded-0: gd-system-plugin/plugins\n"

#: gd-system-plugin.php:342
msgid "I would be interested in hiring a professional to help me with my WordPress site."
msgstr "Olen kiinnostunut palkkaamaan asiantuntijan WordPress-sivustoni suunnittelua varten."

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:83
msgid "Media Temple"
msgstr "Media Temple"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:90
msgid "Managed WordPress"
msgstr "WordPress-hallinta"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:97
#: gd-system-plugin/class-gd-system-plugin-admin-page.php:205
#: gd-system-plugin/class-gd-system-plugin-admin-page.php:206
msgid "GoDaddy"
msgstr "GoDaddy"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:119
msgid "Account Settings"
msgstr "Tilin asetukset"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:131
msgid "Flush Cache"
msgstr "Tyhjennä välimuisti"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:189
msgid "UserVoice"
msgstr "UserVoice"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:192
msgid "Hide the UserVoice feedback button from the Dashboard"
msgstr "Piilota UserVoice-palautepainike dashboard-sivulta"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:244
msgid "For your security, we’ve disabled WordPress’ built-in file editor by default."
msgstr ""
"Tietoturvasyistä WordPressin sisäänrakennettu tiedostoeditori on oletusasetuksena poistettu "
"käytöstä."

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:245
msgid "If you enable editing, all plugin and theme files become editable."
msgstr "Jos otat muokkauksen käyttöön, kaikki plugin- ja teematiedostot ovat jälleen muokattavissa."

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:246
msgid "Enable Editing"
msgstr "Ota muokkaus käyttöön"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:258
msgid "File editing is not enabled on this site"
msgstr "Tiedoston muokkaus ei ole käytössä tässä sivustossa"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:310
#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:361
#, php-format
msgid "Sorry, but you have to <a href=\"%s\">change your domain name here</a>."
msgstr "Pahoittelumme, mutta sinun täytyy <a href=\"%s\">muuttaa verkkotunnuksesi nimeä täällä</a>."

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:99
msgid "FAQ &amp; Support"
msgstr "Usein kysytyt kysymykset ja tuki"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:100
msgid "Hire A Pro"
msgstr "Hanki ammattilainen"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:101
msgid "Plugin Partners"
msgstr "Plugin-kumppanit"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:419
msgid "Find A Freelancer"
msgstr "Etsi freelancer"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:429
msgid ""
"Need a freelancer to design your website? GoDaddy Pro Connect filters through thousands of "
"designers to put you in touch with the best ones for your site and budget."
msgstr ""
"Tarvitsetko freelancerin sivustosi suunnittelua varten? GoDaddy Pro Connect käy läpi tuhansia "
"suunnittelijoita löytääkseen juuri sinun sivustollesi ja budjetillesi sopivat suunnittelijat."

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:431
msgid ""
"We send you three on-budget quotes (no cost or obligation). You choose the best fit, and they get "
"the job done."
msgstr ""
"Lähetämme sinulle kolme budjettisi mukaista hintatarjousta (tähän ei liity kustannuksia tai "
"velvoitteita). Kun olet valinnut sopivimman suunnittelijan, hän tekee työn sinulle."

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:435
#: gd-system-plugin/class-gd-system-plugin-admin-page.php:568
msgid "Learn More"
msgstr "Lisätietoja"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:465
msgid "Meet the plugins that meet our high standards."
msgstr "Tutustu korkeat vaatimuksemme täyttäviin plugin-laajennuksiin."

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:467
msgid ""
"We've partnered with the world’s top WordPress plugin authors to provide a list of plugins that "
"work well with GoDaddy WordPress hosting."
msgstr ""
"Teemme yhteistyötä huippuluokan WordPress-plugin-suunnittelijoiden kanssa. Näin pystymme "
"tarjoamaan joukon plugin-laajennuksia, jotka toimivat erityisen hyvin GoDaddyn WordPress-"
"hallintapalvelun kanssa."

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:477
msgid "Plugins list"
msgstr "Plugin-luettelo"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:485
msgid "Whoops! There was a problem fetching the list of plugins, please try reloading this page."
msgstr ""
"Virhe. Plugin-laajennusten luettelon hakemisessa tapahtui virhe. Yritä ladata tämä sivu uudelleen."

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:530
#, php-format
msgid "More information about %s"
msgstr "Lisätietoja: %s"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:554
#, php-format
msgid "Install %s now"
msgstr "Asenna %s nyt"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:554
msgid "Install Now"
msgstr "Asenna nyt"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:558
#, php-format
msgid "Update %s now"
msgstr "Päivitä %s nyt"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:558
msgid "Update Now"
msgstr "Päivitä nyt"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:562
msgid "This plugin is already installed and is up to date"
msgstr "Tämä laajennus on jo asennettuna ja ajan tasalla"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:562
msgid "Installed"
msgstr "Asennettu"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:582
#, php-format
msgid "By %s"
msgstr "– %s"

#: gd-system-plugin/class-gd-system-plugin-api.php:80
#, php-format
msgid "Unknown verb: %s. Try GET or POST"
msgstr "Tuntematon verbi: %s. Yritä näitä: GET tai POST"

#: gd-system-plugin/class-gd-system-plugin-api.php:115
#, php-format
msgid "API returned bad status: %d: %s"
msgstr "API palautti virheellisen tilan: %d: %s"

#: gd-system-plugin/class-gd-system-plugin-blacklist.php:86
#: gd-system-plugin/class-gd-system-plugin-blacklist.php:101
msgid "Not available"
msgstr "Ei käytettävissä"

#: gd-system-plugin/class-gd-system-plugin-blacklist.php:87
#: gd-system-plugin/class-gd-system-plugin-blacklist.php:102
msgid ""
"This plugin is not allowed on our system due to performance, security, or compatibility concerns. "
"Please contact our support with any questions."
msgstr ""
"Tämä plugin-laajennus ei ole sallittu järjestelmässämme suorituskyky- tietoturva- tai "
"yhteensopivuusongelmien vuoksi. Jos haluat lisätietoja, ota yhteyttä asiakastukeen."

#: gd-system-plugin/class-gd-system-plugin-blacklist.php:123
#, php-format
msgid "This plugin %s is not allowed on our system. It has been automatically deactivated."
msgstr ""
"Tämä plugin-laajennus %s ei ole sallittu järjestelmässämme. Se on automaattisesti poistettu "
"käytöstä."

#: gd-system-plugin/class-gd-system-plugin-cache-purge.php:295
#: gd-system-plugin/class-gd-system-plugin-command-controller.php:66
msgid "Cache cleared"
msgstr "Välimuisti tyhjennetty"

#: gd-system-plugin/class-gd-system-plugin-cname.php:137
#, php-format
msgid ""
"<strong>Note:</strong> You're using the temporary domain <strong>%s</strong>. <a href=\"%s\" "
"target=\"_blank\">Change domain</a>"
msgstr ""
"<strong>Huomautus:</strong> Käytät tilapäistä verkkotunnusta <strong>%s</strong>. <a href=\"%s\" "
"target=\"_blank\">Vaihda verkkotunnus</a>"

#: gd-system-plugin/class-gd-system-plugin-command-controller.php:163
msgid "File editing enabled"
msgstr "Tiedoston muokkaus käytössä"

#: gd-system-plugin/class-gd-system-plugin-dashboard-widgets.php:61
msgid "GoDaddy Garage"
msgstr "GoDaddy Garage (GoDaddy-talli)"

#: gd-system-plugin/class-gd-system-plugin-messages.php:90
msgid "Success"
msgstr "Onnistui"

#: gd-system-plugin/class-gd-system-plugin-pointers.php:47
msgid "Good news!"
msgstr "Hyviä uutisia!"

#: gd-system-plugin/class-gd-system-plugin-pointers.php:48
msgid ""
"You can now access <strong>Flush Cache</strong> and other links directly from the admin bar using "
"your desktop or mobile device."
msgstr ""
"Voit nyt käyttää linkkejä, esim. <strong>Tyhjennä välimuisti</strong>, suoraan hallintapalkista "
"tietokoneella tai mobiililaitteella."

#: gd-system-plugin/class-gd-system-plugin-temp-domain.php:88
#, php-format
msgid "%s This is your staging site and it cannot be indexed by search engines."
msgstr "%s Tämä on muokkaussivustosi, eivätkä hakukoneet voit indeksoida sitä."

#: gd-system-plugin/class-gd-system-plugin-temp-domain.php:89
#: gd-system-plugin/class-gd-system-plugin-temp-domain.php:96
msgid "Note:"
msgstr "Huomautus:"

#: gd-system-plugin/class-gd-system-plugin-temp-domain.php:95
#, php-format
msgid "%s Your site is using a temporary domain that cannot be indexed by search engines."
msgstr "%s Sivustosi käyttää väliaikaista verkkotunnusta, jota hakukoneet eivät voi indeksoida."
