# Copyright (C) 2014 System Plugin
# This file is distributed under the same license as the System Plugin package.
msgid ""
msgstr ""
"Project-Id-Version: System Plugin\n"
"POT-Creation-Date: 2016-02-02 09:56-0600\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2016-02-15 09:10-0600\n"
"Last-Translator: \n"
"X-Generator: Poedit 1.8.7\n"
"X-Poedit-Basepath: ../..\n"
"X-Poedit-KeywordsList: __;_e;__ngettext;_n;__ngettext_noop;_n_noop;_x;_nx;_nx_noop;_ex;"
"esc_attr__;esc_attr_e;esc_attr_x;esc_html__;esc_html_e;esc_html_x;_c;_nc\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Language-Team: \n"
"Language: zh_CN\n"
"X-Poedit-SearchPath-0: gd-system-plugin.php\n"
"X-Poedit-SearchPath-1: gd-system-plugin\n"
"X-Poedit-SearchPathExcluded-0: gd-system-plugin/plugins\n"

#: gd-system-plugin.php:342
msgid "I would be interested in hiring a professional to help me with my WordPress site."
msgstr "我希望聘请一名专业人员，以解决我的 WordPress 网站问题。"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:83
msgid "Media Temple"
msgstr "Media Temple"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:90
msgid "Managed WordPress"
msgstr "WordPress 托管"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:97
#: gd-system-plugin/class-gd-system-plugin-admin-page.php:205
#: gd-system-plugin/class-gd-system-plugin-admin-page.php:206
msgid "GoDaddy"
msgstr "GoDaddy"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:119
msgid "Account Settings"
msgstr "账户设置"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:131
msgid "Flush Cache"
msgstr "刷新缓存"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:189
msgid "UserVoice"
msgstr "UserVoice"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:192
msgid "Hide the UserVoice feedback button from the Dashboard"
msgstr "从仪表板隐藏 UserVoice 反馈按钮"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:244
msgid "For your security, we’ve disabled WordPress’ built-in file editor by default."
msgstr "为安全起见，我们已默认禁用 WordPress 的内置文件编辑器。"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:245
msgid "If you enable editing, all plugin and theme files become editable."
msgstr "如果您启用编辑，所有插件和主题文件都将可编辑。"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:246
msgid "Enable Editing"
msgstr "启用编辑"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:258
msgid "File editing is not enabled on this site"
msgstr "此网站上的文件编辑未启用"

#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:310
#: gd-system-plugin/class-gd-system-plugin-admin-menu.php:361
#, php-format
msgid "Sorry, but you have to <a href=\"%s\">change your domain name here</a>."
msgstr "很抱歉，但您必须<a href=\"%s\">在此处更改您的域名</a>。"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:99
msgid "FAQ &amp; Support"
msgstr "常见问题解答和客户支持"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:100
msgid "Hire A Pro"
msgstr "聘请专业人员"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:101
msgid "Plugin Partners"
msgstr "插件合作伙伴"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:419
msgid "Find A Freelancer"
msgstr "寻找自由工作者"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:429
msgid ""
"Need a freelancer to design your website? GoDaddy Pro Connect filters through thousands of "
"designers to put you in touch with the best ones for your site and budget."
msgstr ""
"需要一名自由工作者设计您的网站？ GoDaddy Pro 将对成千上万名设计师进行筛选，帮助您联系最适合"
"您网站和预算的设计师。"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:431
msgid ""
"We send you three on-budget quotes (no cost or obligation). You choose the best fit, and "
"they get the job done."
msgstr ""
"我们会向您发送三个预算内的报价（无任何费用或责任负担）。 您选择最合适的设计师，由他们为您完"
"成工作。"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:435
#: gd-system-plugin/class-gd-system-plugin-admin-page.php:568
msgid "Learn More"
msgstr "了解详情"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:465
msgid "Meet the plugins that meet our high standards."
msgstr "使用符合我们高标准的插件。"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:467
msgid ""
"We've partnered with the world’s top WordPress plugin authors to provide a list of plugins "
"that work well with GoDaddy WordPress hosting."
msgstr ""
"我们已与世界顶尖的 WordPress 插件作者合作，为您提供各种可与 GoDaddy WordPress 无缝兼容的插"
"件。"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:477
msgid "Plugins list"
msgstr "插件列表"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:485
msgid ""
"Whoops! There was a problem fetching the list of plugins, please try reloading this page."
msgstr "糟糕！ 获取插件列表时出错，请尝试重新加载该页面。"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:530
#, php-format
msgid "More information about %s"
msgstr "更多有关 %s 的信息"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:554
#, php-format
msgid "Install %s now"
msgstr "立即安装 %s"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:554
msgid "Install Now"
msgstr "立即安装"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:558
#, php-format
msgid "Update %s now"
msgstr "立即更新 %s"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:558
msgid "Update Now"
msgstr "立即更新"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:562
msgid "This plugin is already installed and is up to date"
msgstr "此插件已安装且为最新状态"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:562
msgid "Installed"
msgstr "已安装"

#: gd-system-plugin/class-gd-system-plugin-admin-page.php:582
#, php-format
msgid "By %s"
msgstr "依据 %s"

#: gd-system-plugin/class-gd-system-plugin-api.php:80
#, php-format
msgid "Unknown verb: %s. Try GET or POST"
msgstr "未知谓词： %s。 请尝试 GET 或 POST"

#: gd-system-plugin/class-gd-system-plugin-api.php:115
#, php-format
msgid "API returned bad status: %d: %s"
msgstr "API 返回错误状态： %d: %s"

#: gd-system-plugin/class-gd-system-plugin-blacklist.php:86
#: gd-system-plugin/class-gd-system-plugin-blacklist.php:101
msgid "Not available"
msgstr "不可用"

#: gd-system-plugin/class-gd-system-plugin-blacklist.php:87
#: gd-system-plugin/class-gd-system-plugin-blacklist.php:102
msgid ""
"This plugin is not allowed on our system due to performance, security, or compatibility "
"concerns. Please contact our support with any questions."
msgstr "由于性能、安全性或兼容性问题，我们的系统不允许使用此插件。 如有问题，请联系客户支持。"

#: gd-system-plugin/class-gd-system-plugin-blacklist.php:123
#, php-format
msgid "This plugin %s is not allowed on our system. It has been automatically deactivated."
msgstr "我们的系统不允许使用此插件 %s。 它已被自动停用。"

#: gd-system-plugin/class-gd-system-plugin-cache-purge.php:295
#: gd-system-plugin/class-gd-system-plugin-command-controller.php:66
msgid "Cache cleared"
msgstr "缓存已清除"

#: gd-system-plugin/class-gd-system-plugin-cname.php:137
#, php-format
msgid ""
"<strong>Note:</strong> You're using the temporary domain <strong>%s</strong>. <a href=\"%s"
"\" target=\"_blank\">Change domain</a>"
msgstr ""
"<strong>注意：</strong> 您使用的是临时域名<strong>%s</strong>。 <a href=\"%s\" target="
"\"_blank\">变更域名</a>"

#: gd-system-plugin/class-gd-system-plugin-command-controller.php:163
msgid "File editing enabled"
msgstr "文件编辑已启用"

#: gd-system-plugin/class-gd-system-plugin-dashboard-widgets.php:61
msgid "GoDaddy Garage"
msgstr "GoDaddy 资料库"

#: gd-system-plugin/class-gd-system-plugin-messages.php:90
msgid "Success"
msgstr "成功"

#: gd-system-plugin/class-gd-system-plugin-pointers.php:47
msgid "Good news!"
msgstr "好消息！"

#: gd-system-plugin/class-gd-system-plugin-pointers.php:48
msgid ""
"You can now access <strong>Flush Cache</strong> and other links directly from the admin bar "
"using your desktop or mobile device."
msgstr "您现在可以使用桌面或移动设备直接从管理栏访问<strong>刷新缓存</strong>以及其他链接。"

#: gd-system-plugin/class-gd-system-plugin-temp-domain.php:88
#, php-format
msgid "%s This is your staging site and it cannot be indexed by search engines."
msgstr "%s 这是您的暂存网站，不能被搜索引擎索引。"

#: gd-system-plugin/class-gd-system-plugin-temp-domain.php:89
#: gd-system-plugin/class-gd-system-plugin-temp-domain.php:96
msgid "Note:"
msgstr "请注意："

#: gd-system-plugin/class-gd-system-plugin-temp-domain.php:95
#, php-format
msgid "%s Your site is using a temporary domain that cannot be indexed by search engines."
msgstr "%s 您的网站使用的是临时域名，不能被搜索引擎索引。"
