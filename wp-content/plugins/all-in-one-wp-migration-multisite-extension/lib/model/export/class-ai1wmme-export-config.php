<?php
/**
 * Copyright (C) 2014-2016 ServMask Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ███████╗███████╗██████╗ ██╗   ██╗███╗   ███╗ █████╗ ███████╗██╗  ██╗
 * ██╔════╝██╔════╝██╔══██╗██║   ██║████╗ ████║██╔══██╗██╔════╝██║ ██╔╝
 * ███████╗█████╗  ██████╔╝██║   ██║██╔████╔██║███████║███████╗█████╔╝
 * ╚════██║██╔══╝  ██╔══██╗╚██╗ ██╔╝██║╚██╔╝██║██╔══██║╚════██║██╔═██╗
 * ███████║███████╗██║  ██║ ╚████╔╝ ██║ ╚═╝ ██║██║  ██║███████║██║  ██╗
 * ╚══════╝╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚═╝     ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝
 */

class Ai1wmme_Export_Config {

	public static function execute( $params ) {

		// Set progress
		Ai1wm_Status::info( __( 'Adding multisite configuration to archive...', AI1WMME_PLUGIN_NAME ) );

		// Set config
		$config = new Ai1wm_Config;

		// Set Network
		$config->Network = false;
		if ( isset( $params['options']['network'] ) ) {
			$config->Network = true;
		}

		// Set Sites
		if ($config->Network === true) {
			$config->Sites = ai1wmme_sites( $params );
		} else {
			$config->Sites = ai1wmme_include_sites( $params );
		}

		// Save multisite.json file
		$handle = fopen( ai1wm_multisite_path( $params ), 'w' );
		fwrite( $handle, json_encode( $config ) );
		fclose( $handle );

		// Add multisite.json file
		$archive = new Ai1wm_Compressor( ai1wm_archive_path( $params ) );
		$archive->add_file( ai1wm_multisite_path( $params ), AI1WM_MULTISITE_NAME );
		$archive->close();

		// Set progress
		Ai1wm_Status::info( __( 'Done adding multisite configuration to archive.', AI1WMME_PLUGIN_NAME ) );

		return $params;
	}
}
