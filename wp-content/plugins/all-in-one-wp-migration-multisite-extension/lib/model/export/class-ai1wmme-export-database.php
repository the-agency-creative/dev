<?php
/**
 * Copyright (C) 2014-2016 ServMask Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ███████╗███████╗██████╗ ██╗   ██╗███╗   ███╗ █████╗ ███████╗██╗  ██╗
 * ██╔════╝██╔════╝██╔══██╗██║   ██║████╗ ████║██╔══██╗██╔════╝██║ ██╔╝
 * ███████╗█████╗  ██████╔╝██║   ██║██╔████╔██║███████║███████╗█████╔╝
 * ╚════██║██╔══╝  ██╔══██╗╚██╗ ██╔╝██║╚██╔╝██║██╔══██║╚════██║██╔═██╗
 * ███████║███████╗██║  ██║ ╚████╔╝ ██║ ╚═╝ ██║██║  ██║███████║██║  ██╗
 * ╚══════╝╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚═╝     ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝
 */

class Ai1wmme_Export_Database {

	public static function execute( $params ) {
		global $wpdb;

		// Set exclude database
		if ( isset( $params['options']['no-database'] ) ) {
			return $params;
		}

		// Set progress
		Ai1wm_Status::info( __( 'Exporting multisite database...', AI1WMME_PLUGIN_NAME ) );

		$old_table_values = array();
		$new_table_values = array();

		// Find and replace
		if ( isset( $params['options']['replace'] ) && ( $replace = $params['options']['replace'] ) ) {
			for ( $i = 0; $i < count( $replace['old-value'] ); $i++ ) {
				if ( ! empty( $replace['old-value'][$i] ) && ! empty( $replace['new-value'][$i] ) ) {
					$old_table_values[] = $replace['old-value'][$i];
					$new_table_values[] = $replace['new-value'][$i];
				}
			}
		}

		// Get database client
		if ( empty( $wpdb->use_mysqli ) ) {
			$client = new Ai1wm_Database_Mysql( $wpdb );
		} else {
			$client = new Ai1wm_Database_Mysqli( $wpdb );
		}

		$include_table_prefixes = array();

		// Include table prefixes
		if ( ai1wm_table_prefix() ) {
			$include_table_prefixes[] = ai1wm_table_prefix();
		} else {
			foreach ( $client->get_tables() as $table_name ) {
				$include_table_prefixes[] = $table_name;
			}
		}

		// Set include table prefixes
		$client->set_include_table_prefixes( $include_table_prefixes );

		// Set find and replace values
		$client->set_old_replace_values( $old_table_values )
			   ->set_new_replace_values( $new_table_values );

		// Replace table prefix on columns
		$client->set_table_prefix_columns( ai1wm_table_prefix() . 'options', array( 'option_name' ) )
			   ->set_table_prefix_columns( ai1wm_table_prefix() . 'usermeta', array( 'meta_key' ) );

		// Network
		if ( isset( $params['options']['network'] ) ) {

			// Spam comments
			if ( isset( $params['options']['no-spam-comments'] ) ) {
				$client->set_table_query_clauses( ai1wm_table_prefix() . 'comments', " WHERE comment_approved != 'spam' " );
				$client->set_table_query_clauses( ai1wm_table_prefix() . 'commentmeta', sprintf(
					" WHERE comment_id IN ( SELECT comment_ID FROM `%s` WHERE comment_approved != 'spam' ) ",
					ai1wm_table_prefix() . 'comments'
				) );
			}

			// Post revisions
			if ( isset( $params['options']['no-revisions'] ) ) {
				$client->set_table_query_clauses( ai1wm_table_prefix() . 'posts', " WHERE post_type != 'revision' " );
			}

			$old_table_prefixes = array();
			$new_table_prefixes = array();

			// Set table prefixes
			if ( ai1wm_table_prefix() ) {
				$old_table_prefixes[] = ai1wm_table_prefix();
				$new_table_prefixes[] = ai1wm_servmask_prefix();
			} else {
				foreach ( $client->get_tables() as $table_name ) {
					$old_table_prefixes[] = $table_name;
					$new_table_prefixes[] = ai1wm_servmask_prefix() . $table_name;
				}
			}

			// Set database options
			$client->set_old_table_prefixes( $old_table_prefixes )
				   ->set_new_table_prefixes( $new_table_prefixes );

		} else {

			// Set spam comments, post revisions and find and replace
			foreach ( ai1wmme_include_sites( $params ) as $site ) {

				// Spam comments
				if ( isset( $params['options']['no-spam-comments'] ) ) {
					$client->set_table_query_clauses( ai1wm_table_prefix( $site->BlogID ) . 'comments', " WHERE comment_approved != 'spam' " );
					$client->set_table_query_clauses( ai1wm_table_prefix( $site->BlogID ) . 'commentmeta', sprintf(
						" WHERE comment_id IN ( SELECT comment_ID FROM `%s` WHERE comment_approved != 'spam' ) ",
						ai1wm_table_prefix( $site->BlogID ) . 'comments'
					) );
				}

				// Post revisions
				if ( isset( $params['options']['no-revisions'] ) ) {
					$client->set_table_query_clauses( ai1wm_table_prefix( $site->BlogID ) . 'posts', " WHERE post_type != 'revision' " );
				}

				// Replace table prefix on columns
				$client->set_table_prefix_columns( ai1wm_table_prefix( $site->BlogID ) . 'options', array( 'option_name' ) )
					   ->set_table_prefix_columns( ai1wm_table_prefix( $site->BlogID ) . 'usermeta', array( 'meta_key' ) );
			}

			$usermeta = array();

			// Set user meta
			foreach ( ai1wmme_exclude_sites( $params ) as $site ) {
				if ( ai1wm_main_site( $site->BlogID ) === false ) {
					$usermeta[] = sprintf( " meta_key NOT LIKE '%s%%' ", ai1wm_table_prefix( $site->BlogID ) );
				}
			}

			// Exclude user meta
			if ( $usermeta ) {
				$client->set_table_query_clauses( ai1wm_table_prefix() . 'usermeta', sprintf( " WHERE %s ", implode( " AND ", $usermeta ) ) );
			}

			$old_table_prefixes = array();
			$new_table_prefixes = array();

			// Set table prefixes
			foreach ( ai1wmme_include_sites( $params ) as $site ) {
				if ( ai1wm_main_site( $site->BlogID ) === false ) {
					$old_table_prefixes[] = ai1wm_table_prefix( $site->BlogID );
					$new_table_prefixes[] = ai1wm_servmask_prefix( $site->BlogID );
				}
			}

			$mainsite = false;

			// Set main site
			foreach ( ai1wmme_include_sites( $params ) as $site ) {
				if ( ai1wm_main_site( $site->BlogID ) ) {
					$mainsite = true;
					break;
				}
			}

			// Set table prefixes
			if ( $mainsite ) {
				foreach ( array( 'users', 'usermeta' ) as $table_name ) {
					$old_table_prefixes[] = ai1wm_table_prefix() . $table_name;
					$new_table_prefixes[] = ai1wm_servmask_prefix( 'mainsite' ) . $table_name;
				}

				// Set table prefixes if empty
				if ( ai1wm_table_prefix() ) {
					$old_table_prefixes[] = ai1wm_table_prefix();
					$new_table_prefixes[] = ai1wm_servmask_prefix( 'basesite' );
				} else {
					foreach ( $client->get_tables() as $table_name ) {
						$old_table_prefixes[] = $table_name;
						$new_table_prefixes[] = ai1wm_servmask_prefix( 'basesite' ) . $table_name;
					}
				}
			} else {
				// Set table prefixes if empty
				if ( ai1wm_table_prefix() ) {
					$old_table_prefixes[] = ai1wm_table_prefix();
					$new_table_prefixes[] = ai1wm_servmask_prefix( 'mainsite' );
				} else {
					foreach ( $client->get_tables() as $table_name ) {
						$old_table_prefixes[] = $table_name;
						$new_table_prefixes[] = ai1wm_servmask_prefix( 'mainsite' ) . $table_name;
					}
				}
			}

			$include_table_prefixes = array();
			$exclude_table_prefixes = array();

			// Set exclude or include table prefixes
			if ( $mainsite ) {
				foreach ( ai1wmme_exclude_sites( $params ) as $site ) {
					$exclude_table_prefixes[] = ai1wm_table_prefix( $site->BlogID );
				}
			} else {
				foreach ( ai1wmme_include_sites( $params ) as $site ) {
					$include_table_prefixes[] = ai1wm_table_prefix( $site->BlogID );
				}

				// Only WP global tables
				foreach ( array( 'users', 'usermeta' ) as $table_name ) {
					$include_table_prefixes[] = ai1wm_table_prefix() . $table_name;
				}
			}

			// Set database options
			$client->set_old_table_prefixes( $old_table_prefixes )
				   ->set_new_table_prefixes( $new_table_prefixes )
				   ->set_include_table_prefixes( $include_table_prefixes )
				   ->set_exclude_table_prefixes( $exclude_table_prefixes );

		}

		// Export database
		$client->export( ai1wm_database_path( $params ) );

		// Get archive file
		$archive = new Ai1wm_Compressor( ai1wm_archive_path( $params ) );

		// Add database to archive
		$archive->add_file( ai1wm_database_path( $params ), AI1WM_DATABASE_NAME );
		$archive->close();

		// Set progress
		Ai1wm_Status::info( __( 'Done exporting multisite database.', AI1WMME_PLUGIN_NAME ) );

		return $params;
	}
}
