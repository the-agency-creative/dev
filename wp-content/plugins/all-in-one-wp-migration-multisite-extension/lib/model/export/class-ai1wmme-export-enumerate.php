<?php
/**
 * Copyright (C) 2014-2016 ServMask Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ███████╗███████╗██████╗ ██╗   ██╗███╗   ███╗ █████╗ ███████╗██╗  ██╗
 * ██╔════╝██╔════╝██╔══██╗██║   ██║████╗ ████║██╔══██╗██╔════╝██║ ██╔╝
 * ███████╗█████╗  ██████╔╝██║   ██║██╔████╔██║███████║███████╗█████╔╝
 * ╚════██║██╔══╝  ██╔══██╗╚██╗ ██╔╝██║╚██╔╝██║██╔══██║╚════██║██╔═██╗
 * ███████║███████╗██║  ██║ ╚████╔╝ ██║ ╚═╝ ██║██║  ██║███████║██║  ██╗
 * ╚══════╝╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚═╝     ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝
 */

class Ai1wmme_Export_Enumerate {

	public static function execute( $params ) {

		// Set progress
		Ai1wm_Status::info( __( 'Retrieving a list of all WordPress files...', AI1WMME_PLUGIN_NAME ) );

		// Total files
		$total = 0;

		// Create map file
		$filemap = fopen( ai1wm_filemap_path( $params ) , 'a+' );

		// Set include filters
		$include_filters = array();

		// Set exclude filters
		$exclude_filters = ai1wm_content_filters();

		// Exclude themes
		if ( isset( $params['options']['no-themes'] ) ) {
			$exclude_filters[] = 'themes';
		}

		// Exclude plugins
		if ( isset( $params['options']['no-plugins'] ) ) {
			$exclude_filters = array_merge( $exclude_filters, array( 'plugins', 'mu-plugins' ) );
		} else {
			$exclude_filters = array_merge( $exclude_filters, ai1wm_plugin_filters() );
		}

		// Exclude media
		if ( isset( $params['options']['no-media'] ) ) {
			$exclude_filters[] = 'uploads';
		} else if ( empty( $params['options']['network'] ) ) {
			$mainsite = false;

			// Set main site
			foreach ( ai1wmme_include_sites( $params ) as $site ) {
				if ( ai1wm_main_site( $site->BlogID ) ) {
					$mainsite = true;
					break;
				}
			}

			// Set exclude sites
			if ( $mainsite ) {
				foreach ( ai1wmme_exclude_sites( $params ) as $site ) {
					if ( ai1wm_main_site( $site->BlogID ) === false ) {
						$exclude_filters[] = ai1wm_sites_path( $site->BlogID );
					}
				}
			} else {
				// Set include sites
				foreach ( ai1wmme_include_sites( $params ) as $site ) {
					$include_filters[] = ai1wm_sites_path( $site->BlogID );
				}

				// Set exclude uploads
				$exclude_filters[] = 'uploads';
			}
		}

		// Iterate over sites directory
		foreach ( $include_filters as $path ) {
			try {
				$iterator = new RecursiveIteratorIterator(
					new Ai1wm_Recursive_Directory_Iterator( WP_CONTENT_DIR . DIRECTORY_SEPARATOR . $path ),
					RecursiveIteratorIterator::SELF_FIRST
				);

				// Write path line
				foreach ( $iterator as $item ) {
					if ( $item->isFile() ) {
						if ( fwrite( $filemap, $path . DIRECTORY_SEPARATOR . $iterator->getSubPathName() . PHP_EOL ) ) {
							$total++;
						}
					}
				}
			} catch ( Exception $e ) {
				// Skip bad file permissions
			}
		}

		try {
			// Iterate over content directory
			$iterator = new RecursiveIteratorIterator(
				new Ai1wm_Recursive_Exclude_Filter(
					new Ai1wm_Recursive_Directory_Iterator(
						WP_CONTENT_DIR
					),
					$exclude_filters
				),
				RecursiveIteratorIterator::SELF_FIRST
			);

			// Write path line
			foreach ( $iterator as $item ) {
				if ( $item->isFile() ) {
					if ( fwrite( $filemap, $iterator->getSubPathName() . PHP_EOL ) ) {
						$total++;
					}
				}
			}
		} catch ( Exception $e ) {
			// Skip bad file permissions
		}

		// Close handle
		fclose( $filemap );

		// Set total files
		$params['total'] = $total;

		// Set progress
		Ai1wm_Status::info( __( 'Done retrieving a list of all WordPress files.', AI1WMME_PLUGIN_NAME ) );

		return $params;
	}
}
