<?php
/**
 * Copyright (C) 2014-2016 ServMask Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ███████╗███████╗██████╗ ██╗   ██╗███╗   ███╗ █████╗ ███████╗██╗  ██╗
 * ██╔════╝██╔════╝██╔══██╗██║   ██║████╗ ████║██╔══██╗██╔════╝██║ ██╔╝
 * ███████╗█████╗  ██████╔╝██║   ██║██╔████╔██║███████║███████╗█████╔╝
 * ╚════██║██╔══╝  ██╔══██╗╚██╗ ██╔╝██║╚██╔╝██║██╔══██║╚════██║██╔═██╗
 * ███████║███████╗██║  ██║ ╚████╔╝ ██║ ╚═╝ ██║██║  ██║███████║██║  ██╗
 * ╚══════╝╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚═╝     ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝
 */

class Ai1wmme_Import_Blogs {

	public static function execute( $params ) {

		// Read package.json file
		$handle = fopen( ai1wm_package_path( $params ), 'r' );
		if ( $handle === false ) {
			throw new Ai1wm_Import_Exception( 'Unable to read package.json file' );
		}

		// Set progress
		Ai1wm_Status::info( __( 'Preparing blogs...', AI1WMME_PLUGIN_NAME ) );

		// Parse package.json file
		$config = fread( $handle, filesize( ai1wm_package_path( $params ) ) );
		$config = json_decode( $config );

		// Close handle
		fclose( $handle );

		$blogs = array();

		// Single site
		if ( ! is_file( ai1wm_multisite_path( $params ) ) ) {

			// Get domain
			$old_domain = parse_url( $config->SiteURL, PHP_URL_HOST );
			$new_domain = parse_url( site_url(), PHP_URL_HOST );

			// Get sub domain
			if ( ( $position = strpos( $old_domain, '.' ) ) ) {
				$sub_domain = substr( $old_domain, 0, $position );
			} else {
				$sub_domain = $old_domain;
			}

			// Get new path
			$new_path = trailingslashit( parse_url( site_url(), PHP_URL_PATH ) );

			// Set domain or path
			if ( is_subdomain_install() ) {
				$new_domain = sprintf( '%s.%s', $sub_domain, $new_domain );
			} else {
				if ( ( $new_path = trim( $new_path, '/' ) ) ) {
					$new_path = sprintf( '/%s/%s/', $new_path, $sub_domain );
				} else {
					$new_path = sprintf( '/%s/', $sub_domain );
				}
			}

			// Create empty blog
			if ( domain_exists( $new_domain, $new_path ) ) {
				if ( ( $blog_id = get_blog_id_from_url( $new_domain, $new_path ) ) ) {
					$blogs[] = array(
						'Old' => array(
							'Id'      => 0,
							'SiteURL' => $config->SiteURL,
							'HomeURL' => $config->HomeURL,
						),
						'New' => array(
							'Id'      => (int) $blog_id,
							'SiteURL' => get_site_url( $blog_id ),
							'HomeURL' => get_home_url( $blog_id ),
						),
					);
				}
			} else {
				if ( ( $blog_id = ai1wmme_create_blog( $new_domain, $new_path ) ) ) {
					$blogs[] = array(
						'Old' => array(
							'Id'      => 0,
							'SiteURL' => $config->SiteURL,
							'HomeURL' => $config->HomeURL,
						),
						'New' => array(
							'Id'      => (int) $blog_id,
							'SiteURL' => get_site_url( $blog_id ),
							'HomeURL' => get_home_url( $blog_id ),
						),
					);
				}
			}

		} else {

			// Read multisite.json file
			$handle = fopen( ai1wm_multisite_path( $params ), 'r' );
			if ( $handle === false ) {
				throw new Ai1wm_Import_Exception( 'Unable to read multisite.json file' );
			}

			// Parse multisite.json file
			$multisite = fread( $handle, filesize( ai1wm_multisite_path( $params ) ) );
			$multisite = json_decode( $multisite );

			// Close handle
			fclose( $handle );

			// Create blogs
			if ( empty( $multisite->Network ) ) {
				if ( isset( $multisite->Sites ) && ( $sites = $multisite->Sites ) ) {

					// Get or create blogs
					foreach ( $sites as $site ) {

						// Get domain
						$old_domain = parse_url( $config->SiteURL, PHP_URL_HOST );
						$new_domain = parse_url( site_url(), PHP_URL_HOST );

						// Get sub domain
						if ( ( $position = strpos( $site->Domain, '.' ) ) ) {
							$sub_domain = substr( $site->Domain, 0, $position );
						} else {
							$sub_domain = $site->Domain;
						}

						// Get new path
						$old_path = trailingslashit( parse_url( $config->SiteURL, PHP_URL_PATH ) );
						$new_path = trailingslashit( parse_url( site_url(), PHP_URL_PATH ) );

						// Get sub path
						if ( strpos( $site->Path, $old_path ) === 0 ) {
							$sub_path = substr_replace( $site->Path, '', 0, strlen( $old_path ) );
						} else {
							$sub_path = $site->Path;
						}

						// Set domain or path
						if ( is_subdomain_install() ) {
							if ( ( $sub_path = trim( $sub_path, '/' ) ) ) {
								$new_domain = sprintf( '%s.%s', $sub_path, $new_domain );
							} else {
								$new_domain = str_replace( $old_domain, $new_domain, $site->Domain );
							}
						} else {
							if ( ( $sub_path = trim( $sub_path, '/' ) ) ) {
								if ( ( $new_path = trim( $new_path, '/' ) ) ) {
									$new_path = sprintf( '/%s/%s/', $new_path, $sub_path );
								} else {
									$new_path = sprintf( '/%s/', $sub_path );
								}
							} else {
								if ( ( $new_path = trim( $new_path, '/' ) ) ) {
									$new_path = sprintf( '/%s/%s/', $new_path, $sub_domain );
								} else {
									$new_path = sprintf( '/%s/', $sub_domain );
								}
							}
						}

						// Create empty blog
						if ( domain_exists( $new_domain, $new_path ) ) {
							if ( ( $blog_id = get_blog_id_from_url( $new_domain, $new_path ) ) ) {
								$blogs[] = array(
									'Old' => array(
										'Id'      => (int) $site->BlogID,
										'SiteURL' => $site->SiteURL,
										'HomeURL' => $site->HomeURL,
									),
									'New' => array(
										'Id'      => (int) $blog_id,
										'SiteURL' => get_site_url( $blog_id ),
										'HomeURL' => get_home_url( $blog_id ),
									),
								);
							}
						} else {
							if ( ( $blog_id = create_empty_blog( $new_domain, $new_path, null ) ) ) {
								$blogs[] = array(
									'Old' => array(
										'Id'      => (int) $site->BlogID,
										'SiteURL' => $site->SiteURL,
										'HomeURL' => $site->HomeURL,
									),
									'New' => array(
										'Id'      => (int) $blog_id,
										'SiteURL' => get_site_url( $blog_id ),
										'HomeURL' => get_home_url( $blog_id ),
									),
								);
							}
						}
					}
				}
			}
		}

		// Save blogs.json file
		$handle = fopen( ai1wm_blogs_path( $params ), 'w' );
		fwrite( $handle, json_encode( $blogs ) );
		fclose( $handle );

		// Set progress
		Ai1wm_Status::info( __( 'Done preparing blogs...', AI1WMME_PLUGIN_NAME ) );

		return $params;
	}
}
