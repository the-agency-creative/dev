<div class="ai1wm-field-set">
	<ul class="ai1wmme-sites-list">
		<li>
			<input type="checkbox" name="options[network]" value="1" id="ai1wmme-network" class="ai1wmme-network" checked="checked" />
			<label for="ai1wmme-network"><?php _e( 'Network (all sites)', AI1WMME_PLUGIN_NAME ); ?></label>
		</li>

		<?php foreach ( ai1wmme_sites() as $site ): ?>
			<li>
				<input type="checkbox" name="options[sites][]" value="<?php echo $site->BlogID; ?>" id="ai1wmme-site-<?php echo $site->BlogID; ?>" class="ai1wmme-sites" />
				<label for="ai1wmme-site-<?php echo $site->BlogID; ?>">
					<?php echo $site->Domain . $site->Path; ?>
				</label>
			</li>
		<?php endforeach; ?>
	</ul>
</div>
