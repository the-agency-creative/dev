			</section>
			<footer>
	        	<div class="logo">
	        		<a href="http://theagencyre.com/"><img src="<?php bloginfo('template_directory'); ?>/images/agency-footer.png" /></a>
	        	</div>
	        	<div class="credits">
	        		<div class="no-break">Serving the Luxury Real Estate Market Worldwide <span class="hide">|</span></div> <div class="break"><a href="http://theagencyre.com/luxury-real-estate/">View more properties &rarr;</a></div>
	        	</div>
	        </footer>
        <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'XXXXXXXXXX', 'theagencyre.com');
		  ga('send', 'pageview');

		</script>
  		<?php wp_footer(); ?>
    </body>
</html>