<?php
/*
Template Name: Gallery
*/
?><?php get_header(); ?>

<!-- Carousel ================================================== -->

    <div id="pp-carousel" class="carousel-fade carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
	        <?php
	  				$fullattachments = get_field('pp_home_gallery');

		  			foreach ( $fullattachments as $fullattachment ) {
			  			$listingimgsrc = $fullattachment['sizes']['home-gallery'];
						$listingimgalt = $fullattachment['alt'];
			?>
			<div class="item" style="background-image:url('<?php echo $listingimgsrc; ?>');"></div>
			<?php
					}
	  		?>
        </div>
        <a class="left carousel-control" href="#pp-carousel" role="button" data-slide="prev">
	        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	        <span class="sr-only">Previous</span>
	    </a>
	    <a class="right carousel-control" href="#pp-carousel" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		</a>
    </div><!-- /.carousel -->

<?php get_footer(); ?>
