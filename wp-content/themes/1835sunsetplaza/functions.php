<?php

/* register menus */

require_once('wp_bootstrap_navwalker.php');

register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'PP' ),
) );

/* thumbs for the theme */

if ( function_exists( 'add_theme_support' ) ) {
	// Setup thumbnail support
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'home-gallery', 1920, 1080, true );
}

function custom_colors() {
   echo '<style type="text/css">
           .options-saved{background: none repeat scroll 0 0 green;color: #FFFFFF;margin: 20px 0;padding: 1px 0 1px 5px;width: 250px;}
           .custom-title{float:left;}
           #custom-submit{background: none repeat scroll 0 0 red;border: 0 none;color: #FFFFFF;padding: 5px;}
         </style>';
}

add_action('admin_head', 'custom_colors');

// login screen email

function password_protected_request() {
   	echo '<div style="margin:20px auto 0 auto; text-align: center;"></div>';
}
add_action('password_protected_after_login_form', 'password_protected_request');

//login css

function passwd_protect_css() { ?>
    <link rel="stylesheet" id="custom_wp_admin_css"  href="<?php echo get_bloginfo( 'stylesheet_directory' ) . '/css/login.css'; ?>" type="text/css" media="all" />
<?php }
add_action( 'login_enqueue_scripts', 'passwd_protect_css' );

/* register some jquery stuff */

function light_the_fires() {
	wp_deregister_script('jquery');
	wp_enqueue_script( 'jquery', '/wp-includes/js/jquery/jquery.js' , array(), null, true );
	wp_enqueue_script('bootstrap-js', 'http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js', false, null, true);
}
add_action( 'wp_enqueue_scripts', 'light_the_fires' );

/* and then tell them what to do */

function add_footer_scripts() { ?>

	<script type="text/javascript">

		jQuery(document).ready(function($) {

			$("body").fadeIn("fast");
			$(".loader").fadeOut("fast");


			 // add active class to carousel pagination

			$(".carousel-indicators li:first").addClass("active");
			$(".carousel-inner .item:first").addClass("active");

		});

		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
		  var msViewportStyle = document.createElement("style")
		  msViewportStyle.appendChild(
			document.createTextNode(
			  "@-ms-viewport{width:auto!important}"
			)
		  )
		  document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
		}

	</script>


<?php }

//add to wp_footer
add_action( 'wp_footer', 'add_footer_scripts', 100 );

//add the page slug to body_class()
function add_slug_body_class( $classes ) {
	global $post;
		if ( isset( $post ) ) {
			$classes[] = $post->post_type . '-' . $post->post_name;
		}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );