<?php get_header(); ?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div id="wrapper" class="container">
				<div class="row">
					<div class="col-md-12">
						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		<?php endif; ?>
<?php get_footer(); ?>