<?php

/* register menus */

add_action( 'init', 'register_my_menu' );
function register_my_menu() {
	register_nav_menu( 'primary-menu', __( 'Primary Menu' ) );
}

/* thumbs for the theme */

if ( function_exists( 'add_theme_support' ) ) {
	// Setup thumbnail support
	add_theme_support( 'post-thumbnails' );
}

if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'gallery_2', 455, 455, true );
}

function custom_colors() {
   echo '<style type="text/css">
           .options-saved{background: none repeat scroll 0 0 green;color: #FFFFFF;margin: 20px 0;padding: 1px 0 1px 5px;width: 250px;}
           .custom-title{float:left;}
           #custom-submit{background: none repeat scroll 0 0 red;border: 0 none;color: #FFFFFF;padding: 5px;}
         </style>';
}

add_action('admin_head', 'custom_colors');

// let's add a meta box to pages for the bg images

add_action( 'add_meta_boxes', 'cd_meta_box_add' );
function cd_meta_box_add()
{
	add_meta_box( 'bg-image-metabox', 'Page Background Image', 'page_bg_image', 'page', 'normal', 'high' );
}

function page_bg_image( $post )
{
$values = get_post_custom( $post->ID );
$text = isset( $values['page_bg_image_text'] ) ? esc_attr( $values['page_bg_image_text'][0] ) : '';
	?>
<p>To set a background image for the page, click "Set Featured Image" (over there ----->) and then choose or upload an image and click the "Set Featured Image" buttom in the bottom right of the pop-up screen.</p>
	<?php
}

// custom theme options

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' ); 

function theme_options_init(){
 register_setting( 'private_options', 'private_theme_options');
} 

function theme_options_add_page() {
 add_theme_page( __( 'Theme Options', 'privatetheme' ), __( 'Theme Options', 'privatetheme' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}

function theme_options_do_page() { global $select_options; if ( ! isset( $_REQUEST['settings-updated'] ) ) $_REQUEST['settings-updated'] = false;

?>

<div>
<?php screen_icon(); echo "<h2 class='custom-title'>". __( 'Private Theme Options', 'privatetheme' ) . "</h2>"; ?>
<div class="clear"></div>
<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
<div class="options-saved">
<p><strong><?php _e( 'Options saved', 'privatetheme' ); ?></strong></p>
</div>
<?php endif; ?> 
<form method="post" action="options.php">
<?php settings_fields( 'private_options' ); ?>
<?php $options = get_option( 'private_theme_options' ); ?>
<p>To assign a slide image, click the "Select Image" button, find the image, copy the URL and then paste it in the corresponding field.</p>
<table>
<tr valign="middle">
<td>
<input id="upload_image_button" type="button" value="Select Image" />
</td>
</tr>
<tr>
</tr>
<tr valign="middle">
<th scope="row"><?php _e( 'Slide #1 Image URL', 'privatetheme' ); ?></th>
<td>
<input id="private_theme_options[slide1]" type="text" name="private_theme_options[slide1]" value="<?php esc_attr_e( $options['slide1'] ); ?>" style="width:600px;" />
</td>
</tr>
<tr valign="middle">
<th scope="row"><?php _e( 'Slide #2 Image URL', 'privatetheme' ); ?></th>
<td>
<input id="private_theme_options[slide2]" type="text" name="private_theme_options[slide2]" value="<?php esc_attr_e( $options['slide2'] ); ?>" style="width:600px;" />
</td>
</tr>
<tr valign="middle">
<th scope="row"><?php _e( 'Slide #3 Image URL', 'privatetheme' ); ?></th>
<td>
<input id="private_theme_options[slide3]" type="text" name="private_theme_options[slide3]" value="<?php esc_attr_e( $options['slide3'] ); ?>" style="width:600px;" />
</td>
</tr>
<tr valign="middle">
<th scope="row"><?php _e( 'Slide #4 Image URL', 'privatetheme' ); ?></th>
<td>
<input id="private_theme_options[slide4]" type="text" name="private_theme_options[slide4]" value="<?php esc_attr_e( $options['slide4'] ); ?>" style="width:600px;" />
</td>
</tr>
<tr valign="middle">
<th scope="row"><?php _e( 'Slide #5 Image URL', 'privatetheme' ); ?></th>
<td>
<input id="private_theme_options[slide5]" type="text" name="private_theme_options[slide5]" value="<?php esc_attr_e( $options['slide5'] ); ?>" style="width:600px;" />
</td>
</tr>
<tr valign="middle">
<th scope="row"><?php _e( 'Slide #6 Image URL', 'privatetheme' ); ?></th>
<td>
<input id="private_theme_options[slide6]" type="text" name="private_theme_options[slide6]" value="<?php esc_attr_e( $options['slide6'] ); ?>" style="width:600px;" />
</td>
</tr>
</table>
<p>
<input type="submit" id="custom-submit" value="<?php _e( 'Save Options', 'privatetheme' ); ?>" />
</p>
</form>
</div>
<?php }

function private_scripts() {
wp_enqueue_script('media-upload');
wp_enqueue_script('thickbox');
wp_register_script('private-upload', WP_CONTENT_URL.'/themes/privateproperties/js/private.js', array('jquery','media-upload','thickbox'));
wp_enqueue_script('private-upload');
}

function private_styles() {
wp_enqueue_style('thickbox');
}

if (isset($_GET['page']) && $_GET['page'] == 'theme_options') {
add_action('admin_print_scripts', 'private_scripts');
add_action('admin_print_styles', 'private_styles');
}

function column_one_half( $atts, $content = null ) {
   return '<div class="one_half">' . do_shortcode($content) . '</div>';
}

add_shortcode('one_half', 'column_one_half');

function column_one_half_last( $atts, $content = null ) {
   return '<div class="one_half last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

add_shortcode('one_half_last', 'column_one_half_last');

function content_formatter($content) {
	$new_content = '';

	/* Matches the contents and the open and closing tags */
	$pattern_full = '{(\[raw\].*?\[/raw\])}is';

	/* Matches just the contents */
	$pattern_contents = '{\[raw\](.*?)\[/raw\]}is';

	/* Divide content into pieces */
	$pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);

	/* Loop over pieces */
	foreach ($pieces as $piece) {
		/* Look for presence of the shortcode */
		if (preg_match($pattern_contents, $piece, $matches)) {

			/* Append to content (no formatting) */
			$new_content .= $matches[1];
		} else {

			/* Format and append to content */
			$new_content .= wptexturize(wpautop($piece));
		}
	}

	return $new_content;
}

// Remove the 2 main auto-formatters
remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

// Before displaying for viewing, apply this function
add_filter('the_content', 'content_formatter', 99);
add_filter('widget_text', 'content_formatter', 99);

//Long posts should require a higher limit, see http://core.trac.wordpress.org/ticket/8553
@ini_set('pcre.backtrack_limit', 500000);



// Checkbox for Page Content
add_action("admin_init", "checkbox_init");

function checkbox_init(){
  add_meta_box("checkbox", "Hide Page Content", "checkbox", "page", "normal", "high");
}

function checkbox(){
  global $post;
  $custom = get_post_custom($post->ID);
  $field_id = $custom["field_id"][0];
 ?>

  <label>Check to hide page content</label>
  <?php $field_id_value = get_post_meta($post->ID, 'field_id', true);
  if($field_id_value == "yes") $field_id_checked = 'checked="checked"'; ?>
    <input type="checkbox" name="field_id" value="yes" <?php echo $field_id_checked; ?> />
  <?php

}

// Save Meta Details
add_action('save_post', 'save_details');

function save_details(){
  global $post;

if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post->ID;
}

  update_post_meta($post->ID, "field_id", $_POST["field_id"]);
}

// login screen email

function password_protected_request() {
   	echo '<div style="margin:20px auto 0 auto; text-align: center;">To request a password, please <a style="color: #EF4035;" href="http://pages.theagencyre.com/Owlwood.html">contact us</a>.</div>';
}
add_action('password_protected_after_login_form', 'password_protected_request');

//login css

function passwd_protect_css() { ?>
    <link rel="stylesheet" id="custom_wp_admin_css"  href="<?php echo get_bloginfo( 'stylesheet_directory' ) . '/css/login.css'; ?>" type="text/css" media="all" />
<?php }
add_action( 'login_enqueue_scripts', 'passwd_protect_css' );

// remove comments from carousel

function filter_media_comment_status( $open, $post_id ) {
	$post = get_post( $post_id );
	if( $post->post_type == 'attachment' ) {
		return false;
	}
	return $open;
}
add_filter( 'comments_open', 'filter_media_comment_status', 10 , 2 );