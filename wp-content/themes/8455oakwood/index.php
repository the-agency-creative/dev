<?php get_header(); ?>
        <section id="main">
        </section>
        <?php $options = get_option( 'private_theme_options' ); ?>
        <script type="text/javascript">
			$.backstretch([
				"<?php echo $options['slide1'] ?>",
				"<?php echo $options['slide2'] ?>",
				"<?php echo $options['slide3'] ?>",
				"<?php echo $options['slide4'] ?>",
				"<?php echo $options['slide5'] ?>",
				"<?php echo $options['slide6'] ?>"
			], {duration: 3000, fade: 750});
        </script>
<?php get_footer(); ?>