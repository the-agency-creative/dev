// Your JavaScript code
function headernav(){
	var header = $('#menu-header');
	var headerchildren = header.children('li.menu-item-has-children');
	var bodyheight = $('.full-screen').height();

	$('header .hamburger').click(function(){
		$('header .hamburger').toggleClass('active');
		$('.full-screen').toggleClass('slideInDown animated top').css('top', -bodyheight - 120).toggle();
		$('#inquiry-module').removeClass('left');
		$('header .inquire-info button').removeClass('active');
	})


		header.css('height', bodyheight);
		headerchildren.css('height', bodyheight / 2);

	headerchildren.each(function(){
		$(this).children().wrapAll('<div class="vertical-align"><div class="text-inner"><div class="text"></div></div></div>');
	});
	$('<div class="credentials"><div class="copyright col-md-2"> &copy; 2016 <span class="ehl"></span> <span id="disclaimer">Disclaimer</span></div><div class="col-md-8 text-center">8500 West Sunset Boulevard, West Hollywood, California 90069</div><ul class="social"><li class="i"><a href="#"><i class="fa fa-instagram"></i></a></li><li class="f"><a href="#"><i class="fa fa-facebook"></i></a></li><li class="t"><a href="#"><i class="fa fa-twitter"></i></a></li></ul></div>').appendTo('.full-screen');

}
function inquiry(){
	$('header .inquire-info button').click(function(){
		$('header .hamburger').removeClass('active');
		$('.full-screen').hide();
		$(this).toggleClass('active');
		$('#inquiry-module').toggleClass('left slideInLeft animated');
		if($('#inquiry-module').hasClass('left')){
			$('#inquiry-module span#sales, #inquiry-module span#prospect').removeClass('active');
			$('#inquiry-module .part-1').show();
			$('#inquiry-module .part-2').hide();
		};
	});
	$('#inquiry-module span#prospect').click(function(){
		$('#inquiry-module .part-1').hide();
		$('#inquiry-module span#sales').removeClass('active');
		$(this).toggleClass('active');
		$('#inquiry-module .part-2').show();
		$('#inquiry-module .part-2 .future-resident').hide();
		$('#inquiry-module .part-2 .sales-agent').show();
	})
	$('#inquiry-module span#sales').click(function(){
		$('#inquiry-module .part-1').hide();
		$('#inquiry-module span#prospect').removeClass('active');
		$(this).addClass('active');
		$('#inquiry-module .part-2 .future-resident').show();
		$('#inquiry-module .part-2 .sales-agent').hide();
	})
}

var $ = jQuery.noConflict();
jQuery(document).ready(function($){
	headernav();
	inquiry();
});