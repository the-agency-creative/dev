<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Boilerstrap
 * @since Boilerstrap 1.0
 */
?>

  	<footer>
  			<div class="copyright col-md-3">
            &copy; <?php echo date('Y'); ?> | | <a href="#">Disclaimer</a>
        </div>
        <div class="col-md-7 text-center">8500 West Sunset Boulevard, West Hollywood, California 90069</div>
        <ul class="col-md-2 social">
          <li class="i"><a href="#"><i class="fa fa-instagram"></i></a></li>
          <li class="f"><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li class="t"><a href="#"><i class="fa fa-twitter"></i></a></li>
        </ul>
  	</footer>

  <?php wp_footer(); ?>

</body>
</html>