<?php


function enqueue_scripts() {
    wp_enqueue_style( 'stylesheet', get_stylesheet_uri() );
    wp_enqueue_style( 'plugins', get_template_directory_uri() . '/assets/css/plugins.css');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
    wp_enqueue_style( 'fonts', get_template_directory_uri() . '/assets/css/font-awesome.min.css');
    
    wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array(), '1.0.0', true );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.js', array( 'jquery' ), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );


function register_my_menus() {
  register_nav_menus(
    array(
      'primary' => __( 'Header Menu' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );


add_theme_support( 'post-thumbnails' );

add_image_size( 'small', 220, 180 ); 
add_image_size( 'medium', 220, 180 ); 
add_image_size( 'large', 220, 180 ); 
add_image_size( 'xlarge', 220, 180 ); 