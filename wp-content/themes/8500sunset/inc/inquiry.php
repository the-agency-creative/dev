<div id="inquiry-module">
	<div class="text-inner">
		<div class="text">
			<div class="part-1">
				<h1>Quick Question?</h1>
			</div>

				<aside>
					<span id="prospect">Future Resident</span>
					<span class="or">or</span>
					<span id="sales">Sales Agent</span>
				</aside>
			<div class="part-2" style="display:none;">
				<div class="future-resident">
					<form></form>
				</div>
				<div class="sales-agent">
					<form></form>
				</div>
			</div>

		</div>
	</div>
	<p class="text-center">8500 West Sunset Boulevard West Hollywood, California 90069</p>
</div>