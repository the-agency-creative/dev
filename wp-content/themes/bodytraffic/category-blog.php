<?php

get_header();

?>

	<div id="content" class="container">
      <div class="row header-row">
      	<div class="col-xs-12 col-sm-6 wow fadeInRight" data-wow-delay=".2s">
        	<img src="<?php bloginfo('template_directory'); ?>/images/girl-news1.jpg" width="100%" height="auto" alt=""/>
        </div>
        <div class="col-xs-12 col-sm-6 work-text wow fadeIn" data-wow-delay=".2s">
        	<div>
        		<h1><span>News + Updates<span></h1>
            </div>
            <p>Get all of the latest happenings at BODYTRAFFIC.</p>
        </div>
      </div>
	  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="row blog-item">
	    <div class="col-xs-12 col-sm-6 blog-image-wrap">
	        <?php the_post_thumbnail( 'repertory-image' ); ?>
        </div>
	  	<div class="col-xs-12 col-sm-6 blog-text-wrap">
        	<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        	<?php the_excerpt(); ?>
            <div class="text-right"><a href="<?php the_permalink(); ?>">Read More ></a></div>
        </div>
      </div>
      <?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>
	<div class="text-center navigation"><p><?php posts_nav_link(); ?></p></div>
  </div><!-- End container -->

<?php get_footer(); ?>
