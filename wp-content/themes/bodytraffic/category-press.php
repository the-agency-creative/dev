<?php

get_header();

?>

	<div id="content" class="container">
      <div class="row header-row">
      	<div class="col-xs-12 col-sm-6 wow fadeInRight" data-wow-delay=".2s">
        	<img src="<?php bloginfo('template_directory'); ?>/images/press-v3.jpg" width="100%" height="auto" alt=""/>
        </div>
        <div class="col-xs-12 col-sm-6 work-text wow fadeIn" data-wow-delay=".2s">
        	<div>
        		<h1><span>Press Quotes<span></h1>
            </div>
            <p>See what's being said about BODYTRAFFIC.</p>
        </div>
      </div>
	  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="row press-item">
	  	<div class="col-xs-12 col-sm-6">
        	<h2><a target="_blank" href="<?php the_field('bt_press_source_link'); ?>"><?php the_title(); ?></a></h2>
        	<?php the_content(); ?>
        </div>
        <div class="col-xs-12 col-sm-6 press-image-wrap">
	        <?php the_post_thumbnail( 'full', array('class' => "img-responsive center-block")); ?>
        </div>
      </div>
      <?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>
	<div class="navigation text-center"><p><?php posts_nav_link(); ?></p></div>
  </div><!-- End container -->

<?php get_footer(); ?>
