	<div id="Footer" class="container-fluid">
        <div class="container">
            <div class="col-sm-4">
	            <ul class="list-inline text-center">
		            <li><a href="http://twitter.com/bodytraffic"><i class="fa fa-twitter fa-15x"></i></a></li>
		            <li><a href="http://facebook.com/bodytraffic"><i class="fa fa-facebook fa-15x"></i></a></li>
		            <li><a href="http://instagram.com/bodytraffic"><i class="fa fa-instagram fa-15x"></i></a></li>
	        </div>

            <div class="col-sm-4 text-center">
                ©2015 BODYTRAFFIC All Rights Reserved
            </div>

            <div class="col-sm-4 text-center">
                <a href="/donate/">DONATE</a>
            </div>
        </div>
    </div><!-- End container fluid -->
    <?php wp_footer(); ?>
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-65831301-1', 'auto');
	  ga('send', 'pageview');
	</script>
</body>
</html>
