<?php

function company_posttype() {
    register_post_type( 'company',
        array(
            'labels' => array(
                'name' => __( 'Company' ),
                'singular_name' => __( 'Member' ),
                'add_new' => __( 'Add New Member' ),
                'add_new_item' => __( 'Add New Member' ),
                'edit_item' => __( 'Edit Member' ),
                'new_item' => __( 'Add New Member' ),
                'view_item' => __( 'View Member' ),
                'search_items' => __( 'Search Company' ),
                'not_found' => __( 'No members found' ),
                'not_found_in_trash' => __( 'No members found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'company', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'company_posttype' );

function repertory_posttype() {
    register_post_type( 'repertory',
        array(
            'labels' => array(
                'name' => __( 'Repertory' ),
                'singular_name' => __( 'Performance' ),
                'add_new' => __( 'Add New Performance' ),
                'add_new_item' => __( 'Add New Performance' ),
                'edit_item' => __( 'Edit Performance' ),
                'new_item' => __( 'Add New Performance' ),
                'view_item' => __( 'View Performance' ),
                'search_items' => __( 'Search Repertory' ),
                'not_found' => __( 'No repertory found' ),
                'not_found_in_trash' => __( 'No performances found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'repertory', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'repertory_posttype' );

function quotes_posttype() {
    register_post_type( 'quotes',
        array(
            'labels' => array(
                'name' => __( 'Quotes' ),
                'singular_name' => __( 'Quote' ),
                'add_new' => __( 'Add New Quote' ),
                'add_new_item' => __( 'Add New Quote' ),
                'edit_item' => __( 'Edit Quote' ),
                'new_item' => __( 'Add New Quote' ),
                'view_item' => __( 'View Quote' ),
                'search_items' => __( 'Search Quotes' ),
                'not_found' => __( 'No quotes found' ),
                'not_found_in_trash' => __( 'No quotes found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'quote', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'quotes_posttype' );

// register taxonomies

add_action( 'init', 'create_company_taxonomies', 0 );

function create_company_taxonomies() {
	$role_labels = array(
		'name'              => _x( 'Roles', 'taxonomy general name' ),
		'singular_name'     => _x( 'Role', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Roles' ),
		'all_items'         => __( 'All Roles' ),
		'edit_item'         => __( 'Edit Role' ),
		'update_item'       => __( 'Update Role' ),
		'add_new_item'      => __( 'Add New Role' ),
		'new_item_name'     => __( 'New Role Name' ),
		'menu_name'         => __( 'Roles' ),
	);

	$role_args = array(
		'hierarchical'      => true,
		'labels'            => $role_labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'company', 'hierarchical' => true ),
	);

	register_taxonomy( 'role', array( 'company', 'post' ), $role_args );
}


// register multiple options pages

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

if( function_exists('acf_add_options_sub_page') )
{
    acf_add_options_sub_page( 'Home Page - Boxes' );
    acf_add_options_sub_page( 'Home Page - Heroes' );
    acf_add_options_sub_page( 'Home Page - Tagline' );
    acf_add_options_sub_page( 'Footer' );
}

// theme image sizes

add_theme_support( 'post-thumbnails' );
add_image_size('page-header-image', 1900, 900, true);
add_image_size('repertory-image', 570, 261, true);
add_image_size('repertory-gallery-image', 750, 420, true);
add_image_size('company-image', 360, 360, true);
add_image_size('role-image', 1300, 800, true);
add_image_size('home-featured-image-1', 360, 665, true);
add_image_size('home-featured-image-4', 749, 275, true);
add_image_size('home-hero-image', 689, 857, true);

/* register some jquery stuff */

function light_the_fires() {
	wp_deregister_script('jquery');
	wp_enqueue_script( 'jquery', '/wp-includes/js/jquery/jquery.js' , array(), null, true );
	wp_enqueue_script('bootstrap-js', 'http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js', false, null, true);
	wp_enqueue_script('wow-js', get_stylesheet_directory_uri() . '/js/wow.min.js', array( 'jquery' ), '1.0.0', true);
    wp_enqueue_script('jquery-custom', get_stylesheet_directory_uri() . '/js/jquery.main.js', array( 'jquery' ), '1.0.0', true);
}
add_action( 'wp_enqueue_scripts', 'light_the_fires' );

/* and then tell them what to do */

function add_footer_scripts() { ?>

	<div id="fb-root"></div>

	<script type="text/javascript">

		jQuery(document).ready(function($) {

			<?php if(is_front_page()) { ?>
				$(function() {
				    $(window).resize(function() {
			            var width = $(window).width();
			            if( width > 736 ) {
			                // hide nav on home
						    var header = $(".navbar");
						    $(window).scroll(function() {
						        var scroll = $(window).scrollTop();
						        if (scroll >= 450) {
						            header.fadeIn("slow");
						            header.addClass("absolute");
						            $(".dropdown-menu").each(function(){
							            var parentWidth = $(this).parent().innerWidth();
							            var menuWidth = $(this).innerWidth();
							            var margin = (parentWidth / 2 ) - (menuWidth / 2);
							            margin = margin + "px";
							            $(this).css("left", margin);
							        });
						        } else {
						            header.fadeOut("slow");
						            header.removeClass("absolute");
						        }
						    });
			            }
				    });
				    $(window).resize();
				});
			<?php } else { ?>
			$(".dropdown-menu").each(function(){
	            var parentWidth = $(this).parent().innerWidth();
	            var menuWidth = $(this).innerWidth();
	            var margin = (parentWidth / 2 ) - (menuWidth / 2);
	            margin = margin + "px";
	            $(this).css("left", margin);
	        });
			<?php } ?>

			 // add active class to carousel pagination

			$(".carousel-indicators li:first").addClass("active");
			$(".carousel-inner .item:first").addClass("active");

			// slow scroll to anchor with offset, but only on taxonomy role pages

			<?php if (is_tax('role') || is_front_page()) { ?>
				$(function() {
				  $('a[href*=#]:not([href=#])').click(function() {
				    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				      var target = $(this.hash);
				      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				      if (target.length) {
				        $('html,body').animate({
				          scrollTop: target.offset().top - 90
				        }, 1000);
				        return false;
				      }
				    }
				  });
				});
			<?php } ?>

			$('.company-bio').readmore({
			  speed: 300,
			  maxHeight: 240,
			  moreLink: '<a class="marg-above marg-below" href="#">Read more <i class="fa fa-angle-down"></i></a>',
			  lessLink: '<a class="marg-below marg-above" href="#">Close <i class="fa fa-angle-up"></i></a>'
			});

			!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=235108513173333&version=v2.0";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));

		});

		new WOW({mobile: true}).init();

	</script>

<?php }

//add to wp_footer
add_action( 'wp_footer', 'add_footer_scripts', 100 );

//add the page slug to body_class()
function add_slug_body_class( $classes ) {
	global $post;
		if ( isset( $post ) ) {
			$classes[] = $post->post_type . '-' . $post->post_name;
		}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// gives us thickbox in the front end

add_action('init', 'init_theme_method');

function init_theme_method() {
   add_thickbox();
}

// adds bootstrap to a custom navigation walker
require_once('includes/wp_bootstrap_navwalker.php');

register_nav_menus( array(
    'primary' => __( 'Main Nav', 'bodytraffic' ),
    'home' => __( 'Home Nav', 'bodytraffic' ),
) );

?>
