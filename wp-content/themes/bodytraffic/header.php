<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

	    <title><?php wp_title(''); ?></title>

	    <!-- Fonts -->
	    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700' rel='stylesheet' type='text/css'>

	    <!-- Core CSS -->
	    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

	    <!-- Custom styles -->
	    <link href="<?php bloginfo('template_directory'); ?>/css/style-custom.min.css" rel="stylesheet">
	    <link href="<?php bloginfo('template_directory'); ?>/css/animate.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    <?php wp_head(); ?>

	</head>

  <body cz-shortcut-listen="true" <?php body_class(); ?>>
    <!-- Navigation -->
    <?php if(is_front_page()) { ?>
    <nav class="navbar navbar-default navbar-fixed-top disappear">
	<?php } else { ?>
    <nav class="navbar navbar-default navbar-fixed-top">
	<?php } ?>
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#BT-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="<?php bloginfo('template_directory'); ?>/images/B-logo-animated.gif" alt="Logo" height="50" width="60"/></a>
            </div>

            <?php
	            wp_nav_menu( array(
	                'menu'              => 'primary',
	                'theme_location'    => 'primary',
	                'depth'             => 2,
	                'container'         => 'div',
	                'container_class'   => 'collapse navbar-collapse',
					'container_id'      => 'BT-navbar-collapse',
	                'menu_class'        => 'nav navbar-nav navbar-right',
	                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	                'walker'            => new wp_bootstrap_navwalker())
	            );
	        ?>

            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
