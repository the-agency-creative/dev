<?php get_header(); ?>

    <div class="container-fluid">

      <div class="row start-container">
          <div class="col-xs-6 col-sm-4 col-md-5 hero-img hero-left wow fadeInLeft" data-wow-delay=".0s">
          	<div class="hero-button white text-center">
            	<a class="hero-sub-nav white" href="#"><span>SEE US</span></a>
         	</div>
            <img src="<?php bloginfo('template_directory'); ?>/images/Hero1.jpg" width="100%" height="auto" alt=""/>
            <img style="float: left;" src="<?php bloginfo('template_directory'); ?>/images/hero-shadow-left.jpg" width="100%" height="auto" alt=""/>
          </div>

          <div class="hidden-xs col-sm-4 col-md-2 text-center nav-center wow fadeInUp" data-wow-delay=".2s">
            <img id="home-logo" src="<?php bloginfo('template_directory'); ?>/images/B-logo-animated.gif" width="60" height="50" alt=""/>
            <?php
	            wp_nav_menu( array(
	                'menu'              => 'primary',
	                'theme_location'    => 'primary',
	                'depth'             => 2,
	                'menu_class'   		=> 'list-unstyled',
	                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	                'walker'            => new wp_bootstrap_navwalker())
	            );
	        ?>
          </div>

        <div class="col-xs-6 col-sm-4 col-md-5 hero-img hero-right wow fadeInRight" data-wow-delay=".0s">
        	<div class="hero-button white text-center">
            	<a class="hero-sub-nav white" href="#"><span>JOIN US</span></a>
         	</div>
            <img src="<?php bloginfo('template_directory'); ?>/images/Hero2.jpg" width="100%" height="auto" alt=""/>
        	<img style="float: right;" src="<?php bloginfo('template_directory'); ?>/images/hero-shadow-right.jpg" width="100%" height="auto" alt=""/>
        </div>
      
          <div id="Tagline" class="row text-center">
            <img src="<?php bloginfo('template_directory'); ?>/images/logo-bt-full.png" width="169" height="15" alt=""/>&nbsp;&nbsp;&nbsp;uniquely reflects LA’s diverse landscape – warm, innovative, and sexy.
          </div>
      </div>

      
      </div><!-- End container fluid -->

	<div class="container">
      <div class="row">
      	<div class="col-xs-6 col-sm-4 wow fadeInLeft" data-wow-delay=".3s">
        <div class="featured-textbox white text-center">
        	<span class="featured-valign-t tall">
                	<h2>EXPERIENCE</h2>
                    <h3>OUR BODY OF WORK</h3>
                    <a class="hero-sub-nav white" href="#"><span>View Repertory</span></a>
                </span>
            </div>
   	  		<img src="<?php bloginfo('template_directory'); ?>/images/Feature1.jpg" width="100%" height="auto" alt=""/>

      	</div>


			<div class="col-xs-6 col-sm-4 square wow fadeInRight" data-wow-delay=".6s">
            <div class="featured-textbox black text-center">
            <span class="featured-valign-t square">
                	<h2>GET INVOLVED</h2>
                    <h3>AND MAKE A DIFFERENCE</h3>
                    <a class="hero-sub-nav black" href="#"><span>Donate Now</span></a>
                    </span>
                </div>
           	 	<img src="<?php bloginfo('template_directory'); ?>/images/Feature2.jpg" width="100%" height="auto" alt=""/>
             </div>

            <div class="col-xs-6 col-sm-4 square marg-square wow fadeInRight" data-wow-delay=".9s">
            <div class="featured-textbox white text-center">
            <span class="featured-valign-b square">
                	<h2>LA'S FINEST</h2>
                    <h3>CHOREOGRAPHERS</h3>
                    <a class="hero-sub-nav white" href="#"><span>MEET US</span></a>
                    </span>
                </div>
        	    <img src="<?php bloginfo('template_directory'); ?>/images/Feature3.jpg" width="100%" height="auto" alt=""/>
			</div>

          	<div class="col-xs-12 col-sm-8 featured-8 wow fadeInRight" data-wow-delay="1.2s">
            	<div class="featured-textbox textbox-left black text-center">
                <span class="featured-valign-m wide">
                	<h2>CLASSES</h2>
                    <h3>FOR EVERYONE.<br>EVERY LEVEL.</h3>
                    <a class="hero-sub-nav black" href="#"><span>Learn More</span></a>
                    </span>
                </div>
            	<img src="<?php bloginfo('template_directory'); ?>/images/Feature4.jpg" width="100%" height="auto" alt=""/>
            </div>

  </div>

      <div class="row" style="margin-top:30px;">
        <div class="col-xs-12 col-sm-6 wow fadeInLeft" data-wow-delay=".8s">
        <div class="featured-bottom">
        <div class="featured-bottom-text black text-center bottom-outline">
                	<h2>PERFORMANCE</h2>
                    <h3>AT THE LAGUNA DANCE FESTIVAL</h3>
                    <a class="hero-sub-nav black" href="#"><span>BUY TICKETS</span></a>
          </div>
          <img src="<?php bloginfo('template_directory'); ?>/images/featured-shadow-L.jpg" width="100%" height="auto" alt=""/>
        </div>
        </div>

        <div class="col-xs-12 col-sm-6 wow fadeInRight" data-wow-delay=".8s">
        <div class="featured-bottom">
        <div class="featured-bottom-text black text-center bottom-fill">

                        <h2>MASTER CLASS</h2>
                        <h3>AT THE POSTHOUSE WITH JOSHUA PEUGH</h3>
                        <a class="hero-sub-nav black" href="#"><span>INFO + REGISTRATION</span></a>

                </div>
                <img src="<?php bloginfo('template_directory'); ?>/images/featured-shadow-R.jpg" width="100%" height="auto" alt=""/>
        </div>
        </div>

      </div>

	</div><!-- End container -->

<?php get_footer(); ?>
