<?php
/*
Template Name: About
*/

get_header();

?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <div class="row hero-full hero-about" style="background-image:url('<?php bloginfo('template_directory'); ?>/images/Header-img-about.jpg'); ?>">
      	<div class="container">
        	<div class="row text-center">
            	<h1><span><?php the_title(); ?></span></h1>
            </div>

            <div class="row">
                <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-delay=".3s">
                	<p>BODYTRAFFIC is helping establish Los Angeles as a major center for contemporary dance.  Founded in 2007 by Lillian Barbeito and Tina Finkelman Berkett, BODYTRAFFIC has surged to the forefront of the concert dance world.  Named “the company of the future” by The Joyce Theater Foundation, Dance Magazine’s 25 to Watch in 2013 and Best of Culture by the Los Angeles Times, the young company is already internationally recognized for their high quality of work.</p>
                </div>
             </div>

        </div>
      </div>

      </div><!-- End container fluid -->

	<div class="container">
    	<div class="row">
        	<div class="col-xs-12 col-sm-6">
            	<div class="quote-box text-center wow flipInX" data-wow-delay=".4s">
                	<h4>“Six dancers displayed fierce technique. The company is a welcome addition to the local dance scene.”</h4>
					<p>- Los Angeles Times, Victoria Looseleaf</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 wow fadeIn" data-wow-delay=".4s">
            	<p>Repertory includes works by Kyle Abraham, Lillian Barbeito, Sidra Bell, Stijn Celis, Sarah Elgart, Alex Ketley, Loni Landon, Barak Marshall, Andrea Miller, Laura Gorenstein Miller, Joshua Peugh, Miguel Perez, Victor Quijada, Zoe Scofield, Hofesh Shechter, Richard Siegal, and Guy Weizman & Roni Haver.</p>
            </div>
        </div>

      <div class="row pad-below">
	      <div class="col-xs-12 col-sm-push-6 col-sm-6 col-md-push-7 col-md-5 col-lg-push-8 col-lg-4 text-center wow fadeInRight" data-wow-delay=".2s">
	   	    	<img src="<?php bloginfo('template_directory'); ?>/images/about-01c.jpg" width="100%" height="auto" alt="dancer"/>
	      </div>
	      	<div class="col-xs-12 col-sm-pull-6 col-sm-6 col-md-pull-5 col-md-7 col-lg-pull-4 col-lg-8 make-table">
		      	<div class="row">
			      	<div class="col-xs-12">
	        			<p class="about-valign-m wow fadeIn" data-wow-delay=".6s">BODYTRAFFIC has performed for sold-out audiences at prestigious theaters and festivals throughout North America, including Jacob’s Pillow Dance Festival, New York City Center’s Fall for Dance, Chutzpah! Festival in Vancouver, Laguna Dance Festival, The Broad Stage in Santa Monica, Annenberg Center in Philadelphia, and World Music/CRASHarts in Boston. Recent performance highlights include: The Los Angeles Philharmonic’s Opening Night Gala at Walt Disney Concert Hall conducted by Gustavo Dudamel and a collaboration with choreographer Victor Qujida, sculptor Gustavo Godoy, and composer Jasper Gahunia for a site-specific endeavor for Dance Camera West at the Music Center Plaza. In 2015, BODYTRAFFIC engagements include The Broad Stage in Santa Monica, American Dance Festival in North Carolina as well as Jacob’s Pillow Dance Festival in Massachusetts.</p>
			      	</div>
		      	</div>
		      	<div class="row">
			      	<div class="col-xs-12 col-sm-6">
			        	<div class="quote-box text-center wow flipInX" data-wow-delay=".4s">
		                	<h4>“BODYTRAFFIC displays sheer animal magnetism...”</h4>
							<p>- San Francisco Weekly, Irene Hsiao</p>
		                </div>
			      	</div>
			      	<div class="col-xs-12 col-sm-6">
		                <div class="quote-box text-center wow flipInX" data-wow-delay=".4s">
		                	<h4>“It’s all green lights for BODYTRAFFIC”</h4>
							<p>- Los Angeles Times, Laura Bleiberg</p>
		                </div>
			      	</div>
		      	</div>
	        </div>
      </div>

	</div><!-- End container -->

    <div class="container-fluid">
    	<div class="row about-quote page-fixed" style="background-image:url('<?php bloginfo('template_directory'); ?>/images/fixed-img01.jpg'); ?>">
        	<div class="col-xs-12 text-center">
            	<h3>“…one of the most talked-about young companies, not just in L.A. but nationwide…”</h3>
                <p>- Los Angeles Times, Laura Bleiberg</p>
            </div>
        </div>
    </div><!-- End container -->

    <div class="container">

      <div class="row pad-below marg-above">

                <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 text-center wow fadeInLeft" data-wow-delay=".2s">
                    <img src="<?php bloginfo('template_directory'); ?>/images/about-02c.jpg" width="100%" height="auto" alt="dancer"/>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-7 col-lg-8 make-table">

                    <p class="about-valign-m wow fadeIn" data-wow-delay=".6s">BODYTRAFFIC has received generous support from The Joyce Theater Foundation, The Sydney D. Holland Foundation, The Mortimer & Mimi Levitt Foundation, Jewish Community Foundation of Los Angeles, The Rosalinde and Arthur Gilbert Foundation, and Mid Atlantic Arts Foundation. BODYTRAFFIC was awarded NEFA’s National Dance Project Production and Touring Grants to commission Once again, before you go by choreographer Victor Quijada. Additionally, in 2013 choreographer Loni Landon was awarded a Choreography Fellowship from the Princess Grace Foundation for a creation with BODYTRAFFIC.</p>

            </div>
    	</div>
        <div class="row">
        	<div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3">
            	<div class="quote-box text-center wow flipInX" data-wow-delay=".4s">
                	<h4>“BODYTRAFFIC suggests invention, attitude, and urban edge.”</h4>
					<p>- The Boston Globe, Karen Campbell</p>
                </div>
            </div>
        </div>
	</div><!-- End container -->

<?php endwhile; else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

<?php get_footer(); ?>
