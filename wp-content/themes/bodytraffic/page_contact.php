<?php

/*****

Template Name: Contact

******/

get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="container-fluid text-center">
	    <div class="row header-row text-center">
	        <div class="col-xs-12 contact-header">
	            <h1 class="wow fadeInUp"><span><?php the_title(); ?></span></h1>
	        </div>
	    </div>

	</div><!-- End container fluid -->

    <div class="container">

        <div id="content" class="row contact-txt-h">
        	<div class="col-xs-12 contact-txt-right">
        		<?php the_field('bt_contact_left'); ?>
        	</div>
        	<div class="col-xs-12 contact-txt-right">
        		<?php the_field('bt_contact_right'); ?>
        	</div>
        </div>
    </div><!-- End container -->


    <?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>
<?php get_footer(); ?>
