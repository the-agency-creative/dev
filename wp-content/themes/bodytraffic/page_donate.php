<?php

/*
Template Name: Support
*/

get_header();

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>

    <div class="container">
      <div class="row header-row support-header">
      	<div class="col-xs-12 col-sm-6 no-edges wow fadeInRight" data-wow-delay=".2s">
        	<img src="<?php bloginfo('template_directory'); ?>/images/donate_v3.jpg" width="100%" height="auto" alt=""/>
        </div>
        <div class="col-xs-12 col-sm-6 work-text support-item bkg-white">
        	<?php the_field('ways_to_support'); ?>
        </div>
      </div>


    </div><!-- End container -->
    <div class="container">


        <div class="row">
	        <div class="col-xs-12 col-sm-6 wow fadeIn" data-wow-delay=".2s">
	        	<div>
	        		<h1><span><?php the_title(); ?><span></h1>
	            </div>
	            <p>
					<?php the_content(); ?>
	            </p>
	        </div>

            <div class="col-xs-12 col-sm-6 smile-border">
        		<?php the_field('amazon_smile'); ?>
        	</div>
        </div><!-- End row -->


    <?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>

    </div><!-- End container -->

<?php get_footer(); ?>
