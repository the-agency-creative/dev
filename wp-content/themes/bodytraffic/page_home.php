<?php
/*
Template Name: Home
*/

get_header();

?>

    <div class="container-fluid height-fix">

      <div class="row start-container">
		    <?php
				$left_hero_image_obj = get_field('bt_home_left_hero_image', 'option');
				$left_hero_image_size = 'home-hero-image';
				$left_hero_image = $left_hero_image_obj['sizes'][$left_hero_image_size];
			?>
          <div class="col-xs-12 col-sm-4 col-md-5 hero-img hero-left wow fadeInLeft" data-wow-delay=".0s" style="background-image: url('<?php echo $left_hero_image; ?>');">
          	<div class="hero-button white text-center">
            	<a class="hero-sub-nav aleft white" href="<?php the_field('bt_home_left_hero_button_link', 'option'); ?>"><span><?php the_field('bt_home_left_hero_button_text', 'option'); ?></span></a>
         	</div>
            <img class="pull-left hero-shadow hidden-xs" src="<?php bloginfo('template_directory'); ?>/images/hero-shadow-left.jpg" width="100%" height="auto" alt=""/>
          </div>

          <div class="hidden-xs col-sm-4 col-md-2 text-center nav-center wow fadeInUp" data-wow-delay=".2s">
            <img id="home-logo" src="<?php bloginfo('template_directory'); ?>/images/B-logo-animated.gif" width="60" height="50" alt=""/>
            <?php
	            wp_nav_menu( array(
	                'menu'              => 'primary',
	                'theme_location'    => 'primary',
	                'depth'             => 2,
	                'menu_class'   		=> 'list-unstyled',
	                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	                'walker'            => new wp_bootstrap_navwalker())
	            );
	        ?>
			<a id="home-arrow" class="text-center" href="#grid-container"><i class="fa fa-angle-down"></i></a>
          </div>
	  	<?php
		  	$right_hero_image_obj = get_field('bt_home_right_hero_image', 'option');
		  	$right_hero_image_size = 'home-hero-image';
		  	$right_hero_image = $right_hero_image_obj['sizes'][$right_hero_image_size];
		?>
        <div class="col-xs-12 col-sm-4 col-md-5 hero-img hero-right wow fadeInRight" data-wow-delay=".0s" style="background-image: url('<?php echo $right_hero_image; ?>');">
        	<div class="hero-button white text-center">
            	<a class="hero-sub-nav aright white" href="<?php the_field('bt_home_right_hero_button_link', 'option'); ?>"><span><?php the_field('bt_home_right_hero_button_text', 'option'); ?></span></a>
         	</div>
        	<img class="pull-right hero-shadow hidden-xs" src="<?php bloginfo('template_directory'); ?>/images/hero-shadow-right.jpg" width="100%" height="auto" alt=""/>
        </div>
      </div>
    </div><!-- End container fluid -->

    <div class="container-fluid">

        <div id="Tagline" class="row text-center">
   	  	<img src="<?php bloginfo('template_directory'); ?>/images/logo-bt-full.png" width="169" height="15" alt=""/>&nbsp;&nbsp;&nbsp;<?php the_field('bt_home_tagline', 'option'); ?>
      	</div>

    </div><!-- End container fluid -->

	<div class="container">
      <div id="grid-container" class="row no-edges">
	      <?php
			$box1_image_obj = get_field('bt_home_box_1_image', 'option');
			$box1_image_size = 'home-featured-image-1';
			$box1_image = $box1_image_obj['sizes'][$box1_image_size];
		?>
      	<div class="col-xs-12 col-sm-4 wow fadeInLeft grid-item tall" data-wow-delay=".3s" style="background-image: url('<?php echo $box1_image; ?>')">
        	<div class="featured-textbox white text-center">
        		<span class="featured-valign-t tall">
                	<h2><?php the_field('bt_home_box_1_headline', 'option'); ?></h2>
                    <h3><?php the_field('bt_home_box_1_subhead', 'option'); ?></h3>
                    <a class="hero-sub-nav white" href="<?php the_field('bt_home_box_1_button_link', 'option'); ?>"><span><?php the_field('bt_home_box_1_button_text', 'option'); ?></span></a>
                </span>
            </div>
      	</div>
      	<?php
			$box2_image_obj = get_field('bt_home_box_2_image', 'option');
			$box2_image_size = 'company-image';
			$box2_image = $box2_image_obj['sizes'][$box2_image_size];
		?>
		<div class="col-xs-12 col-sm-4 square wow fadeInRight grid-item" data-wow-delay=".6s" style="background-image: url('<?php echo $box2_image; ?>');">
        	<div class="featured-textbox black text-center">
	            <span class="featured-valign-t square">
                	<h2><?php the_field('bt_home_box_2_headline', 'option'); ?></h2>
                    <h3><?php the_field('bt_home_box_2_subhead', 'option'); ?></h3>
                    <a class="hero-sub-nav black" href="<?php the_field('bt_home_box_2_button_link', 'option'); ?>"><span><?php the_field('bt_home_box_2_button_text', 'option'); ?></span></a>
                </span>
            </div>
         </div>
         <?php
			$box3_image_obj = get_field('bt_home_box_3_image', 'option');
			$box3_image_size = 'company-image';
			$box3_image = $box3_image_obj['sizes'][$box3_image_size];
		?>
        <div class="col-xs-12 col-sm-4 square marg-square wow fadeInRight grid-item" data-wow-delay=".9s" style="background-image: url('<?php echo $box3_image; ?>');">
        	<div class="featured-textbox white text-center">
	            <span class="featured-valign-b square">
                	<h2><?php the_field('bt_home_box_3_headline', 'option'); ?></h2>
                    <h3><?php the_field('bt_home_box_3_subhead', 'option'); ?></h3>
                    <a class="hero-sub-nav white" href="<?php the_field('bt_home_box_3_button_link', 'option'); ?>"><span><?php the_field('bt_home_box_3_button_text', 'option'); ?></span></a>
                </span>
            </div>
		</div>
		<?php
			$box4_image_obj = get_field('bt_home_box_4_image', 'option');
			$box4_image_size = 'home-featured-image-4';
			$box4_image = $box4_image_obj['sizes'][$box4_image_size];
		?>
      	<div class="col-xs-12 col-sm-8 featured-8 wow fadeInRight wide grid-item" data-wow-delay="1.2s" style="background-image: url('<?php echo $box4_image; ?>');">
        	<div class="featured-textbox textbox-left black text-center">
                <span class="featured-valign-m wide">
                	<h2><?php the_field('bt_home_box_4_headline', 'option'); ?></h2>
                    <h3><?php the_field('bt_home_box_4_subhead', 'option'); ?></h3>
                    <a class="hero-sub-nav black" href="<?php the_field('bt_home_box_4_button_link', 'option'); ?>"><span><?php the_field('bt_home_box_4_button_text', 'option'); ?></span></a>
                </span>
            </div>
        </div>
  	</div>
      <div class="row" style="margin-top:30px;">
        <div class="col-xs-12 col-sm-6 wow fadeInLeft" data-wow-delay=".8s">
        	<div class="featured-bottom">
				<div class="featured-bottom-text black text-center bottom-outline">
					<h2>PERFORMANCE</h2>
					<?php
					global $post;
					$all_events = tribe_get_events(
						array(
							'eventDisplay'=>'list',
							'posts_per_page'=>1,
							'tax_query'=> array(
								array(
									'taxonomy' => 'tribe_events_cat',
									'field' => 'slug',
									'terms' => 'performances'
								)
							)
						)
					);
					foreach($all_events as $post) {
					setup_postdata($post);
					?>
			        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			        <a class="hero-sub-nav black" href="<?php the_permalink(); ?>"><span>BUY TICKETS</span></a>
					<?php } //endforeach ?>
				    <?php wp_reset_query(); ?>
          		</div>
		  		<img src="<?php bloginfo('template_directory'); ?>/images/featured-shadow-L.jpg" width="100%" height="auto" alt=""/>
        	</div>
        </div>

        <div class="col-xs-12 col-sm-6 wow fadeInRight" data-wow-delay=".8s">
        	<div class="featured-bottom">
				<div class="featured-bottom-text black text-center bottom-fill">
                    <h2>EDUCATION</h2>
                    <?php
					global $post;
					$all_events = tribe_get_events(
						array(
							'eventDisplay'=>'list',
							'posts_per_page'=>1,
							'tax_query'=> array(
								array(
									'taxonomy' => 'tribe_events_cat',
									'field' => 'slug',
									'terms' => 'classes'
								)
							)
						)
					);
					foreach($all_events as $post) {
					setup_postdata($post);
					?>
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
			        <a class="hero-sub-nav black" href="<?php the_permalink(); ?>"><span>INFO + REGISTRATION</span></a>
                    <?php } //endforeach ?>
				    <?php wp_reset_query(); ?>
                </div>
                <img src="<?php bloginfo('template_directory'); ?>/images/featured-shadow-R.jpg" width="100%" height="auto" alt=""/>
        	</div>
        </div>
      </div>
	</div><!-- End container -->

<?php get_footer(); ?>
