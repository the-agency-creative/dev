<?php

/*
Template Name: Repertory
*/

get_header();

?>

	<div id="content" class="container">
      <div class="row header-row">
      	<div class="col-xs-12 col-sm-6 wow fadeInRight" data-wow-delay=".2s">
        	<img src="<?php bloginfo('template_directory'); ?>/images/girl3.jpg" width="100%" height="auto" alt=""/>
        </div>
        <div class="col-xs-12 col-sm-6 work-text wow fadeIn" data-wow-delay=".2s">
        	<div>
        		<h1><span><?php the_title(); ?><span></h1>
            </div>
            <p><?php the_field('bt_work_description'); ?></p>
        </div>
      </div>

      <?php $repertory = new WP_Query("post_type=repertory"); while($repertory->have_posts()) : $repertory->the_post();?>
      <div class="row work-item">
	  	<div class="col-xs-12 col-sm-6">
        	<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <ul class="list-unstyled">
	            <li>
	            	<strong>Choreography:</strong>
		           <?php

			        $choreographers = get_field('bt_performance_choreographer');

					if( $choreographers ):
						foreach( $choreographers as $choreographer ): // variable must NOT be called $post (IMPORTANT)
							$choreographer_names[] = get_the_title( $choreographer->ID );
						endforeach;
						echo implode(', ', $choreographer_names);
						unset($choreographer_names);
					endif;
					?>
				</li>
				<?php
					$dancers = get_field('bt_performance_dancers');
					$dancer_count = count($dancers);
					if(!empty($dancers)) {
						echo '<li><strong>Performers:</strong> ' . $dancer_count . ' dancers</li>';
					} else {}
				?>
				<?php if(get_field('bt_performance_creation_period')) : ?><li><strong>Creation Period:</strong>  <?php the_field('bt_performance_creation_period'); ?></li><?php endif; ?>
				<?php if(get_field('bt_performance_premiere')) : ?><li><strong>Premiere:</strong>  <?php the_field('bt_performance_premiere'); ?></li><?php endif; ?>
			</ul>
            <div class="text-right"><a href="<?php the_permalink(); ?>">See More ></a></div>
        </div>
        <div class="col-xs-12 col-sm-6 m-no-pad work-image-wrap">
	        <?php
		        $repertory_image_obj = get_field('bt_performance_image');
		        if( !empty($repertory_image_obj) ):
		        $size = 'repertory-image';
				$repertory_image = $repertory_image_obj['sizes'][ $size ];
			?>
				<a href="<?php the_permalink(); ?>"><img class="work-image" src="<?php echo $repertory_image; ?>" alt="<?php the_title(); ?>" /></a>
				<h3 class="work-more get-up"><a href="<?php the_permalink(); ?>" class="hero-sub-nav">See More</a></h3>
			<?php endif; ?>
        </div>
      </div>
      <?php endwhile; ?>
      <?php wp_reset_query(); ?>

    </div><!-- End container -->

<?php get_footer(); ?>
