<?php get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php
		if(get_field('bt_performance_header_image')) :
	        $repertory_header_image_obj = get_field('bt_performance_header_image');
	        $size = 'role-image';
			$repertory_header_image = $repertory_header_image_obj['sizes'][ $size ];
	?>
    <div class="container-fluid hero-full text-center" style="background-image: url('<?php echo $repertory_header_image; ?>');">
	    <div class="row text-center no-edges">
	        <div class="col-xs-12">
	            <h1 class="wow fadeInUp"><span><?php the_title(); ?></span></h1>
	            <?php
	            $choreographers = get_field('bt_performance_choreographer');
	            if($choreographers) {
		        ?>
            	<h3>
		           <?php
			        if( !empty($choreographers) ) {
				        echo '<em>by</em> ';
				    } else { }
					if( count($choreographers) ):
						foreach( $choreographers as $c=>$choreography ): // variable must NOT be called $post (IMPORTANT)
					    	if($c) echo ', ';
					        echo get_the_title( $choreography->ID );
						endforeach;
					endif;
					?>
				</h3>
				<?php } ?>
	        </div>
	    </div>
	</div><!-- End container fluid -->
	<?php endif; ?>

    <div class="container">
        <div class="row pad-below wow fadeInRight" data-wow-delay=".2s">
            <div class="col-xs-12">
	            <h2>synopsis</h2>
	            <?php the_field('bt_performance_description'); ?>
            </div>
        </div>

        <div class="row pad-below marg-above">
            <div class="col-xs-12 col-sm-7 col-lg-8 wow fadeInLeft" data-wow-delay=".4s">
				<?php
				$bt_performance_gallery_images = get_field('bt_performance_image_gallery');
				if( $bt_performance_gallery_images ): $i=0; ?>
				<div id="bt-performance-gallery" class="carousel slide carousel-fade" data-ride="carousel">
			      <ol class="carousel-indicators">
			      	<?php foreach( $bt_performance_gallery_images as $bt_performance_gallery_image ): ?>
			        <li data-target="#bt-performance-gallery" data-slide-to="<?php echo $i++; ?>"></li>
			        <?php endforeach; ?>
			      </ol>
			      <div class="carousel-inner">
			      	<?php foreach( $bt_performance_gallery_images as $bt_performance_gallery_image ):
				      		$bt_performance_gallery_image_size = "repertory-gallery-image";
			      			$bt_performance_gallery_image_url = $bt_performance_gallery_image['sizes'][$bt_performance_gallery_image_size];
				  	?>
			        <div class="item">
						<img class="img-responsive" src="<?php echo $bt_performance_gallery_image_url; ?>" alt="<?php the_title(); ?>" />
			        </div>
			        <?php endforeach; ?>
			      </div>
			      <a class="left bt-carousel-control" href="#bt-performance-gallery" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
				  <a class="right bt-carousel-control" href="#bt-performance-gallery" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
			    </div><!-- /.carousel -->
				<?php endif; ?>
	        </div>

            <div class="col-xs-12 col-sm-5 col-lg-4 wow fadeIn" data-wow-delay=".4s">
                <h2>ABOUT THE WORK</h2>
                <ul class="list-unstyled">
	                <?php
		            $choreographers = get_field('bt_performance_choreographer');
		            if($choreographers) {
			        ?>
                	<li>
			           <?php
				        if( !empty($choreographers) ) {
					        echo '<strong>Choreography:</strong> ';
					    } else { }
						if( count($choreographers) ):
							foreach( $choreographers as $c=>$choreography ): // variable must NOT be called $post (IMPORTANT)
						    	if($c) echo ', ';
						        echo get_the_title( $choreography->ID );
							endforeach;
						endif;
						?>
					</li>
					<?php } ?>
					<?php
					$dancers = get_field('bt_performance_dancers');
					if($dancers) {
					?>
					<li>
			           <?php
				        $dancers = get_field('bt_performance_dancers');
				        if( !empty($dancers) ) {
					        echo '<strong>Dancers:</strong>';
					    } else { }
						if( count($dancers) ):
							foreach( $dancers as $k=>$dancer ): // variable must NOT be called $post (IMPORTANT)
						    	if($k) echo ', ';
						        echo get_the_title( $dancer->ID );
							endforeach;
						endif;
						?>
					</li>
					<?php } ?>
					<?php if(get_field('bt_performance_music')) : ?><li><strong>Music:</strong>  <?php the_field('bt_performance_music'); ?></li><?php endif; ?>
					<?php if(get_field('bt_performance_lighting_design')) : ?><li><strong>Lighting Design:</strong>  <?php the_field('bt_performance_lighting_design'); ?></li><?php endif; ?>
					<?php if(get_field('bt_performance_costume_design')) : ?><li><strong>Costume Design:</strong>  <?php the_field('bt_performance_costume_design'); ?></li><?php endif; ?>
					<?php if(get_field('bt_performance_running_time')) : ?><li><strong>Running Time:</strong>  <?php the_field('bt_performance_running_time'); ?></li><?php endif; ?>
					<?php if(get_field('bt_performance_premiere')) : ?><li><strong>Premiere:</strong>  <?php the_field('bt_performance_premiere'); ?></li><?php endif; ?>
                </ul>
            </div>
        </div>

		<?php
		if( have_rows('bt_performance_quotes') ):
		?>
		<div class="row marg-above marg-below">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 wow flipInX" data-wow-delay=".5s">
                <div class="quote-box text-center">
		<?php
		while ( have_rows('bt_performance_quotes') ) : the_row();
		?>
					<div class="quote-wrap">
	                    <h4>"<?php the_sub_field('bt_performance_quote'); ?>"</h4>
	                    <?php if(get_sub_field('bt_performance_quote_attribution')) : ?><attr><a href="<?php the_sub_field('bt_performance_quote_link'); ?>">- <?php the_sub_field('bt_performance_quote_attribution'); ?></a></attr><?php endif; ?>
					</div>
        <?php endwhile; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>

		<?php if(get_field('bt_performance_additional_content')): ?>
        <div class="row">
            <div class="col-xs-12 wow fadeIn" data-wow-delay=".2s">
                <?php the_field('bt_performance_additional_content'); ?>
            </div>
        </div>
		<?php endif; ?>

		<div class="row pad-below marg-above wow fadeInRight" data-wow-delay=".3s">
		<?php
		if( have_rows('bt_performance_videos') ):
		while ( have_rows('bt_performance_videos') ) : the_row();
		?>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-center">
                <?php if(get_sub_field('bt_performance_video_title')) : ?><h2 class="text-left"><?php the_sub_field('bt_performance_video_title'); ?></h2><?php endif; ?>
                <div id="bt-performance-video">
	            	<?php the_sub_field('bt_performance_video'); ?>
	            </div>
            </div>
        <?php
		endwhile;
		endif;
		?>
		</div>

		<?php if(get_field('bt_performance_video')): ?>
        <div class="row pad-below marg-above wow fadeInRight" data-wow-delay=".3s">
            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 text-center">
                <?php if(get_field('bt_performance_video_header')) : ?><h2 class="text-left"><?php the_field('bt_performance_video_header'); ?></h2><?php endif; ?>
            </div>
            <div class="col-xs-12">
	            <div id="bt-performance-video">
	            	<?php the_field('bt_performance_video'); ?>
	            </div>
	        </div>
        </div>
        <?php endif; ?>

    </div><!-- End container -->
    <?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>
<?php get_footer(); ?>
