<?php get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php
		if(get_field('bt_page_header_image')) {
		    $page_header_image_obj = get_field('bt_page_header_image');
			$page_header_image_size = 'page-header-image';
			$page_header_image = $page_header_image_obj['sizes'][ $page_header_image_size ];
		}
	?>
    <div class="container-fluid hero-full text-center" style="background-image: url(<?php if(get_field('bt_page_header_image')) { echo $page_header_image; } ?>);">
	    <div class="row text-center">
	        <div class="col-xs-12">
	            <h1 class="wow fadeInUp"><span><?php the_title(); ?></span></h1>
	        </div>
	    </div>
	</div><!-- End container fluid -->

    <div id="content" class="container">
        <?php the_content(); ?>
    </div><!-- End container -->
    <?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>
<?php get_footer(); ?>
