<?php get_header(); ?>

		<?php
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

		$tax_title = $term->name;
		$tax_description = $term->description;
		$tax_slug = $term->slug;

		$role_image_obj = get_field('bt_role_header_image', $term);
		if( !empty($role_image_obj) ) {
		$role_image_size = 'role-image';
		$role_image = $role_image_obj['sizes'][ $role_image_size ];
		?>

        <div class="row hero-full hero-dancers" style="background-image: url('<?php echo $role_image; ?>');">
	    <?php } else { ?>
	    <div class="row hero-full hero-dancers" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/Header-img-dancers.jpg');">
		<?php } ?>
            <div class="container">
                <div class="row text-center">
                    <h1><span><?php echo $tax_title; ?></span></h1>
                </div>

                <div class="row sub-nav-pad text-center row-centered">
	                <?php
		            $i = 1;
					$term_id = 5; //collaborators
					$taxonomy_name = 'role';
					$termchildren = get_term_children( $term_id, $taxonomy_name );
					foreach ( $termchildren as $child ) {
						$term_child = get_term_by( 'id', $child, $taxonomy_name );
						$term_link = get_term_link( $term_child );
					?>
                	<div class="col-xs-4 col-sm-3 col-md-2 hero-sub-nav wow fadeInUp col-centered" data-wow-delay=".1s">
                        <div>
                            <a href="<?php echo $term_link; ?>"><?php echo $term_child->name; ?></a>
                        </div>
                    </div>
	                <?php } ?>
                </div>
            </div>
        </div>
    </div><!-- End container fluid -->

    <!-- <div class="container">
        <?php
	        $i = 0;
	        if ( have_posts() ) :
	        while ( have_posts() ) :
	        the_post();
	        $i++;
	    ?>
        <div class="row pad-below">
	        <a name="<?php echo $tax_slug . '-' . $i; ?>"></a>
            <div class="col-sm-4 <?php if( $i % 2 == 0 ) : echo 'col-sm-push-8 '; endif; ?>text-center with-shadow wow <?php if( $i % 2 == 0 ) { echo 'fadeInRight'; } else { echo 'fadeInLeft'; } ?>" data-wow-delay=".2s">
	            <?php
			     	$company_image_obj = get_field('bt_company_image');
			        if( !empty($company_image_obj) ) {
			        $company_image_size = 'company-image';
					$company_image = $company_image_obj['sizes'][ $company_image_size ];
				?>
				<img src="<?php echo $company_image; ?>"  width="100%" height="auto" alt="<?php the_title(); ?>" />
				<?php } else { ?>
				<img src="<?php bloginfo('template_directory'); ?>/images/headshots-placeholder.jpg" width="100%" height="auto" alt="">
				<?php } ?>
	            <img src="<?php bloginfo('template_directory'); ?>/images/shadow-box.png" width="100%" height="auto" alt="">
	        </div>
            <div class="col-sm-8 <?php if( $i % 2 == 0 ) : echo 'col-sm-pull-4 '; endif; ?>text-justify wow fadeIn" data-wow-delay=".2s">
                <h2><?php the_title(); ?></h2>

                <div class="company-bio"><?php the_field('bt_company_bio'); ?></div>

            </div>
        </div>
        <?php endwhile; else: ?>
        	<p><?php _e('Sorry, no company members matched your criteria.'); ?></p>
        <?php endif; ?>
    </div><!-- End container -->

<?php get_footer(); ?>
