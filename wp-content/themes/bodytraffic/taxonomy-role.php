<?php get_header(); ?>

		<?php
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

		$tax_title = $term->name;
		$tax_description = $term->description;
		$tax_slug = $term->slug;

		$role_image_obj = get_field('bt_role_header_image', $term);
		if( !empty($role_image_obj) ) {
		$role_image_size = 'role-image';
		$role_image = $role_image_obj['sizes'][ $role_image_size ];
		?>

        <div class="row hero-full hero-dancers" style="background-image: url('<?php echo $role_image; ?>');">
	    <?php } else { ?>
	    <div class="row hero-full hero-dancers" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/Header-img-dancers.jpg');">
		<?php } ?>
            <div class="container">
                <div class="row text-center">
                    <h1><span><?php echo $tax_title; ?></span></h1>
                </div>

                <div class="row">
	                <div class="pad-above col-md-8 col-md-offset-2">
	                	<p><?php echo $tax_description; ?></p>
	                </div>
                </div>

                <div class="row sub-nav-pad text-center row-centered">
	                <?php
		            $i = 1;
		            $args = array(
						'post_type' => 'company',
						'role'    => $tax_slug,
					);
					$company_names = new WP_Query( $args );
		            while($company_names->have_posts()) :
		            $company_names->the_post();
		            ?>
                	<div class="col-xs-4 col-sm-3 col-md-2 hero-sub-nav wow fadeInUp col-centered" data-wow-delay=".1s">
                        <div>
                            <a href="#<?php echo $tax_slug . '-' . $i++; ?>"><?php the_title(); ?></a>
                        </div>
                    </div>
	                <?php endwhile; ?>
	                <?php wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div><!-- End container fluid -->

    <div class="container">
        <?php
	        $i = 0;
	        if ( have_posts() ) :
	        while ( have_posts() ) :
	        the_post();
	        $i++;
	    ?>
        <div class="row pad-below">
	        <a name="<?php echo $tax_slug . '-' . $i; ?>"></a>
            <div class="col-sm-4 <?php if( $i % 2 == 0 ) : echo 'col-sm-push-8 '; endif; ?>text-center with-shadow wow <?php if( $i % 2 == 0 ) { echo 'fadeInRight'; } else { echo 'fadeInLeft'; } ?>" data-wow-delay=".2s">
	            <?php
			     	$company_image_obj = get_field('bt_company_image');
			        if( !empty($company_image_obj) ) {
			        $company_image_size = 'company-image';
					$company_image = $company_image_obj['sizes'][ $company_image_size ];
				?>
				<img src="<?php echo $company_image; ?>"  width="100%" height="auto" alt="<?php the_title(); ?>" />
				<?php } else { ?>
				<img src="<?php bloginfo('template_directory'); ?>/images/headshots-placeholder.jpg" width="100%" height="auto" alt="">
				<?php } ?>
	            <img src="<?php bloginfo('template_directory'); ?>/images/shadow-box.png" width="100%" height="auto" alt="">
	            <div class="company-social">
		            <ul class="list-inline">
			            <?php if(get_field('bt_company_instagram')) { ?>
						<li class="insta"><a href="https://instagram.com/<?php the_field('bt_company_instagram'); ?>"><i class="fa fa-instagram"></i></a></li>
						<?php } ?>
			            <?php if(get_field('bt_company_twitter')) { ?>
						<li class="twitter-fix"><a href="https://twitter.com/<?php the_field('bt_company_twitter'); ?>" class="twitter-follow-button" data-show-count="false">Follow @<?php the_field('bt_company_twitter'); ?></a></li>
						<?php } ?>
						<?php if(get_field('bt_company_facebook')) { ?>
						<li><div class="fb-like" data-href="https://facebook.com/<?php the_field('bt_company_facebook'); ?>" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div></li>
						<?php } ?>
						<?php if(get_field('bt_company_custom_link_text')) { ?>
						<li><a href="<?php the_field('bt_company_custom_link_url'); ?>"><?php the_field('bt_company_custom_link_text'); ?> <i class="fa fa-external-link"></i></a></li>
						<?php } ?>
		            </ul>
	            </div>
	        </div>
            <div class="col-sm-8 <?php if( $i % 2 == 0 ) : echo 'col-sm-pull-4 '; endif; ?>text-justify wow fadeIn" data-wow-delay=".2s">
                <h2 <?php if( $i % 2 == 0 ) : echo 'class="text-right"'; endif; ?>><?php the_title(); ?></h2>
                	<?php if(get_field('bt_company_related_repertory')) : ?>
                	<div class="pieces-list<?php if( $i % 2 == 0 ) : echo ' text-right'; endif; ?>">
	                	<ul class="list-inline">
		                	<li><span>Piece(s):</span></li>
		                	<li>
	                	<?php
				        $pieces = get_field('bt_company_related_repertory');
						if( $pieces ):
							foreach( $pieces as $piece ): // variable must NOT be called $post (IMPORTANT)
								$piece_name = get_the_title($piece->ID);
								$piece_link = get_the_permalink($piece->ID);
								$pieces_list[] = '<a href="' . $piece_link . '" title="' . $piece_name . '"><em>' . $piece_name .'</em></a>';
							endforeach;
							echo implode(', ', $pieces_list);
							unset($pieces_list);
						endif;
						?>
		                	</li>
	                	</ul>
                	</div>
					<?php endif; ?>
                <div class="company-bio"><?php the_field('bt_company_bio'); ?></div>
            </div>
        </div>
        <?php endwhile; else: ?>
        	<p><?php _e('Sorry, no company members matched your criteria.'); ?></p>
        <?php endif; ?>
    </div><!-- End container -->

<?php get_footer(); ?>
