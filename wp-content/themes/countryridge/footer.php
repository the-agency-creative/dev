
    <!-- Start Contact 1 -->
    <section id="to-iquire-now" class="content-block contact-1">
        <div class="container text-center">

            <div class="col-sm-10 col-sm-offset-1">

                <div class="underlined-title">
                    <h2>Request  Information</h2>
                    <hr>
                </div>

                <p>Complete the form below to stay up to date on the latest information from
                    <br>Country&nbsp;Ridge or call <a href="tel:424.221.5039">424.221.5039</a> to speak with a member&nbsp;of&nbsp;our&nbsp;team&nbsp;today.</p>

                <div id="contact" class="form-container">

                    <div id="message"></div>


                    <iframe src="http://go.theagencyre.com/l/75802/2015-09-21/bv18r" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>


                </div>
                <!-- /.form-container -->

                <hr class="middle-divider">

                <div class="underlined-title pre-qualify">
                    <h2>Pre-Qualify Now</h2>
                    <hr>
                    <p>Visit <a href="https://2048902603.mortgage-application.net/WebApp/Start.aspx?cm_sp=lending-_-experts-_-applynow">EverBank</a> to pre&#8209;qualify today for this limited&nbsp;residential&nbsp;offering.</p>
                </div>
            </div>
            <!-- /.col-sm-10 -->

        </div>
        <!-- /.container -->
    </section>
    <!-- /.content-block -->


    <!-- Start Copyright Bar 1 -->
    <div class="copyright-bar">
        <div class="container">

            <div class="row flex-contain-footer">
                <div class="col-sm-1 col-xs-12">
                    <a href="http://www.theagencyre.com/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/brand/agency-logo.png" alt="The Agency" class="img-responsive inline-img"></a>
                </div>

                <div class="equal-housing">
                    <img src="<?php bloginfo('template_directory'); ?>/images/brand/equal-housing.png" alt="Equal Housing">
                </div>
                <div class="col-sm-3 col-xs-12">
                    <span>SITE BY THE AGENCY CREATIVE</span>
                </div>


                <div class="col-sm-2 col-sm-offset-4 col-xs-12 text-right">
                    <p>Inquire Now</p>
                    <a href="tel:424.221.5039" class="active">Tel: 424.221.5039</a>
                </div>
                <div class="col-sm-1 col-xs-12">
                    <img src="<?php bloginfo('template_directory'); ?>/images/brand/cr-vertical-logo.png" alt="Country Ridge">
                </div>
            </div>
        </div>
    </div>
    <!--// End Copyright Bar 1 -->


    <!-- // END Landing Page Content -->

    <!-- GA SRIPT -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');​
        ga('create', 'UA-71549422-1', 'auto');
        ga('send', 'pageview');​
    </script>
    
    <?php wp_footer(); ?>

</body>

</html>
