<?php


function enqueue_styles_scripts(){
    
   // stylesheets
    
    //Bootstrap
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    //Font Awesome
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
    //Custom
    wp_enqueue_style( 'style', get_template_directory_uri() . '/css/custom-style.min.css'); 
    
    
  //scripts
    
    wp_deregister_script('jquery');
    //JQuery
	wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/js/min/jquery-1.11.1.min.js', true, null, true);
    //Modernizer
    wp_enqueue_script('modernizer', get_stylesheet_directory_uri() . '/js/modernizr.custom.js', '', '', true);
    //Headroom Main
    wp_enqueue_script('headroom', get_stylesheet_directory_uri() . '/js/headroom.js', array( 'jquery' ), '', true);
    //Headroom JQuery
    wp_enqueue_script('headroom-jQuery', get_stylesheet_directory_uri() . '/js/jquery.headroom.js', array( 'jquery' ), '', true);
    //Iframe Main
    wp_enqueue_script('iframe-main', get_stylesheet_directory_uri() . '/js/iframeResizer.min.js', array( 'jquery' ), '', true);
    //Iframe Window
    wp_enqueue_script('iframe-windows', get_stylesheet_directory_uri() . '/js/iframeResizer.contentWindow.min.js', array( 'jquery' ), '', true);
    //Bootstrap
    wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri() . '/js/min/bootstrap.min.js', array( 'jquery' ), '1.0.0', true);
    //ScrollingNav
    wp_enqueue_script('scrolling-nav', get_stylesheet_directory_uri() . '/js/min/scrolling-nav-min.js', array( 'jquery' ), '1.0.0', true);
    //Easing Js
    wp_enqueue_script('jQuery-easing', get_stylesheet_directory_uri() . '/js/jquery.easing.min.js', array( 'jquery' ), '', true);
    //Custom Scripts
    wp_enqueue_script('custom', get_stylesheet_directory_uri() . '/js/custom-scripts.js', array( 'jquery', ), '', true);
    
}

add_action( 'wp_enqueue_scripts', 'enqueue_styles_scripts' );



add_theme_support( 'post-thumbnails' );
add_image_size('full-width', 1920); // full width image
add_image_size('page-header', 1400, 933, true); // home page header
add_image_size('features-block', 800, 800, true); // features block
add_image_size('half-width', 844); // half width image
add_image_size('estate-logo', 363, 96, true); // estate logo
add_image_size('estate-round', 354, 356, true); // estate round image
add_image_size('four-columns-half', 380, 135, true); // four columns half

add_image_size('twelve-columns', 1140, 270, true); // twelve columns collage
add_image_size('eleven-columns', 1045, 270, true); // eleven columns collage
add_image_size('ten-columns', 950, 270, true); // ten columns collage
add_image_size('nine-columns', 855, 270, true); // nine columns collage
add_image_size('eight-columns', 760, 270, true); // eight columns collage
add_image_size('seven-columns', 665, 270, true); // seven columns collage
add_image_size('six-columns', 570, 270, true); // six columns collage
add_image_size('five-columns', 475, 270, true); // five columns collage
add_image_size('four-columns', 380, 270, true); // four columns collage
add_image_size('three-columns', 285, 270, true); // three columns collage
add_image_size('two-columns', 190, 270, true); // two columns collage
add_image_size('one-columns', 95, 270, true); // one columns collage



// create custom post types

function estate_posttype() {
    register_post_type( 'estate',
        array(
            'labels' => array(
                'name' => __( 'Estates' ),
                'singular_name' => __( 'Estate' ),
                'add_new' => __( 'Add New Estate' ),
                'add_new_item' => __( 'Add New Estate' ),
                'edit_item' => __( 'Edit Estate' ),
                'new_item' => __( 'Add New Estate' ),
                'view_item' => __( 'View Estate' ),
                'search_items' => __( 'Search Estate' ),
                'not_found' => __( 'No Estate found' ),
                'not_found_in_trash' => __( 'No Estate found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'residence', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'estate_posttype' );

//add the page slug to body_class()
function add_slug_body_class( $classes ) {
	global $post;
		if ( isset( $post ) ) {
			$classes[] = $post->post_name;
		}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

function page_class_name($classes) {
	// add 'homepage' to the $classes array
	if( is_front_page() ) $classes[] = 'homepage';
	// return the $classes array
	return $classes;
}
add_filter('body_class','page_class_name');

// STOP THE MADNESS
function change_css_hide_wpmu_dash()
{
    echo '<style type="text/css"> .wp-admin div.error, #cpt_info_box, .term-description-wrap {display: none !important;}</style>';
}
add_action('all_admin_notices', 'change_css_hide_wpmu_dash');class WPMUDEV_Update_Notifications {}

// add .active along with .current-menu-item
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}

//Register Menu

add_theme_support('menus');


function register_theme_menus(){
    register_nav_menus(
        array(
            'primary_menu' => _('Primary Menu')
        )
    );
}

add_action('init', 'register_theme_menus');


?>
