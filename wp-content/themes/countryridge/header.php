<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title><?php wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Country Ridge Estates is a private estate community in the Calabasas area that offers a distinct style of living at the foothills of the Santa Monica Mountains." />

    <link rel="shortcut icon" href="ico/favicon.png">

    <!-- Core CSS -->
    
    <?php wp_head(); ?>

</head>

<body>

    <!-- Landing Page Content -->

    <header id="header-3" class="marg-top30">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a id="nav-toggle" class="nav-slide-btn hidden-xs pull-right" href="#"><span></span></a>
                </div>

            </div>
        </div>


        <nav class="nav-holder ">
            <div class="container-fluid ">
                <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
                    <nav class="pull">
                        <h2 class="text-center h2-headers-pages">Contact Us</h2>
                        <hr>
                        <ul class="top-nav text-center">
                            <li><a href="tel:424.221.5039" class="active">Tel:424.221.5039</a></li>
                            <li><a href="#to-iquire-now" class="page-scroll active">Inquire Below</a></li>
                            <li><a href="https://2048902603.mortgage-application.net/WebApp/Start.aspx?cm_sp=lending-_-experts-_-applynow" target="_blank">Get Qualified</a></li>
                        </ul>

                        <div class="row">
                            <div class="col-xs-6 col-xs-offset-3 pad30-bottom">
                                <a href="#" class="btn-red btn-block text-center">Download Brochure</a>
                            </div>
                        </div>

                    </nav>



                </div>
            </div>
            <!-- /.container-fluid -->
        </nav>
        <!-- /.nav -->

        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-4 col-sm-offset-4 text-center text-center">
                    <a href="<?php bloginfo('url'); ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/images/brand/nav-logo.png" alt="Country Ridge Estates logo" class="img-responive full-width">
                    </a>
                </div>

                <div class="col-xs-5 col-xs-offset-1 col-sm-1 col-sm-offset-3 text-right">
                    <div class="navicon">

                        <a class="nav-slide-btn hidden-xs" href="#"><span></span></a>


                        <div class="navbar-header">
                            <a href="tel:424.221.5039" class="active visible-xs-block tel-mobile">Tel: 424.221.5039</a>


                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                        </div>
                    </div>


                </div>
            </div>
        </div>


        <nav class="navbar pad15-bottom marg-top50">


            <div id="navbar" class="collapse navbar-collapse">
               
               <?php

                $defaults = array(
                    'container' => false,
                    'theme_location' => 'primary_menu',
                    'menu_class' => 'nav navbar-nav text-center'
                );

                wp_nav_menu($defaults);

                ?>
                <!--<ul class="nav navbar-nav text-center">
                    <li><a href="estates.html" class="active">Estates</a></li>
                    <li><a href="calabasas.html">Calabasas</a></li>
                    <li><a href="heritage.html">Heritages</a></li>
                    <li><a href="design.html">Interior Design</a></li>
                    <li><a href="contact.html">Contact</a></li>
                    <li>
                        <a href="#to-iquire-now" class="page-scroll active">Inquire Now</a>
                    </li>
                    <li class="visible-xs-block"><a href="https://2048902603.mortgage-application.net/WebApp/Start.aspx?cm_sp=lending-_-experts-_-applynow">Get Qualified Now</a></li>
                    <li class="visible-xs-block">
                        <a href="#" class="btn-red btn-block text-center">Download Brochure</a>
                    </li>
                </ul>
                -->
            </div>
            <!--/.nav-collapse -->

        </nav>

    </header>
