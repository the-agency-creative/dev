<?php get_header(); ?>

    <hr class="pad15-bottom">


    <section class=" hero flex-contain">
        <div class="container ">
            <div class="row ">
                <div class="col-sm-8 col-sm-offset-2 text-center ">
                    <div class="editContent ">
                        <h1>An Exceptional Lifestyle, an Idyllic Setting</h1>
                        <p>Country Ridge is a limited collection of only six spacious estate residences situated on expansive,gated homesites surrounded by the sun-drenched, rolling hillsides of the Santa Monica Mountains. Offering the utmost in privacy, space and serenity, semi-custom 5,500 to 7,000-square foot residences boast timeless architecture, modern style, exquisitely crafted interiors and gracious outdoor living.</p>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <hr>

    <!-- Start Intro -->
    <section id="content-intro " class="content-block-halfpad ">
        <div class="container ">
            <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 text-center ">
                <div class="col-md-6 ">
                    <a href="estates.html" class="btn btn-block page-scroll ">EXPLORE THE ESTATES</a>
                </div>
                <div class="col-md-6 ">
                    <a href="heritage.html" class="btn btn-block page-scroll ">LEARN ABOUT HERITAGE</a>
                </div>
            </div>
        </div>
    </section>
    <!-- END Intro -->

    <hr class="pad15-bottom">

    <!-- Start Content Block 2-9 -->
    <section id="content-2-9 " class="content-2-9 ">

        <div class="project-wrapper ">

            <div class="col-md-3 col-sm-6 project ">
                <div class="background-image-holder4 ">
                    <img class="background-image " alt="Background Image " src="<?php bloginfo('template_directory'); ?>/images/home-horses.jpg">
                </div>
                <div class="hover-state ">
                    <div class="align-vertical ">
                        <h3>Community</h3>
                        <hr>
                        <p>Acclaimed schools and a boundless array of activities for families to enjoy.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 project ">
                <div class="background-image-holder2 ">
                    <img class="background-image" alt="Background Image " src="<?php bloginfo('template_directory'); ?>/images/home-lemons.jpg">
                </div>
                <div class="hover-state ">
                    <div class="align-vertical ">
                        <h3>Adventure</h3>
                        <hr>
                        <p>Endless mountains, beach, parks and nature&nbsp;to&nbsp;explore.</p>
                    </div>
                </div>
            </div>


            <div class="col-md-3 col-sm-6 project ">
                <div class="background-image-holder2 ">
                    <img class="background-image " alt="Background Image " src="<?php bloginfo('template_directory'); ?>/images/project-3.jpg ">
                </div>
                <div class="hover-state ">
                    <div class="align-vertical ">
                        <h3>Entertainment</h3>
                        <hr>
                        <p>Shopping, arts and cultural attractions just minutes&nbsp;away.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 project ">
                <div class="background-image-holder1 ">
                    <img class="background-image " alt="Background Image " src="<?php bloginfo('template_directory'); ?>/images/home-food.jpg">
                </div>
                <div class="hover-state ">
                    <div class="align-vertical ">
                        <h3>Dining</h3>
                        <hr>
                        <p>An array of renowned restaurants, bistros and cafes&nbsp;nearby. </p>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.project-wrapper -->

    </section>
    <!-- // End Content Block 2-9 -->


    <!-- Start Content Block 1-5 -->
    <section class="content-1-5 content-block-halfpad ">

        <div class="container ">

            <div class="row ">
                <div class="col-xs-12 ">
                    <h2 class="h2-content-blocks ">Ideal Location, Minutes from it All</h2>
                </div>
            </div>
            <div class="row space-drawing ">

                <div class="col-md-2 col-xs-12 logo-estate ">

                    <i class="fa fa-caret-right red-arrow "></i><a href="calabasas.html" class="red-link ">Explore Calabasas</a>
                </div>
                <div class="col-md-9 col-xs-12 ">
                    <p>Country Ridge is a short drive away from the renowned community amenities of Calabasas and Malibu while providing close proximity to the very best of Los Angeles.</p>
                </div>

            </div>


        </div>
        <!-- /.container -->

    </section>
    <!-- // End Content Block 1-5 -->


    <!-- Start Content Block 1-5 -->
    <section class="content-1-5 content-block-halfpad ">

        <div class="container ">
            <div class="row ">

                <div class="col-md-12 ">
                    <img class="img-responsive full-width " src="<?php bloginfo('template_directory'); ?>/images/line-art-home-long.png ">
                </div>


            </div>

            <div class="row pad30 ">
                <div class="col-md-12 ">
                    <h2 class="headers-kepler h2-headers-pages ">Timeless Architecture</h2>
                    <hr>
                </div>
            </div>

            <div class="row space-drawing ">

                <div class="col-md-2 col-xs-12 logo-estate ">
                    <i class="fa fa-caret-right red-arrow "></i><a href="calabasas.html" class="red-link ">Explore Calabassas</a>
                    <br>
                    <i class="fa fa-caret-right red-arrow pad15 "></i><a href="design.html" class="red-link ">Interior Design</a>
                </div>
                <div class="col-md-9 col-xs-12 ">
                    <p>Residences at Country Ridge are as elegant as they are timeless, featuring architecture inspired by renowned California architect Wallace Neff. Semi-custom 5,500 to 7,000-square-foot homes evoke classic, Spanish Hacienda style that is organic to the Calabasas area, while interiors feature flexible, open floor plans designed for the modern lifestyle. High ceilings soar above grand, inviting interior living spaces while floor-to-ceiling windows capture abundant natural light and offer magnificent views of the property and the surrounding hillsides.</p>
                </div>

            </div>


        </div>
        <!-- /.container -->

    </section>
    <!-- // End Content Block 1-5 -->


    <?php get_footer(); ?>
