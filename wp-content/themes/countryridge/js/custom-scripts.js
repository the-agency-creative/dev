/* -------- Header Nav -------- */
$(window).load(function () {

    $('.nav-slide-btn').click(function () {
        $('.pull').slideToggle();
    });

});

document.querySelector("#nav-toggle").addEventListener("click", function () {
    this.classList.toggle("active");
});

$(".nav li a[href^='#']").on('click', function (event) {
    var target;
    target = this.hash;

    event.preventDefault();

    var navOffset;
    navOffset = $('#navbar').height();

    return $('html, body').animate({
        scrollTop: $(this.hash).offset().top - navOffset
    }, 300, function () {
        return window.history.pushState(null, null, target);
    });
});

//Page Scroll
$(function () {
    $('a.page-scroll').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

//Typkit

(function (d) {
    var config = {
            kitId: 'ecu0wmb',
            scriptTimeout: 3000,
            async: true
        },
        h = d.documentElement,
        t = setTimeout(function () {
            h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
        }, config.scriptTimeout),
        tk = d.createElement("script"),
        f = false,
        s = d.getElementsByTagName("script")[0],
        a;
    h.className += " wf-loading";
    tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
    tk.async = true;
    tk.onload = tk.onreadystatechange = function () {
        a = this.readyState;
        if (f || a && a != "complete" && a != "loaded") return;
        f = true;
        clearTimeout(t);
        try {
            Typekit.load(config)
        } catch (e) {}
    };
    s.parentNode.insertBefore(tk, s)
})(document);

//Log Iframe
iFrameResize({
    log: true
});

//Init Iframe
$(function () {
    $('iframe').iFrameResize();
});
