<?php
/*
Template Name: Calabasas
*/
?>

    <?php get_header(); ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


            <!-- Start Intro -->
            <section id="content-intro" class="content-block-halfpad">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 text-center">
                            <h2 class="headers-kepler h2-headers-pages"><?php the_field('cr_cal_int_headline'); ?></h2>
                            <hr>
                            <p>
                                <?php the_field('cr_cal_int_copy'); ?>
                            </p>

                        </div>
                    </div>
                </div>
            </section>


            <!-- Start Content Block 1-5 -->
            <section class="content-1-5-no-margin content-block-nopad">

                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                                    <?php
                                        $calabasas_map_obj = get_field('cr_calabasas_full_width_map');
                                        $calabasas_map_size = 'full-width';
                                        $calabasas_map_url = $calabasas_map_obj['sizes'][$calabasas_map_size];
                                        $calabasas_map_title = $calabasas_map_obj['title'];
                                        $calabasas_map_alt = $calabasas_map_obj['alt'];
                                        $calabasas_map_height = $calabasas_map_obj['height'];
                                        $calabasas_map_width = $calabasas_map_obj['width'];
                                     ?>
                                    <picture>
                                        <!--[if IE 9]><video style="display: none;"><![endif]-->
                                        <source srcset="<?php echo $calabasas_map_url; ?>">
                                        <!--[if IE 9]></video><![endif]-->
                                        <img class="img-responsive full-width" src="<?php echo $calabasas_map_url; ?>" alt="<?php echo $calabasas_map_alt; ?>">
                                    </picture>

                        </div>
                    </div>

                </div>
                <!-- /.container -->

            </section>
            <!-- // End Content Block 1-5 -->


            <hr>


            <!-- Start Content Block 1-5 -->
            <section class="content-1-5-no-margin content-block">

                <div class="container">
                    <div class="row">

                        <div class="col-sm-6">
                            <h2 class="headers-kepler h2-headers-pages"><?php the_field('cr_cal_dine_headline'); ?></h2>
                            <hr>
                            <p>
                                <?php the_field('cr_cal_dine_copy'); ?>
                            </p>
                        </div>

                        <div class="col-sm-6">
                            <?php
                                        $calabasas_dining_obj = get_field('cr_cal_dine_image');
                                        $calabasas_dining_size = 'half-width';
                                        $calabasas_dining_url = $calabasas_dining_obj['sizes'][$calabasas_dining_size];
                                        $calabasas_dining_title = $calabasas_dining_obj['title'];
                                        $calabasas_dining_alt = $calabasas_dining_obj['alt'];
                                        $calabasas_dining_height = $calabasas_dining_obj['height'];
                                        $calabasas_dining_width = $calabasas_dining_obj['width'];
                                     ?>
                                <picture>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $calabasas_dining_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img class="img-responsive full-width" src="<?php echo $calabasas_dining_url; ?>" alt="<?php echo $calabasas_dining_alt; ?>">
                                </picture>
                        </div>

                    </div>

                </div>
                <!-- /.container -->

            </section>
            <!-- // End Content Block 1-5 -->

            <hr>


            <section class="image-collage pad45">
                <div class="container">
                    <div class="row">

                        <?php
								// check if the repeater field has rows of data
								if( have_rows('cr_cal_img_row') ):
								 	// loop through the rows of data
								    while ( have_rows('cr_cal_img_row') ) : the_row();
								?>

                            <?php  
                                        $image_width_selected = get_sub_field('cr_cal_img_wide');
                                        $calabasas_first_img_obj = get_sub_field('cr_cal_img_img');
                                        $calabasas_first_img_size = 'features-block';
                                        $calabasas_first_img_url = $calabasas_first_img_obj['sizes'][$calabasas_first_img_size];
                                        $calabasas_first_img_title = $calabasas_first_img_obj['title'];
                                        $calabasas_first_img_alt = $calabasas_first_img_obj['alt'];
                                        $calabasas_first_img_height = $calabasas_first_img_obj['height'];
                                        $calabasas_first_img_width = $calabasas_first_img_obj['width'];
    

                                        if($image_width_selected == 3) { ?>
                                                <div class="col-sm-3 no-pad">
                                                            <picture>
                                                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                                                <source srcset="<?php echo $calabasas_first_img_url; ?>">
                                                                <!--[if IE 9]></video><![endif]-->
                                                                <img class="img-responsive full-width" src="<?php echo $calabasas_first_img_url; ?>" alt="<?php echo $calabasas_first_img_alt; ?>">
                                                            </picture>
                                                </div>
                                    <?php } else if($image_width_selected == 4) { ?>
                                               
                                               <div class="col-sm-4 no-pad">
                                                            <picture>
                                                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                                                <source srcset="<?php echo $calabasas_first_img_url; ?>">
                                                                <!--[if IE 9]></video><![endif]-->
                                                                <img class="img-responsive full-width" src="<?php echo $calabasas_first_img_url; ?>" alt="<?php echo $calabasas_first_img_alt; ?>">
                                                            </picture>
                                                </div>

                                               
                                    <?php } else { ?>
                                                <div class="col-sm-6 no-pad">
                                                            <picture>
                                                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                                                <source srcset="<?php echo $calabasas_first_img_url; ?>">
                                                                <!--[if IE 9]></video><![endif]-->
                                                                <img class="img-responsive full-width" src="<?php echo $calabasas_first_img_url; ?>" alt="<?php echo $calabasas_first_img_alt; ?>">
                                                            </picture>
                                                </div>
                                    
                                    <?php } ?>                                                                  
                                                <?php
								    endwhile;
								else :
								    // no features found
								endif;
								?>
                        </div>
                    </div>
            </section>
            <!-- /.container -->



            <!-- Start Content Block 1-5 -->
            <section class="content-1-5 content-block">

                <div class="container">
                    <div class="row space-drawing">

                        <div class="col-md-3 col-xs-12 logo-estate">
                            <h2 class="headers-kepler h2-headers-pages margin-none"><?php the_field('cr_cal_head_text'); ?></h2>
                            <hr>
                        </div>
                        <div class="col-md-9 col-xs-12">
                            <p><?php the_field('cr_cal_copy_sec'); ?></p>
                        </div>

                    </div>


                </div>
                <!-- /.container -->

            </section>
            <!-- // End Content Block 1-5 -->

            <!-- Start Content Block 2-10 -->
            <section id="cta-bar" class="cta-bar">
                <div class="container-fluid border-box-nopad">
                    <div class="container">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">
                            <p><?php the_field('cr_cal_quo_text'); ?></p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // End Content Block 2-10 -->



            <!-- Start Content Block 1-5 -->
            <section class="content-1-5-no-margin content-block">

                <div class="container">
                    <div class="row flex-contain-footer">


                        <div class="col-sm-6">
                           
                           <?php
                                        $calabasas_comm_obj = get_field('cr_cal_com_img');
                                        $calabasas_comm_size = 'half-width';
                                        $calabasas_comm_url = $calabasas_comm_obj['sizes'][$calabasas_comm_size];
                                        $calabasas_comm_title = $calabasas_comm_obj['title'];
                                        $calabasas_comm_alt = $calabasas_comm_obj['alt'];
                                        $calabasas_comm_height = $calabasas_comm_obj['height'];
                                        $calabasas_comm_width = $calabasas_comm_obj['width'];
                                     ?>
                                <picture>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $calabasas_comm_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img class="img-responsive full-width" src="<?php echo $calabasas_comm_url; ?>" alt="<?php echo $calabasas_comm_alt; ?>">
                                </picture>
                           
                        </div>


                        <div class="col-sm-6 ">
                            <div class="">

                                <h2 class="headers-kepler h2-headers-pages text-left"><?php the_field('cr_cal_com_headline'); ?></h2>
                                <hr>
                                <p class="no-pad"><?php the_field('cr_calabasas_community_copy'); ?></p>
                            </div>
                        </div>


                    </div>

                </div>
                <!-- /.container -->

            </section>
            <!-- // End Content Block 1-5 -->

            <hr>


            <section class="image-collage pad75">
                <div class="container">
                   <div class="row">

                        <?php
								// check if the repeater field has rows of data
								if( have_rows('cr_cal_img_row_second') ):
								 	// loop through the rows of data
								    while ( have_rows('cr_cal_img_row_second') ) : the_row();
								?>

                            <?php  
                                        $image_width_selected_sec = get_sub_field('cr_calabasas_sec_wide');
                                        $calabasas_second_img_obj = get_sub_field('cr_cala_sec_image');
                                        $calabasas_second_img_size = 'features-block';
                                        $calabasas_second_img_url = $calabasas_second_img_obj['sizes'][$calabasas_second_img_size];
                                        $calabasas_second_img_title = $calabasas_second_img_obj['title'];
                                        $calabasas_second_img_alt = $calabasas_second_img_obj['alt'];
                                        $calabasas_second_img_height = $calabasas_second_img_obj['height'];
                                        $calabasas_second_img_width = $calabasas_second_img_obj['width'];

                                        if($image_width_selected_sec == 3) { ?>
                                                <div class="col-sm-3 no-pad">
                                                            <picture>
                                                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                                                <source srcset="<?php echo $calabasas_second_img_url; ?>">
                                                                <!--[if IE 9]></video><![endif]-->
                                                                <img class="img-responsive full-width" src="<?php echo $calabasas_second_img_url; ?>" alt="<?php echo $calabasas_second_img_alt; ?>">
                                                            </picture>
                                                </div>
                                    <?php } else if($image_width_selected_sec == 4) { ?>
                                               
                                               <div class="col-sm-4 no-pad">
                                                             <picture>
                                                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                                                <source srcset="<?php echo $calabasas_second_img_url; ?>">
                                                                <!--[if IE 9]></video><![endif]-->
                                                                <img class="img-responsive full-width" src="<?php echo $calabasas_second_img_url; ?>" alt="<?php echo $calabasas_second_img_alt; ?>">
                                                            </picture>
                                                </div>

                                               
                                    <?php } else { ?>
                                                <div class="col-sm-6 no-pad">
                                                                    <picture>
                                                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                                                <source srcset="<?php echo $calabasas_second_img_url; ?>">
                                                                <!--[if IE 9]></video><![endif]-->
                                                                <img class="img-responsive full-width" src="<?php echo $calabasas_second_img_url; ?>" alt="<?php echo $calabasas_second_img_alt; ?>">
                                                            </picture>
                                                </div>
                                    
                                    <?php } ?>                                                                  
                                                <?php
								    endwhile;
								else :
								    // no features found
								endif;
								?>
                        </div>
                </div>
            </section>
            <!-- /.container -->

            <!-- Start Content Block 1-5 -->
            <section class="content-1-5 content-block-halfpad">

                <div class="container">
                    <div class="row space-drawing">

                        <div class="col-md-3 col-xs-12 logo-estate">
                            <h2 class="headers-kepler h2-headers-pages margin-none"><?php the_field('cr_cal_adv_head'); ?></h2>
                            <hr>
                        </div>
                        <div class="col-md-9 col-xs-12">
                            <p><?php the_field('cr_cal_adv_copy'); ?></p>
                        </div>

                    </div>


                </div>
                <!-- /.container -->

            </section>
            <!-- // End Content Block 1-5 -->

            <hr>


            <?php endwhile; else: ?>
                <p>
                    <?php _e('Sorry, no posts matched your criteria.'); ?>
                </p>
                <?php endif; ?>
                    <?php get_footer(); ?>
