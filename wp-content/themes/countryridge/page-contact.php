<?php
/*
Template Name: Contact
*/
?>

    <?php get_header(); ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


            <!-- Start Content Block 1-5 -->
            <section class="content-1-5-no-margin content-block-nopad">

                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                              <?php
                                        $contact_hero_obj = get_field('cr_co_hero');
                                        $contact_hero_size = 'full-width';
                                        $contact_hero_url = $contact_hero_obj['sizes'][$contact_hero_size];
                                        $contact_hero_title = $contact_hero_obj['title'];
                                        $contact_hero_alt = $contact_hero_obj['alt'];
                                        $contact_hero_height = $contact_hero_obj['height'];
                                        $contact_hero_width = $contact_hero_obj['width'];
                                     ?>
                                <picture>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $contact_hero_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img class="img-responsive full-width" src="<?php echo $contact_hero_url; ?>" alt="<?php echo $contact_hero_alt; ?>">
                                </picture>
                        </div>
                    </div>

                </div>
                <!-- /.container -->

            </section>
            <!-- // End Content Block 1-5 -->

            <!-- Start Intro -->
            <section id="content-intro" class="content-block-halfpad">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <h2 class="h2-headers-pages margin-small"><?php the_field('cr_co_headline'); ?></h2>


                            <div class="row">
                               
                               <?php the_field('cr_co_info'); ?>
                                
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-sm-offset-3 element-spacing-top">

                            <a href="<?php the_field('br_co_bro_file'); ?>" class="btn btn-block"><?php the_field('cr_co_bro_text'); ?></a>

                        </div>
                    </div>
                </div>
            </section>



            <!-- Start Content Block 2-10 -->
            <section id="cta-bar" class="cta-bar">
                <div class="container-fluid border-box-nopad">
                    <div class="container">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">
                           <?php the_field('cont_quote'); ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // End Content Block 2-10 -->



            <!-- Start Content Block 1-5 -->
            <section class="content-1-5-no-margin content-block">

                <div class="container">
                    <div class="row">

                        <div class="col-md-12">
                           
                            <?php
                                        $contact_foot_obj = get_field('cont_foot_map');
                                        $contact_foot_size = 'full-width';
                                        $contact_foot_url = $contact_foot_obj['sizes'][$contact_foot_size];
                                        $contact_foot_title = $contact_foot_obj['title'];
                                        $contact_foot_alt = $contact_foot_obj['alt'];
                                        $contact_foot_height = $contact_foot_obj['height'];
                                        $contact_foot_width = $contact_foot_obj['width'];
                                     ?>
                                <picture>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $contact_foot_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img class="img-responsive full-width" src="<?php echo $contact_foot_url; ?>" alt="<?php echo $contact_foot_alt; ?>">
                                </picture>
                        </div>
                    </div>

                </div>
                <!-- /.container -->

            </section>
            <!-- // End Content Block 1-5 -->



            <?php endwhile; else: ?>
                <p>
                    <?php _e('Sorry, no posts matched your criteria.'); ?>
                </p>
                <?php endif; ?>
                    <?php get_footer(); ?>
