<?php
/*
Template Name: Design
*/
?>

    <?php get_header(); ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <!-- Start Design Intro -->
            <section id="content-intro" class="content-block-halfpad">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 text-center">
                            <h2 class="headers-kepler h2-headers-pages"><?php the_field('cr-de-head'); ?></h2>
                            <hr>
                           <?php the_field('cr-de-copy'); ?>
                        </div>
                    </div>
                </div>
            </section>
            
            
                          <?php
								// check if the repeater field has rows of data
								if( have_rows('cr_de_feature') ):
								 	// loop through the rows of data
								    while ( have_rows('cr_de_feature') ) : the_row();
								?>

            <section class="content-block-halfpad pad45">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="headers-kepler h2-headers-pages"><?php the_sub_field('cr_de_feat_head'); ?></h2>
                            <hr>
                            <p><?php the_sub_field('cr_de_feat_copy'); ?></p>
                            <i class="fa fa-caret-right red-arrow"></i><a href="<?php the_sub_field('cr_de_feat_link_url'); ?>" class="red-link"><?php the_sub_field('cr_de_feat_link_text'); ?></a>
                        </div>
                    </div>
                </div>
            </section>


            <section class="image-collage">
                <div class="container">
                    <div class="row">
                       
                        <?php
								// check if the repeater field has rows of data
								if( have_rows('cr_de_feat_collage') ):
								 	// loop through the rows of data
								    while ( have_rows('cr_de_feat_collage') ) : the_row();
                        ?>
                       
                        <?php 

                            $design_image_width = get_sub_field('cr_de_feat_coll_width'); 
                           

                            if($design_image_width == 2){ 
                                $col_size_sm = 'col-sm-2 no-pad';
                                $design_collage_size = 'two-columns';
                                $design_case = 1;
                                
                            } else if($design_image_width == 3){
                                $col_size_sm = 'col-sm-3 no-pad';
                                $design_collage_size = 'three-columns';
                                $design_case = 1;
                            } else if($design_image_width == 8){
                                $col_size_sm = 'col-sm-8 no-pad';
                                $design_collage_size = 'eight-columns';
                                $design_case = 1;
                            } else if($design_image_width == 6){
                                $col_size_sm = 'col-sm-6 no-pad';
                                $design_collage_size = 'six-columns';
                                $design_case = 1;
                            }
                            else {
                                
                                $design_images_amount = get_sub_field('cr_de_feat_coll_amount');
                                
                                if($design_images_amount == 1){
                                
                                    $col_size_sm = 'col-sm-4 no-pad';
                                    $design_collage_size = 'four-columns';
                                    $design_case = 2;
                                    
                                } else{
                                    $col_size_sm = 'col-sm-4 no-pad';
                                    $design_collage_size = 'four-columns-half';
                                    $design_case = 3;
                                }
                            }

                            if($design_case == 1){

                                $design_collage_obj = get_sub_field('cr_de_feat_collage');
                                $design_collage_url = $design_collage_obj['sizes'][$design_collage_size];
                                $design_collage_title = $design_collage_obj['title'];
                                $design_collage_alt = $design_collage_obj['alt'];
                                $design_collage_height = $design_collage_obj['height'];
                                $design_collage_width = $design_collage_obj['width'];
                            } else if($design_case == 2){
                                $design_collage_obj = get_sub_field('cr_de_feat_coll_third');
                                $design_collage_url = $design_collage_obj['sizes'][$design_collage_size];
                                $design_collage_title = $design_collage_obj['title'];
                                $design_collage_alt = $design_collage_obj['alt'];
                                $design_collage_height = $design_collage_obj['height'];
                                $design_collage_width = $design_collage_obj['width'];
                            
                            } else{
                                $design_collage1_obj = get_sub_field('cr_de_feat_coll_first');
                                $design_collage1_url = $design_collage1_obj['sizes'][$design_collage_size];
                                $design_collage1_title = $design_collage1_obj['title'];
                                $design_collage1_alt = $design_collage1_obj['alt'];
                                $design_collage1_height = $design_collage1_obj['height'];
                                $design_collage1_width = $design_collage1_obj['width'];
                                
                                $design_collage2_obj = get_sub_field('cr_de_feat_coll_second');
                                $design_collage2_url = $design_collage2_obj['sizes'][$design_collage_size];
                                $design_collage2_title = $design_collage2_obj['title'];
                                $design_collage2_alt = $design_collage2_obj['alt'];
                                $design_collage2_height = $design_collage2_obj['height'];
                                $design_collage2_width = $design_collage2_obj['width'];
                            
                            }
                        ?>
                          
                        <?php if($design_case == 1 || $design_case == 2){ ?>
                           
                        <div class="<?php echo $col_size_sm; ?>">
                                <picture>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $design_collage_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img class="img-responsive full-width" src="<?php echo $design_collage_url; ?>" alt="<?php echo $design_collage_alt; ?>">
                                </picture>
                        </div>
                        
                      <?php }  else { ?>
                       
                        <div class="<?php echo $col_size_sm; ?>">
                                <picture>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $design_collage1_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img class="img-responsive full-width" src="<?php echo $design_collage1_url; ?>" alt="<?php echo $design_collage1_alt; ?>">
                                </picture>
                                
                                
                                <picture>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $design_collage2_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img class="img-responsive full-width" src="<?php echo $design_collage2_url; ?>" alt="<?php echo $design_collage2_alt; ?>">
                                </picture>
                        </div>
                       
                    <?php } ?>     
                        
                         <?php
				            endwhile;
                            else :
                           // no features found
                            endif;
                         ?>
                    
                    </div>
                </div>
            </section>
            
            
            
             <?php
				endwhile;
					else :
				   // no features found
					endif;
				?>
                        


            <!-- Start Content Block 2-10 -->
            <section id="cta-bar" class="cta-bar pad60">
                <div class="container-fluid border-box-nopad">
                    <div class="container">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">
                           <?php the_field('cr_des_quo'); ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // End Content Block 2-10 -->



            <?php endwhile; else: ?>
                <p>
                    <?php _e('Sorry, no posts matched your criteria.'); ?>
                </p>
                <?php endif; ?>
                    <?php get_footer(); ?>
