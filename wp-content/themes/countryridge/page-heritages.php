<?php
/*
Template Name: Heritages
*/
?>

    <?php get_header(); ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


            <!-- Start Design Intro -->
            <section id="content-intro" class="content-block-halfpad">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 text-center">
                            <h2 class="headers-kepler h2-headers-pages"><?php the_field('cr_her_int_head'); ?></h2>
                            <hr>
                            <p><?php the_field('cr_her_int_copy'); ?></p>
                        </div>
                    </div>
                </div>
            </section>


            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                           
                             <?php
                                        $heritages_her_obj = get_field('cr_her_hero_image');
                                        $heritages_her_size = 'full-width';
                                        $heritages_her_url = $heritages_her_obj['sizes'][$heritages_her_size];
                                        $heritages_her_title = $heritages_her_obj['title'];
                                        $heritages_her_alt = $heritages_her_obj['alt'];
                                        $heritages_her_height = $heritages_her_obj['height'];
                                        $heritages_her_width = $heritages_her_obj['width'];
                                     ?>
                                <picture>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $heritages_her_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img class="img-responsive full-width" src="<?php echo $heritages_her_url; ?>" alt="<?php echo $heritages_her_alt; ?>">
                                </picture>
                           

                        </div>
                    </div>
                </div>
            </section>


            <section class="content-block-halfpad pad60">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                           
                           
                            <?php
								// check if the repeater field has rows of data
								if( have_rows('cr_her_estates') ):
								 	// loop through the rows of data
								    while ( have_rows('cr_her_estates') ) : the_row();
								?>
                           
                            <div class="col-md-4 col-sm-6 estate-entry">
                                  <?php
                                        $heritages_estate_obj = get_sub_field('cr_her_est_thumb');
                                        $heritages_estate_size = 'estate-round';
                                        $heritages_estate_url = $heritages_estate_obj['sizes'][$heritages_estate_size];
                                        $heritages_estate_title = $heritages_estate_obj['title'];
                                        $heritages_estate_alt = $heritages_estate_obj['alt'];
                                        $heritages_estate_height = $heritages_estate_obj['height'];
                                        $heritages_estate_width = $heritages_estate_obj['width'];
                                     ?>
                                
                                <a href="<?php the_sub_field('cr_her_est_link_url'); ?>">         
                                <picture>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $heritages_estate_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img class="img-responsive full-width" src="<?php echo $heritages_estate_url; ?>" alt="<?php echo $heritages_estate_alt; ?>">
                                </picture>
                                </a>
                                
                                <div class="content-estates">
                                    <h3 class="headline-estate headers-kepler"><?php the_sub_field('cr_her_est_name'); ?></h3>
                                    <hr>
                                <?php the_sub_field('cr_her_est_small_description'); ?>
                                </div>
                                <i class="fa fa-caret-right red-arrow"></i><a href="<?php the_sub_field('cr_her_est_link_url'); ?>" class="red-link"><?php the_sub_field('cr_her_est_link_text'); ?></a>
                            </div>


                         <?php
								    endwhile;
								else :
								    // no features found
								endif;
								?>
                        
                        </div>
                    </div>
                </div>
            </section>



            <!-- Start Content Block 2-10 -->
            <section id="cta-bar" class="cta-bar">
                <div class="container-fluid border-box-nopad">
                    <div class="container">
                        <div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">
                            <p><?php the_field('cr_her_quote_text'); ?></p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- // End Content Block 2-10 -->




            <?php endwhile; else: ?>
                <p>
                    <?php _e('Sorry, no posts matched your criteria.'); ?>
                </p>
                <?php endif; ?>
                    <?php get_footer(); ?>
