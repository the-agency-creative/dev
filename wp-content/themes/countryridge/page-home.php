<?php
/*
Template Name: Homepage
*/
?>

    <?php get_header(); ?>
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <hr class="pad15-bottom">


            <?php the_content(); ?>

                <section class=" hero flex-contain">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2 text-center ">
                                <div class="editContent ">
                                    <h1><?php the_field('cr_hero_headline'); ?></h1>
                                    <p>
                                        <?php the_field('cr_hero_copy'); ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>


                <hr>

                <!-- Start Intro -->
                <section id="content-intro " class="content-block-halfpad ">
                    <div class="container ">
                        <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 text-center ">
                            <div class="col-md-6 ">
                                <a href="<?php the_field('button_home_left_link'); ?>" class="btn btn-block page-scroll ">
                                    <?php the_field('cr_button_home_left'); ?>
                                </a>
                            </div>
                            <div class="col-md-6 ">
                                <a href="<?php the_field('button_home_right_link'); ?>" class="btn btn-block page-scroll ">
                                    <?php the_field('cr_button_home_right'); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- END Intro -->

                <hr class="pad15-bottom">

                <!-- Start Content Block 2-9 -->
                <section id="content-2-9 " class="content-2-9 ">

                    <div class="project-wrapper ">

                        <?php
								// check if the repeater field has rows of data
								if( have_rows('cr_four_images_row') ):
								 	// loop through the rows of data
								    while ( have_rows('cr_four_images_row') ) : the_row();
								?>

                            <div class="col-md-3 col-sm-6 project ">
                                <div class="background-image-holder4 ">
                                    <?php
                                        $feature_image_obj = get_sub_field('cr_feature_image');
                                        $feature_image_size = 'features-block';
                                        $feature_image_url = $feature_image_obj['sizes'][$feature_image_size];
                                        $feature_image_title = $feature_image_obj['title'];
                                        $feature_image_alt = $feature_image_obj['alt'];
                                        $feature_image_height = $feature_image_obj['height'];
                                        $feature_image_width = $feature_image_obj['width'];
                                     ?>
                                        <picture>
                                            <!--[if IE 9]><video style="display: none;"><![endif]-->
                                            <source srcset="<?php echo $feature_image_url; ?>">
                                            <!--[if IE 9]></video><![endif]-->
                                            <img class="background-image" src="<?php echo $feature_image_url; ?>" alt="<?php echo $feature_image_alt; ?>">
                                        </picture>

                                </div>
                                <div class="hover-state ">
                                    <div class="align-vertical ">
                                        <h3><?php the_sub_field('cr_feature_headline'); ?></h3>
                                        <hr>
                                        <p>
                                            <?php the_sub_field('cr_feature_description'); ?></p>
                                    </div>
                                </div>
                            </div>

                            <?php
								    endwhile;
								else :
								    // no features found
								endif;
								?>

                    </div>
                    <!-- /.project-wrapper -->

                </section>
                <!-- // End Content Block 2-9 -->


                <!-- Start Content Block 1-5 -->
                <section class="content-1-5 content-block-halfpad">

                    <div class="container">
                        
                         <div class="row pad30 ">
                            <div class="col-md-12 ">
                                <h2 class="headers-kepler h2-headers-pages "><?php the_field('cr_location_headline'); ?></h2>
                                <hr>
                            </div>
                        </div>
                        
                        
                        <div class="row space-drawing ">

                            <div class="col-md-2 col-xs-12 logo-estate ">
                               <?php
								// check if the repeater field has rows of data
								if( have_rows('cr_location_links') ):
								 	// loop through the rows of data
								    while ( have_rows('cr_location_links') ) : the_row();
								?>
                                    <i class="fa fa-caret-right red-arrow"></i><a href="<?php the_sub_field('cr_location_url'); ?>" class="red-link pad15-bottom"><?php the_sub_field('cr_location_link_text'); ?></a>

                                    <?php
								    endwhile;
								else :
								    // no features found
								endif;
								?>
                            </div>
                            <div class="col-md-9 col-xs-12 ">
                                <p><?php the_field('cr_location_copy'); ?></p>
                            </div>

                        </div>


                    </div>
                    <!-- /.container -->

                </section>
                <!-- // End Content Block 1-5 -->


                <!-- Start Content Block 1-5 -->
                <section class="content-1-5 content-block-halfpad ">

                    <div class="container ">
                        <div class="row ">

                            <div class="col-md-12 ">
                               
                               <?php
                                        $full_location_obj = get_field('cr_full_width_drawing');
                                        $full_location_size = 'full-width';
                                        $full_location_url = $full_location_obj['sizes'][$full_location_size];
                                        $full_location_title = $full_location_obj['title'];
                                        $full_location_alt = $full_location_obj['alt'];
                                        $full_location_height = $full_location_obj['height'];
                                        $full_location_width = $full_location_obj['width'];
                                     ?>
                                        <picture>
                                            <!--[if IE 9]><video style="display: none;"><![endif]-->
                                            <source srcset="<?php echo $full_location_url; ?>">
                                            <!--[if IE 9]></video><![endif]-->
                                            <img class="img-responsive full-width" src="<?php echo $full_location_url; ?>" alt="<?php echo $full_location_alt; ?>">
                                        </picture>      
                            </div>


                        </div>

                        <div class="row pad30 ">
                            <div class="col-md-12 ">
                                <h2 class="headers-kepler h2-headers-pages "><?php the_field('cr_headline_architecture '); ?></h2>
                                <hr>
                            </div>
                        </div>

                        <div class="row space-drawing ">

                            <div class="col-md-2 col-xs-12 logo-estate ">
                               <?php
								// check if the repeater field has rows of data
								if( have_rows('cr_architecture_links_copy') ):
								 	// loop through the rows of data
								    while ( have_rows('cr_architecture_links_copy') ) : the_row();
								?>
                                    <i class="fa fa-caret-right red-arrow"></i><a href="<?php the_sub_field('cr_architecture_url'); ?>" class="red-link pad15-bottom"><?php the_sub_field('cr_architecture_link_text'); ?></a>

                                    <?php
								    endwhile;
								else :
								    // no features found
								endif;
								?>
                            </div>
                            <div class="col-md-9 col-xs-12 ">
                                <p><?php the_field('cr_arch_copy'); ?></p>
                            </div>

                        </div>


                    </div>
                    <!-- /.container -->

                </section>
                <!-- // End Content Block 1-5 -->

                <?php endwhile; else: ?>
                    <p>
                        <?php _e('Sorry, no posts matched your criteria.'); ?>
                    </p>
                    <?php endif; ?>
                        <?php get_footer(); ?>
