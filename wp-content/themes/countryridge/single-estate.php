<?php get_header(); ?>

    <!-- Start Content Block 1-5 -->
    <section class="content-1-5-no-margin content-block-nopad">

        <div class="container">
            <div class="row">

                <div class="col-md-12 text-center">

                    <?php
                                        $estates_logo_obj = get_field('cr_single_estate_logo');
                                        $estates_logo_size = 'estate-logo';
                                        $estates_logo_url = $estates_logo_obj['sizes'][$estates_logo_size];
                                        $estates_logo_title = $estates_logo_obj['title'];
                                        $estates_logo_alt = $estates_logo_obj['alt'];
                                        $estates_logo_height = $estates_logo_obj['height'];
                                        $estates_logo_width = $estates_logo_obj['width'];
                                     ?>
                        <picture>
                            <!--[if IE 9]><video style="display: none;"><![endif]-->
                            <source srcset="<?php echo $estates_logo_url; ?>">
                            <!--[if IE 9]></video><![endif]-->
                            <img class="estate-header-logo" src="<?php echo $estates_logo_url; ?>" alt="<?php echo $estates_logo_alt; ?>">
                        </picture>



                        <?php
                                        $estates_main_obj = get_field('cr_single_estate_main_image');
                                        $estates_main_size = 'full-width';
                                        $estates_main_url = $estates_main_obj['sizes'][$estates_main_size];
                                        $estates_main_title = $estates_main_obj['title'];
                                        $estates_main_alt = $estates_main_obj['alt'];
                                        $estates_main_height = $estates_main_obj['height'];
                                        $estates_main_width = $estates_main_obj['width'];
                                     ?>
                            <picture>
                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                <source srcset="<?php echo $estates_main_url; ?>">
                                <!--[if IE 9]></video><![endif]-->
                                <img class="img-responsive full-width" src="<?php echo $estates_main_url; ?>" alt="<?php echo $estates_main_alt; ?>">
                            </picture>


                </div>
            </div>

        </div>
        <!-- /.container -->

    </section>
    <!-- // End Content Block 1-5 -->


    <!-- Start Intro -->
    <section id="content-intro" class="content-block-halfpad">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 text-center">
                    <h2 class="headers-kepler h2-headers-pages margin-small"><?php the_field('cr_single_estate_name') ?></h2>
                    <p class="margin-small">
                        <?php the_field('cr_single_estate_square_footage') ?> Sq. Ft. |
                            <?php the_field('cr_single_estate_acreage') ?> Acres |
                                <?php the_field('cr_single_estate_bedrooms') ?> Bedrooms |
                                    <?php the_field('cr_est_baths') ?> Baths</p>
                    <hr>
                    <?php the_field('cr_single_estate_copy') ?>


                        <div class="row">
                            <ul class="property-details-list">
                                <?php
								// check if the repeater field has rows of data
								if( have_rows('cr_single_estate_features') ):
								 	// loop through the rows of data
								    while ( have_rows('cr_single_estate_features') ) : the_row();
								?>
                                    <div class="col-sm-4 text-center pad5">
                                        <li>
                                            <?php the_sub_field('cr_sin_est_feat_item'); ?>
                                        </li>
                                    </div>
                                    <?php
								    endwhile;
								else :
								    // no features found
								endif;
								?>
                            </ul>
                        </div>

                        <div class="col-sm-6 col-sm-offset-3 text-center pad30">
                            <a href="<?php the_field('cr_single_estate_download_pdf') ?>" class="btn-red btn-block">
                                <?php the_field('cr_single_estate_download_link_text') ?>
                            </a>
                        </div>

                </div>
            </div>
        </div>
    </section>

    <?php
        // check if the repeater field has rows of data
        if( have_rows('cr_single_estate_floor_plan') ):
        // loop through the rows of data
        while ( have_rows('cr_single_estate_floor_plan') ) : the_row();
        ?>

        <!-- Start Content Block 1-5 -->
        <section class="content-1-5-no-margin content-block">

            <div class="container">
                <div class="row">

                    <div class="col-xs-6">
                        <h2 class="headers-kepler h2-headers-pages"><?php the_sub_field('cr_single_floor_plan_name'); ?></h2>
                    </div>
                    
                    
                      <?php
                                        $estates_floor_obj = get_sub_field('cr_single_floor_plan_image');
                                        $estates_floor_size = 'full-width';
                                        $estates_floor_url = $estates_floor_obj['sizes'][$estates_floor_size];
                                        $estates_floor_title = $estates_floor_obj['title'];
                                        $estates_floor_alt = $estates_floor_obj['alt'];
                                        $estates_floor_height = $estates_floor_obj['height'];
                                        $estates_floor_width = $estates_floor_obj['width'];
                        ?>
                    
                    
                    <div class="col-xs-6">
                        <a href="<?php echo $estates_floor_url; ?>" class="pull-right" rel="floorplans"><i class="fa fa-plus image-cross"></i></a>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 pad25">
                       
                         <a href="<?php echo $estates_floor_url; ?>">
                         <picture>
                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                <source srcset="<?php echo $estates_floor_url; ?>">
                                <!--[if IE 9]></video><![endif]-->
                                <img class="img-responsive full-width" src="<?php echo $estates_floor_url; ?>" alt="<?php echo $estates_floor_alt; ?>">
                            </picture>
                        </a>
                        
                    </div>
                </div>

            </div>
            <!-- /.container -->

        </section>
        <!-- // End Content Block 1-5 -->


        <?php
            endwhile;
				else :
                // no features found
                endif;
				?>

            <section id="content-intro" class="content-block-halfpad">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 text-center">
                            <h2 class="headers-kepler h2-headers-pages"><?php the_field('cr_single_second_headline') ?></h2>
                            <hr>
                            <?php the_field('cr_single_second_copy_block') ?>


                                <div class="col-sm-6 col-sm-offset-3 text-center pad30">
                                    <a href="<?php the_field('cr_single_ft_link_url') ?>" class="btn-red btn-block">
                                        <?php the_field('cr_single_ft_link') ?>
                                    </a>
                                </div>

                        </div>
                    </div>
                </div>
            </section>








            <?php get_footer(); ?>
