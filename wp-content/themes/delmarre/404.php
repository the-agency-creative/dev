<?php

get_header();

?>
			<section class="container page-no-image">
				<div class="row col-area">
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
						<!-- textarea -->
						<div class="textarea">
							<header class="header text-center">
								<h1>Page Not Found</h1>
							</header>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 space-above">
						<p>We're sorry the page you have requested cannot be found or no longer exists. Please search below.</p>
						<form action="<?php bloginfo('url'); ?>" method="get" name="s" class="clearfix">
							<input name="s" type="text" placeholder="Search by neighborhood or keyword..." title="Search Properties" class="property-search-input form-control" /><br />
							<button type="submit" class="btn btn-primary pull-right">Search</button>
							<input type="hidden" name="search_type" value="property_search">
						</form>
					</div>
				</div>
			</section>
<?php get_footer(); ?>
