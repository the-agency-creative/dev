<?php get_header(); ?>
			<!-- feature block -->
			<section class="container page-no-image extra-space-above">
				<div class="row col-area">
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
						<!-- textarea -->
						<div class="textarea">
							<header class="header text-center">
								<h1>Del Mar Real Estate Sales</h1>
							</header>
						</div>
					</div>
				</div>
				<div class="row extra-space-above">
					<div class="col-md-5 col-md-offset-2 col-xs-12">
						<p>Real Estate Information:<br />
						Call <a id="telephone" href="tel:1-877-847-1662">1.877.847.1662</a>, or <a href="tel:144-5470">144-5470</a> while in Los Cabos.<br />
						You may also <a href="#contact">fill out the form below</a> and someone will be in touch.</p>
					</div>
					<div class="col-md-5 col-xs-12">
					<p>Del Mar Development<br />
					Carretera Traspeninsular km 27.5<br />
					Palmilla<br />
					San Jose del Cabo, B.C.S Mexico 23400<br />
					</p>
					</div>
				</div>
			</section>
			<div class="container agent-content">
				<div class="row col-area">
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
						<!-- textarea -->
						<div class="textarea">
							<header class="header text-center">
								<h1>The Del Mar Real Estate Team</h1>
							</header>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 space-above space-below">
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div class="row space-above space-below agent-item">
							<div class="col-md-4 col-xs-12">
								<img src="<?php the_field('dmre_agent_headshot'); ?>" class="img-responsive" />
							</div>
							<div class="col-md-8 col-xs-12 agent-meta">
								<h2><?php the_title(); ?></h2>
								<h3 class="space-below"><em><?php the_field('dmre_agent_title'); ?></em></h3>
								<p><?php the_field('dmre_agent_bio_excerpt'); ?></p>
								<ul class="list-unstyled">
									<?php if(get_field('dmre_agent_office_phone')) { ?><li>Office: <a target="_blank" href="tel:<?php the_field('dmre_agent_office_phone'); ?>"><?php the_field('dmre_agent_office_phone'); ?></a></li><?php } ?>
									<?php if(get_field('dmre_agent_mobile_phone')) { ?><li>Mobile: <a target="_blank" href="tel:<?php the_field('dmre_agent_mobile_phone'); ?>"><?php the_field('dmre_agent_mobile_phone'); ?></a></li><?php } ?>
									<?php if(get_field('dmre_guide_email')) { ?><li>Email: <a target="_blank" href="mailto:<?php the_field('dmre_agent_email'); ?>"><?php the_field('dmre_agent_email'); ?></a></li><?php } ?>
								</ul>
								<a href="<?php the_permalink(); ?>" class="btn btn-primary text-right">Read More</a>
							</div>
						</div>
						<?php endwhile; else: ?>
						<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
						<?php endif; ?>
					</div>
				</div>
			</div>
<?php get_footer(); ?>
