<?php

function property_posttype() {
    register_post_type( 'property',
        array(
            'labels' => array(
                'name' => __( 'Properties' ),
                'singular_name' => __( 'Property' ),
                'add_new' => __( 'Add New Property' ),
                'add_new_item' => __( 'Add New Property' ),
                'edit_item' => __( 'Edit Property' ),
                'new_item' => __( 'Add New Property' ),
                'view_item' => __( 'View Property' ),
                'search_items' => __( 'Search Properties' ),
                'not_found' => __( 'No Properties found' ),
                'not_found_in_trash' => __( 'No Properties found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'property', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'property_posttype' );

function amenity_posttype() {
    register_post_type( 'amenity',
        array(
            'labels' => array(
                'name' => __( 'Amenities' ),
                'singular_name' => __( 'Amenity' ),
                'add_new' => __( 'Add New Amenity' ),
                'add_new_item' => __( 'Add New Amenity' ),
                'edit_item' => __( 'Edit Amenity' ),
                'new_item' => __( 'Add New Amenity' ),
                'view_item' => __( 'View Amenity' ),
                'search_items' => __( 'Search Amenities' ),
                'not_found' => __( 'No Amenities found' ),
                'not_found_in_trash' => __( 'No Amenities found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title', 'page-attributes' ),
            'hierarchical' => true,
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'amenity', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'amenity_posttype' );

function service_posttype() {
    register_post_type( 'service',
        array(
            'labels' => array(
                'name' => __( 'Services' ),
                'singular_name' => __( 'Service' ),
                'add_new' => __( 'Add New Service' ),
                'add_new_item' => __( 'Add New Service' ),
                'edit_item' => __( 'Edit Service' ),
                'new_item' => __( 'Add New Service' ),
                'view_item' => __( 'View Service' ),
                'search_items' => __( 'Search Services' ),
                'not_found' => __( 'No Services found' ),
                'not_found_in_trash' => __( 'No Services found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'service', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'service_posttype' );

function agent_posttype() {
    register_post_type( 'agent',
        array(
            'labels' => array(
                'name' => __( 'Agents' ),
                'singular_name' => __( 'Agent' ),
                'add_new' => __( 'Add New Agent' ),
                'add_new_item' => __( 'Add New Agent' ),
                'edit_item' => __( 'Edit Agent' ),
                'new_item' => __( 'Add New Agent' ),
                'view_item' => __( 'View Agent' ),
                'search_items' => __( 'Search Agents' ),
                'not_found' => __( 'No Agents found' ),
                'not_found_in_trash' => __( 'No Agents found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'hierarchical' => true,
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'team', 'with_front' => false ),
            'has_archive' => true,
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'agent_posttype' );

// register taxonomies

add_action( 'init', 'create_property_taxonomies', 0 );

function create_property_taxonomies() {
	$hood_labels = array(
		'name'              => _x( 'Neighborhood', 'taxonomy general name' ),
		'singular_name'     => _x( 'Neighborhood', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Neighborhoods' ),
		'all_items'         => __( 'All Neighborhoods' ),
		'edit_item'         => __( 'Edit Neighborhood' ),
		'update_item'       => __( 'Update Neighborhood' ),
		'add_new_item'      => __( 'Add New Neighborhood' ),
		'new_item_name'     => __( 'New Neighborhood Name' ),
		'menu_name'         => __( 'Neighborhoods' ),
	);

	$hood_args = array(
		'hierarchical'      => true,
		'labels'            => $hood_labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'neighborhood' ),
	);

	register_taxonomy( 'neighborhood', array( 'property', 'post' ), $hood_args );

	$status_labels = array(
		'name'              => _x( 'Status', 'taxonomy general name' ),
		'singular_name'     => _x( 'Status', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Status' ),
		'all_items'         => __( 'All Status' ),
		'edit_item'         => __( 'Edit Status' ),
		'update_item'       => __( 'Update Status' ),
		'add_new_item'      => __( 'Add New Status' ),
		'new_item_name'     => __( 'New Status Name' ),
		'menu_name'         => __( 'Status' ),
	);

	$status_args = array(
		'hierarchical'      => true,
		'labels'            => $status_labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'real-estate' ),
	);

	register_taxonomy( 'status', array( 'property' ), $status_args );

}


// register multiple options pages

if( function_exists('acf_add_options_sub_page') )
{
    acf_add_options_sub_page( 'Home Page - Boxes' );
    acf_add_options_sub_page( 'Home Page - Slideshow' );
    acf_add_options_sub_page( 'Home Page - Welcome' );
    acf_add_options_sub_page( 'Footer' );
}

// register some nav menus

register_nav_menus( array(
	'real-estate-subnav' => 'Real Estate Subnav',
	'footer-nav' => 'Footer Menu',
) );


// theme image sizes

add_theme_support( 'post-thumbnails' );
add_image_size('page-header-image', 1900, 900, true);
add_image_size('page-header-mobile-image', 640, 520, true);
add_image_size('mobile-image-w', 640);
add_image_size('carousel-image', 1140, 620, true);
add_image_size('carousel-image-mobile', 640, 345, true);
add_image_size('grid-image', 355, 197, true);
add_image_size('home-tile1-image', 745, 560, true);
add_image_size('home-tile23-image', 355, 260, true);
add_image_size('home-tile4-image', 1140, 253, true);

/* register some jquery stuff */

function light_the_fires() {
	wp_deregister_script('jquery');
	wp_enqueue_script( 'jquery', '/wp-includes/js/jquery/jquery.js' , array(), null, true );
	wp_enqueue_script('bootstrap-js', 'http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js', false, null, true);
    wp_enqueue_script('jquery-custom', get_stylesheet_directory_uri() . '/js/min/jquery.main-min.js', array( 'jquery' ), '1.0.0', true);
    wp_enqueue_script('picturefill', 'https://cdnjs.cloudflare.com/ajax/libs/picturefill/2.3.0/picturefill.min.js', array( 'jquery' ), '1.0.0', true);
    wp_enqueue_script('jquery-mobile', get_stylesheet_directory_uri() . '/js/min/jquery.mobile.min.js', array( 'jquery' ), '1.0.0', true);
}
add_action( 'wp_enqueue_scripts', 'light_the_fires' );

/* and then tell them what to do */

function add_footer_scripts() { ?>

	<script type="text/javascript">
		// Picture element HTML5 shiv
		document.createElement( "picture" );
		WebFontConfig = {
		  typekit: {
			  id: 'rye6zwm'
		  },
		  google: {
		      families: ['Cinzel']
		  }
		};
		(function() {
		  var wf = document.createElement('script');
		  wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		              '://ajax.googleapis.com/ajax/libs/webfont/1.5.6/webfont.js';
		  wf.type = 'text/javascript';
		  wf.async = 'true';
		  var s = document.getElementsByTagName('script')[0];
		  s.parentNode.insertBefore(wf, s);
		})();
		jQuery(document).ready(function($) {
			 // add active class to carousel pagination
			$(".carousel-indicators li:first").addClass("active");
			$(".carousel-inner .item:first").addClass("active");
			$("#vdm-carousel").swiperight(function() {
		    	$(this).carousel('prev');
			});
			$("#vdm-carousel").swipeleft(function() {
				$(this).carousel('next');
			});
			$(window).scroll(function() {
				if ( $(window).width() > 480 ) {
				    if ($(this).scrollTop() > 10) {
				    } else {
				    }
				}
			});
		});
		(function($){
		   $(window).load(function(){
		       $("header").fadeIn("fast");
			   $(".loader").fadeOut("fast");
		   })
		})(jQuery);
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
		  var msViewportStyle = document.createElement("style")
		  msViewportStyle.appendChild(
			document.createTextNode(
			  "@-ms-viewport{width:auto!important}"
			)
		  )
		  document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
		}
	</script>
<?php }
//add to wp_footer
add_action( 'wp_footer', 'add_footer_scripts', 100 );
//add the page slug to body_class()
function add_slug_body_class( $classes ) {
	global $post;
		if ( isset( $post ) ) {
			$classes[] = $post->post_type . '-' . $post->post_name;
		}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// modify the tax queries for neighborhood & status

function vdm_tax_query ( $query ) {
	// not an admin page and is the main query
	if (!is_admin() && $query->is_main_query() && $query->is_tax()) {
		$query->set('post_type','property'); // specifying post type so just properties show
		$query->set('posts_per_page','-1');  // show all of the properties
		return $query; // have to come back here... i think?
	}
}
add_action( 'pre_get_posts', 'vdm_tax_query' );

// gives us thickbox in the front end

add_action('init', 'init_theme_method');

function init_theme_method() {
   add_thickbox();
}

// shows all search results on a single page

add_filter('post_limits', 'postsperpage');
function postsperpage($limits) {
	if (is_search()) {
		global $wp_query;
		$wp_query->query_vars['posts_per_page'] = 18;
	}
	return $limits;
}

?>
