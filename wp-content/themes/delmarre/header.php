<!doctype html>
<html>
	<head>
		<!-- set the encoding of the site -->
		<meta charset="utf-8">
		<!-- set our favicons -->
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_site_url(); ?>/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_site_url(); ?>/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_site_url(); ?>/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_site_url(); ?>/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_site_url(); ?>/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_site_url(); ?>/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_site_url(); ?>/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_site_url(); ?>/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_site_url(); ?>/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_site_url(); ?>/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_site_url(); ?>/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_site_url(); ?>/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_site_url(); ?>/favicon-16x16.png">
		<link rel="manifest" href="<?php echo get_site_url(); ?>/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<!-- set the viewport width and initial-scale on mobile devices  -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?php wp_title(''); ?></title>
		<link media="all" rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css">
		<link href="<?php echo get_site_url(); ?>//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- include the site stylesheet -->
		<link media="all" rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/custom.min.css">
		<?php wp_head(); ?>
	</head>
<body <?php body_class(); ?>>
	<div class="loader"></div>
	<!-- header of the page -->
	<header id="header" class="container-fluid">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<!-- navbar header -->
						<div class="navbar-header">
							<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<!-- page logo -->
							<div id="vdm-logo" class="logo">
								<a href="<?php echo get_site_url(); ?>/">
									<?php if(is_front_page()) : ?>
									<h1>Del Mar Real Estate - Los Cabos Luxury Real Estate</h1>
									<?php endif; ?>
									<img width="149" id="logo-large" src="<?php bloginfo('template_directory'); ?>/images/logo-dmre.png" alt="Del Mar Real Estate" class="img-responsive">
								</a>
							</div>
						</div>
						<!-- main navigation of the page -->
						<nav id="dmre-nav" class="collapse navbar-collapse">
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" id="homes-dropdown" data-toggle="dropdown">Real Estate</a>
									<ul class="dropdown-menu text-center" role="menu" aria-labelledby="homes-dropdown">
										<li>
											<span class="title text-uppercase">Neighborhoods</span>
											<ul class="list-unstyled">
												<li><a href="http://delmarre.com/neighborhood/espiritu/" title="Espiritu">Espiritu</a></li>
												<li><a href="http://delmarre.com/neighborhood/oasis-palmilla/" title="Oasis Palmilla">Oasis Palmilla</a></li>
												<li><a href="http://delmarre.com/neighborhood/palmilla/" title="Palmilla">Palmilla</a></li>
												<li><a href="http://delmarre.com/neighborhood/villas-del-mar/" title="Villas Del Mar">Villas Del Mar</a></li>
											</ul>
										</li>
										<li>
											<span class="title text-uppercase">Properties</span>
											<ul class="list-unstyled">
												<?php
												 $status = get_terms("status", array( "hide_empty" => 0 ));
												 	foreach ($status as $status) {
													 	echo '<li><a href="'.get_term_link($status).'" title="'.$status->name.'">'.$status->name.'</a></li>';
													}
												?>
											</ul>
										</li>
										<li>
											<span class="title text-uppercase">Maps</span>
											<ul class="list-unstyled">
												<!-- <li><a href="<?php echo get_site_url(); ?>/resources/maps/">Property maps</a></li> -->
												<li><a href="<?php echo get_site_url(); ?>/resources/master-plans/">Master Plans</a></li>
											</ul>
										</li>
										<li>
											<span class="title text-uppercase">Contact Info</span>
											<ul class="list-unstyled">
												<li><a href="<?php echo get_site_url(); ?>/team/">Sales</a></li>
												<li><a href="<?php echo get_site_url(); ?>/contact/brokers/">Broker Registration</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" id="lifestyle-dropdown" data-toggle="dropdown">Lifestyle</a>
									<ul class="dropdown-menu text-center" role="menu" aria-labelledby="lifestyle-dropdown">
										<li>
											<span class="title text-uppercase">Amenities</span>
											<ul class="list-unstyled">
												<?php $amenities = new WP_Query("post_type=amenity&posts_per_page=99&post_parent=0"); while($amenities->have_posts()) : $amenities->the_post();?>
												<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
												<?php endwhile; ?>
												<?php wp_reset_query(); ?>
											</ul>
										</li>
										<li>
											<span class="title text-uppercase">Services</span>
											<ul class="list-unstyled">
												<?php $services = new WP_Query("post_type=service&posts_per_page=99&post_parent=0"); while($services->have_posts()) : $services->the_post();?>
												<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
												<?php endwhile; ?>
												<?php wp_reset_query(); ?>
											</ul>
										</li>
									</ul>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" id="resources-dropdown" data-toggle="dropdown">Vision</a>
									<ul class="dropdown-menu half text-center" role="menu" aria-labelledby="resources-dropdown">
										<li>
											<ul class="list-unstyled">
												<li><a href="<?php echo get_site_url(); ?>/vision/del-mar-difference/">The Del Mar Difference</a></li>
												<li><a href="<?php echo get_site_url(); ?>/vision/philanthropy/">Philanthropy</a></li>
												<li><a href="<?php echo get_site_url(); ?>/vision/investors/">Investors</a></li>
												<li><a href="<?php echo get_site_url(); ?>/vision/management/">Management Team</a></li>
												<li><a href="<?php echo get_site_url(); ?>/vision/architecture/">Architecture</a></li>
												<li><a href="<?php echo get_site_url(); ?>/vision/quality-promise/">Quality Promise</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" id="resources-dropdown" data-toggle="dropdown">Resources</a>
									<ul class="dropdown-menu half text-center" role="menu" aria-labelledby="resources-dropdown">
										<li>
											<ul class="list-unstyled">
												<li><a href="<?php echo get_site_url(); ?>/resources/buying-real-estate-in-mexico/">Real Estate in Mexico</a></li>
												<li><a href="<?php echo get_site_url(); ?>/resources/guide/">Los Cabos Guide</a></li>
												<li><a href="<?php echo get_site_url(); ?>/resources/escapes-magazine/">Escapes Magazine</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" id="news-dropdown" data-toggle="dropdown">News</a>
									<ul class="dropdown-menu half text-center" role="menu" aria-labelledby="news-dropdown">
										<li>
											<ul class="list-unstyled">
												<li><a href="<?php echo get_site_url(); ?>/news/">Articles</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" id="contact-dropdown" data-toggle="dropdown">Contact</a>
									<ul class="dropdown-menu half text-center" role="menu" aria-labelledby="contact-dropdown">
										<li>
											<ul class="list-unstyled">
												<li><a href="<?php echo get_site_url(); ?>/team/">Sales Team</a></li>
												<li><a href="<?php echo get_site_url(); ?>/contact/brokers/">Broker Registration</a></li>
											</ul>
										</li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- main container of all the page elements -->
	<div id="wrapper">
		<div class="w1">
