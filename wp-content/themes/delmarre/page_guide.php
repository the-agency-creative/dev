<?php
/*
Template Name: Cabo Guide
*/
?>
<?php get_header(); ?>
			<!-- feature block -->
			<?php while ( have_posts() ) : the_post(); ?>
			<!-- feature block -->
			<?php if(get_field('vdm_hide_page_header_image')) { ?>
			<section class="container page-no-image">
				<div class="row col-area">
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
						<!-- textarea -->
						<div class="textarea">
							<header class="header text-center">
								<h1><?php the_title(); ?></h1>
							</header>
						</div>
					</div>
				</div>
			</section>
			<?php } else { ?>
			<section class="container-fluid feature-block page-image">
				<div class="row">
					<?php
						$vdm_page_header_image_obj = get_field('vdm_page_header_image');
						$vdm_page_header_image_size = 'page-header-image';
						$vdm_page_header_image = $vdm_page_header_image_obj['sizes'][$vdm_page_header_image_size];
					?>
					<img src="<?php echo $vdm_page_header_image; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" class="img-responsive">
					<div class="carousel-caption text-center">
						<div class="container">
							<div class="row col-area">
								<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
									<!-- textarea -->
									<div class="textarea">
										<header class="header">
											<h1><?php the_title(); ?></h1>
										</header>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php } ?>
			
			
			
			<div class="container">
				<div class="row">
					<div class="col-md-12 space-above">
						<?php
						the_content();
						// check for rows (guide categories)
						if( have_rows('dmre_guide') ):
							// loop through rows (guide categories)
							while( have_rows('dmre_guide') ): the_row(); ?>
								   
								<div class="row">
                                
									<h2><?php the_sub_field('dmre_guide_category_name'); ?> Recommendations</h2>
			            	<?php
									// check for places
									if( have_rows('dmre_guide_places') ):
										// loop through places
										while( have_rows('dmre_guide_places') ): the_row();
											// display each place as a block
											?>
											<div class="col-md-4 guide-item">
												<h3><?php the_sub_field('dmre_guide_place_name'); ?></h3>
												<?php if(get_sub_field('dmre_guide_place_description')) { ?><?php the_sub_field('dmre_guide_place_description'); ?><?php } ?>
												<ul class="list-unstyled">
													<?php if(get_sub_field('dmre_guide_place_website')) { ?><li><i class="fa fa-link"></i> <a class="guide-meta" target="_blank" href="<?php the_sub_field('dmre_guide_place_website'); ?>">Website</a></li><?php } ?>
													<?php if(get_sub_field('dmre_guide_place_phone')) { ?><li><i class="fa fa-phone"></i> <a class="guide-meta" target="_blank" href="tel:<?php the_sub_field('dmre_guide_place_phone'); ?>"><?php the_sub_field('dmre_guide_place_phone'); ?></a></li><?php } ?>
												</ul>
											</div>
										<?php endwhile; // while( has_sub_field('dmre_guide_places') ): ?>
									<?php endif; //if( get_sub_field('dmre_guide_places') ): ?>
								</div>
							<?php endwhile; // while( has_sub_field('dmre_guide') ): ?>
						<?php endif; // if( get_field('dmre_guide') ): ?>
					</div>
				</div>
			</div>
			<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>
