<?php

/**********

Template Name: Home

**********/

get_header();

?>
			<!-- slider holder -->
			<section class="container-fluid slider">
				<div class="row">
					<div class="info-caption hidden-xs">
						<div class="container">
							<div class="row">
								<!-- <div class="col-lg-12 col-md-12 col-sm-12 text-right"><span class="temp">92° / 75°</span></div> -->
							</div>
						</div>
					</div>
					<?php if(get_field('vdm_home_page_slides', 'option')): ?>
					<!-- slider -->
					<div id="vdm-carousel" class="carousel slide" data-ride="carousel" data-interval="3000">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<?php
								$i=0;
								while(has_sub_field('vdm_home_page_slides', 'option')):
							?>
							<li data-target="#vdm-carousel" data-slide-to="<?php echo $i++; ?>"></li>
							<?php endwhile; ?>
						</ol>
						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<?php
								while(has_sub_field('vdm_home_page_slides', 'option')):
								$vdm_home_slide_image_obj = get_sub_field('vdm_home_slide_image');
								$vdm_home_slide_image_size = 'page-header-image';
								$vdm_home_slide_image = $vdm_home_slide_image_obj['sizes'][$vdm_home_slide_image_size];
								$vdm_mobile_slide_obj = get_sub_field('vdm_home_slide_mobile_image');
								$vdm_mobile_slide_size = 'page-header-mobile-image';
								$vdm_mobile_slide = $vdm_mobile_slide_obj['sizes'][$vdm_mobile_slide_size];
							?>
							<div class="item">
								<picture>
				      				<!--[if IE 9]><video style="display: none;"><![endif]-->
									<source srcset="<?php echo $vdm_home_slide_image; ?>" media="(min-width: 481px)">
									<source srcset="<?php echo $vdm_mobile_slide ?> 2x" media="(max-width: 480px)">
									<!--[if IE 9]></video><![endif]-->
									<img class="img-responsive" srcset="<?php echo $vdm_home_slide_image; ?>" alt="<?php the_title(); ?>">
								</picture>
								<!-- carousel caption -->
								<div class="carousel-caption">
									<div class="container">
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 text-right">
												<div class="caption-text">
													<h3><?php the_sub_field('vdm_home_slide_headline'); ?></h3>
													<?php the_sub_field('vdm_home_slide_subhead'); ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php endwhile; ?>
						</div>
					</div>
					 <?php endif; ?>
				</div>
			</section>
			<div class="container">
				<!-- intro content -->
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="row col-area">
							<div class="col-lg-8 col-md-8 col-sm-9 content">
								<?php the_field('vdm_home_welcome_text', 'option'); ?>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-3 cta-section">
								<div class="property-search">
									<a href="/real-estate/available-new-homes/" class="btn btn-default aside-link text-center text-uppercase"><?php the_field('vdm_home_button_text', 'option'); ?></a>
									<form action="<?php bloginfo('url'); ?>" method="get" name="s" class="property-search-form clearfix">
										<input name="s" type="text" placeholder="Search properties..." title="Search Properties" class="property-search-input form-control" />
										<button type="submit" class="btn btn-primary pull-right">Search</button>
										<input type="hidden" name="search_type" value="property_search">
									</form>
								</div>
							</div>
						</div>
						<!-- feature section -->
						<section class="row feature-section">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="row">
									<div class="col-lg-8 col-md-8 col-sm-12">
										<!-- img holder -->
										<div class="img-holder">
											<?php
												$box1_image_obj = get_field('vdm_home_box1_image', 'option');
												$box1_image_size = 'home-tile1-image';
												$box1_image = $box1_image_obj['sizes'][$box1_image_size];
											?>
											<a href="<?php echo the_field('vdm_home_box1_url', 'option'); ?>">
											<img src="<?php echo $box1_image; ?>" alt="<?php echo the_field('vdm_home_box1_link_text', 'option'); ?>" class="img-responsive">
                                            </a>
											<!-- caption -->
											<div class="caption">
												<h4><?php echo the_field('vdm_home_box1_headline', 'option'); ?></h4>
												<strong class="sub-head"><?php echo the_field('vdm_home_box1_subhead', 'option'); ?></strong>
												<div class="link-holder">
													<a href="<?php echo the_field('vdm_home_box1_url', 'option'); ?>" class="link"><?php echo the_field('vdm_home_box1_link_text', 'option'); ?></a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 imgs-area">
										<!-- img holder -->
										<div class="img-holder alignright">
											<?php
												$box2_image_obj = get_field('vdm_home_box2_image', 'option');
												$box2_image_size = 'home-tile23-image';
												$box2_image = $box2_image_obj['sizes'][$box2_image_size];
											?>
											<a href="<?php echo the_field('vdm_home_box2_url', 'option'); ?>">
											<img src="<?php echo $box2_image; ?>" alt="<?php echo the_field('vdm_home_box2_link_text', 'option'); ?>" class="img-responsive">
                                            </a>
											<!-- caption -->
											<div class="caption">
												<h4><?php echo the_field('vdm_home_box2_headline', 'option'); ?></h4>
												<strong class="sub-head"><?php echo the_field('vdm_home_box2_subhead', 'option'); ?></strong>
												<div class="link-holder">
													<a href="<?php echo the_field('vdm_home_box2_url', 'option'); ?>" class="link"><?php echo the_field('vdm_home_box2_link_text', 'option'); ?></a>
												</div>
											</div>
										</div>
										<!-- img holder -->
										<div class="img-holder alignright add">
											<?php
												$box3_image_obj = get_field('vdm_home_box3_image', 'option');
												$box3_image_size = 'home-tile23-image';
												$box3_image = $box3_image_obj['sizes'][$box3_image_size];
											?>
											<a href="<?php echo the_field('vdm_home_box3_url', 'option'); ?>">
											<img src="<?php echo $box3_image; ?>" alt="<?php echo the_field('vdm_home_box3_link_text', 'option'); ?>" class="img-responsive">
                                            </a>
											<!-- caption -->
											<div class="caption">
												<h4><?php echo the_field('vdm_home_box3_headline', 'option'); ?></h4>
												<strong class="sub-head"><?php echo the_field('vdm_home_box3_subhead', 'option'); ?></strong>
												<div class="link-holder">
													<a href="<?php echo the_field('vdm_home_box3_url', 'option'); ?>" class="link"><?php echo the_field('vdm_home_box3_link_text', 'option'); ?></a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<!-- img holder -->
										<div class="img-holder">
											<?php
												$box4_image_obj = get_field('vdm_home_box4_image', 'option');
												$box4_image_size = 'home-tile4-image';
												$box4_image = $box4_image_obj['sizes'][$box4_image_size];
											?>
											<a href="<?php echo the_field('vdm_home_box4_url', 'option'); ?>">
											<img src="<?php echo $box4_image; ?>" alt="<?php echo the_field('vdm_home_box4_link_text', 'option'); ?>" class="img-responsive">
                                            </a>
											<!-- caption -->
											<div class="caption">
												<h4><?php echo the_field('vdm_home_box4_headline', 'option'); ?></h4>
												<strong class="sub-head"><?php echo the_field('vdm_home_box4_subhead', 'option'); ?></strong>
												<div class="link-holder">
													<a href="<?php echo the_field('vdm_home_box4_url', 'option'); ?>" class="link"><?php echo the_field('vdm_home_box4_link_text', 'option'); ?></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
						<!-- info row -->
						<section class="row info-row">
							<?php $home_news = new WP_Query("post_type=post&posts_per_page=3"); while($home_news->have_posts()) : $home_news->the_post();?>
							<div class="col-lg-4 col-md-4 col-sm-4 info-col">
								<!-- block -->
								<div class="block">
									<div class="holder">
										<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
										<a href="<?php the_permalink(); ?>" class="btn-read">Read more</a>
									</div>
								</div>
							</div>
							<?php endwhile; ?>
							<?php wp_reset_query(); ?>
						</section>
					</div>
				</div>
			</div>
			<!-- feature block -->
			<section class="container-fluid feature-block">
				<div class="row">
					<!-- heading -->
					<header class="container heading">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 text-center">
								<h2>Featured Los Cabos Properties</h2>
							</div>
						</div>
					</header>
					<!-- slider -->
					<div class="slide gallery1 container">
						<!-- Wrapper for slides -->
						<div class="mask">
							<div class="slideset">
								<div class="item">
									<picture>
					      				<!--[if IE 9]><video style="display: none;"><![endif]-->
										<source srcset="http://espiritudelmar.com/wp-content/uploads/2015/04/the-ledges-at-espiritu-1140x620.jpg" media="(min-width: 481px)">
										<source srcset="http://espiritudelmar.com/wp-content/uploads/2015/04/the-ledges-at-espiritu-640x345.jpg 2x" media="(max-width: 480px)">
										<!--[if IE 9]></video><![endif]-->
										<img src="http://espiritudelmar.com/wp-content/uploads/2015/04/the-ledges-at-espiritu-1140x620.jpg" alt="Espiritu Estate 16 at Villas del Mar" class="img-responsive">
									</picture>
									<!-- carousel caption -->
									<div class="carousel-caption text-center">
										<div class="container">
											<div class="row">
												<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
													<!-- textarea -->
													<div class="textarea">
														<a href="#" id="property-nav" class="next"><i class="fa fa-angle-right"></i></a>
														<a href="#" id="property-nav" class="prev"><i class="fa fa-angle-left"></i></a>
														<header class="header">
															<h2><a class="cinzel" title="Espiritu Estate 16" href="http://espiritudelmar.com/neighborhood/the-ledges/">The Ledges at Espiritu</a></h2>
															<em>Now taking reservations for the first 8 Residences and 2 Penthouses</em>
														</header>
														<p>The Ledges at Espiritu represents the first residential offering of their kind in the Del Mar communities — a limited collection of residences and penthouses featuring, a sophisticated yet casual contemporary design. The Ledges is located in the heart of Espiritu, just a short stroll away from the Club Espiritu Health and Fitness Club and a golf cart ride away from the Club Ninety Six beach club.</p>
														<a href="http://espiritudelmar.com/neighborhood/the-ledges/" class="cinzel btn btn-primary space-above">Learn More</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php $home_featured_images = new WP_Query(
															array(
																'post_type'=>'property',
																'meta_query' => array(
																		         array(
																		            'key' => 'vdm_property_featured',
																		            'value' => 'Yes',
																		            'compare' => 'LIKE'
																		        )
																		    )
															)
														);
								while($home_featured_images->have_posts()) :
								$home_featured_images->the_post();
								$vdm_property_images = get_field('vdm_property_gallery');
								$vdm_property_image = $vdm_property_images[0];
								$vdm_property_image_size = 'carousel-image';
								$vdm_property_image_src = $vdm_property_image['sizes'][$vdm_property_image_size];
								$vdm_property_image_size_mobile = 'carousel-image-mobile';
								$vdm_property_image_src_mobile = $vdm_property_image['sizes'][$vdm_property_image_size_mobile];
								?>
								<div class="item">
									<picture>
					      				<!--[if IE 9]><video style="display: none;"><![endif]-->
										<source srcset="<?php echo $vdm_property_image_src; ?>" media="(min-width: 481px)">
										<source srcset="<?php echo $vdm_property_image_src_mobile; ?> 2x" media="(max-width: 480px)">
										<!--[if IE 9]></video><![endif]-->
										<img src="<?php echo $vdm_property_image_src; ?>" alt="<?php the_title(); ?> at Villas del Mar" class="img-responsive">
									</picture>
									<!-- carousel caption -->
									<div class="carousel-caption text-center">
										<div class="container">
											<div class="row">
												<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
													<!-- textarea -->
													<div class="textarea">
														<a href="#" id="property-nav" class="next"><i class="fa fa-angle-right"></i></a>
														<a href="#" id="property-nav" class="prev"><i class="fa fa-angle-left"></i></a>
														<header class="header">
															<h2><a class="cinzel" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
															<em><?php the_field('vdm_property_subheader'); ?></em>
														</header>
														<?php the_field('vdm_property_description'); ?>
														<a href="<?php the_permalink(); ?>" class="cinzel btn btn-primary space-above">View Property</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php endwhile; ?>
								<?php wp_reset_query(); ?>
							</div>
						</div>
						<a href="#" class="next"></a>
						<a href="#" class="prev"></a>
					</div>
				</div>
			</section>
<?php get_footer(); ?>
