<?php

// search template

get_header();

?>
			<!-- feature block -->
			<section class="container-fluid feature-block">
				<div class="row">
					<!-- slider -->
					<div class="slide gallery1 container">
						<!-- Wrapper for slides -->
						<div class="mask">
							<div class="slideset">
								<?php $status_images = new WP_Query(
															array(
																'post_type'=>'property',
																'meta_query' => array(
																		         array(
																		            'key' => 'vdm_property_featured',
																		            'value' => 'Yes',
																		            'compare' => 'LIKE'
																		        )
																		    )
															)
														);
								while($status_images->have_posts()) :
								$status_images->the_post();
								$vdm_property_images = get_field('vdm_property_gallery');
								$vdm_property_image = $vdm_property_images[0];
								$vdm_property_image_size = 'carousel-image';
								$vdm_property_image_src = $vdm_property_image['sizes'][$vdm_property_image_size];
								?>
								<div class="item">
									<img src="<?php echo $vdm_property_image_src; ?>" alt="<?php the_title(); ?> - Los Cabos Luxury Real Estate" class="img-responsive">
									<div class="carousel-property-info">
										<h3>Featured Property</h3>
										<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?> - Los Cabos Luxury Real Estate"><?php the_title(); ?></a></h2>
									</div>
								</div>
								<?php endwhile; ?>
								<?php wp_reset_query(); ?>
							</div>
						</div>
						<!-- carousel caption -->
						<div class="carousel-caption text-center">
							<div class="container">
								<div class="row col-area">
									<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
										<!-- textarea -->
										<div class="textarea">
											<a href="#" id="property-nav" class="next"><i class="fa fa-angle-right"></i></a>
											<a href="#" id="property-nav" class="prev"><i class="fa fa-angle-left"></i></a>
                                        	<!-- Indicators -->
                                            <ol class="carousel-indicators">
                                                <?php $status_indicators = new WP_Query(
                                                                                array(
                                                                                    'post_type'=>'property',
                                                                                    'meta_query' => array(
                                                                                                     array(
                                                                                                        'key' => 'vdm_property_featured',
                                                                                                        'value' => 'Yes',
                                                                                                        'compare' => 'LIKE'
                                                                                                    )
                                                                                                )
                                                                                )
                                                                            );
                                                $i=0;
                                                while($status_indicators->have_posts()) :
                                                $status_indicators->the_post();
                                                ?>
                                                <li data-target="#vdm-service-gallery" data-slide-to="<?php echo $i++; ?>"></li>
                                                <?php endwhile; ?>
                                                <?php wp_reset_query(); ?>
                                            </ol>
											<header class="header">
												<h1>Real Estate Search for "<?php the_search_query(); ?>"</h1>
											</header>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<div class="container">
				<div class="row space-above space-below">

					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<?php if(get_field('vdm_property_gallery')) {

					    $vdm_property_images = get_field('vdm_property_gallery');
					    $vdm_property_image = $vdm_property_images[0];
					    $vdm_property_image_size = 'grid-image';
					    $vdm_property_image_src = $vdm_property_image['sizes'][$vdm_property_image_size];

					}

				    ?>

					<div class="col-md-4 col-sm-6 col-xs-12 grid-item space-below space-above">
						<?php if(get_field('vdm_property_gallery')) { ?><a href="<?php the_permalink(); ?>"><img src="<?php echo $vdm_property_image_src; ?>" /></a><?php } ?>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php if(has_term('sold', 'status')) : ?><span class="btn btn-primary space-left btn-sold disabled">Sold</span><?php endif; ?></h3>
						<?php if(get_field('vdm_property_subheader')) { ?><p><?php the_field('vdm_property_subheader'); ?></p><?php } ?>
						<a href="<?php the_permalink(); ?>">View Property &rarr;</a>
					</div>

					<?php endwhile; else: ?>
					<h3 class="text-center"><?php _e('Your search did not match any properties at this time.'); ?></h3>
					<?php endif; ?>
				</div>
			</div>

<?php get_footer(); ?>
