<?php get_header(); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<!-- feature block -->
			<section class="container page-no-image space-above">
				<div class="row col-area">
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
						<!-- textarea -->
						<div class="textarea">
							<header class="header text-center">
								<h1><?php the_title(); ?> - <?php the_field('dmre_agent_title'); ?></h1>
							</header>
						</div>
					</div>
				</div>
			</section>
			<div class="container">
				<div class="row">
					<div class="col-md-12 space-above">
						<div class="row space-above space-below agent-item">
							<div class="col-md-4 col-xs-12">
								<img src="<?php the_field('dmre_agent_headshot'); ?>" class="img-responsive" />
							</div>
							<div class="col-md-8 col-xs-12 agent-meta">
								<p><?php the_field('dmre_agent_bio'); ?></p>
								<ul class="list-unstyled">
									<?php if(get_field('dmre_agent_office_phone')) { ?><li>Office: <a target="_blank" href="tel:<?php the_field('dmre_agent_office_phone'); ?>"><?php the_field('dmre_agent_office_phone'); ?></a></li><?php } ?>
									<?php if(get_field('dmre_agent_mobile_phone')) { ?><li>Mobile: <a target="_blank" href="tel:<?php the_field('dmre_agent_mobile_phone'); ?>"><?php the_field('dmre_agent_mobile_phone'); ?></a></li><?php } ?>
									<?php if(get_field('dmre_guide_email')) { ?><li>Email: <a target="_blank" href="mailto:<?php the_field('dmre_agent_email'); ?>"><?php the_field('dmre_agent_email'); ?></a></li><?php } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="row space-above space-below">
					<h2>Properties offered by <?php the_title(); ?></h2>
					<?php

					/*
					*  Query posts for a relationship value.
					*  This method uses the meta_query LIKE to match the string "123" to the database value a:1:{i:0;s:3:"123";} (serialized array)
					*/

					$properties = get_posts(array(
						'post_type' => 'property',
						'meta_query' => array(
							array(
								'key' => 'vdm_property_agent', // name of custom field
								'value' => '"' . get_the_ID() . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
								'compare' => 'LIKE'
							)
						),
						'posts_per_page' => '99'
					));

					?>
					<?php if( $properties ): ?>
						<?php foreach( $properties as $property ):

							if(get_field('vdm_property_gallery', $property->ID )) {

							    $vdm_property_images = get_field('vdm_property_gallery', $property->ID);
							    $vdm_property_image = $vdm_property_images[0];
							    $vdm_property_image_size = 'grid-image';
							    $vdm_property_image_src = $vdm_property_image['sizes'][$vdm_property_image_size];

							}

						?>
							<div class="col-md-4 col-sm-6 col-xs-12 grid-item space-below space-above">
								<?php if(get_field('vdm_property_gallery', $property->ID)) { ?><a href="<?php echo get_permalink( $property->ID ); ?>"><img src="<?php echo $vdm_property_image_src; ?>" /></a><?php } ?>
								<h3><a href="<?php echo get_permalink( $property->ID ); ?>"><?php echo get_the_title( $property->ID ); ?></a></h3>
								<?php if(get_field('vdm_property_subheader', $property->ID)) { ?><p><?php the_field('vdm_property_subheader', $property->ID); ?></p><?php } ?>
								<a href="<?php echo get_permalink( $property->ID ); ?>">View Property &rarr;</a>
							</div>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
			<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
			<?php endif; ?>
<?php get_footer(); ?>
