<?php

// template for single property pages

get_header();

?>
			<!-- feature block -->
			<section class="container-fluid feature-block">
				<div class="row">
					<!-- slider -->
					<div class="slide gallery1 container" data-interval="false">
						<?php
							$vdm_property_images = get_field('vdm_property_gallery');
							if( $vdm_property_images ):
							$i=0;
						?>
						<!-- Wrapper for slides -->
						<div class="mask clearfix">
							<div class="slideset">
								<?php foreach( $vdm_property_images as $vdm_property_image ):
								$vdm_property_image_size = 'carousel-image';
								$vdm_property_image_size_mobile = 'carousel-image-mobile';
								$vdm_property_image_url = $vdm_property_image['sizes'][$vdm_property_image_size];
								$vdm_property_image_alt = $vdm_property_image['alt'];
								$vdm_property_image_url_mobile = $vdm_property_image['sizes'][$vdm_property_image_size_mobile];
								?>
								<div class="item">
									<img src="<?php echo $vdm_property_image_url; ?>" alt="<?php the_title(); ?> at Villas del Mar" class="img-responsive">
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
						<!-- carousel caption -->
						<div class="carousel-caption text-center">
							<div class="container">
								<div class="row col-area">
									<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
										<!-- textarea -->
										<div class="textarea">
											<a href="#" id="property-nav" class="next"><i class="fa fa-angle-right"></i></a>
											<a href="#" id="property-nav" class="prev"><i class="fa fa-angle-left"></i></a>
                                        	<ol class="carousel-indicators">
												<?php foreach( $vdm_property_images as $vdm_property_image ): ?>
                                                <li data-target="#vdm-property-gallery" data-slide-to="<?php echo $i++; ?>"></li>
                                                <?php endforeach; ?>
                                            </ol>
											<header class="header">
												<h1><?php the_title(); ?><?php if(has_term('sold', 'status')) : ?><span class="btn btn-primary space-left btn-sold disabled">Sold</span><?php endif; ?></h1>
												<em><?php the_field('vdm_property_subheader'); ?></em>
											</header>
											<?php the_field('vdm_property_description'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<div class="container">
				<div class="row feature-row">
					<div class="col-lg-8 col-md-8 col-sm-8">
						<!-- feature list -->
						<?php if( have_rows('vdm_property_details') ): ?>
						<ul class="feature-list list-unstyled text-capitalize">
							<?php while( have_rows('vdm_property_details') ): the_row();
							$vdm_property_detail_main = get_sub_field('vdm_property_detail_main');
							$vdm_property_detail_subhead = get_sub_field('vdm_property_detail_subhead');
							?>
							<li>
								<!-- block -->
								<div class="block">
									<div class="holder">
										<span class="title"><?php echo $vdm_property_detail_main; ?></span>
										<p><?php echo $vdm_property_detail_subhead; ?></p>
									</div>
								</div>
							</li>
							<?php endwhile; ?>
						</ul>
						<?php endif; ?>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4">
						<!-- description list -->
						<?php if( have_rows('vdm_property_additional_details') ): ?>
						<dl class="dl-horizontal">
							<?php while( have_rows('vdm_property_additional_details') ): the_row();
							$vdm_property_additional_detail_title = get_sub_field('vdm_property_additional_detail_title');
							$vdm_property_additional_detail_info = get_sub_field('vdm_property_additional_detail_info');
							?>
							<dt><?php echo $vdm_property_additional_detail_title; ?>:</dt>
							<dd><?php echo $vdm_property_additional_detail_info; ?></dd>
							<?php endwhile; ?>
						</dl>
						<?php endif; ?>
					</div>
				</div>
				<div class="row video-section">
					<div class="col-lg-4 col-lg-push-8 col-md-4 col-md-push-8 col-sm-4 col-sm-push-8">
						<!-- widget -->
						<section class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="widget">
									<h2>Inquire</h2>
									<?php

									$posts = get_field('vdm_property_agent');

									if( $posts ): ?>
									    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
									        <?php setup_postdata($post); ?>
									        <?php if( get_field('dmre_agent_headshot') ): ?><img class="pull-left agent-headshot" width="100" src="<?php the_field('dmre_agent_headshot'); ?>" /><?php endif; ?>
									        <ul class="agent-details list-unstyled space-below">
										        <li><span><strong>Agent: </strong></span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
										        <?php if(get_field('dmre_agent_email')) { ?><li><span><strong>Email: </strong></span><a href="mailto:<?php the_field('dmre_agent_email'); ?>"><?php the_field('dmre_agent_email'); ?></a></li><?php } ?>
										        <?php if(get_field('dmre_agent_office_phone')) { ?><li><span><strong>Office: </strong></span><a href="tel:<?php the_field('dmre_agent_office_phone'); ?>" class="tel"><?php the_field('dmre_agent_office_phone'); ?></a></li><?php } ?>
												<?php if(get_field('dmre_agent_mobile_phone')) { ?><li><span><strong>Mobile: </strong></span><a href="tel:<?php the_field('dmre_agent_mobile_phone'); ?>" class="tel"><?php the_field('dmre_agent_mobile_phone'); ?></a></li><?php } ?>
											</ul>
									    <?php endforeach; ?>
									    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
									<?php endif; ?>
									<div class="clearfix space-below"></div>
									<?php if( get_field('vdm_property_brochure') ): ?>
									<a href="<?php the_field('vdm_property_brochure'); ?>" class="btn btn-default">Download Brochure</a>
									<?php endif; ?>
								</div>
							</div>
						</section>
						<!-- widget -->
						<section class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="widget">
									<!-- header-box -->
									<header class="header-box">
										<h2>More Information</h2>
										<p>Complete the form below and we will be in contact with you shortly.</p>
									</header>
									<form accept-charset="utf-8" name="1fa21903-0165-4d11-a7be-017e807d155e" class="maForm" method="POST" action="https://apps.net-results.com/data/public/IncomingForm/Ma/submit">
									    <div class="maFormElement2">
									        <label for="First_Name" id="First_NameLabel" class="">First Name

									        </label>
									         <input type="text" placeholder="First Name" name="First_Name" class="formText " value="" />
									    </div>
									    <div class="maFormElement2">
									        <label for="Last_Name" id="Last_NameLabel" class="">Last Name

									        </label>
									        <input type="text" placeholder="Last Name" name="Last_Name" class="formText " value="" />
									    </div>
									    <div class="maFormElement2">
									        <label for="Email" id="EmailLabel" class="">Email

									        </label>
									         <input type="text" placeholder="Email" name="Email" class="formText " value="" />
									    </div>
									    <div class="maFormElement2">
									        <label for="Phone_Number" id="Phone_NumberLabel" class="">Phone Number

									        </label>
									        <input type="text" placeholder="Phone Number" name="Phone_Number" class="formText " value="" />
									    </div>
									    <div class="maFormElement2">
									        <label for="How_Did_You_Hear_About_Us" id="How_Did_You_Hear_About_UsLabel" class="">How Did You Hear About US?<span class="formRequiredLabel">*</span>

									        </label>

									        <select name="How_Did_You_Hear_About_Us" class="formSelect formRequired">
									                <option value="">How Did You Hear About Us?</option>
									                <option value="Search Engine">Search Engine</option>
									                <option value="Broker">Broker</option>
									                <option value="News Article">News Article</option>
									                <option value="Story">Story</option>
									                <option value="Current Owner">Current Owner</option>
									                <option value="Guest of the Resort">Guest of the Resort</option>
									                <option value="Friend">Friend</option>
									                <option value="Cabo Advertisement">Cabo Advertisement</option>
									                <option value="US Advertisement">US Advertisement</option>
									            </select>
									    </div>
									    <div class="maFormElement2">
									        <button name="SUBMIT" class="formSubmit " value="submit" type="submit">SUBMIT</button>
									    </div>
									    <input type="hidden" name="Page" value="<?php the_title(); ?>" />
									    <input type="hidden" name="form_id" value="1fa21903-0165-4d11-a7be-017e807d155e" />
									    <input type="hidden" name="form_access_id" value="" />
									    <input type="hidden" name="__mauuid" value="" />
									</form>
								</div>
							</div>
						</section>
					</div>
					<div class="col-lg-8 col-lg-pull-4 col-md-8 col-md-pull-4 col-sm-8 col-sm-pull-4">
						<!-- feature area -->
						<div class="row features-area">
							<div class="col-lg-12">
								<h2>Features</h2>
								<!-- list -->
								<?php the_field('vdm_property_features'); ?>
							</div>
						</div>

						<!-- community features -->
						<?php if(get_field('vdm_property_community')) { ?>
						
<div class="row features-area">
							<div class="col-lg-12 col-md-12 col-sm-12">
							<h2>Community Features</h2>

							<?php $posts = get_field('vdm_property_community');

if( $posts ): ?>
    <ul class="list-unstyled">
    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        <li>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </li>
    <?php endforeach; ?>
    </ul>
    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>
	
					</div>
				</div>
				<?php } ?>
</div></div>
				<?php if(get_field('vdm_property_video')) { ?>
				<!-- video section -->
				<section class="row video-section">
					<div class="col-lg-12 col-md-12 col-sm-12 text-center">
						<h2>Property Video</h2>
						<!-- video-holder -->
						<div class="video-holder">
							<?php the_field('vdm_property_video'); ?>
						</div>
					</div>
				</section>
				<?php } ?>
				<!-- properties section -->
				<section class="row properties-section">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<!-- heading -->
						<header class="row heading">
							<div class="col-lg-12 col-md-12 col-sm-12 text-center">
								<h2>Similar Properties</h2>
							</div>
						</header>
						<div class="row">
							<?php
							$current_property = $post->ID;
							$term_list = wp_get_post_terms($current_property, 'neighborhood', array("fields" => "names"));
							$vdm_property_hood = $term_list[0];
							$vdm_related_properties = new WP_Query(
														array('post_type' => 'property',
															  'neighborhood' => $vdm_property_hood,
															  'posts_per_page' => 3,
															  'post__not_in' => array($current_property)
															 )
														);
							while($vdm_related_properties->have_posts()) :
							$vdm_related_properties->the_post();
							if(get_field('vdm_property_gallery')) {
							$vdm_related_property_images = get_field('vdm_property_gallery');
							$vdm_related_property_image = $vdm_related_property_images[0];
							$vdm_related_property_image_size = 'grid-image';
							$vdm_related_property_image_src = $vdm_related_property_image['sizes'][$vdm_related_property_image_size];
							}
							?>
							<!-- info section -->
							<section class="col-lg-4 col-md-4 col-sm-4">
								<?php if(get_field('vdm_property_gallery')) { ?><div class="img-holder"><a href="<?php the_permalink(); ?>"><img src="<?php echo $vdm_related_property_image_src; ?>" alt="<?php the_title(); ?>" class="img-responsive"></a></div><? } ?>
								<div class="text">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<?php
									if(get_field('vdm_property_price')) {
										$vdm_property_price_unformatted = get_field('vdm_property_price');
										$vdm_property_price = number_format($vdm_property_price_unformatted);
									}
									?>
									<?php if(get_field('vdm_property_price')) { ?><p>$<?php echo $vdm_property_price; ?></p><?php } ?>
									<a href="<?php the_permalink(); ?>" class="link">View listing</a>
								</div>
							</section>
							<?php endwhile; ?>
							<?php wp_reset_query(); ?>
						</div>
					</div>
				</section>
			</div>
<?php get_footer(); ?>
