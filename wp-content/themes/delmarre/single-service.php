<?php

// template for the single service view

get_header();

?>
			<!-- feature block -->
			<section class="container-fluid feature-block">
				<div class="row">
					<!-- slider -->
					<div class="slide gallery1 container">
						<!-- Indicators -->
						<?php
							$vdm_service_images = get_field('vdm_service_gallery');
							if( $vdm_service_images ):
							$i=0;
						?>

						<!-- Wrapper for slides -->
						<div class="mask clearfix">
							<div class="slideset">
								<?php foreach( $vdm_service_images as $vdm_service_image ):
								$vdm_service_image_size = 'carousel-image';
								$vdm_service_image_url = $vdm_service_image['sizes'][$vdm_service_image_size];
								$vdm_service_image_alt = $vdm_service_image['alt'];
								?>
								<div class="item">
									<img src="<?php echo $vdm_service_image_url; ?>" alt="<?php the_title(); ?> at Villas del Mar" class="img-responsive">
								</div>
								<?php endforeach; ?>
							</div>
						</div>
						<?php endif; ?>
						<!-- carousel caption -->
						<div class="carousel-caption text-center">
							<div class="container">
								<div class="row col-area">
									<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
										<!-- textarea -->
										<div class="textarea">
											<a href="#" id="property-nav" class="next"><i class="fa fa-angle-right"></i></a>
											<a href="#" id="property-nav" class="prev"><i class="fa fa-angle-left"></i></a>
                                            <!-- Indicators -->
                                            <ol class="carousel-indicators">
                                                <?php foreach( $vdm_service_images as $vdm_service_image ): ?>
                                                <li data-target="#vdm-service-gallery" data-slide-to="<?php echo $i++; ?>"></li>
                                                <?php endforeach; ?>
                                            </ol>
											<header class="header">
												<h1><?php the_title(); ?></h1>
											</header>
											<?php the_field('vdm_service_description'); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<?php the_field('vdm_service_features'); ?>
					</div>
					<div class="col-md-4">
						<?php
						$vdm_service_related_posts = get_posts(array(
							'post_type' => 'post',
							'meta_query' => array(
								array(
									'key' => 'vdm_post_related_service',
									'value' => '"' . get_the_ID() . '"',
									'compare' => 'LIKE'
								)
							)
						));
						?>
						<?php if( $vdm_service_related_posts ): ?>
							<ul id="related-post-list" class="list-unstyled">
							<?php foreach( $vdm_service_related_posts as $vdm_service_related_post ): ?>
								<li>
									<h3><a href="<?php echo get_permalink( $vdm_service_related_post->ID ); ?>"><?php echo get_the_title( $vdm_service_related_post->ID ); ?></a></h3>
									<a href="<?php echo get_permalink( $vdm_service_related_post->ID ); ?>">Read More <i class="fa fa-angle-right"></i></a>
								</li>
							<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
				<div class="row extra-space-above extra-space-below">
					<?php

					$vdm_service_related_services = get_field('vdm_service_related_services');

					if( $vdm_service_related_services ): ?>
						<h2 class="text-center space-below">Related to <?php the_title(); ?></h2>
					    <?php foreach( $vdm_service_related_services as $post): // variable must be called $post (IMPORTANT) ?>
					        <?php
					        setup_postdata($post);
					        if(get_field('vdm_service_gallery')) {
					        $vdm_related_service_images = get_field('vdm_service_gallery');
							$vdm_related_service_image = $vdm_related_service_images[0];
							$vdm_related_service_image_size = 'grid-image';
							$vdm_related_service_image_src = $vdm_related_service_image['sizes'][$vdm_related_service_image_size];
							}
							$vdm_related_service_character_count = 100;
							$vdm_related_service_full_description = get_field('vdm_service_description');
							$vdm_related_service_short_description = substr($vdm_related_service_full_description, 0, $vdm_related_service_character_count);
					        ?>
					        <div class="grid-item col-md-4 col-sm-6 col-xs-12 space-below space-above">
								<?php if(get_field('vdm_service_gallery')) { ?>
								<a href="<?php the_permalink(); ?>"><img src="<?php echo $vdm_related_service_image_src; ?>" class="img-responsive" /></a>
								<?php } ?>
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<p><?php echo $vdm_related_service_short_description; ?>...</p>
								<a href="<?php the_permalink(); ?>" rel="nofollow">View service &rarr;</a>
							</div>
					    <?php endforeach; ?>
					    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif; ?>
				</div>
				<?php if(get_field('vdm_service_video')) { ?>
				<!-- video section -->
				<section class="row video-section">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<h2>Property Video</h2>
						<!-- video-holder -->
						<div class="video-holder">
							<?php the_field('vdm_service_video'); ?>
						</div>
					</div>
				</section>
				<?php } ?>
			</div>

<?php get_footer(); ?>
