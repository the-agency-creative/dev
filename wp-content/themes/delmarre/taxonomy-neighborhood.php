<?php

// template for the archives of the neighborhood taxonomy

get_header();

$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

$tax_title = $term->name;
$tax_description = $term->description;
$tax_image_obj = get_field('vdm_neighborhood_image', $term);
$tax_image_size = 'page-header-image';
$tax_image = $tax_image_obj['sizes'][$tax_image_size];

?>

			<!-- feature block -->
			<section class="container-fluid feature-block">
				<div class="row">
					<img src="<?php echo $tax_image; ?>" alt="<?php echo $tax_title; ?> at Villas del Mar" class="img-responsive">
					<div class="carousel-caption text-center">
						<div class="container">
							<div class="row col-area">
								<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
									<!-- textarea -->
									<div class="textarea">
										<header class="header">
											<h1><?php echo $tax_title; ?></h1>
										</header>
										<?php echo $tax_description; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<div class="container">
				<div class="row space-above space-below">
					<?php the_field('vdm_neighborhood_more_content', $term); ?>
				</div>
				<div class="row extra-space-above">
					<div class="col-md-4 col-md-offset-4 text-center">
						<a href="#contact" class="btn btn-default">Request Information</a>
					</div>
				</div>
				<div class="row extra-space-above extra-space-below">
					<?php

					$vdm_neighborhood_related_amenities = get_field('vdm_neighborhood_related_amenities', $term);

					if( $vdm_neighborhood_related_amenities ): ?>
						<h2 class="text-center space-below">Espiritu Amenities</h2>
					    <?php foreach( $vdm_neighborhood_related_amenities as $post): // variable must be called $post (IMPORTANT) ?>
					        <?php
					        setup_postdata($post);
					        if(get_field('vdm_amenity_gallery')) {
					        $vdm_related_amenity_images = get_field('vdm_amenity_gallery');
							$vdm_related_amenity_image = $vdm_related_amenity_images[0];
							$vdm_related_amenity_image_size = 'grid-image';
							$vdm_related_amenity_image_src = $vdm_related_amenity_image['sizes'][$vdm_related_amenity_image_size];
							}
							$vdm_related_amenity_character_count = 100;
							$vdm_related_amenity_full_description = get_field('vdm_amenity_description');
							$vdm_related_amenity_short_description = substr($vdm_related_amenity_full_description, 0, $vdm_related_amenity_character_count);
					        ?>
					        <div class="grid-item col-md-4 col-sm-6 col-xs-12 space-below space-above">
								<?php if(get_field('vdm_amenity_gallery')) { ?>
								<a href="<?php the_permalink(); ?>"><img src="<?php echo $vdm_related_amenity_image_src; ?>" class="img-responsive" /></a>
								<?php } ?>
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<p><?php echo $vdm_related_amenity_short_description; ?>...</p>
								<a href="<?php the_permalink(); ?>" rel="nofollow">View amenity &rarr;</a>
							</div>
					    <?php endforeach; ?>
					    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif; ?>
				</div>
				<div class="row space-above space-below property-search">
					<form action="<?php bloginfo('url'); ?>" method="get" name="s" class="property-search-form clearfix">
					  	<div class="col-md-6 col-md-offset-2">
					    	<input name="s" type="text" placeholder="Search properties by keyword, address, amenity..." title="Search properties by keyword, address, amenity..." class="property-search-input form-control" />
					  	</div>
					  	<div class="col-md-2">
					  		<button type="submit" class="btn btn-primary">Search</button>
					  	</div>
					  	<input type="hidden" name="search_type" value="property_search">
					</form>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h2 class="text-center">Properties in <?php echo $tax_title; ?></h2>
					</div>
				</div>
				<div class="row space-above space-below">
					<?php
					if ( have_posts() ) : while ( have_posts() ) : the_post();

					if(get_field('vdm_property_gallery')) {

					    $vdm_property_images = get_field('vdm_property_gallery');
					    $vdm_property_image = $vdm_property_images[0];
					    $vdm_property_image_size = 'grid-image';
					    $vdm_property_image_src = $vdm_property_image['sizes'][$vdm_property_image_size];

					}

				    ?>

					<div class="col-md-4 col-sm-6 col-xs-12 grid-item space-below space-above">
						<?php if(get_field('vdm_property_gallery')) { ?><a href="<?php the_permalink(); ?>"><img src="<?php echo $vdm_property_image_src; ?>" /></a><?php } ?>
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><?php if(has_term('sold', 'status')) : ?><span class="btn btn-primary space-left btn-sold disabled">Sold</span><?php endif; ?></h3>
						<?php if(get_field('vdm_property_subheader')) { ?><p><?php the_field('vdm_property_subheader'); ?></p><?php } ?>
						<a href="<?php the_permalink(); ?>">View Property &rarr;</a>
					</div>

					<?php endwhile; else: ?>
					<h3 class="text-center"><?php _e('Now taking reservations for the first 8 Residences and 2 Penthouses.'); ?></h3>
					<?php endif; ?>
				</div>
			</div>

<?php get_footer(); ?>
