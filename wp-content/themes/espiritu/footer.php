			<!-- form section -->
			<section class="container form-section">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<!-- heading -->
						<header class="heading">
							<h4>Request Information</h4>
							<p>Complete the form below and we will be in contact with you shortly.</p>
						</header>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<form accept-charset="utf-8" name="05894420-d435-4909-835a-fcb7a76cb8ab" class="maForm" method="POST" action="https://apps.net-results.com/data/public/IncomingForm/Ma/submit">

    <div class="row">
    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
        <label for="First_Name" id="First_NameLabel" class="">First Name

        </label>
        <input type="text" placeholder="First Name" name="First_Name" class="formText " value="" />
    </div>
    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
        <label for="Last_Name" id="Last_NameLabel" class="">Last Name

        </label>
        <input type="text" placeholder="Last Name"name="Last_Name" class="formText " value="" />
    </div>
    </div>

    <div class="row">
    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
        <label for="Email" id="EmailLabel" class="">Email

        </label>
        <input type="text" placeholder="Email" name="Email" class="formText " value="" />
    </div>
    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
        <label for="Phone_Number" id="Phone_NumberLabel" class="">Phone Number

        </label>
        <input type="text" placeholder="Phone Number" name="Phone_Number" class="formText " value="" />
    </div>
    </div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6">
	    	<div class="maFormElement">
			    <ul class="list-inline">
				    <li><p>Are you a broker?</p><label for="are_you_broker" id="are_you_brokerLabel" class="">Are you a broker?<span class="formRequiredLabel">*</span></label></li>
					<li><span class="lbl">Yes</span> <input id="are_you_broker-0" class="formRadio formRequired radio-inline" type="radio"  value="Yes" name="are_you_broker"/><label class="formRadioLabel"> Yes</label></li>
					<li><span class="lbl">No</span> <input id="are_you_broker-1" class="formRadio formRequired radio-inline" type="radio"  value="No" name="are_you_broker"/><label class="formRadioLabel"> No</label></li>
			    </ul>
			</div>
			<div class="maFormElement">
				<label for="brokerage" id="brokerageLabel" class="">If so, which brokerage?</label>
				<input type="text" name="brokerage" class="formText formPrepopulate" value="" placeholder="If so, which brokerage?" />
			</div>
			<div class="maFormElement">
		        <label for="How_Did_You_Hear_About_Us" id="How_Did_You_Hear_About_UsLabel" class="">How Did You Hear About US?<span class="formRequiredLabel">*</span></label>
		          <select name="How_Did_You_Hear_About_Us" class="formSelect formRequired">
		                <option value="">How Did You Hear About Us?</option>
		                <option value="Search Engine">Search Engine</option>
		                <option value="Broker">Broker</option>
		                <option value="News Article">News Article</option>
		                <option value="Story">Story</option>
		                <option value="Current Owner">Current Owner</option>
		                <option value="Guest of the Resort">Guest of the Resort</option>
		                <option value="Friend">Friend</option>
		                <option value="Cabo Advertisement">Cabo Advertisement</option>
		                <option value="US Advertisement">US Advertisement</option>
		            </select>
		    </div>
		</div>
		<div class="maFormElement col-lg-6 col-md-6 col-sm-6">
	        <label for="How_Can_We_Help_" id="How_Can_We_Help_Label" class="">How Can We Help?</label>
	        <textarea placeholder="How Can We Help?" name="How_Can_We_Help_" class="formTextarea "></textarea>
	    </div>
    </div>
    <div class="row">
	    <input type="hidden" name="Page" value="<?php the_title(); ?>" />
	    <input type="hidden" name="form_id" value="05894420-d435-4909-835a-fcb7a76cb8ab" />
	    <input type="hidden" name="form_access_id" value="" />
	    <input type="hidden" name="__mauuid" value="" />
    </div>
    <div class="maFormElement">
        <button name="formSubmit" class="formSubmit " value="submit" type="submit">SUBMIT</button>
    </div>
						</form>
                    </div>
				</div>
			</section>
		</div>
	</div>
	<!-- footer of the page -->
	<footer id="footer" class="container">
		<div class="row">
			<div class="col-lg-2 col-md-2 col-lg-push-5 col-md-push-5 col-sm-4 col-sm-push-4">
				<!-- footer logo -->
				<div class="footer-logo"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/footer-logo.png" alt="Del Mar resort Communities Development" class="img-responsive"></a></div>
			</div>




			<div class="col-lg-5 col-md-5 col-lg-pull-2 col-md-pull-2 col-sm-4 col-sm-pull-4 col-xs-5">
				<!-- social networks -->
				<ul class="social-networks list-inline">
					<li><a href="https://www.facebook.com/DelMarLosCabos">facebook</a></li>
					<!-- <li class="twitter"><a href="https://twitter.com/espiritudelmar">twitter</a></li> -->
					<li class="instagram"><a href="https://instagram.com/DelMarLosCabos/">instagram</a></li>
				</ul>
				Site by <a href="http://wickedta.com" rel="nofollow">Wicked + The Agency</a>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-4 col-xs-7 text-right">
				<a href="tel:1234567890" class="tel">Phone: 1.877.847.1662</a>
				<p><a href="/">Espiritu Del Mar</a>, &copy; 2014<br /><a href="http://espiritudelmar.com/wp-content/uploads/2015/03/PrivacyPolicy.pdf">Privacy Policy</a></p>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
	<?php the_field('vdm_tracking_codes', 'option'); ?>
</body>
</html>
