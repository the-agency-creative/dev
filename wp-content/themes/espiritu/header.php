<!doctype html>
<html>
	<head>
		<!-- set the encoding of the site -->
		<meta charset="utf-8">
		<!-- set our favicons -->
		<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<!-- set the viewport width and initial-scale on mobile devices  -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title><?php wp_title(''); ?></title>
		<link media="all" rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css">
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- include the site stylesheet -->
		<link media="all" rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/custom.min.css">
		<?php wp_head(); ?>
	</head>
<body <?php body_class(); ?>>
	<div class="loader"></div>
	<!-- header of the page -->
	<header id="header" class="container-fluid">
		<div class="row">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<!-- navbar header -->
						<div class="navbar-header">
							<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<?php if(is_front_page()) { ?>
							<!-- page logo -->
							<div id="vdm-logo" class="logo">
								<a href="/">
									<?php if(is_front_page()) : ?>
									<h1>Espiritu - Private Luxury Residential Resort - Los Cabos, Mexico</h1>
									<?php endif; ?>
									<img width="149" id="logo-large" src="<?php bloginfo('template_directory'); ?>/images/logo_espiritu.png" alt="Espiritu del Mar" class="img-responsive">
									<img width="99" id="logo-small" src="<?php bloginfo('template_directory'); ?>/images/logo_espiritu_mobile.png" alt="Espiritu del Mar" class="img-responsive">
								</a>
							</div>
							<?php } else { ?>
							<!-- page logo -->
							<div id="vdm-logo" class="logo">
								<a href="/">
									<img width="99" src="<?php bloginfo('template_directory'); ?>/images/logo_espiritu_mobile.png" alt="Espiritu del Mar" class="img-responsive">
								</a>
							</div>
							<?php } ?>
						</div>
						<!-- main navigation of the page -->
						<nav class="collapse navbar-collapse">
							<ul class="nav navbar-nav">
								<li>
									<a href="#" class="dropdown-toggle" id="homes-dropdown" data-toggle="dropdown">Homes</a>
									<ul class="dropdown-menu text-center" role="menu" aria-labelledby="homes-dropdown">
										<li>
											<span class="title text-uppercase">Neighborhoods</span>
											<ul class="list-unstyled">
												<?php
												 $hoods = get_terms("neighborhood", array( "hide_empty" => 0 ));
												 	foreach ($hoods as $hood) {
													 	echo '<li><a href="'.get_term_link($hood).'" title="'.$hood->name.'">'.$hood->name.'</a></li>';
													 }
												?>
											</ul>
										</li>
										<li>
											<span class="title text-uppercase">Real Estate</span>
											<ul class="list-unstyled">
												<?php
												 $status = get_terms("status", array( "hide_empty" => 0 ));
												 	foreach ($status as $status) {
													 	echo '<li><a href="'.get_term_link($status).'" title="'.$status->name.'">'.$status->name.'</a></li>';
													}
												?>
											</ul>
										</li>
										<li>
											<span class="title text-uppercase">Maps</span>
											<ul class="list-unstyled">
												<!-- <li><a href="/resources/maps/">Property maps</a></li> -->
												<li><a href="/resources/master-plans/">Master Plans</a></li>
											</ul>
										</li>
										<li>
											<span class="title text-uppercase">Contact Info</span>
											<ul class="list-unstyled">
												<li><a href="/contact/sales/">Sales</a></li>
												<li><a href="/contact/brokers/">Broker Registration</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li>
									<a href="#" class="dropdown-toggle" id="lifestyle-dropdown" data-toggle="dropdown">Lifestyle</a>
									<ul class="dropdown-menu text-center" role="menu" aria-labelledby="lifestyle-dropdown">
										<li>
											<span class="title text-uppercase">Amenities</span>
											<ul class="list-unstyled">
												<?php $amenities = new WP_Query("post_type=amenity&posts_per_page=99&post_parent=0"); while($amenities->have_posts()) : $amenities->the_post();?>
												<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
												<?php endwhile; ?>
												<?php wp_reset_query(); ?>
											</ul>
										</li>
										<li>
											<span class="title text-uppercase">Services</span>
											<ul class="list-unstyled">
												<?php $services = new WP_Query("post_type=service&posts_per_page=99&post_parent=0"); while($services->have_posts()) : $services->the_post();?>
												<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
												<?php endwhile; ?>
												<?php wp_reset_query(); ?>
											</ul>
										</li>
									</ul>
								</li>
								<li>
									<a href="#" class="dropdown-toggle" id="community-dropdown" data-toggle="dropdown">Community</a>
									<ul class="dropdown-menu text-center" role="menu" aria-labelledby="community-dropdown">
										<li>
											<span class="title text-uppercase">Vision</span>
											<ul class="list-unstyled">
												<li><a href="/vision/vision-history/">Vision &amp; History</a></li>
												<li><a href="/vision/philanthropy/">Philanthropy</a></li>
												<li><a href="/vision/investors/">Investors</a></li>
												<li><a href="/vision/management/">Management Team</a></li>
												<li><a href="/vision/architecture/">Architecture</a></li>
												<li><a href="/vision/quality-promise/">Quality Promise</a></li>
											</ul>
										</li>
										<li>
											<!-- <span class="title text-uppercase">Resources</span>
											<ul class="list-unstyled">
												<li><a href="/resources/brochure/">Brochure</a></li>
												<li><a href="/resources/maps/">Maps</a></li>
											</ul>
											<br /> -->
											<span class="title text-uppercase">Escapes Magazine</span>
											<ul class="list-unstyled">
												<li><a href="/resources/escapes-magazine/">Latest Issue</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li>
									<a href="#" class="dropdown-toggle" id="news-dropdown" data-toggle="dropdown">News</a>
									<ul class="dropdown-menu half text-center" role="menu" aria-labelledby="news-dropdown">
										<li>
											<ul class="list-unstyled">
												<li><a href="/news/">Updates</a></li>
												<li><a href="/press/">In the News</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li>
									<a href="#" class="dropdown-toggle" id="contact-dropdown" data-toggle="dropdown">Contact</a>
									<ul class="dropdown-menu half text-center" role="menu" aria-labelledby="contact-dropdown">
										<li>
											<ul class="list-unstyled">
												<li><a href="/contact/sales/">Sales</a></li>
												<li><a href="/contact/brokers/">Broker Registration</a></li>
												<li><a href="/service/del-mar-escapes/">Vacation Rentals</a></li>
												<li><a href="/contact/media/">Media Contact</a></li>
											</ul>
										</li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</header>
	<!-- main container of all the page elements -->
	<div id="wrapper">
		<div class="w1">
