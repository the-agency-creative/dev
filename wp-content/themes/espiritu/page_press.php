<?php

/*
Template Name: Press
*/

get_header();

?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<!-- feature block -->
			<section class="container-fluid feature-block">
				<div class="row">
					<?php
						$vdm_page_header_image_obj = get_field('vdm_page_header_image');
						$vdm_page_header_image_size = 'page-header-image';
						$vdm_page_header_image = $vdm_page_header_image_obj['sizes'][$vdm_page_header_image_size];
					?>
					<img src="<?php echo $vdm_page_header_image; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" class="img-responsive">
					<div class="carousel-caption text-center">
						<div class="container">
							<div class="row col-area">
								<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
									<!-- textarea -->
									<div class="textarea">
										<header class="header">
											<h1><?php the_title(); ?></h1>
										</header>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
			<?php endif; ?>
			<div class="container">
				<?php $page = (get_query_var('paged')) ? get_query_var('paged') : 1; query_posts("post_type=press&paged=$page"); while ( have_posts() ) : the_post() ?>
				<div class="row news-row">
					<div class="col-md-12 space-below">
						<div class="news-image pull-left space-above">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('grid-image'); ?></a>
						</div>
						<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
						<?php the_excerpt(); ?>
						<a class="pull-right" href="<?php the_permalink(); ?>">View Article <i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<?php endwhile; ?>
				<nav>
					<div class="pull-left"><?php previous_posts_link( '&laquo; Previous Entries' ); ?></div>
					<div class="pull-right"><?php next_posts_link( 'Next Entries &raquo;', '' ); ?></div>
				</nav>
			</div>
<?php get_footer(); ?>
