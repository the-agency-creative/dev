<?php

// Template Name: Wide

get_header();

?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<!-- feature block -->
			<?php if(get_field('vdm_hide_page_header_image')) { ?>
			<section class="container page-no-image">
				<div class="row col-area">
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
						<!-- textarea -->
						<div class="textarea">
							<header class="header text-center">
								<h1><?php the_title(); ?></h1>
							</header>
						</div>
					</div>
				</div>
			</section>
			<?php } else { ?>
			<section class="container-fluid feature-block page-image">
				<div class="row">
					<?php
						$vdm_page_header_image_obj = get_field('vdm_page_header_image');
						$vdm_page_header_image_size = 'page-header-image';
						$vdm_page_header_image = $vdm_page_header_image_obj['sizes'][$vdm_page_header_image_size];
					?>
					<img src="<?php echo $vdm_page_header_image; ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" class="img-responsive">
					<div class="carousel-caption text-center">
						<div class="container">
							<div class="row col-area">
								<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
									<!-- textarea -->
									<div class="textarea">
										<header class="header">
											<h1><?php the_title(); ?></h1>
										</header>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php } ?>
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12 space-above">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
			<?php endif; ?>

<?php get_footer(); ?>
