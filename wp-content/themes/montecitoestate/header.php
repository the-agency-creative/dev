<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/normalize.min.css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/main.css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/nav/meanmenu.css">
        <link href='http://fonts.googleapis.com/css?family=Elsie' rel='stylesheet' type='text/css'>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php bloginfo('template_directory'); ?>/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
        <script src="<?php bloginfo('template_directory'); ?>/js/jquery.backstretch.min.js"></script>
        <?php wp_head(); ?>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->

        <header id="header">
        		<div class="logo">
			        <h1><a href="/" title="<?php bloginfo('name'); ?> - The Agency"><?php bloginfo('name'); ?></a></h1>
		        </div>
		        <nav id="nav">
		        	<ul>
		        		<?php 	
					    		wp_nav_menu( 
					    			array( 
					    			      'theme_location' 	=> 'primary-menu',
					    			      'container' => false,
					    			      'menu_class' => '',
					    			      'menu_id' => '',
					    			      'items_wrap' => '%3$s'
					    			     ) 
					    			  ); 
				    			?>
		        	</ul>
		        <div class="clear"></div>
		        </nav>
        </header>
        <div class="clear"></div>