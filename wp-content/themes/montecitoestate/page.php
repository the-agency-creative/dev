<?php get_header(); ?>
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php $bg_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
        <script type="text/javascript">
			$.backstretch("<?php echo $bg_image; ?>");
		</script>
			<div id="wrapper">
				<h2><?php the_title(); ?></h2>
				<?php
					if(function_exists('pronamic_google_maps')) {
						pronamic_google_maps(array(
							'width' => 960 ,
							'height' => 450,
							'label' => 'M'
						));
					}
				?>
				<?php the_content(); ?>
			</div>
		<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		<?php endif; ?>
<?php get_footer(); ?>