<!-- request-block -->
        <section class="section request-block" id="request-form-block">
          <div class="container">
            <div class="inner-holder text-center">
              <div class="intro-text">
                <div class="headline text-uppercase">
                  <h2>Request Information</h2>
                </div>
                <p>Learn more about Mr. C Residences by completing the form below. We’ll be in touch soon.</p>
                <ul class="list-inline info-list">
                  <li class="hidden-sm"><address><i class="fa fa-map-marker"></i> 1224 Beverwil Drive Los Angeles, CA 90035</address></li>
                  <li><a href="tel:4242307801"><i class="fa fa-phone"></i> (424) 230-7801</a></li>
                  <li><a href="mailto:&#114;&#101;&#115;&#105;&#100;&#101;&#110;&#99;&#101;&#115;&#64;&#109;&#114;&#99;&#104;&#111;&#116;&#101;&#108;&#115;&#46;&#99;&#111;&#109;"><i class="fa fa-envelope"></i> &#114;&#101;&#115;&#105;&#100;&#101;&#110;&#99;&#101;&#115;&#64;&#109;&#114;&#99;&#104;&#111;&#116;&#101;&#108;&#115;&#46;&#99;&#111;&#109;</a> </li>
                </ul>
              </div>
              <div class="form-holder">
                <!-- request-form -->
                <iframe src="http://go.theagencyre.com/l/75802/2015-12-10/qvw4d" width="100%" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
              </div>
            </div>
          </div>
        <a href="#request-form-block" data-target="#request-form-block" data-target-top="#main" id="request-link" class="anchor"> <span class="fa fa-arrow-left"></span> <span class="text">Request Information</span></a>
        </section>
      </main>
    </div>
    <!-- footer of the page -->
    <div id="footer">
      <div class="container">
        <div class="footer-holder">
          <div class="row">
            <div class="col-xs-4 col-sm-2 col-md-1">
              <!-- footer-logo -->
              <div class="footer-logo">
                <a href="#"><img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/images/footer-logo.png" height="150" width="59" alt="Mr.C Residences"></a>
              </div>
            </div>
            <div class="col-xs-8 col-sm-5 col-md-6">
              <div class="main-block">
                <div class="row">
                  <div class="col-xs-6 col-sm-4 col-md-offset-1">
                    <ul class="footer-list list-unstyled">
                      <li><a href="<?php echo home_url(); ?>">Overview</a></li>
                      <li><a href="<?php echo home_url(); ?>/vision/">Vision</a></li>
                      <li><a href="<?php echo home_url(); ?>/lifestyle/">Lifestyle</a></li>
                    </ul>
                  </div>
                  <div class="col-xs-6 col-sm-4">
                    <ul class="footer-list list-unstyled">
                      <li><a href="<?php echo home_url(); ?>/residences/">Residences</a></li>
                      <li><a href="<?php echo home_url(); ?>/location/">Location</a></li>
                      <li><a href="<?php echo home_url(); ?>/contact/">Contact</a></li>
                    </ul>
                  </div>
                  <div class="col-xs-12 col-sm-3">
                    <!-- social-networks -->
                    <ul class="social-networks list-unstyled">
                      <li class="facebook"><a href="https://www.facebook.com/mrcbeverlyhillshotel/" class="fa fa-facebook"></a></li>
                      <li class="twitter"><a href="https://twitter.com/mrcbeverlyhills" class="fa fa-twitter"></a></li>
                      <li class="instagram"><a href="https://www.instagram.com/mrcbeverlyhills/" class="fa fa-instagram"></a></li>
                    </ul>
                  </div>
                  <div class="col-xs-12  col-md-offset-1">
                    <div class="site-link">
	                  	<a href="<?php echo home_url(); ?>/disclaimer/"><strong>Disclaimer</strong></a>
	                  	&nbsp;&nbsp;|&nbsp;&nbsp;
                    	<a href="http://mrchotels.com"><strong>Visit MrChotels.com</strong></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4 col-md-offset-1">
              <div class="info-block">
                <div class="row">
                  <div class="col-sm-12">
                    <address>
                      <a href="mailto:&#114;&#101;&#115;&#105;&#100;&#101;&#110;&#99;&#101;&#115;&#64;&#109;&#114;&#99;&#104;&#111;&#116;&#101;&#108;&#115;&#46;&#99;&#111;&#109;">RESIDENCES@MRCHOTELS.COM</a><br>
                      1224 Beverwil Drive Los Angeles, CA 90035
                    </address>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <!-- logo-list -->
                    <ul class="logo-list list-inline">
                      <li><a href="http://www.lhw.com/"><img src="<?php bloginfo('template_directory'); ?>/images/footer-logo002.png" height="48" width="108" alt="the leading hotels of the world" target="_blank"></a></li>
                      <li><a href="#wrapper" class="anchor"><img src="<?php bloginfo('template_directory'); ?>/images/footer-icon-home.png" height="19" width="26" alt="image description"></a></li>
                      <li><a href="http://www.theagencyre.com/"><img src="<?php bloginfo('template_directory'); ?>/images/TA_Logo_gray.png" height="50" width="49" alt="TheAgencyRE" target="_blank"></a></li>
                      <!-- <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/footer-logo001.png" height="60" width="87" alt="CIPRIANI"></a></li> -->
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php wp_footer(); ?>
  <script>
      $('iframe').iFrameResize();
      iFrameResize({log:true});
 </script>
 
 <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-66457908-1', 'auto');
	  ga('send', 'pageview');

	</script>

</body>
</html>
