<?php
/* kick the tires */

function kick_the_tires() {
	// stylesheets
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/min/bootstrap.min.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/css/min/main.min.css');

    // scripts
	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/js/min/jquery-1.11.2.min.js', false, null, true);
	wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri() . '/js/min/bootstrap.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('jquery-custom', get_stylesheet_directory_uri() . '/js/min/jquery.main.min.js', array( 'jquery' ), '1.0.0', true);
    wp_enqueue_script('iframe-window', get_stylesheet_directory_uri() . '/js/min/iframeResizer.contentWindow.min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('iframe-resize', get_stylesheet_directory_uri() . '/js/min/iframeResizer.min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('wow', get_stylesheet_directory_uri() . '/js/min/wow.min.js', array(), '1.0.0', true);

}
add_action( 'wp_enqueue_scripts', 'kick_the_tires' );

/* light the fires */

function light_the_fires() { ?>

<script>
wow = new WOW(
		{
			mobile: false
		})
wow.init();
</script>

<?php }
//add to wp_footer
add_action( 'wp_footer', 'light_the_fires', 100 );

// set up our sitewide image sizes

add_theme_support( 'post-thumbnails' );
add_image_size('full-width', 1920); // full width image
add_image_size('page-header', 1920, 740, true); // carousel image
add_image_size('page-footer', 1920, 1050, true); // carousel image
add_image_size('address-logo', 756, 140, true); // address logo
add_image_size('residence-thumb', 654, 400, true); // residence thumb
add_image_size('home-feature', 1120, 740, true); // home feature
add_image_size('location-square', 700, 700, true); // location square
add_image_size('location-large', 1612, 1260, true); // location large
add_image_size('residence-more', 530, 386, true); // location large

// create custom post types

function residence_posttype() {
    register_post_type( 'residence',
        array(
            'labels' => array(
                'name' => __( 'Residences' ),
                'singular_name' => __( 'Residence' ),
                'add_new' => __( 'Add New Residence' ),
                'add_new_item' => __( 'Add New Residence' ),
                'edit_item' => __( 'Edit Residence' ),
                'new_item' => __( 'Add New Residence' ),
                'view_item' => __( 'View Residence' ),
                'search_items' => __( 'Search Residences' ),
                'not_found' => __( 'No Residences found' ),
                'not_found_in_trash' => __( 'No Residences found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'residence', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}
add_action( 'init', 'residence_posttype' );

//add the page slug to body_class()
function add_slug_body_class( $classes ) {
	global $post;
		if ( isset( $post ) ) {
			$classes[] = $post->post_name;
		}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

function page_class_name($classes) {
	// add 'homepage' to the $classes array
	if( is_front_page() ) $classes[] = 'homepage';
	// return the $classes array
	return $classes;
}
add_filter('body_class','page_class_name');

// STOP THE MADNESS
function change_css_hide_wpmu_dash()
{
    echo '<style type="text/css"> .wp-admin div.error, #cpt_info_box, .term-description-wrap {display: none !important;}</style>';
}
add_action('all_admin_notices', 'change_css_hide_wpmu_dash');class WPMUDEV_Update_Notifications {}

if( function_exists('acf_add_options_page') ) {

	$page = acf_add_options_page(array(
		'page_title' 	=> 'Home Page Details',
		'menu_title' 	=> 'Home Page',
		'menu_slug' 	=> 'home-page-details',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));

	$footer_page = acf_add_options_page(array(
		'page_title' 	=> 'Footer Details',
		'menu_title' 	=> 'Footer',
		'menu_slug' 	=> 'footer-details',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
}

register_nav_menus( array(
	'main-menu' => 'Main Nav',
) );

// add .active along with .current-menu-item
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}
?>
