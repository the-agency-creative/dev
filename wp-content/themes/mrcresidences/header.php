<!DOCTYPE html>
<html>
<head>
  <!-- set the encoding of your site -->
  <meta charset="utf-8">
  <!-- set the viewport width and initial-scale on mobile devices -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php wp_title(''); ?></title>
  <!-- include font-awesome icons -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="shortcut icon" href="ico/favicon.png">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <div class="main-holder">
    <!-- main container of all the page elements -->
    <div id="wrapper">
      <!-- header of the page -->
      <header id="header">
        <div class="container-fluid">
          <!-- page logo -->
          <?php if(is_front_page()) { ?>
          <a href="<?php echo home_url(); ?>" class="navbar-brand">
            <img src="<?php bloginfo('template_directory'); ?>/images/logo.png" height="142" width="56" alt="Mr. C Residences" class="for-static hidden-xs">
            <img src="<?php bloginfo('template_directory'); ?>/images/footer-logo.png" height="150" width="59" alt="Mr. C Residences" class="for-static visible-xs">
            <img src="<?php bloginfo('template_directory'); ?>/images/logo-alt.png" height="25" width="50" alt="Mr. C Residences" class="for-fixed">
          </a>
          <?php } else { ?>
          <a href="<?php echo home_url(); ?>" class="navbar-brand">
            <img src="<?php bloginfo('template_directory'); ?>/images/footer-logo.png" height="150" width="59" alt="Mr. C Residences" class="for-static">
            <img src="<?php bloginfo('template_directory'); ?>/images/logo-alt.png" height="25" width="50" alt="Mr. C Residences" class="for-fixed">
          </a>
          <?php } ?>
          <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-2">
              <div class="info clearfix">
                <!-- navbar -->
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="tel:4242307801">(424) 230-7801</a></li>
                  <li><a href="mailto:&#114;&#101;&#115;&#105;&#100;&#101;&#110;&#099;&#101;&#115;&#064;&#109;&#114;&#099;&#104;&#111;&#116;&#101;&#108;&#115;&#046;&#099;&#111;&#109;">RESIDENCES@MRCHOTELS.COM</a></li>
                </ul>
              </div>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar top"></span>
                <span class="icon-bar center"></span>
                <span class="icon-bar bottom"></span>
              </button>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="social-networks visible-xs list-unstyled">
                  <li><a href="#" class="fa fa-facebook"></a></li>
                  <li><a href="#" class="fa fa-twitter"></a></li>
                  <li><a href="#" class="fa fa-instagram"></a></li>
                </ul>
                <!-- navbar -->
                <ul class="nav navbar-nav main-nav navbar-right">
	              <?php
	              	wp_nav_menu(
	              		array(
	              			'theme_location' 	=> 'main-menu',
	              			'container' => false,
	              			'menu_class' => '',
	              			'menu_id' => '',
	              			'items_wrap' => '%3$s'
	              		)
	              	);
	              ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </header>
