<?php get_header(); ?>

    <!-- visual of the page -->
    <div class="visual">
	    <style>
		    <?php
	            $hero_carousel_images = get_field('mrc_hero_caro', 'option');
				if( $hero_carousel_images ):
					$pi = 0;
					foreach( $hero_carousel_images as $hero_carousel_image ):
		      			$hero_carousel_image_size = 'full-width';
			  			$hero_carousel_image_url = $hero_carousel_image['sizes'][$hero_carousel_image_size];
			  			$hero_carousel_image_alt = $hero_carousel_image['alt'];
			?>
			.preload-<?php echo $pi; ?> { background: url(<?php echo $hero_carousel_image_url; ?>) no-repeat -9999px -9999px; }
			<?php
					$pi++;
					endforeach;
				endif;
			?>
		</style>
			<?php
	            $hero_carousel_images = get_field('mrc_hero_caro', 'option');
				if( $hero_carousel_images ):
					$pi = 0;
					foreach( $hero_carousel_images as $hero_carousel_image ):
		      			$hero_carousel_image_size = 'full-width';
			  			$hero_carousel_image_url = $hero_carousel_image['sizes'][$hero_carousel_image_size];
			  			$hero_carousel_image_alt = $hero_carousel_image['alt'];
			?>
			<div style="display:none;" class="preload-<?php echo $pi; ?>"></div>
			<?php
					$pi++;
					endforeach;
				endif;
			?>
        <div id="hero-carousel" class="carousel carousel-fade" data-ride="carousel" data-interval="4000">
            <div class="carousel-holder">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                	<?php
			            $hero_carousel_images = get_field('mrc_hero_caro', 'option');
						if( $hero_carousel_images ):
							foreach( $hero_carousel_images as $hero_carousel_image ):
				      			$hero_carousel_image_size = 'full-width';
					  			$hero_carousel_image_url = $hero_carousel_image['sizes'][$hero_carousel_image_size];
					  			$hero_carousel_image_alt = $hero_carousel_image['alt'];
					?>
                    <div class="item">
                        <div class="bg-stretch">
                            <span data-srcset="<?php echo $hero_carousel_image_url; ?>"></span>
                        </div>
                    </div>
                    <?php
							endforeach;
						endif;
					?>
                </div>
            </div>
        </div>
        <div class="jumbotron text-center">
            <div class="container">
                <p>
                    <?php the_field('mrc_home_hero_text', 'option'); ?>
                </p>
            </div>
        </div>
        <a href="#section" class="anchor btn-next hidden-xs"><span class="fa fa-arrow-down"></span></a>
    </div>
    <!-- contain main informative part of the site -->
    <main id="main" role="main">
        <!-- overview-section -->
        <section class="section overview-section" id="section">
            <div class="container">
                <div class="wow fadeIn headline text-center" data-wow-delay=".5s" data-wow-duration="1.5s">
                    <p>
                        <?php the_field('mrc_home_intro_text', 'option'); ?>
                    </p>
                </div>
                <div class="overview-block same-height-block">
                    <?php
				// check if the repeater field has rows of data
				if( have_rows('mrc_home_feature_rows', 'option') ):
				 	// loop through the rows of data
				    while ( have_rows('mrc_home_feature_rows', 'option') ) : the_row();
				?>
                        <?php if (get_sub_field('row_img_pos', 'option') == 'Left') : ?>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-sm-push-8 col-md-6 col-md-push-6">
                                    <div class="text-block">
                                        <div class="text-holder">
                                            <h2><?php the_sub_field('row_headline', 'option'); ?></h2>
                                            <?php the_sub_field('row_copy', 'option'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="wow slideInLeft col-xs-12 col-sm-8 col-sm-pull-4 col-md-6 col-md-pull-6">
                                    <picture>
                                        <?php
		            $row_image_obj = get_sub_field('row_image', 'option');
		    		$row_image_size = 'home-feature';
					$row_image_url = $row_image_obj['sizes'][$row_image_size];
					$row_image_title = $row_image_obj['title'];
					$row_image_alt = $row_image_obj['alt'];
					$row_image_height = $row_image_obj['height'];
					$row_image_width = $row_image_obj['width'];
				  ?>
                                            <!--[if IE 9]><video style="display: none;"><![endif]-->
                                            <source srcset="<?php echo $row_image_url; ?>" media="(max-width: 767px)">
                                            <source srcset="<?php echo $row_image_url; ?>" media="(max-width: 1024px)">
                                            <!--[if IE 9]></video><![endif]-->
                                            <img src="<?php echo $row_image_url; ?>" height="370" width="560" alt="image description">
                                    </picture>
                                </div>
                            </div>
                            <?php else : ?>


                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-6">
                                        <div class="text-block">
                                            <div class="text-holder">
                                                <h2><?php the_sub_field('row_headline', 'option'); ?></h2>
                                                <?php the_sub_field('row_copy', 'option'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wow slideInRight col-xs-12 col-sm-8 col-md-6">
                                        <picture>
                                            <?php
		            $row_image_obj = get_sub_field('row_image', 'option');
		    		$row_image_size = 'home-feature';
					$row_image_url = $row_image_obj['sizes'][$row_image_size];
					$row_image_title = $row_image_obj['title'];
					$row_image_alt = $row_image_obj['alt'];
					$row_image_height = $row_image_obj['height'];
					$row_image_width = $row_image_obj['width'];
				  ?>
                                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                                <source srcset="<?php echo $row_image_url; ?>" media="(max-width: 767px)">
                                                <source srcset="<?php echo $row_image_url; ?>" media="(max-width: 1024px)">
                                                <!--[if IE 9]></video><![endif]-->
                                                <img src="<?php echo $row_image_url; ?>" height="370" width="560" alt="image description">
                                        </picture>
                                    </div>
                                </div>
                                <?php
	              endif;

	              endwhile;

				else :

				    // no rows found

				endif;
              ?>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="intro-holder text-center">
                            <h2><?php the_field('mrc_home_residence_headline', 'option'); ?></h2>
                            <?php the_field('mrc_home_residence_description', 'option'); ?>
                                <div class="btns-holder">
                                    <a href="<?php echo home_url(); ?>/residences/" class="btn btn-primary">view Residences</a>
                                    <a href="#request-form-block" class="btn btn-default">Request Information</a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- latest-projects -->
        <section class="section latest-carousel">
            <div class="container-fluid">
                <div id="latest-carousel" class="carousel slide" data-ride="carousel" data-interval="3000">
                    <div class="carousel-holder">
                        <!-- Wrapper for slides -->

                        <div id="home-gallery" class="carousel-inner" role="listbox">
                            <?php $residences = new WP_Query("post_type=residence"); while($residences->have_posts()) : $residences->the_post();?>
                                <div class="item">
                                    <?php
			            $residence_images = get_field('mrc_residence_gallery');
						$residence_image_first = $residence_images[0];
		      			$residence_image_size = 'full-width';
			  			$residence_image_url = $residence_image_first['sizes'][$residence_image_size];
			  			$residence_image_alt = $residence_image_first['alt'];
					?>
                                        <picture>
                                            <!--[if IE 9]><video style="display: none;"><![endif]-->
                                            <source srcset="<?php echo $residence_image_url; ?>">
                                            <!--[if IE 9]></video><![endif]-->
                                            <img src="<?php echo $residence_image_url; ?>" alt="<?php the_title(); ?>">
                                        </picture>
                                </div>
                                <?php endwhile; ?>
                                    <?php wp_reset_postdata(); ?>
                        </div>

                        <a class="left carousel-control" href="#latest-carousel" role="button" data-slide="prev">
                            <!-- <span class="fa fa-arrow-left" aria-hidden="true"></span> -->
                            <img src="<?php bloginfo('template_directory'); ?>/images/left-arrow.png" />
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#latest-carousel" role="button" data-slide="next">
                            <!-- <span class="fa fa-arrow-right" aria-hidden="true"></span> -->
                            <img src="<?php bloginfo('template_directory'); ?>/images/right-arrow.png" />
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-8">
                            <div class="control-row">
                                <a class="left carousel-control visible-xs" href="#latest-carousel" role="button" data-slide="prev">
                                    <!-- <span class="fa fa-arrow-left" aria-hidden="true"></span> -->
                                    <img src="<?php bloginfo('template_directory'); ?>/images/left-arrow.png" />
                                    <span class="sr-only">Previous</span>
                                </a>
                                <!-- Indicators
	                <ol class="carousel-indicators">
	                  <?php $residences = new WP_Query("post_type=residence"); $i = 0; $c = 0; while($residences->have_posts()) : $residences->the_post(); $c++;?>
                      <li data-target="#latest-carousel" data-slide-to="<?php echo $i; ?>" class=""><?php echo sprintf( '%02d', $c ); ?></li>
                      <?php $i++; endwhile; ?>
					  <?php wp_reset_postdata(); ?>
	                </ol>  -->
                                <a class="right carousel-control visible-xs" href="#latest-carousel" role="button" data-slide="next">
                                    <!-- <span class="fa fa-arrow-right" aria-hidden="true"></span> -->
                                    <img src="<?php bloginfo('template_directory'); ?>/images/right-arrow.png" />
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-6  col-md-4">
                            <!-- partner-holder -->
                            <div class="partner-holder text-right">
                                <a href="<?php echo home_url(); ?>/residences/" class="btn btn-primary"><span class="hidden-xs">Explore</span><span class="visible-xs-inline">view</span> Residences</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- location section -->
        <section class="section location">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="img-holder">
                            <picture class="visual-img style1 wow slideInLeft">
                                <?php
			            $location_1_image_obj = get_field('mrc_home_location_img_1', 'option');
			    		$location_1_image_size = 'location-square';
						$location_1_image_url = $location_1_image_obj['sizes'][$location_1_image_size];
						$location_1_image_title = $location_1_image_obj['title'];
						$location_1_image_alt = $location_1_image_obj['alt'];
						$location_1_image_height = $location_1_image_obj['height'];
						$location_1_image_width = $location_1_image_obj['width'];
					  ?>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $location_1_image_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img src="<?php echo $location_1_image_url; ?>" alt="<?php echo $location_1_image_alt; ?>">
                            </picture>
                            <picture class="visual-img style2 wow slideInRight">
                                <?php
			            $location_2_image_obj = get_field('mrc_home_location_img_2', 'option');
			    		$location_2_image_size = 'location-square';
						$location_2_image_url = $location_2_image_obj['sizes'][$location_2_image_size];
						$location_2_image_title = $location_2_image_obj['title'];
						$location_2_image_alt = $location_2_image_obj['alt'];
						$location_2_image_height = $location_2_image_obj['height'];
						$location_2_image_width = $location_2_image_obj['width'];
					  ?>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $location_2_image_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img src="<?php echo $location_2_image_url; ?>" alt="<?php echo $location_2_image_alt; ?>">
                            </picture>
                            <picture class="visual-img style3 wow slideInLeft">
                                <?php
			            $location_3_image_obj = get_field('mrc_home_location_img_3', 'option');
			    		$location_3_image_size = 'location-large';
						$location_3_image_url = $location_3_image_obj['sizes'][$location_3_image_size];
						$location_3_image_title = $location_3_image_obj['title'];
						$location_3_image_alt = $location_3_image_obj['alt'];
						$location_3_image_height = $location_3_image_obj['height'];
						$location_3_image_width = $location_3_image_obj['width'];
					  ?>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $location_3_image_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img src="<?php echo $location_3_image_url; ?>" alt="<?php echo $location_3_image_alt; ?>">
                            </picture>
                        </div>
                        <div class="text-holder">
                            <h2><?php the_field('mrc_home_location_headline', 'option'); ?></h2>
                            <?php the_field('mrc_home_location_copy', 'option'); ?>
                                <a href="<?php echo home_url(); ?>/beverly-hills/" class="btn btn-primary">Explore Beverly Hills</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- follow-us -->
        <section class="section follow-us">
            <div class="container-fluid">
                <h2><i class="fa fa-instagram"></i> The Latest @mrcbeverlyhills</h2>
                <div class="visuals-boxes">
                    <div class="row">
                        <?php echo do_shortcode('[instashow source="@mrcbeverlyhills" limit="6" columns="3" gutter="15" scroll_control="false" scrollbar="false" color_gallery_bg="rgba(255, 255, 255, 0.9)" color_gallery_overlay="rgba(105, 47, 37, 0.9)" color_gallery_arrows="rgb(176, 176, 176)" color_gallery_arrows_hover="rgb(255, 255, 255)" color_gallery_scrollbar_slider="rgb(255, 255, 255)" color_popup_overlay="rgba(255, 255, 255, 0.9)" color_popup_username="rgb(105, 47, 37)" color_popup_username_hover="rgba(105, 47, 37, 0.9)" color_popup_instagram_link="rgb(105, 47, 37)" color_popup_instagram_link_hover="rgba(105, 47, 37, 0.9)" color_popup_counters="rgb(255, 255, 255)" color_popup_anchor="rgb(105, 47, 37)" color_popup_anchor_hover="rgba(105, 47, 37, 0.9)" color_popup_controls="rgb(74, 74, 74)" color_popup_controls_hover="rgb(188, 188, 188)" color_popup_mobile_controls="rgb(74, 74, 74)"]'); ?>
                    </div>
                    <a href="https://instagram.com/mrcbeverlyhills" class="btn btn-primary pull-right">Follow Us</a>
                </div>
            </div>
        </section>

        <?php get_footer(); ?>
