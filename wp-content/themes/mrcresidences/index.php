<!DOCTYPE html>
<html>
<head>
  <!-- set the encoding of your site -->
  <meta charset="utf-8">
  <!-- set the viewport width and initial-scale on mobile devices -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Mr.c</title>
  <!-- include font-awesome icons -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- include the bootstrap stylesheet -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <!-- include the site stylesheet -->
  <link rel="stylesheet" href="css/main.css">
</head>
<body class="homepage">
  <div class="main-holder">
    <!-- main container of all the page elements -->
    <div id="wrapper">
      <!-- header of the page -->
      <header id="header">
        <div class="container-fluid">
          <!-- page logo -->
          <a href="#" class="navbar-brand">
            <img src="images/logo.png" height="142" width="56" alt="Mr. C Residences" class="for-static hidden-xs">
            <img src="images/footer-logo.png" height="150" width="59" alt="Mr. C Residences" class="for-static visible-xs">
            <img src="images/logo-alt.png" height="25" width="50" alt="Mr. C Residences" class="for-fixed">
          </a>
          <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-2">
              <div class="info clearfix">
                <!-- navbar -->
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="tel:+123456789">(123) 456-7890</a></li>
                  <li><a href="mailto:&#114;&#101;&#115;&#105;&#100;&#101;&#110;&#099;&#101;&#115;&#064;&#109;&#114;&#099;&#104;&#111;&#116;&#101;&#108;&#115;&#046;&#099;&#111;&#109;">&#114;&#101;&#115;&#105;&#100;&#101;&#110;&#099;&#101;&#115;&#064;&#109;&#114;&#099;&#104;&#111;&#116;&#101;&#108;&#115;&#046;&#099;&#111;&#109;</a></li>
                </ul>
              </div>
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar top"></span>
                <span class="icon-bar center"></span>
                <span class="icon-bar bottom"></span>
              </button>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="social-networks visible-xs list-unstyled">
                  <li><a href="#" class="fa fa-facebook"></a></li>
                  <li><a href="#" class="fa fa-twitter"></a></li>
                  <li><a href="#" class="fa fa-instagram"></a></li>
                </ul>
                <!-- navbar -->
                <ul class="nav navbar-nav main-nav navbar-right">
                  <li class="active"><a href="#">Overview</a></li>
                  <li><a href="#">History</a></li>
                  <li><a href="#">Lifestyle</a></li>
                  <li><a href="#">Residences</a></li>
                  <li><a href="#">Location</a></li>
                  <li><a href="#">Contact</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </header>
      <!-- visual of the page -->
      <div class="visual">
        <div class="bg-stretch">
          <span data-srcset="images/intro-01.jpg, images/intro-01-2x.jpg 2x"></span>
        </div>
        <div class="jumbotron text-center">
          <div class="container">
            <p>Mr. C. Residences,<br>a distinct living experience in Beverly Hills.</p>
          </div>
        </div>
        <a href="#section" class="anchor btn-next hidden-xs"><span class="fa fa-arrow-down"></span></a>
      </div>
      <!-- contain main informative part of the site -->
      <main id="main" role="main">
        <!-- overview-section -->
        <section class="section overview-section" id="section">
          <div class="container-fluid">
            <div class="headline text-center">
              <p>A Cipriani living experience like no other, introducing a collection of five distinct residences exclusively at Mr. C Beverly Hills.</p>
            </div>
            <div class="overview-block same-height-block">
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-sm-push-8 col-md-6 col-md-push-6">
                  <div class="text-block">
                    <div class="text-holder">
                      <h2>Exquisitely Crafted Residences</h2>
                      <p>A collection of five modern residences offer the ultimate in California living paired with the world-class amenities and contemporary hospitality, expertly refined by the Cipriani family at Mr. C Beverly Hills.</p>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-sm-pull-4 col-md-6 col-md-pull-6">
                  <picture>
                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                    <source srcset="images/overview-01-mobile.jpg, images/overview-01-mobile-2x.jpg 2x" media="(max-width: 767px)">
                    <source srcset="images/overview-01-tablet.jpg, images/overview-01-tablet-2x.jpg 2x" media="(max-width: 1024px)">
                    <!--[if IE 9]></video><![endif]-->
                    <img src="images/overview-01.jpg" height="370" width="560" alt="image description">
                  </picture>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-6">
                  <div class="text-block">
                    <div class="text-holder">
                      <h2>Renowned Design Duo</h2>
                      <p>Architect Ray Kappe and designer Marcello Pozzi have envisioned the latest in contemporary architecture with sophisticated design that melds indoor and outdoor living using elements of glass, steel, redwoodand teak.</p>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-6">
                  <picture>
                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                    <source srcset="images/overview-02-mobile.jpg, images/overview-02-mobile-2x.jpg 2x" media="(max-width: 767px)">
                    <source srcset="images/overview-02-tablet.jpg, images/overview-02-tablet-2x.jpg 2x" media="(max-width: 1024px)">
                    <!--[if IE 9]></video><![endif]-->
                    <img src="images/overview-02.jpg" height="370" width="560" alt="image description">
                  </picture>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-sm-push-8 col-md-6 col-md-push-6">
                  <div class="text-block">
                    <div class="text-holder">
                      <h2>Unparalleled Service</h2>
                      <p>Four generations of Cipriani bring renowned, white glove service and world-class amenities to your doorstep. Every detail is in place, and every desire is skillfully met for an exemplary living experience.</p>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-sm-pull-4 col-md-6 col-md-pull-6">
                  <picture>
                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                    <source srcset="images/overview-03-mobile.jpg, images/overview-03-mobile-2x.jpg 2x" media="(max-width: 767px)">
                    <source srcset="images/overview-03-tablet.jpg, images/overview-03-tablet-2x.jpg 2x" media="(max-width: 1024px)">
                    <!--[if IE 9]></video><![endif]-->
                    <img src="images/overview-03.jpg" height="370" width="559" alt="image description">
                  </picture>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                  <div class="intro-holder text-center">
                      <h2>Five Residences, One Unparalleled Living Experience</h2>
                      <p>Mr. C Residences embodies the essence of Cipriani with five private, exquisitely crafted residences showcasing contemporary design and a level of service in which perfection is the standard.</p>
                      <div class="btns-holder">
                        <a href="#" class="btn btn-primary">view Residences</a>
                        <a href="#" class="btn btn-default">Request Information</a>
                      </div>
                  </div>
                </div>
              </div>
          </div>
        </section>
        <!-- latest-projects -->
        <section class="section latest-carousel">
          <div class="container-fluid">
            <div id="latest-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
              <div class="carousel-holder">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                  <div class="item active">
                    <picture>
                      <!--[if IE 9]><video style="display: none;"><![endif]-->
                      <source srcset="images/slide-01.jpg, images/slide-01-2x.jpg 2x">
                      <!--[if IE 9]></video><![endif]-->
                      <img src="slide-01.jpg" alt="image description">
                    </picture>
                  </div>
                  <div class="item">
                    <picture>
                      <!--[if IE 9]><video style="display: none;"><![endif]-->
                      <source srcset="images/slide-01.jpg, images/slide-01-2x.jpg 2x">
                      <!--[if IE 9]></video><![endif]-->
                      <img src="slide-01.jpg" alt="image description">
                    </picture>
                  </div>
                  <div class="item">
                    <picture>
                      <!--[if IE 9]><video style="display: none;"><![endif]-->
                      <source srcset="images/slide-01.jpg, images/slide-01-2x.jpg 2x">
                      <!--[if IE 9]></video><![endif]-->
                      <img src="slide-01.jpg" alt="image description">
                    </picture>
                  </div>
                  <div class="item">
                    <picture>
                      <!--[if IE 9]><video style="display: none;"><![endif]-->
                      <source srcset="images/slide-01.jpg, images/slide-01-2x.jpg 2x">
                      <!--[if IE 9]></video><![endif]-->
                      <img src="slide-01.jpg" alt="image description">
                    </picture>
                  </div>
                  <div class="item">
                    <picture>
                      <!--[if IE 9]><video style="display: none;"><![endif]-->
                      <source srcset="images/slide-01.jpg, images/slide-01-2x.jpg 2x">
                      <!--[if IE 9]></video><![endif]-->
                      <img src="slide-01.jpg" alt="image description">
                    </picture>
                  </div>
                </div>
                <a class="left carousel-control" href="#latest-carousel" role="button" data-slide="prev">
                  <span class="fa fa-arrow-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#latest-carousel" role="button" data-slide="next">
                  <span class="fa fa-arrow-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
              <div class="row">
                <div class="col-sm-6 col-md-8">
                  <div class="control-row">
                    <a class="left carousel-control visible-xs" href="#latest-carousel" role="button" data-slide="prev">
                      <span class="fa fa-arrow-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                      <li data-target="#latest-carousel" data-slide-to="0" class="active">01</li>
                      <li data-target="#latest-carousel" data-slide-to="1">02</li>
                      <li data-target="#latest-carousel" data-slide-to="2">03</li>
                      <li data-target="#latest-carousel" data-slide-to="3">04</li>
                      <li data-target="#latest-carousel" data-slide-to="4">05</li>
                    </ol>
                      <a class="right carousel-control visible-xs" href="#latest-carousel" role="button" data-slide="next">
                    <span class="fa fa-arrow-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                  </div>
                </div>
                <div class="col-sm-6  col-md-4">
                  <!-- partner-holder -->
                  <div class="partner-holder text-center">
                    <a href="#" class="partner-logo">
                      <img src="images/logo-beverwill.png" height="60" width="327" alt="200 Beverwill Drive Beverly Hills">
                    </a>
                    <a href="#" class="btn btn-primary"><span class="hidden-xs">Explore</span><span class="visible-xs-inline">view</span> Residence</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- location section -->
        <section class="section location">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4 col-sm-6">
                <div class="img-holder">
                    <picture class="visual-img style1">
                      <!--[if IE 9]><video style="display: none;"><![endif]-->
                      <source srcset="images/location-01.jpg, images/location-01-2x.jpg 2x">
                      <!--[if IE 9]></video><![endif]-->
                      <img src="location-01.jpg" alt="image description">
                    </picture>
                    <picture class="visual-img style2">
                      <!--[if IE 9]><video style="display: none;"><![endif]-->
                      <source srcset="images/location-02.jpg, images/location-02-2x.jpg 2x">
                      <!--[if IE 9]></video><![endif]-->
                      <img src="location-02.jpg" alt="image description">
                    </picture>
                    <picture class="visual-img style3">
                      <!--[if IE 9]><video style="display: none;"><![endif]-->
                      <source srcset="images/location-03.jpg, images/location-03-2x.jpg 2x">
                      <!--[if IE 9]></video><![endif]-->
                      <img src="location-03.jpg" alt="image description">
                    </picture>
                </div>
                <div class="text-holder">
                  <h2>AN IDEAL LOCATION</h2>
                  <p>Mr. C Residences embody the beauty of modern city living with the best of Beverly Hills and Los Angeles within close reach. The location is perfectly central, minutes away from Rodeo Drive, Century City, Sunset Strip, Downtown Los Angeles, and the beaches of Santa Monica and Malibu.</p>
                  <a href="#" class="btn btn-primary">Explore Beverly Hills</a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- follow-us -->
        <section class="section follow-us">
          <div class="container-fluid">
            <h2><i class="fa fa-instagram"></i> The Latest @mrcbeverlyhills</h2>
            <div class="visuals-boxes">
              <div class="row">
                <div class="col-sm-4 col-xs-6">
                  <!-- box -->
                  <a href="#" class="box">
                    <img src="images/fake-square.png" height="400" width="400" alt="image description" class="fake-square">
                    <div class="bg-stretch">
                      <span data-srcset="images/follow-01.jpg, images/follow-01-2x.jpg 2x"></span>
                    </div>
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <!-- box -->
                  <a href="#" class="box">
                  <img src="images/fake-square.png" height="400" width="400" alt="image description" class="fake-square">
                    <div class="bg-stretch">
                      <span data-srcset="images/follow-02.jpg, images/follow-02-2x.jpg 2x"></span>
                    </div>
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <!-- box -->
                  <a href="#" class="box">
                  <img src="images/fake-square.png" height="400" width="400" alt="image description" class="fake-square">
                    <div class="bg-stretch">
                       <span data-srcset="images/follow-03.jpg, images/follow-03-2x.jpg 2x"></span>
                    </div>
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <!-- box -->
                  <a href="#" class="box">
                  <img src="images/fake-square.png" height="400" width="400" alt="image description" class="fake-square">
                    <div class="bg-stretch">
                       <span data-srcset="images/follow-04.jpg, images/follow-04-2x.jpg 2x"></span>
                    </div>
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <!-- box -->
                  <a href="#" class="box">
                  <img src="images/fake-square.png" height="400" width="400" alt="image description" class="fake-square">
                    <div class="bg-stretch">
                      <span data-srcset="images/follow-05.jpg, images/follow-05-2x.jpg 2x"></span>
                    </div>
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <!-- box -->
                  <a href="#" class="box">
                  <img src="images/fake-square.png" height="400" width="400" alt="image description" class="fake-square">
                    <div class="bg-stretch">
                       <span data-srcset="images/follow-06.jpg, images/follow-06-2x.jpg 2x"></span>
                    </div>
                  </a>
                </div>
              </div>
              <a href="#" class="btn btn-primary pull-right">Follow Us</a>
            </div>
          </div>
        </section>
        <!-- request-block -->
        <section class="section request-block" id="request-form-block">
          <div class="container-fluid">
            <div class="inner-holder text-center">
              <div class="intro-text">
                <div class="headline text-uppercase">
                  <h2>Request Information</h2>
                </div>
                <p>Learn more about Mr. C Residences by completing the form below. We’ll be in touch soon.</p>
                <ul class="list-inline info-list">
                  <li class="hidden-sm"><address><i class="fa fa-map-marker"></i> 1224 Beverwil Drive Los Angeles, CA 90035</address></li>
                  <li><a href="tel:1234567890"><i class="fa fa-phone"></i> (123) 456-7890</a></li>
                  <li><a href="mailto:&#114;&#101;&#115;&#105;&#100;&#101;&#110;&#99;&#101;&#115;&#64;&#109;&#114;&#99;&#104;&#111;&#116;&#101;&#108;&#115;&#46;&#99;&#111;&#109;"><i class="fa fa-envelope"></i> &#114;&#101;&#115;&#105;&#100;&#101;&#110;&#99;&#101;&#115;&#64;&#109;&#114;&#99;&#104;&#111;&#116;&#101;&#108;&#115;&#46;&#99;&#111;&#109;</a> </li>
                </ul>
              </div>
              <div class="form-holder">
                <!-- request-form -->
                <form class="request-form">
                  <div class="row">
                    <div class="col-sm-6 col-md-4">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="First Name">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Last Name">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Zip Code">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Phone Number">
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                      <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email Address">
                      </div>
                    </div>
                  </div>
                  <div class="button-holder">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
        <a href="#request-form-block" data-target="#request-form-block" data-target-top="#main" id="request-link" class="anchor"> <span class="fa fa-arrow-left"></span> <span class="text">Request Information</span></a>
      </main>
    </div>
    <!-- footer of the page -->
    <div id="footer">
      <div class="container-fluid">
        <div class="footer-holder">
          <div class="row">
            <div class="col-xs-4 col-sm-2 col-md-1">
              <!-- footer-logo -->
              <div class="footer-logo">
                <a href="#"><img class="img-responsive" src="images/footer-logo.png" height="150" width="59" alt="Mr.C Residences"></a>
              </div>
            </div>
            <div class="col-xs-8 col-sm-5 col-md-6">
              <div class="main-block">
                <div class="row">
                  <div class="col-xs-6 col-sm-4 col-md-offset-1">
                    <ul class="footer-list list-unstyled">
                      <li><a href="#">Overview</a></li>
                      <li><a href="#">History</a></li>
                      <li><a href="#">Lifestyle</a></li>
                    </ul>
                  </div>
                  <div class="col-xs-6 col-sm-4">
                    <ul class="footer-list list-unstyled">
                      <li><a href="#">Residences</a></li>
                      <li><a href="#">Location</a></li>
                      <li><a href="#">Contact</a></li>
                    </ul>
                  </div>
                  <div class="col-xs-12 col-sm-3">
                    <!-- social-networks -->
                    <ul class="social-networks list-unstyled">
                      <li class="facebook"><a href="#" class="fa fa-facebook"></a></li>
                      <li class="twitter"><a href="#" class="fa fa-twitter"></a></li>
                      <li class="instagram"><a href="#" class="fa fa-instagram"></a></li>
                    </ul>
                  </div>
                  <div class="col-xs-12  col-md-offset-1">
                    <div class="site-link">
                      <a href="#"><strong>Visit MrChotels.com</strong></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4 col-md-offset-1">
              <div class="info-block">
                <div class="row">
                  <div class="col-sm-12">
                    <address>
                      <a href="mailto:&#114;&#101;&#115;&#105;&#100;&#101;&#110;&#99;&#101;&#115;&#64;&#109;&#114;&#99;&#104;&#111;&#116;&#101;&#108;&#115;&#46;&#99;&#111;&#109;">&#114;&#101;&#115;&#105;&#100;&#101;&#110;&#99;&#101;&#115;&#64;&#109;&#114;&#99;&#104;&#111;&#116;&#101;&#108;&#115;&#46;&#99;&#111;&#109;</a><br>
                      1224 Beverwil Drive Los Angeles, CA 90035
                    </address>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-12">
                    <!-- logo-list -->
                    <ul class="logo-list list-inline">
                      <li><a href="#"><img src="images/footer-logo002.png" height="48" width="108" alt="the leading hotels of the world"></a></li>
                      <li><a href="#wrapper" class="anchor"><img src="images/footer-icon-home.png" height="19" width="26" alt="image description"></a></li>
                      <li><a href="#"><img src="images/footer-logo001.png" height="60" width="87" alt="CIPRIANI"></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>
  <!-- include custom JavaScript -->
  <script src="js/jquery.main.js"></script>
</body>
</html>
