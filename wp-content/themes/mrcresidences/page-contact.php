<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<!-- intro of the page -->
      <div class="intro-visual">
        <div class="container-fluid">
          <div class="img-holder">
            <?php
	            $page_header_obj = get_field('mrc_page_header_image');
	    		$page_header_size = 'page-header';
				$page_header_url = $page_header_obj['sizes'][$page_header_size];
				$page_header_title = $page_header_obj['title'];
				$page_header_alt = $page_header_obj['alt'];
				$page_header_height = $page_header_obj['height'];
				$page_header_width = $page_header_obj['width'];
			?>
			<picture>
              <!--[if IE 9]><video style="display: none;"><![endif]-->
              <source srcset="<?php echo $page_header_url; ?>">
              <!--[if IE 9]></video><![endif]-->
              <img src="<?php echo $page_header_url; ?>" alt="<?php echo $page_header_alt; ?>">
            </picture>
          </div>
        </div>
      </div>
      <!-- contain main informative part of the site -->
      <main id="main" role="main">
        <section class="section amenities-section">
          <div class="container-fluid">
            <div class="intro-block wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.5s">
              <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <div class="intro-text">
	                <h2 class="text-center"><?php the_field('mrc_page_headline'); ?></h2>
	                <div class="text-center">
                    	<?php the_field('mrc_page_intro'); ?>
	                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div class="section map-placeholder">
	        <?php echo(do_shortcode('[gjmaps map_id="1" zoom="13"]')); ?>
        </div>
    <?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>

<?php get_footer(); ?>
