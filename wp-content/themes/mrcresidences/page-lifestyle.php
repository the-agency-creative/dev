<?php
/*
Template Name: Lifestyle
*/
?>

<?php get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<!-- intro of the page -->
      <div class="intro-visual">
        <div class="container-fluid">
          <div class="img-holder">
            <?php
	            $page_header_obj = get_field('mrc_page_header_image');
	    		$page_header_size = 'page-header';
				$page_header_url = $page_header_obj['sizes'][$page_header_size];
				$page_header_title = $page_header_obj['title'];
				$page_header_alt = $page_header_obj['alt'];
				$page_header_height = $page_header_obj['height'];
				$page_header_width = $page_header_obj['width'];
			?>
			<picture>
              <!--[if IE 9]><video style="display: none;"><![endif]-->
              <source srcset="<?php echo $page_header_url; ?>">
              <!--[if IE 9]></video><![endif]-->
              <img src="<?php echo $page_header_url; ?>" alt="<?php echo $page_header_alt; ?>">
            </picture>
          </div>
        </div>
      </div>
      <!-- contain main informative part of the site -->
      <main id="main" role="main">
        <!-- amenities-section -->
        <section class="section amenities-section">
          <div class="container-fluid">
           
           
            <div class="intro-block wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.5s">
              <h2><?php the_field('mrc_page_headline'); ?></h2>
              <div class="row">
                <div class="col-md-8 col-md-offset-2">
                  <div class="intro-text">
                    <?php the_field('mrc_page_intro'); ?>
                  </div>
                </div>
              </div>
            </div>
            
            
            <div class="amenities-block">
              <div class="row">
                <div class="col-sm-6 col-md-7">
                  <div class="text-box wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.5s">
                    <h2><?php the_field('mrc_amenities_headline'); ?></h2>
                    <?php the_field('mrc_amenities_description'); ?>
                  </div>
                </div>
                <?php
				// check if the repeater field has rows of data
				if( have_rows('mrc_amenities_blocks') ):
				 	// loop through the rows of data
				 	$counter = 1;
				    while ( have_rows('mrc_amenities_blocks') ) : the_row();
				?>
                <div class="col-sm-6 col-md-4<?php if($counter == 1) {?> col-md-offset-1<?php } ?>">
                  <!-- visual-box -->
                  <div class="visual-box">
                    <div class="bg-stretch">
	                    <?php
				           $amenity_image_obj = get_sub_field('amenity_image', 'option');
						   $amenity_image_size = 'location-square';
						   $amenity_image_url = $amenity_image_obj['sizes'][$amenity_image_size];
						   $amenity_image_title = $amenity_image_obj['title'];
						   $amenity_image_alt = $amenity_image_obj['alt'];
						   $amenity_image_height = $amenity_image_obj['height'];
						   $amenity_image_width = $amenity_image_obj['width'];
						?>
                      <span data-srcset="<?php echo $amenity_image_url; ?>"></span>
                    </div>
                    <div class="overlay-box">
                      <h3><?php the_sub_field('amenity_headline'); ?></h3>
                      <div class="text">
                        <?php the_sub_field('amenity_description'); ?>
                      </div>
                    </div>
                    <img class="fake-square" src="<?php bloginfo('template_directory'); ?>/images/fake-square.png" height="400" width="400" alt="fake square">
                  </div>
                </div>
                <?php if($counter == 1) {?><div class="clearfix hidden-xs-block"></div><?php } ?>
                <?php
	                $counter++;
				    endwhile;
				else :
				    // no rows found
				endif;
				?>
              </div>
            </div>
          </div>
        </section>
        <!-- gallery-section -->
		<section class="section gallery-section">
			<div class="container">
				<div id="carousel1" class="carousel slide" data-ride="carousel" data-interval="false">
					<!-- Wrapper for slides -->
					<div class="carousel-holder right-border">
						<div class="carousel-inner" role="listbox">
							<?php
					            $footer_gallery_images = get_field('mrc_page_footer_gallery');
								if( $footer_gallery_images ):
									foreach( $footer_gallery_images as $footer_gallery_image ):
						      			$footer_gallery_image_size = 'page-footer';
							  			$footer_gallery_image_url = $footer_gallery_image['sizes'][$footer_gallery_image_size];
							  			$footer_gallery_image_alt = $footer_gallery_image['alt'];
							?>
							<div class="item">
								<picture>
			                      <!--[if IE 9]><video style="display: none;"><![endif]-->
			                      <source srcset="<?php echo $footer_gallery_image_url; ?>">
			                      <!--[if IE 9]></video><![endif]-->
			                      <img src="<?php echo $footer_gallery_image_url; ?>" alt="<?php echo $footer_gallery_image_alt; ?>">
			                    </picture>
							</div>
							<?php
									endforeach;
								endif;
							?>
						</div>
						<a class="left carousel-control" href="#carousel1" role="button" data-slide="prev">
		                  <!-- <span class="fa fa-arrow-left" aria-hidden="true"></span> -->
		                  <img src="<?php bloginfo('template_directory'); ?>/images/left-arrow.png" />
		                  <span class="sr-only">Previous</span>
		                </a>
		                <a class="right carousel-control" href="#carousel1" role="button" data-slide="next">
		                  <!-- <span class="fa fa-arrow-right" aria-hidden="true"></span> -->
			              <img src="<?php bloginfo('template_directory'); ?>/images/right-arrow.png" />
		                  <span class="sr-only">Next</span>
		                </a>
					</div>
					<!-- <div class="control-row">
						<a class="left carousel-control" href="#carousel1" role="button" data-slide="prev">
							<span class="fa fa-arrow-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<!-- Indicators
						<ol class="carousel-indicators">
							<?php
				            	if( $footer_gallery_images ):
				            		$i = 0;
				            		$c = 0;
									foreach( $footer_gallery_images as $footer_gallery_image ):
									$c++;
						  	?>
						  	<li data-target="#carousel1" data-slide-to="<?php echo $i; ?>"><?php echo sprintf( '%02d', $c ); ?></li>
						  	<?php
									$i++; endforeach;
								endif;
							?>
						</ol>
							<a class="right carousel-control" href="#carousel1" role="button" data-slide="next">
							<span class="fa fa-arrow-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div> -->
				</div>
			</div>
		</section>
	<?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>

<?php get_footer(); ?>
