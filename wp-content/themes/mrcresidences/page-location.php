<?php
/*
Template Name: Location
*/
?>
<?php get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<!-- intro of the page -->
      <div class="intro-visual">
        <div class="container-fluid">
          <div class="img-holder">
            <?php
	            $page_header_obj = get_field('mrc_page_header_image');
	    		$page_header_size = 'page-header';
				$page_header_url = $page_header_obj['sizes'][$page_header_size];
				$page_header_title = $page_header_obj['title'];
				$page_header_alt = $page_header_obj['alt'];
				$page_header_height = $page_header_obj['height'];
				$page_header_width = $page_header_obj['width'];
			?>
			<picture>
              <!--[if IE 9]><video style="display: none;"><![endif]-->
              <source srcset="<?php echo $page_header_url; ?>">
              <!--[if IE 9]></video><![endif]-->
              <img src="<?php echo $page_header_url; ?>" alt="<?php echo $page_header_alt; ?>">
            </picture>
          </div>
        </div>
      </div>
      <!-- contain main informative part of the site -->
      <main id="main" role="main">
        <!-- amenities-section -->
        <section class="section amenities-section">
          <div class="container-fluid">
           
              <div class="intro-block wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.5s">
              <h2><?php the_field('mrc_page_headline'); ?></h2>
              <div class="row">
                <div class="col-md-8 col-md-offset-2">
                  <div class="intro-text">
                    <?php the_field('mrc_page_intro'); ?>
                  </div>
                </div>
              </div>
            </div>
            </div>   
             
         <div class="container">   
            <div class="exclusive-features">
				<h2><?php the_field('mrc_poi_headline'); ?></h2>
				<div class="row">
					<?php
					// check if the repeater field has rows of data
					if( have_rows('mrc_pois') ):
						$i = 0;
					 	// loop through the rows of data
					    while ( have_rows('mrc_pois') ) : the_row();
					    $i++;
					?>
					<div class="col-sm-6 wow fadeIn" data-wow-delay=".5s" data-wow-duration="1s">
						<div class="inner-area">
							<h3><?php the_sub_field('poi_headline'); ?></h3>
							<?php the_sub_field('poi_description'); ?>
						</div>
					</div>
				<?php if($i%2==0) { ?>
				</div>
				<div class="row">
				<?php } ?>
					<?php
					    endwhile;
					else :
					    // no features found
					endif;
					?>
				</div>
			</div>
          </div>
        </section>
        <div class="section map-placeholder">
	        <?php echo(do_shortcode('[gjmaps map_id="1" zoom="13"]')); ?>
        </div>
    <?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>

<?php get_footer(); ?>
