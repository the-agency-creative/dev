<?php
/*
Template Name: Residences
*/
?>

    <?php get_header(); ?>

        <!-- intro of the page -->
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="intro-visual">
                <div class="container-fluid">
                    <div class="img-holder">
                        <?php
				            $page_header_obj = get_field('mrc_page_header_image');
				    		$page_header_size = 'page-header';
							$page_header_url = $page_header_obj['sizes'][$page_header_size];
							$page_header_title = $page_header_obj['title'];
							$page_header_alt = $page_header_obj['alt'];
							$page_header_height = $page_header_obj['height'];
							$page_header_width = $page_header_obj['width'];
						?>
                            <picture>
                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                <source srcset="<?php echo $page_header_url; ?>">
                                <!--[if IE 9]></video><![endif]-->
                                <img src="<?php echo $page_header_url; ?>" alt="<?php echo $page_header_alt; ?>">
                            </picture>
                    </div>
                </div>
            </div>
            <!-- contain main informative part of the site -->
            <main id="main" role="main">
                <!-- collection -->
                <section class="collection section">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="topic wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.5s">

                                    <h2 class="text-center"><?php the_field('mrc_page_headline'); ?></h2>
                                    <?php the_field('mrc_page_intro'); ?>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="residense-boxes">
                            <?php $residences = new WP_Query("post_type=residence"); while($residences->have_posts()) : $residences->the_post();?>
                                <div class="residence">
                                    <div class="img-holder wow slideInLeft">
                                        <?php
							            $residence_images = get_field('mrc_residence_gallery');
										$residence_image_first = $residence_images[0];
						      			$residence_image_size = 'residence-thumb';
							  			$residence_image_url = $residence_image_first['sizes'][$residence_image_size];
							  			$residence_image_alt = $residence_image_first['alt'];
									?>
                                            <picture>
                                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                                <source srcset="<?php echo $residence_image_url; ?>">
                                                <!--[if IE 9]></video><![endif]-->
                                                <img src="<?php echo $residence_image_url; ?>" alt="<?php the_title(); ?>">
                                            </picture>
                                    </div>
                                    <div class="text-holder">
                                        <div class="inner-area">
                                            <?php
								            $address_logo_obj = get_field('mrc_residence_logo');
								    		$address_logo_size = 'address-logo';
											$address_logo_url = $address_logo_obj['sizes'][$address_logo_size];
											$address_logo_title = $address_logo_obj['title'];
											$address_logo_alt = $address_logo_obj['alt'];
											$address_logo_height = $address_logo_obj['height'];
											$address_logo_width = $address_logo_obj['width'];
										  ?>
                                                <img class="logo-img wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.5s" src="<?php echo $address_logo_url ?>" alt="<?php the_title(); ?>" width="327">
                                        </div>
                                        <div class="btn-holder wow slideInRight">
                                            <a href="<?php the_permalink(); ?>" class="btn btn-primary">View Residence</a>
                                        </div>
                                    </div>
                                </div>
                                <?php endwhile; ?>
                                    <?php wp_reset_postdata(); ?>
                        </div>
                        <div class="exclusive-features">
                            <h2 class="wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.5s"><?php the_field('mr_residences_features_headline'); ?></h2>
                            <div class="row">
                                <?php
								// check if the repeater field has rows of data
								if( have_rows('mrc_residences_features') ):
								 	// loop through the rows of data
								    while ( have_rows('mrc_residences_features') ) : the_row();
								?>
                                    <div class="col-sm-6">
                                        <div class="inner-area wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.5s">
                                            <h3><?php the_sub_field('features_headline'); ?></h3>
                                            <?php the_sub_field('features_description'); ?>
                                        </div>
                                    </div>
                                    <?php
								    endwhile;
								else :
								    // no features found
								endif;
								?>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- gallery-section -->
                <section class="section gallery-section">
                    <div class="container">
                        <div id="carousel1" class="carousel slide" data-ride="carousel" data-interval="false">
                            <!-- Wrapper for slides -->
                            <div class="carousel-holder right-border">
                                <div class="carousel-inner" role="listbox">
                                    <?php
							            $footer_gallery_images = get_field('mrc_page_footer_gallery');
										if( $footer_gallery_images ):
											foreach( $footer_gallery_images as $footer_gallery_image ):
								      			$footer_gallery_image_size = 'page-footer';
									  			$footer_gallery_image_url = $footer_gallery_image['sizes'][$footer_gallery_image_size];
									  			$footer_gallery_image_alt = $footer_gallery_image['alt'];
									?>
                                        <div class="item">
                                            <picture>
                                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                                <source srcset="<?php echo $footer_gallery_image_url; ?>">
                                                <!--[if IE 9]></video><![endif]-->
                                                <img src="<?php echo $footer_gallery_image_url; ?>" alt="<?php echo $footer_gallery_image_alt; ?>">
                                            </picture>
                                        </div>
                                        <?php
											endforeach;
										endif;
									?>
                                </div>
                            </div>
                            <!-- <div class="control-row">
								<a class="left carousel-control" href="#carousel1" role="button" data-slide="prev">
									<span class="fa fa-arrow-left" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<!-- Indicators
								<ol class="carousel-indicators">
									<?php
						            	if( $footer_gallery_images ):
						            		$i = 0;
						            		$c = 0;
											foreach( $footer_gallery_images as $footer_gallery_image ):
											$c++;
								  	?>
								  	<li data-target="#carousel1" data-slide-to="<?php echo $i; ?>"><?php echo sprintf( '%02d', $c ); ?></li>
								  	<?php
											$i++; endforeach;
										endif;
									?>
								</ol>
									<a class="right carousel-control" href="#carousel1" role="button" data-slide="next">
									<span class="fa fa-arrow-right" aria-hidden="true"></span>
									<span class="sr-only">Next</span>
								</a>
							</div> -->
                        </div>
                    </div>
                </section>
                <?php endwhile; else: ?>
                    <p>
                        <?php _e('Sorry, no posts matched your criteria.'); ?>
                    </p>
                    <?php endif; ?>

                        <?php get_footer(); ?>
