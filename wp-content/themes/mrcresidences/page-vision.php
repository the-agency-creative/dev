<?php
/*
Template Name: Vision
*/
?>

<?php get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<!-- intro of the page -->
      <div class="intro-visual">
        <div class="container-fluid">
          <div class="img-holder">
            <?php
	            $page_header_obj = get_field('mrc_page_header_image');
	    		$page_header_size = 'page-header';
				$page_header_url = $page_header_obj['sizes'][$page_header_size];
				$page_header_title = $page_header_obj['title'];
				$page_header_alt = $page_header_obj['alt'];
				$page_header_height = $page_header_obj['height'];
				$page_header_width = $page_header_obj['width'];
			?>
			<picture>
              <!--[if IE 9]><video style="display: none;"><![endif]-->
              <source srcset="<?php echo $page_header_url; ?>">
              <!--[if IE 9]></video><![endif]-->
              <img src="<?php echo $page_header_url; ?>" alt="<?php echo $page_header_alt; ?>">
            </picture>
          </div>
        </div>
      </div>
      <!-- contain main informative part of the site -->
      <main id="main" role="main">
        <!-- vision-section -->
        <section class="section vision-section">
          <div class="container-fluid">
           
           
            <div class="intro-block">
              <div class="row">
                <div class="col-md-8 col-md-offset-2">
                  <div class="intro-text wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.5s">
	                <h2><?php the_field('mrc_page_headline'); ?></h2>
                    <?php the_field('mrc_page_intro'); ?>
                  </div>
                </div>
              </div>
            </div>
            
            
            <div class="overview-block same-height-block">
	          <?php
				// check if the repeater field has rows of data
				if( have_rows('mrc_page_feature_rows') ):
				 	// loop through the rows of data
				    while ( have_rows('mrc_page_feature_rows') ) : the_row();
				?>
	          <?php if (get_sub_field('row_img_pos', 'option') == 'Left') : ?>
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-sm-push-8 col-md-6 col-md-push-6">
                  <div class="text-block">
                    <div class="text-holder">
                      <h2><?php the_sub_field('row_headline'); ?></h2>
                      <?php the_sub_field('row_copy'); ?>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-sm-pull-4 col-md-6 col-md-pull-6 wow slideInLeft">
                  <picture>
                  <?php
		            $row_image_obj = get_sub_field('row_image');
		    		$row_image_size = 'home-feature';
					$row_image_url = $row_image_obj['sizes'][$row_image_size];
					$row_image_title = $row_image_obj['title'];
					$row_image_alt = $row_image_obj['alt'];
					$row_image_height = $row_image_obj['height'];
					$row_image_width = $row_image_obj['width'];
				  ?>
                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                    <source srcset="<?php echo $row_image_url; ?>" media="(max-width: 767px)">
                    <source srcset="<?php echo $row_image_url; ?>" media="(max-width: 1024px)">
                    <!--[if IE 9]></video><![endif]-->
                    <img src="<?php echo $row_image_url; ?>" height="370" width="560" alt="image description">
                  </picture>
                </div>
              </div>
              <?php else : ?>
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-6">
                  <div class="text-block">
                    <div class="text-holder">
                      <h2><?php the_sub_field('row_headline'); ?></h2>
                      <?php the_sub_field('row_copy'); ?>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-6 wow slideInRight">
                  <picture>
                  <?php
		            $row_image_obj = get_sub_field('row_image');
		    		$row_image_size = 'home-feature';
					$row_image_url = $row_image_obj['sizes'][$row_image_size];
					$row_image_title = $row_image_obj['title'];
					$row_image_alt = $row_image_obj['alt'];
					$row_image_height = $row_image_obj['height'];
					$row_image_width = $row_image_obj['width'];
				  ?>
                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                    <source srcset="<?php echo $row_image_url; ?>" media="(max-width: 767px)">
                    <source srcset="<?php echo $row_image_url; ?>" media="(max-width: 1024px)">
                    <!--[if IE 9]></video><![endif]-->
                    <img src="<?php echo $row_image_url; ?>" height="370" width="560" alt="image description">
                  </picture>
                </div>
              </div>
              <?php
	              endif;

	              endwhile;

				else :

				    // no rows found

				endif;
              ?>
            </div>
          </div>
        </section>
        <!-- gallery-section -->
		<section class="section gallery-section">
			<div class="container">
				<div id="carousel1" class="carousel slide" data-ride="carousel" data-interval="false">
					<!-- Wrapper for slides -->
					<div class="carousel-holder right-border">
						<div class="carousel-inner" role="listbox">
							<?php
					            $footer_gallery_images = get_field('mrc_page_footer_gallery');
								if( $footer_gallery_images ):
									foreach( $footer_gallery_images as $footer_gallery_image ):
						      			$footer_gallery_image_size = 'page-footer';
							  			$footer_gallery_image_url = $footer_gallery_image['sizes'][$footer_gallery_image_size];
							  			$footer_gallery_image_alt = $footer_gallery_image['alt'];
							?>
							<div class="item">
								<picture>
			                      <!--[if IE 9]><video style="display: none;"><![endif]-->
			                      <source srcset="<?php echo $footer_gallery_image_url; ?>">
			                      <!--[if IE 9]></video><![endif]-->
			                      <img src="<?php echo $footer_gallery_image_url; ?>" alt="<?php echo $footer_gallery_image_alt; ?>">
			                    </picture>
							</div>
							<?php
									endforeach;
								endif;
							?>
						</div>
						<a class="left carousel-control" href="#carousel1" role="button" data-slide="prev">
		                  <!-- <span class="fa fa-arrow-left" aria-hidden="true"></span> -->
		                  <img src="<?php bloginfo('template_directory'); ?>/images/left-arrow.png" />
		                  <span class="sr-only">Previous</span>
		                </a>
		                <a class="right carousel-control" href="#carousel1" role="button" data-slide="next">
		                  <!-- <span class="fa fa-arrow-right" aria-hidden="true"></span> -->
			              <img src="<?php bloginfo('template_directory'); ?>/images/right-arrow.png" />
		                  <span class="sr-only">Next</span>
		                </a>
					</div>
					<!-- <div class="control-row">
						<a class="left carousel-control" href="#carousel1" role="button" data-slide="prev">
							<span class="fa fa-arrow-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<!-- Indicators
						<ol class="carousel-indicators">
							<?php
				            	if( $footer_gallery_images ):
				            		$i = 0;
				            		$c = 0;
									foreach( $footer_gallery_images as $footer_gallery_image ):
									$c++;
						  	?>
						  	<li data-target="#carousel1" data-slide-to="<?php echo $i; ?>"><?php echo sprintf( '%02d', $c ); ?></li>
						  	<?php
									$i++; endforeach;
								endif;
							?>
						</ol>
							<a class="right carousel-control" href="#carousel1" role="button" data-slide="next">
							<span class="fa fa-arrow-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div> -->
				</div>
			</div>
		</section>
	<?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>

<?php get_footer(); ?>
