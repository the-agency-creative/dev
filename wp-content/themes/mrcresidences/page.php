<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <!-- contain main informative part of the site -->
      <main id="main" role="main">
        <section class="section amenities-section">
          <div class="container-fluid">
            <div class="intro-block wow fadeIn" data-wow-delay=".5s" data-wow-duration="1.5s">
              <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <div class="intro-text">
	                <h2 class="text-center"><?php the_title(); ?></h2>
                  </div>
                  <div class="content">
	                  <?php the_content(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    <?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php endif; ?>

<?php get_footer(); ?>
