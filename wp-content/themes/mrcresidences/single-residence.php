<?php get_header(); ?>
    <div class="intro-carousel">
        <div class="container-fluid">
            <div id="intro-carousel" class="carousel slide" data-ride="carousel" data-interval="false">
                <!-- Wrapper for slides -->
                <div class="carousel-holder">
                    <div class="carousel-inner" role="listbox">
                        <?php
		        //    $residence_images = get_field('mrc_residence_gallery');
				//	if( $residence_images ):
				//		foreach( $residence_images as $residence_image ):
			    //  			$residence_image_size = 'page-header';
				//  			$residence_image_url = $residence_image['sizes'][$residence_image_size];
				//  			$residence_image_alt = $residence_image['alt'];

				$residence_image = get_field('mrc_residence_header_image');
				$residence_image_size = 'page-header';
				$residence_image_url = $residence_image['sizes'][$residence_image_size];
				$residence_image_alt = $residence_image['alt'];

				?>
                            <div class="item">
                                <picture>
                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                    <source srcset="<?php echo $residence_image_url; ?>">
                                    <!--[if IE 9]></video><![endif]-->
                                    <img src="<?php echo $residence_image_url; ?>" alt="<?php the_title(); ?>">
                                </picture>
                            </div>
                            <?php
				//		endforeach;
				//	endif;
				?>
                    </div>
                    <!-- <a class="left carousel-control" href="#intro-carousel" role="button" data-slide="prev">
                <span class="fa fa-arrow-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>https://scontent-lax3-1.xx.fbcdn.net/hphotos-xlp1/t31.0-8/12240197_10153576930910860_6518196444258924761_o.jpg
              </a>
              <a class="right carousel-control" href="#intro-carousel" role="button" data-slide="next">
                <span class="fa fa-arrow-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a> -->
                </div>
            </div>
        </div>
    </div>
    <!-- contain main informative part of the site -->
    <main id="main" role="main">
        <!-- info-box -->
        <section class="info-box section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-11 col-md-offset-1">
                        <h1><?php the_title(); ?></h1>
                    </div>
                    <div class="col-sm-3 col-sm-push-9 col-md-3 col-md-push-9">
                        <?php
		            $address_logo_obj = get_field('mrc_residence_logo');
		    		$address_logo_size = 'address-logo';
					$address_logo_url = $address_logo_obj['sizes'][$address_logo_size];
					$address_logo_title = $address_logo_obj['title'];
					$address_logo_alt = $address_logo_obj['alt'];
					$address_logo_height = $address_logo_obj['height'];
					$address_logo_width = $address_logo_obj['width'];
				?>
                            <h2 class="residence-logo text-center wow slideInDown" title="<?php the_title(); ?>"><img class="img-responsive" title="<?php the_title(); ?>" src="<?php echo $address_logo_url ?>" alt="<?php the_title(); ?>" width="367" /></h2>
                            <ul class="data-list list-unstyled">
                                <li>
                                    <span class="name">Beds:</span>
                                    <span class="value"><?php the_field('mrc_residence_bedrooms'); ?></span>
                                </li>
                                <li>
                                    <span class="name">Baths:</span>
                                    <span class="value"><?php the_field('mrc_residence_bathrooms'); ?></span>
                                </li>
                                <li>
                                    <span class="name">SQFT:</span>
                                    <span class="value"><?php the_field('mrc_residence_sqft'); ?> sqft.</span>
                                </li>
                                <?php
					// check if the repeater field has rows of data
					if( have_rows('mrc_residence_details') ):
					 	// loop through the rows of data
					    while ( have_rows('mrc_residence_details') ) : the_row();
					?>
                                    <li>
                                        <span class="name"><?php the_sub_field('detail_title'); ?>:</span>
                                        <span class="value"><?php the_sub_field('detail_info'); ?></span>
                                    </li>
                                    <?php
					    endwhile;
					else :
					    // no rows found
					endif;
					?>
                            </ul>
                    </div>
                    <div class="col-sm-9  col-sm-pull-3 col-md-7 col-md-offset-0 col-md-pull-2">
                        <div class="inner-area">
                            <?php the_field('mrc_residence_description'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="residence-features section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="img-holder wow slideInLeft">
                            <picture>
                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                <source srcset="<?php bloginfo('template_directory'); ?>/images/exterior.jpg ">
                                <!--[if IE 9]></video><![endif]-->
                                <img src="residences-01.jpg" alt="image description">
                            </picture>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-8 wow fadeIn">
                        <h2>Residence Features</h2>
                    </div>
                    <?php
				// check if the repeater field has rows of data
				if( have_rows('mrc_residence_features') ):
				 	// loop through the rows of data
				    while ( have_rows('mrc_residence_features') ) : the_row();
				?>
                        <div class="col-sm-6 col-md-4">
                            <div class="box wow fadeIn">
                                <h3><?php the_sub_field('feature_title'); ?></h3>
                                <p>
                                    <?php the_sub_field('feature_copy'); ?>
                                </p>
                            </div>
                        </div>
                        <?php
				    endwhile;
				else :
				    // no rows found
				endif;
				?>
                            <div class="clearfix visible-sm-block"></div>

                            <!-- <div class="col-sm-6 col-md-4">
                <div class="box">
                  <h3>Feature 2</h3>
                  <p>Nullam gravida odio quis ligula hendrerit, a consectetur nulla tincidunt. Ut finibus, quam id sodales tempus, neque diam fringilla lectus, vel ultricies est neque eget metus.</p>
                </div>
              </div>
              <div class="clearfix visible-lg-block visible-md-block"></div>
              <div class="col-sm-6 col-md-4">
                <div class="box">
                  <h3>Feature 3</h3>
                  <p>Nullam gravida odio quis ligula hendrerit, a consectetur nulla tincidunt. Ut finibus, quam id sodales tempus, neque diam fringilla lectus, vel ultricies est neque eget metus.</p>
                </div>
              </div>
              <div class="clearfix visible-sm-block"></div>
              <div class="col-sm-6 col-md-4">
                <div class="box">
                  <h3>Feature 4</h3>
                  <p>Nullam gravida odio quis ligula hendrerit, a consectetur nulla tincidunt. Ut finibus, quam id sodales tempus, neque diam fringilla lectus, vel ultricies est neque eget metus.</p>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="box">
                  <h3>Feature 5</h3>
                  <p>Nullam gravida odio quis ligula hendrerit, a consectetur nulla tincidunt. Ut finibus, quam id sodales tempus, neque diam fringilla lectus, vel ultricies est neque eget metus.</p>
                </div>
              </div>
              <div class="clearfix visible-lg-block visible-md-block visible-sm-block"></div> -->
                </div>
            </div>
        </section>
        <!-- gallery-section -->
        <section class="section plans-carousel">
            <div class="container">

                <div class="row">
                    <?php
				// check if the repeater field has rows of data

				if( have_rows('mrc_residence_floorplans') ):
				 	// loop through the rows of data
                     $counter = 0;
                    while ( have_rows('mrc_residence_floorplans') ) : the_row();

		            $floorplan_image = get_sub_field('floorplan');
	      			$floorplan_image_size = 'full-width';
		  			$floorplan_image_url = $floorplan_image['sizes'][$floorplan_image_size];
		  			$floorplan_image_alt = $floorplan_image['alt'];
				?>

                        <?php
                  if( is_single( '1253-edris-drive' ) == "1253 Edris Drive" && $counter == 0) { ?>

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <strong class="title"><?php the_sub_field('floorplan_title'); ?>
                        </span>
                        </strong>
                                <div class="image-frame">
                                    <a rel="lightbox" href="<?php echo $floorplan_image_url; ?>" title="<?php the_sub_field('floorplan_title'); ?>" alt="<?php the_sub_field('floorplan_title'); ?>" description="<?php the_sub_field('floorplan_title'); ?>">
                                        <i class="fa fa-search-plus fa-4x"></i>
                                        <picture>
                                            <!--[if IE 9]><video style="display: none;"><![endif]-->
                                            <source srcset="<?php echo $floorplan_image_url; ?>">
                                            <!--[if IE 9]></video><![endif]-->
                                            <img src="<?php echo $floorplan_image_url; ?>" alt="<?php echo $floorplan_image_alt; ?>">
                                        </picture>
                                    </a>
                                </div>
                            </div>

                            <?php  }

                        else if(is_single( '1253-edris-drive' ) && $counter > 0 ) { ?>

                                <div class="col-md-2 col-sm-6 col-xs-12">
                                    <strong class="title"><?php the_sub_field('floorplan_title'); ?>
                        </span>
                        </strong>
                                    <div class="image-frame">
                                        <a rel="lightbox" href="<?php echo $floorplan_image_url; ?>" title="<?php the_sub_field('floorplan_title'); ?>" alt="<?php the_sub_field('floorplan_title'); ?>" description="<?php the_sub_field('floorplan_title'); ?>">
                                            <i class="fa fa-search-plus fa-4x"></i>
                                            <picture>
                                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                                <source srcset="<?php echo $floorplan_image_url; ?>">
                                                <!--[if IE 9]></video><![endif]-->
                                                <img src="<?php echo $floorplan_image_url; ?>" alt="<?php echo $floorplan_image_alt; ?>">
                                            </picture>
                                        </a>
                                    </div>
                                </div>

                                <?php

                        }
                            else {
                    ?>


                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <strong class="title"><?php the_sub_field('floorplan_title'); ?>
                        </span>
                        </strong>
                                        <div class="image-frame">
                                            <a rel="lightbox" href="<?php echo $floorplan_image_url; ?>" title="<?php the_sub_field('floorplan_title'); ?>" alt="<?php the_sub_field('floorplan_title'); ?>" description="<?php the_sub_field('floorplan_title'); ?>">
                                                <i class="fa fa-search-plus fa-4x"></i>
                                                <picture>
                                                    <!--[if IE 9]><video style="display: none;"><![endif]-->
                                                    <source srcset="<?php echo $floorplan_image_url; ?>">
                                                    <!--[if IE 9]></video><![endif]-->
                                                    <img src="<?php echo $floorplan_image_url; ?>" alt="<?php echo $floorplan_image_alt; ?>">
                                                </picture>
                                            </a>
                                        </div>
                                    </div>

                                    <?php
                  }
                    $counter++;
	              	endwhile;
				endif;
				?>
                </div>



                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-holder text-right">
                            <a href="<?php the_field('mrc_residences_floorplans_pdf'); ?>" class="btn btn-primary">Download floor plans</a>
                        </div>
                    </div>
                </div>

            </div>
        </section>


        <section class="more-residences ">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h2>View More Residences</h2>
                    </div>
                </div>

                <div class="row">

                       <?php 
                           // global $wp_query;
                            $current = get_the_ID();
                            $residences = new WP_Query(array('post_type' => 'residence', 'post__not_in' => array($current))); while($residences->have_posts()) : $residences->the_post();?>


                            <div class="col-xs-6 col-md-3 residences-next wow fadeInUp">
                                <a href="<?php the_permalink(); ?>">
                      
                            <?php
				            $residence_foot_obj = get_field('mrc_res_foot_thum');
				    		$residence_foot_size = 'residence-more';
							$residence_foot_url = $residence_foot_obj['sizes'][$residence_foot_size];
							$residence_foot_title = $residence_foot_obj['title'];
							$residence_foot_alt = $residence_foot_obj['alt'];
							$residence_foot_height = $residence_foot_obj['height'];
							$residence_foot_width = $residence_foot_obj['width'];
						    ?>
                            <picture>
                                <!--[if IE 9]><video style="display: none;"><![endif]-->
                                <source srcset="<?php echo $residence_foot_url; ?>">
                                <!--[if IE 9]></video><![endif]-->
                                <img src="<?php echo $residence_foot_url; ?>" alt="<?php echo $residence_foot_alt; ?>">
                            </picture>
                            </a>                

                            </div>

                            <?php endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                                
                </div>
            </div>
        </section>




        <!-- <div class="single-img section">
          <div class="container">
            <picture>
              <!--[if IE 9]><video style="display: none;"><![endif]-->
        <source srcset="<?php bloginfo('template_directory'); ?>/images/single-img.jpg, <?php bloginfo('template_directory'); ?>/images/single-img-2x.jpg 2x">
        <!--[if IE 9]></video><![endif]
              <img src="single-img.jpg" alt="image description">
            </picture>
          </div>
        </div>-->
        <div class="section map-placeholder">
        </div>
        <?php get_footer(); ?>
