		<div id="ogden-location" class="row room-above<?php if(is_page('mobile')) {} else { echo ' space-above'; } ?> bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>The Location</h3>
                        <h4>Downtown Las Vegas</h4>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <p>At the corner of Las Vegas Blvd. and everywhere you want to be.</p>
                    </div>
                    <div id="footer-hours" class="col-xs-12 col-sm-offset-0 col-sm-3 col-md-offset-1">
                        <p>Mon - Fri 10am - 6pm<br>
                        Sat 10am - 5pm<br />
                        Sun 12pm - 5pm<br />
                        Tel 702.478.4700</p>
                    </div>
                    <div id="footer-address" class="col-xs-12 col-sm-4 col-md-3">
                            <p>The Ogden<br>
                                150 Las Vegas Blvd. N.<br>
                                Las Vegas, NV 89101</p>
                    </div>
                </div>
			</div>
         </div>
		 <div class="row room-above bg-gray">
            <!-- map -->
			<div class="map">
				<div class="m1">
					<div id="map" class="m2">
						<?php echo do_shortcode('[put_wpgm id=1]'); ?>
					</div>
				</div>
			</div>
         </div>
         <div id="ogden-contact" class="row room-above room-below bg-light-gray">
           <div class="container">
               <div class="row">
               		<div class="col-xs-12 col-sm-6">
	               		<a id="own" name="own"></a>
                        <h3>Interested In Owning?</h3>
                        <h4>Schedule a private presentation</h4>
                        <p>To learn more about our modern, urban residences please fill out the form below, or schedule a presentation with one of our condominium specialists by calling <a href="tel:702-478-4700">702.478.4700</a>.</p>
					</div>
               </div>
               <div class="row space-above">
					<div class="col-md-12">
						<form accept-charset="utf-8" name="3ecef0b5-fb9d-494d-a09c-9c740bb92860" class="maForm" method="POST" action="https://apps.net-results.com/data/public/IncomingForm/Ma/submit">

    <div class="row">
    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
       <label for="FirstName" id="FirstNameLabel" class="">First Name<span class="formRequiredLabel">*</span>

        </label>
        <input type="text" placeholder="First name*" name="FirstName" class="formText formRequired" value="" />
    </div>


    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
       <label for="LastName" id="LastNameLabel" class="">Last Name<span class="formRequiredLabel">*</span>
        </label>
        <input type="text" placeholder="Last name*" name="LastName" class="formText formRequired" value="" />
    </div>
    </div>

    <div class="row">
    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
       <label for="EmailAddress" id="EmailAddressLabel" class="">Email Address<span class="formRequiredLabel">*</span>

        </label>
        <input type="text" placeholder="Email*" name="EmailAddress" class="formText formEmail formRequired" value="" />
    </div>
    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
        <label for="Phone" id="PhoneLabel" class="">Phone<span class="formRequiredLabel">*</span>
        </label>
        <input type="text" placeholder="Phone*" name="Phone" class="formText formRequired" value="" />
    </div>
    </div>

     <div class="row">
    <div class="maFormElement maFormComboFirstHalf col-lg-4 col-md-4 col-sm-4">
       <label for="City" id="CityLabel" class="">City<span class="formRequiredLabel">*</span>

        </label>
        <input  placeholder="City*" type="text" name="City" class="formText formRequired" value="" />

    </div>

    <div class="maFormElement maFormComboFirstHalf col-lg-4 col-md-4 col-sm-4">
       <label for="State" id="StateLabel" class="">State<span class="formRequiredLabel">*</span>

        </label>

          <input type="text" placeholder="State*" name="State" class="formText formRequired" value="" />

    </div>


    <div class="maFormElement maFormComboFirstHalf col-lg-4 col-md-4 col-sm-4">
      <label for="ZipCode" id="ZipCodeLabel" class="">Zip Code<span class="formRequiredLabel">*</span>

        </label>
        <input type="text" placeholder="ZipCode*" name="ZipCode" class="formText formRequired" value="" />

    </div>
    </div>

     <div class="row">
	     <div class="maFormElement maFormComboFirstHalf col-lg-4 col-md-4 col-sm-4">
	      <label for="How_did_you_hear_about_us" id="How_did_you_hear_about_usLabel" class="">How did you hear about us?<span class="formRequiredLabel">*</span>

	        </label>
	             <input type="text" placeholder="How did you hear about us?*" name="How_did_you_hear_about_us" class="formText formRequired" value="" />

	    </div>
	    <div class="maFormElement col-lg-4 col-md-4 col-sm-4 text-right">
		    <ul class="list-inline">
			    <li><p>Are you a broker?*</p><label for="are_you_broker" id="are_you_brokerLabel" class="">Are you a broker?<span class="formRequiredLabel">*</span></label></li>
				<li><span class="lbl">Yes</span> <input id="are_you_broker-0" class="formRadio formRequired radio-inline" type="radio"  value="Yes" name="are_you_broker"/><label class="formRadioLabel"> Yes</label></li>
				<li><span class="lbl">No</span> <input id="are_you_broker-1" class="formRadio formRequired radio-inline" type="radio"  value="No" name="are_you_broker"/><label class="formRadioLabel"> No</label></li>
		    </ul>
		</div>
		<div class="maFormElement col-lg-4 col-md-4 col-sm-4">
			<label for="brokerage" id="brokerageLabel" class="">If so, which brokerage?</label>
			<input type="text" name="brokerage" class="formText " value="" placeholder="If so, which brokerage?" />
		</div>
    </div>


    <div class="row">
    <div class="maFormElement maFormComboFirstHalf col-lg-4 col-md-4 col-sm-4" style="margin-bottom:15px;">
     <label for="Desired_Floor_Plan" id="Desired_Floor_PlanLabel" class="">Desired Floor Plan<span class="formRequiredLabel">*</span>

        </label>

        <select name="Desired_Floor_Plan" class="formSelect formRequired">
                <option value="">Desired Floor Plan*</option>
                <option value="1 Bedroom - Starting at $200,000">1 Bedroom - Starting at $200,000</option>
                <option value="2 Bedroom - Starting at $300,000">2 Bedroom - Starting at $300,000</option>
                <option value="3 Bedroom - Starting at $500,000 ">3 Bedroom - Starting at $500,000 </option>
            </select>
    </div>

    <div class="maFormElement maFormComboFirstHalf col-lg-4 col-md-4 col-sm-4" style="margin-bottom:15px;">
       <label for="Desired_Price_Range" id="Desired_Price_RangeLabel" class="">Desired Price Range<span class="formRequiredLabel">*</span>
        </label>
        <select name="Desired_Price_Range" class="formSelect formRequired">
         <option value="">Desired Price Range*</option>
                <option value="$200,000 - $300,000">$200,000 - $300,000</option>
                <option value="$300,000 - $400,000">$300,000 - $400,000</option>
                <option value="$400,000 - $500,000">$400,000 - $500,000</option>
                <option value="$500,000 - $600,000">$500,000 - $600,000</option>
            </select>
    </div>
    <div class="maFormElement maFormComboFirstHalf col-lg-4 col-md-4 col-sm-4" style="margin-bottom:15px;">
        <label for="Reason_For_Purchase" id="Reason_For_PurchaseLabel" class="">Reason For Purchase<span class="formRequiredLabel">*</span>

        </label>
         <select name="Reason_For_Purchase" class="formSelect formRequired">
         		<option value="">Reason For Purchase</option>
                <option value="Primary Residence">Primary Residence</option>
                <option value="Second/Vacation Home">Second/Vacation Home</option>
                <option value="Investment">Investment</option>
                <option value="Other">Other</option>
            </select>
    </div>
    </div>

    <div class="maFormElement submitAlign">
        <button name="ma-form-element-yui_3_18_1_1_1429639675167_3806" class="formSubmit " value="submit" type="submit">Submit</button>
    </div>
    <input type="hidden" name="form_id" value="3ecef0b5-fb9d-494d-a09c-9c740bb92860" />
    <input type="hidden" name="form_access_id" value="" />
    <input type="hidden" name="__mauuid" value="" />
</form>
    </div>
					</div>
				</div><!-- End Row -->
            </div>
         </div><!-- End Row -->
	</div><!-- End Container Fluid -->
	<footer class="new-section-p bg-gray">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-2 social-icons">
            		<ul class="list-inline">
	                	<li><a href="https://twitter.com/ogdenlv"><i class="fa fa-twitter"></i></a></li>
	                	<li><a href="https://www.facebook.com/OgdenLV"><i class="fa fa-facebook"></i></a></li>
	                	<li><a href="http://instagram.com/ogdenlv_/"><i class="fa fa-instagram"></i></a></li>
            		</ul>
                </div>
                <div class="col-md-8 text-center">
                	<p class="footer-address">150 Las Vegas Blvd. N., Las Vegas, NV 89101 |  Tel 702.478.4700</p>
                </div>
                <div class="col-md-2 pull-right">
	                <p class="credit">Site by <a href="http://wickedta.com" rel="nofollow">Wicked + The Agency</a></p>
                </div>
            </div><!-- End Row -->
            <div class="row">
            	<div class="col-md-12">
            		<p class="text-center footer-copy">&copy; 2014 The Ogden Las Vegas All Rights Reserved.</p>
            	</div>
            </div>
        </div>
	<?php if(is_page('home')) { } else { ?><a href="#ogden-contact" id="ogden-request-btn" class="center-block">Request Information</a><?php } ?>
    </footer><!-- End Footer -->
    <?php wp_footer(); ?>
	<?php // include(TEMPLATEPATH."/includes/cookies.php");?>
    <script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-18060764-13']);

		setTimeout(function() {
		  var s, ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.defer='defer';
		  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		  s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga,s);
		},5);
	</script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55502590-1', 'auto');
	  ga('send', 'pageview');
	</script>

	<script type="text/javascript">
		(function() {
		  var didInit = false;
		  function initMunchkin() {
		    if(didInit === false) {
		      didInit = true;
		      Munchkin.init('642-QZN-110');
		    }
		  }
		  var s = document.createElement('script');
		  s.type = 'text/javascript';
		  s.async = true;
		  s.src = '//munchkin.marketo.net/munchkin.js';
		  s.onreadystatechange = function() {
		    if (this.readyState == 'complete' || this.readyState == 'loaded') {
		      initMunchkin();
		    }
		  };
		  s.onload = initMunchkin;
		  document.getElementsByTagName('head')[0].appendChild(s);
		})();
	</script>

	<script type="text/javascript"><!--
		try {
		var tracker = new LassoAnalytics('LAS-866249-31');
		tracker.setTrackingDomain(_ldstJsHost);
		tracker.init();
		tracker.track();
		} catch(error) {}
		-->
	</script>

    <script id="__maSrc" type="text/javascript" data-pid="11778">
(function () {
    var d=document,t='script',c=d.createElement(t),s=(d.URL.indexOf('https:')==0?'s':''),p;
    c.type = 'text/java'+t;
    c.src = 'http'+s+'://'+s+'c.cdnma.com/apps/capture.js';
    p=d.getElementsByTagName(t)[0];p.parentNode.insertBefore(c,p);
}());
</script>

</body>
</html>
