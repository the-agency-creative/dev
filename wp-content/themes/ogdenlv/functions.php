<?php

function dtlv_posttype() {
    register_post_type( 'dtlv',
        array(
            'labels' => array(
                'name' => __( 'DTLV' ),
                'singular_name' => __( 'Businesses' ),
                'add_new' => __( 'Add New Business' ),
                'add_new_item' => __( 'Add New Business' ),
                'edit_item' => __( 'Edit Business' ),
                'new_item' => __( 'Add New Business' ),
                'view_item' => __( 'View Business' ),
                'search_items' => __( 'Search Businesses' ),
                'not_found' => __( 'No Businesses found' ),
                'not_found_in_trash' => __( 'No Businesses found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'dtlv', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'dtlv_posttype' );

function amenities_posttype() {
    register_post_type( 'amenities',
        array(
            'labels' => array(
                'name' => __( 'In the Life' ),
                'singular_name' => __( 'Amenity' ),
                'add_new' => __( 'Add New Amenity' ),
                'add_new_item' => __( 'Add New Amenity' ),
                'edit_item' => __( 'Edit Amenity' ),
                'new_item' => __( 'Add New Amenity' ),
                'view_item' => __( 'View Amenity' ),
                'search_items' => __( 'Search Amenities' ),
                'not_found' => __( 'No Amenities found' ),
                'not_found_in_trash' => __( 'No Amenities found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title', 'editor' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'amenity', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'amenities_posttype' );

function residences_posttype() {
    register_post_type( 'residences',
        array(
            'labels' => array(
                'name' => __( 'Residences' ),
                'singular_name' => __( 'Residence' ),
                'add_new' => __( 'Add New Residence' ),
                'add_new_item' => __( 'Add New Residence' ),
                'edit_item' => __( 'Edit Residence' ),
                'new_item' => __( 'Add New Residence' ),
                'view_item' => __( 'View Residence' ),
                'search_items' => __( 'Search Residences' ),
                'not_found' => __( 'No Residences found' ),
                'not_found_in_trash' => __( 'No Residences found in trash' )
            ),
            'public' => true,
            'supports' => array( 'thumbnail', 'title', 'editor' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'residence', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'residences_posttype' );

function tiles_posttype() {
    register_post_type( 'tiles',
        array(
            'labels' => array(
                'name' => __( 'Tiles' ),
                'singular_name' => __( 'Tile' ),
                'add_new' => __( 'Add New Tile' ),
                'add_new_item' => __( 'Add New Tile' ),
                'edit_item' => __( 'Edit Tile' ),
                'new_item' => __( 'Add New Tile' ),
                'view_item' => __( 'View Tile' ),
                'search_items' => __( 'Search Tiles' ),
                'not_found' => __( 'No Tiles found' ),
                'not_found_in_trash' => __( 'No Tiles found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'tile', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'tiles_posttype' );

function message_posttype() {
    register_post_type( 'message',
        array(
            'labels' => array(
                'name' => __( 'Messages' ),
                'singular_name' => __( 'Message' ),
                'add_new' => __( 'Add New Message' ),
                'add_new_item' => __( 'Add New Message' ),
                'edit_item' => __( 'Edit Message' ),
                'new_item' => __( 'Add New Message' ),
                'view_item' => __( 'View Message' ),
                'search_items' => __( 'Search Messages' ),
                'not_found' => __( 'No Messages found' ),
                'not_found_in_trash' => __( 'No Messages found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title' ),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'message', 'with_front' => false ),
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'message_posttype' );

/* register some jquery stuff */

function light_the_fires() {
	wp_deregister_script('jquery');
	wp_deregister_script('wpgmp_googlemap_script');
	wp_enqueue_script( 'jquery', '/wp-includes/js/jquery/jquery.js' , array(), null, true );
	wp_enqueue_script('bootstrap-js', 'http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js', false, null, true);
    wp_enqueue_script('jquery-custom', get_stylesheet_directory_uri() . '/js/jquery.main.js', array( 'jquery' ), '1.0.0', true);
    wp_enqueue_script('match-height',  get_stylesheet_directory_uri() . '/js/jquery.matchheight.min.js', array( 'jquery' ), '1.0.0', true);
    wp_enqueue_script('js-cookie',  get_stylesheet_directory_uri() . '/js/js.cookie.js', false, '1.0.0', true);
    wp_enqueue_script('jquery-mobile', get_stylesheet_directory_uri() . '/js/jquerymobile.min.js', array('jquery'), '1.2.0', true);
    wp_enqueue_script('ie10-viewport', get_stylesheet_directory_uri() . '/js/ie10-viewport-bug-workaround.js', false, '1.0.0', true);
    wp_enqueue_script('imagesLoaded',  get_stylesheet_directory_uri() . '/js/imagesloaded.pkgd.min.js', false, null, true);
    wp_enqueue_script('wpgmp_googlemap_script', plugins_url() . '/wp-google-map-pro/js/wpgmp-google-map.js', array( 'jquery' ), '1.0.0', true);
    wp_enqueue_script('jquery-masonry', array('imagesLoaded'), null, true );
}
add_action( 'wp_enqueue_scripts', 'light_the_fires' );

/* and then tell them what to do */

function add_footer_scripts() { ?>

	<script>
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
		  var msViewportStyle = document.createElement('style');
		  msViewportStyle.appendChild(
		    document.createTextNode(
		      '@-ms-viewport{width:auto!important}'
		    )
		  );
		  document.querySelector('head').appendChild(msViewportStyle)
		}

		jQuery(document).ready(function($) {

			$("#myCarousel").swiperight(function() {
	    		$(this).carousel('prev');
		    });
			$("#myCarousel").swipeleft(function() {
			    $(this).carousel('next');
		   	});

			$(function() {
			    var header = $(".navbar");
			    $(window).scroll(function() {
			        var scroll = $(window).scrollTop();

			        if (scroll >= 550) {
			            header.addClass("solid");
			        } else {
			            header.removeClass("solid");
			        }
			    });
			});

			// tile heights

			$(function() {
			    $('#home-tiles .tile').matchHeight();
			});

			// masonry party!

			var $container = $('.masonry-container');

			$container.imagesLoaded( function() {
			  $container.masonry({
			    itemSelector	: '.masonry-item'
			  });
			});

			// add active class to carousel pagination

			$(".carousel-indicators li:first").addClass("active");
			$(".carousel-inner .item:first").addClass("active");

			$(function() {
			    $('[data-toggle="elementscroll"]').click(function() {
			        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
			                && location.hostname == this.hostname) {

			            var target = $(this.hash);
			            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			            if (target.length) {
			                $('html,body').animate({
			                    scrollTop: target.offset().top -57 //head space
			                }, 1000); //scroll speed
			                return false;
			            }
			        }
			    });
			});

		    $(function() {
				$('html, body').hide();
				if (window.location.hash) {
				        setTimeout(function() {
				                $('html, body').scrollTop(0).show();
				                $('html, body').animate({
				                        scrollTop: $(window.location.hash).offset().top -57 // head space
				                        }, 1000) //scrollspeed
				        }, 0);
				}
				else {
				        $('html, body').show();
				}
		    });
		});
	</script>

<?php }
//add to wp_footer
add_action( 'wp_footer', 'add_footer_scripts', 100 );

// set up our sitewide image sizes

add_theme_support( 'post-thumbnails' );
add_image_size('dtlv-image', 275 ); // texture image size for dtlv businesses
add_image_size('amenity-image', 360, 277, true ); // image size for amenity grid
add_image_size('page-header-image', 1920, 900, true ); // image size for page header
add_image_size('home-singlewide', 360, 360, true ); // image size for home page single block
add_image_size('home-doublewide', 750, 360, true ); // image size for home page double block
add_image_size('iframe-gallery', 740, 440, true ); // image size for the broker iframe gallery
add_image_size('residences-gallery', 1200, 700, true ); // image size for the broker iframe gallery

//add the page slug to body_class()
function add_slug_body_class( $classes ) {
	global $post;
		if ( isset( $post ) ) {
			$classes[] = $post->post_type . '-' . $post->post_name;
		}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// add custom admin columns

add_filter('manage_residences_posts_columns' , 'add_residences_columns');

function add_residences_columns($columns) {
    return array_merge($columns,
          array('unit' => 'Unit Number'));
}

add_action('manage_pages_custom_column' , 'residences_custom_columns', 10, 2 );

function residences_custom_columns( $column, $post_id ) {
    switch ( $column ) {

    case 'unit' :
        echo get_field('ogden_residence_unit_number');
        break;
    }
}

function add_image_responsive_class($content) {
   global $post;
   $pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
   $replacement = '<img$1class="$2 img-responsive"$3>';
   $content = preg_replace($pattern, $replacement, $content);
   return $content;
}
add_filter('the_content', 'add_image_responsive_class');
?>
