		<?php if(isset($_COOKIE['closed_ogden_form']) || isset($_COOKIE['submitted_ogden_form'])) { } else { ?>
		  <?php if(is_page('thank-you')) { ?>
		  	<script>
			  	jQuery(document).ready(function($) {
				  	$(window).load(function(){
					    Cookies.set('submitted_ogden_form', '1', { path: '/', expires: 999999 });
					});
				});
			</script>
		  <?php } else { ?>
		  	<script>
			  	jQuery(document).ready(function($) {
		  			$(window).load(function(){
				        $('#ogden-modal').modal('show');
				    });
				    /* center modal */
					function centerModals(){
					  $('.modal').each(function(i){
					    var $clone = $(this).clone().css('display', 'block').appendTo('body');
					    var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
					    top = top > 0 ? top : 0;
					    $clone.remove();
					    $(this).find('.modal-content').css("margin-top", top);
					  });
					}
					$('.modal').on('show.bs.modal', centerModals);
					$(window).on('resize', centerModals);

					/* set cookie on modal submit (for thank you page load) */

					$('#close').click(function() {
						Cookies.set('closed_ogden_form', '1', { path: '/', expires: 7 });
					});
				});
			</script>
		    <!-- modal div -->
		   	<div class="modal fade" id="ogden-modal" tabindex="-1" role="dialog" aria-labelledby="ogden-modal" aria-hidden="true">
			  	<div class="modal-dialog modal-lg">
				  	<div class="modal-content">
					  	<div class="modal-header" style="border-bottom: 0px;">
						  	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span id="close" aria-hidden="true">&times;</span></button>
						</div>
						<div class="modal-body">
							<div class="container-fluid">
								<div class="row">
									<div class="col-md-12">
										<div id="ogden-location">
										  	<h3>Discover Life Reimagined in Downtown Las Vegas</h3>
					                        <p>To learn more about our current offer and modern, urban residences please fill out the form below, or schedule a presentation with one of our condominium specialists by calling <a href="tel:702-478-4700">702.478.4700</a>.</p>
									  	</div>
										<form id="lightbox-form" accept-charset="utf-8" name="4878f646-b41b-4183-a8a5-18d9aa1d30fb" class="maForm" method="post" action="https://apps.net-results.com/data/public/IncomingForm/Ma/submit">
							                <div class="row">
												<div class="maFormElement col-lg-6 col-md-6 col-sm-6">
							                        <label for="first_name" id="first_nameLabel" class="formComboNameLabel ">First Name<span class="formRequiredLabel">*</span></label><input type="text" id="first_name" name="first_name" class="formText formComboName formRequired" value="" placeholder="First Name*">
							                    </div>
							                    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
							                        <label for="last_name" id="last_nameLabel" class="formComboNameLabel ">Last Name<span class="formRequiredLabel">*</span></label><input type="text" id="last_name" name="last_name" class="formText formComboName formRequired" value="" placeholder="Last Name*">
							                    </div>
							                </div>
							                <div class="row">
												<div class="maFormElement col-lg-6 col-md-6 col-sm-6">
							                    	<label for="Email" id="Email" class="">Email Address<span class="formRequiredLabel">*</span></label><input type="text" name="Email" class="formText formEmail formRequired" value="" placeholder="Email Address*">
							                	</div>
							                	<div class="maFormElement col-lg-6 col-md-6 col-sm-6">
							                		<label for="telephone" id="telephoneLabel" class="">Phone</label><input type="text" name="telephone" class="formText" value="" placeholder="Phone Number">
							                	</div>
							                </div>
							                <div class="row">
												<div class="maFormElement maFormComboFirstHalf col-lg-6 col-md-6 col-sm-6" style="margin-bottom:15px;">
											       <label for="Desired_Price_Range" id="Desired_Price_RangeLabel" class="">Desired Price Range<span class="formRequiredLabel">*</span>
											        </label>
											        <select name="Desired_Price_Range" class="formSelect formRequired">
											         <option value="">Desired Price Range</option>
											                <option value="$200,000 - $300,000">$200,000 - $300,000</option>
											                <option value="$300,000 - $400,000">$300,000 - $400,000</option>
											                <option value="$400,000 - $500,000">$400,000 - $500,000</option>
											                <option value="$500,000 - $600,000">$500,000 - $600,000</option>
											            </select>
											    </div>
								                <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
												    <label for="how_heard" id="how_heardLabel" class="">How did you hear about us?<span class="formRequiredLabel">*</span></label><input type="text" name="how_heard" class="formText formRequired" value="" placeholder="How did you hear about us?*" />
												</div>
							                </div>
							                <div class="row">
												<div class="maFormElement col-lg-6 col-md-6 col-sm-6 submitAlign text-right">
													<button name="ma-form-element-yui_3_18_1_1_1431558031146_1735" class="formSubmit " value="submit" type="submit">SCHEDULE</button>
												</div>
							                </div>
							                <input type="hidden" name="form_id" value="4878f646-b41b-4183-a8a5-18d9aa1d30fb">
							                <input type="hidden" name="form_access_id" value="">
							                <input type="hidden" name="__mauuid" value="">
							            </form>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-image space-above">
							<img src="http://ogdenlv.com/wp-content/uploads/2015/05/footer-pool.jpg" class="img-responsive" />
						</div>
				  	</div>
			  	</div>
			</div>
			<!-- /modal -->
			<?php } ?>
		<?php } ?>
