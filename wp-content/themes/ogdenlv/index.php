<?php get_header(); ?>

    <div class="container-fluid">
    	<div class="row">
		    <!-- Page Content -->
		    <div class="container">
		       <div class="row space-above">
		        	<div class="col-xs-12 col-sm-8">
		       	    	<img src="<?php bloginfo('template_directory'); ?>/images/home-01.jpg" width="100%" height="auto" alt="The Ogden"/>
		            </div>
		            <div class="col-xs-12 col-sm-4 text-center">
		            	<div class="text-box bg-blue-gray">
		                    <h1>Your New Home</h1>
		                    <h3>Endless possibilities</h3>
		                    <p class="home-grid-text">Downtown Las Vegas
		    is home to so many entrepreneurs and creative&nbsp;individuals.</p>
		                    <a class="btn-default btn btn-lines">LEARN MORE</a>
		                </div>
		            </div>
		       </div><!-- End Row -->
		       <div class="row space-above">
		           	<div class="col-xs-12 col-sm-4">
		            	<img src="<?php bloginfo('template_directory'); ?>/images/home-02.jpg" width="100%" height="auto" alt="Nest"/>
		            </div>
		            <div class="col-xs-12 col-sm-4 text-center">
		            	<div class="text-box bg-blue-gray">
		                    <h1>Ready For You</h1>
		                    <h3>Nest and Sonos</h3>
		                    <p class="home-grid-text">Some of the best equipment is needed, hooked up and ready to go.</p>
		                    <a class="btn-default btn btn-lines">LEARN MORE</a>
		                </div>
		            </div>
		            <div class="col-xs-12 col-sm-4">
		            	<img src="<?php bloginfo('template_directory'); ?>/images/home-03.jpg" width="100%" height="auto" alt="Sonos"/>
		            </div>
		        </div><!-- End Row -->
		        <div class="row space-above">
		            <div class="col-xs-12 col-sm-4 text-center">
		            	<div class="text-box bg-blue-gray">
		                    <h1>Your Location</h1>
		                    <h3>Downtown Las Vegas</h3>
		                    <p class="home-grid-text">Just spend a few minutes walking the area and you’ll know what we are talking about.</p>
		                    <a class="btn-default btn btn-lines">LEARN MORE</a>
		                </div>
		            </div>
		            <div class="col-xs-12 col-sm-8">
		            	<img src="<?php bloginfo('template_directory'); ?>/images/home-04.jpg" width="100%" height="auto" alt="in the life"/>
		            </div>
				</div><!-- End Row -->
			</div><!-- End container -->
		 </div>
<?php get_footer(); ?>
