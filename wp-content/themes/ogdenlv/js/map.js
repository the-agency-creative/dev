var map;
function initialize() {
  var myLatLng = new google.maps.LatLng(36.1700878,-115.1396153);
  var stylez = [
	    {
	      featureType: "all",
	      elementType: "all",
	      stylers: [
	        { saturation: -100 } // <-- THIS
	      ]
	    }
	];
  var mapOptions = {
    zoom: 17,
    scrollwheel: false,
    center: myLatLng,
    mapTypeControlOptions: {
         mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'tehgrayz']
    }
  };
  
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
      
  var image = '/wp-content/themes/ogdenlv/images/map-pin.png';
  var encoreMarker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: image
  });
  var mapType = new google.maps.StyledMapType(stylez, { name:"Grayscale" });    
	  map.mapTypes.set('tehgrayz', mapType);
	  map.setMapTypeId('tehgrayz');
}

google.maps.event.addDomListener(window, 'load', initialize);


