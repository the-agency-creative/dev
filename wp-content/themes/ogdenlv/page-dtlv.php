<?php

/*
Template Name: DTLV
*/

get_header();

?>

    <div class="container-fluid">
    	<div class="row bg-blue-gray">
		    <!-- Page Content -->
		    <div class="container">
		        <div class="row space-above space-below">
		       		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		       		<div class="col-sm-12 col-md-9">
		           		<?php the_content(); ?>
		            </div>
		       		<?php endwhile; else: ?>
		       		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		       		<?php endif; ?>
		        </div><!-- End Row -->
		    </div>
    	</div>
    	<div class="row space-above">
		    <!-- Page Content -->
		    <div class="container">
		       <div class="row space-above">
				   <div class="masonry-container">
						<?php $page = (get_query_var('paged')) ? get_query_var('paged') : 1; query_posts("post_type=dtlv&posts_per_page=20&paged=$page"); while ( have_posts() ) : the_post() ?>
						<article class="masonry-item col-md-3" id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
							<div class="masonry-thumbnail">
								<?php
								$ogden_biz_image_obj = get_field('ogden_dtlv_image');
								// vars
								$ogden_biz_image_src = $ogden_biz_image_obj['url'];
								$ogden_biz_image_size = 'dtlv-image';
								$ogden_biz_image = $ogden_biz_image_obj['sizes'][ $ogden_biz_image_size ];
								$ogden_biz_image_width = $ogden_biz_image_obj['sizes'][ $ogden_biz_image_size . '-width' ];
								$ogden_biz_image_height = $ogden_biz_image_obj['sizes'][ $ogden_biz_image_size . '-height' ];
								?>
								<img class="img-responsive" src="<?php echo $ogden_biz_image; ?>" />
								<div class="dtlv-caption">
									<h3><a target="_blank" href="<?php the_field('ogden_dtlv_link'); ?>"><?php the_field('ogden_dtlv_name'); ?></a></h3>
									<p><?php the_field('ogden_dtlv_blurb'); ?></p>
								</div>
							</div><!--.masonry-thumbnail-->
						</article><!--/.masonry-item-->
						<?php endwhile; ?>
			      	</div>
		        </div><!-- End Row -->
			</div><!-- End container -->
		 </div>

<?php get_footer(); ?>
