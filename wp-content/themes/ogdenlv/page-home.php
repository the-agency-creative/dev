<?php

/*
Template Name: Home
*/

get_header();

?>
    <div class="container-fluid">
    	<div class="row bg-light-gray">
       	  <div id="myCarousel" class="carousel slide" data-interval="5000" data-ride="carousel">
          	<!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class=""></li>
              <li data-target="#myCarousel" data-slide-to="1" class=""></li>
              <li data-target="#myCarousel" data-slide-to="2" class=""></li>
            </ol>

            <!-- Carousel items -->
              <div class="carousel-inner" role="listbox">
	            <?php $ogden_messages = new WP_Query("post_type=message&posts_per_page=3"); while($ogden_messages->have_posts()) : $ogden_messages->the_post();?>
	            <div class="item">
                  <div class="container">
                    <div class="carousel-caption">
	                  <?php if(get_field('ogden_message_heading')) { ?>
                      <h2><?php the_field('ogden_message_heading'); ?></h2>
                      <?php } ?>
                      <?php if(get_field('ogden_message_subheading')) { ?>
                      <h3><?php the_field('ogden_message_subheading'); ?></h3>
                      <?php } ?>
                      <?php if(get_field('ogden_message_content')) { ?>
                      <p><?php the_field('ogden_message_content'); ?></p>
                      <?php } ?>
                      <?php
					  if( have_rows('ogden_message_buttons') ):
					  while ( have_rows('ogden_message_buttons') ) : the_row();
					  ?>
					  <a href="<?php the_sub_field('ogden_message_button_link'); ?>" class="btn btn-lg btn-primary get-up" role="button"><?php the_sub_field('ogden_message_button_text'); ?></a>
					  <?php endwhile; endif; ?>
                    </div>
                  </div>
                </div>
	            <?php endwhile; ?>
	            <?php wp_reset_postdata(); ?>
              </div>

              <!-- Carousel nav -->
                <a class="carousel-control left hidden-xs" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon-chevron-left"><img src="<?php bloginfo('template_directory'); ?>/images/arrow-left.png" width="18" height="44"></span>
                </a>
                <a class="carousel-control right hidden-xs" href="#myCarousel" data-slide="next">
                    <span class="glyphicon-chevron-right"><img src="<?php bloginfo('template_directory'); ?>/images/arrow-right.png" width="18" height="44"></span>
                </a>
    		</div>
        </div><!-- END Carousel Row -->

    	<div class="row">
		    <!-- Page Content -->
		    <div id="home-tiles" class="container">
		       <div class="row">

		       		<?php
		       		$home_tiles = new WP_Query("post_type=tiles&posts_per_page=12"); while($home_tiles->have_posts()) : $home_tiles->the_post();

		       		if(get_field('ogden_tile_text_image') == 'Text') {

		       		?>

		       		<div class="col-xs-12 col-sm-4 text-center tile">
		            	<div class="text-box bg-blue-gray">
		                    <h2><?php the_field('ogden_tile_text_headline'); ?></h2>
		                    <h3><?php the_field('ogden_tile_text_subheader'); ?></h3>
		                    <p class="home-grid-text"><?php the_field('ogden_tile_text_blurb'); ?></p>
		                    <a target="_blank" href="<?php the_field('ogden_tile_link'); ?>" class="btn-default btn btn-lines"><?php the_field('ogden_tile_text_button'); ?></a>
		                </div>
		            </div>

		            <?php } else {

			        	if(get_field('ogden_tile_single_double') == 'Single') {

				        $tile_image_obj = get_field('ogden_tile_image');
					    $tile_image_size = 'home-singlewide';
						$tile_image_url = $tile_image_obj['sizes'][$tile_image_size];
						$tile_image_title = $tile_image_obj['title'];
						$tile_image_alt = $tile_image_obj['alt'];

		            ?>

		            <div class="col-xs-12 col-sm-4 tile">
		            	<img class="img-responsive" src="<?php echo $tile_image_url; ?>" alt="<?php echo $tile_image_alt; ?>"/>
		            </div>

		            <?php

		            	} else {

		            	$tile_image_obj = get_field('ogden_tile_image');
					    $tile_image_size = 'home-doublewide';
						$tile_image_url = $tile_image_obj['sizes'][$tile_image_size];
						$tile_image_title = $tile_image_obj['title'];
						$tile_image_alt = $tile_image_obj['alt'];
					?>

		            <div class="col-xs-12 col-sm-8 tile">
		       	    	<img class="img-responsive" src="<?php echo $tile_image_url; ?>" alt="<?php echo $tile_image_alt; ?>"/>
		            </div>

		       		<?php
		       			}

		       		}
		       		?>
		       		<?php endwhile; ?>
		       		<?php wp_reset_query(); ?>

				</div><!-- End Row -->
			</div><!-- End container -->
		 </div>

<?php get_footer(); ?>
