<?php

/*
Template Name: In the Life
*/

get_header();

?>

    <div class="container-fluid">
    	<div class="row bg-blue-gray">
		    <!-- Page Content -->
		    <div class="container">
		        <div class="row space-above space-below">
		       		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		       		<div class="col-sm-12 col-md-9">
		           		<?php the_content(); ?>
		            </div>
		       		<?php endwhile; else: ?>
		       		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		       		<?php endif; ?>
		        </div><!-- End Row -->
		    </div>
    	</div>
    	<div class="row space-above">
		    <!-- Page Content -->
		    <div id="life-grid-container" class="container">
		    	<?php
		    	$amenities = new WP_Query("post_type=amenities&posts_per_page=99"); while($amenities->have_posts()) : $amenities->the_post();
		    	
				$ogden_amenity_image_obj = get_field('ogden_amenity_image');
				// vars
				$ogden_amenity_image_src = $ogden_amenity_image_obj['url'];
				$ogden_amenity_image_size = 'amenity-image';
				$ogden_amenity_image = $ogden_amenity_image_obj['sizes'][ $ogden_amenity_image_size ];
				$ogden_amenity_image_width = $ogden_amenity_image_obj['sizes'][ $ogden_amenity_image_size . '-width' ];
				$ogden_amenity_image_height = $ogden_amenity_image_obj['sizes'][ $ogden_amenity_image_size . '-height' ];
		    	?>
		    	<?php if(get_field('ogden_amenity_page_exists')) { ?>
		    	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" target="_blank">	
			    	<div class="col-xs-12 col-sm-6 col-md-4 life-item">
	                	<img class="img-responsive" src="<?php echo $ogden_amenity_image; ?>" alt="The Ogden"/>
	                	<div class="life-caption">
							<h3><span><?php the_field('ogden_amenity_title'); ?></span></h3>
							<p><?php the_field('ogden_amenity_blurb'); ?></p>
						</div>
	                </div>
		    	</a>
                <?php } else { ?>
                <div class="col-xs-12 col-sm-6 col-md-4 life-item">
                	<img class="img-responsive" src="<?php echo $ogden_amenity_image; ?>" alt="The Ogden"/>
                	<div class="life-caption">
						<h3><span><?php the_field('ogden_amenity_title'); ?></span></h3>
						<p><?php the_field('ogden_amenity_blurb'); ?></p>
					</div>
                </div>
                <?php } ?>	
		    	<?php endwhile; ?>
		    	<?php wp_reset_query(); ?>
            </div><!-- End container -->
		</div>

<?php get_footer(); ?>
