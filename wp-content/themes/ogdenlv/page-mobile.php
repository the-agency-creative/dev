<?php

/*
Template Name: Mobile
*/

?>

<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="http://www.ogdenlv.com//favicon.ico">

    <title><?php wp_title(''); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles -->
    <link href="<?php bloginfo('template_directory'); ?>/css/custom.css" rel="stylesheet">

	<?php wp_head(); ?>

  </head>

  <body <?php body_class(); ?>>

    <!-- Navigation -->
    <nav id="ogden-nav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#ogden-nav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-left">
                	<a class="navbar-brand page-scroll" href="/"><img src="<?php bloginfo('template_directory'); ?>/images/logo-ogden-mark.png" width="21" height="21" alt="logo mark"/></a>
                    <span class="tel">Tel. <a href="tel:7024784700">702.478.4700</a></span>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="ogden-nav-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a href="/residences/">RESIDENCES</a>
                    </li>
                    <li>
                        <a href="/in-the-life/">IN THE LIFE</a>
                    </li>
                    <li>
                        <a href="/downtown-las-vegas/">DOWNTOWN LV</a>
                    </li>
                    <li>
                        <a href="/news/">NEWS &amp; UPDATES</a>
                    </li>
                    <li>
                        <a href="/team/">TEAM</a>
                    </li>
                    <li>
                        <a href="/contact/">CONTACT</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <?php
    if(get_field('ogden_page_header_image')) {
	    $ogden_page_header_image_obj = get_field('ogden_page_header_image');
		// vars
		$ogden_page_header_image_src = $ogden_page_header_image_obj['url'];
		$ogden_page_header_image_size = 'page-header-image';
		$ogden_page_header_image = $ogden_page_header_image_obj['sizes'][ $ogden_page_header_image_size ];
    ?>
    <header style="background-image: url('<?php echo $ogden_page_header_image; ?>');">
        <div class="container hero-hundred-h">
            <div class="row hero-hundred-h">
            	<div class="col-xs-6 hero-hundred-h">
                	<div id="hero-text">
                		<h3>	Condominium Residences</h3>
                    	<h5>from the $200,000s to the&nbsp;mid&nbsp;$600,000s</h5>
                    </div>
            	</div>
                	<div class="col-xs-6 hero-hundred-h">
                        <div id="hero-text-r">
                        <h3>10 <span>%</span></h3>
                        <h5>Down Payment</h5>
                        <a href="tel:7024784700" class="btn-default btn">CALL NOW</a>
                    </div>
            	</div>
            </div>
        </div>
    </header>
    <?php } else { ?>
    <header style="background-image: url('<?php bloginfo('template_directory'); ?>/images/header-image01.jpg');">
        <div class="container hero-hundred-h">
            <div class="row hero-hundred-h">
            	<div class="col-xs-6 hero-hundred-h">
                	<div id="hero-text">
                		<h3>	Condominium Residences</h3>
                    	<h5>from the $200,000s to the&nbsp;mid&nbsp;$600,000s</h5>
                    </div>
            	</div>
                	<div class="col-xs-6 hero-hundred-h">
                        <div id="hero-text-r">
                        <h3>10%</h3>
                        <h5>Down Payment</h5>
                        <a href="tel:7024784700" class="btn-default btn">CALL NOW</a>
                    </div>
            	</div>
            </div>
        </div>
    </header>
    <?php } ?>

    <div class="container-fluid">

		<div id="ogden-location" class="row room-above bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>The Location</h3>
                        <h4>Downtown Las Vegas</h4>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <p>At the corner of Las Vegas Blvd. and everywhere you want to be.</p>
                    </div>
                    <div id="footer-hours" class="col-xs-12 col-sm-offset-0 col-sm-3 col-md-offset-1">
                        <p>Mon - Fri 10am – 6pm<br>
                        Sat 10am - 5pm<br />
                        Sun 12pm - 5pm<br />
                        Tel 702.478.4700</p>
                    </div>
                    <div id="footer-address" class="col-xs-12 col-sm-4 col-md-3">
                            <p>The Ogden<br>
                                150 Las Vegas Blvd. N.<br>
                                Las Vegas, NV 89101</p>
                    </div>
                </div>
			</div>
         </div>
		 <div class="row room-above bg-gray">
            <!-- map -->
			<div class="map">
				<div class="m1">
					<div id="map" class="m2">
						<?php echo do_shortcode('[put_wpgm id=1]'); ?>
					</div>
				</div>
			</div>
         </div>
         <div id="ogden-contact" class="row room-above room-below bg-light-gray">
           <div class="container">
               <div class="row">
               		<div class="col-xs-12 col-sm-6">
	               		<a id="own" name="own"></a>
                        <h3>Interested In Owning</h3>
                        <h4>Schedule a private presentation</h4>
                        <p>To learn more about our modern, urban residences please fill out the form below, or schedule a presentation with one of our condominium specialists by calling <a href="tel:702-478-4700">702.478.4700</a>.</p>
					</div>
               </div>
               <div class="row">
					<div class="col-md-12">
						<script src="//app-sjg.marketo.com/js/forms2/js/forms2.js"></script>
						<a id="contact" name="contact"></a>
						<form id="mktoForm_1016"></form>
						<script>MktoForms2.loadForm("//app-sjg.marketo.com", "642-QZN-110", 1016);</script>
					</div>
				</div><!-- End Row -->
            </div>
         </div><!-- End Row -->
	</div><!-- End Container Fluid -->
	<footer class="new-section-p bg-gray">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-2 pull-left">
            		<ul class="list-inline">
	                	<li><a href="https://twitter.com/ogdenlv"><i class="fa fa-twitter fa-2x"></i></a></li>
	                	<li><a href="https://www.facebook.com/OgdenLV"><i class="fa fa-facebook fa-2x"></i></a></li>
	                	<li><a href="http://instagram.com/ogdenlv_/"><i class="fa fa-instagram fa-2x"></i></a></li>
            		</ul>
                </div>
                <div class="col-md-8 text-center">
                	<p>150 Las Vegas Blvd. N., Las Vegas, NV 89101 |  Tel 702.478.4700</p>
                </div>
                <div class="col-md-2 pull-right">
                </div>
            </div><!-- End Row -->
            <div class="row">
            	<div class="col-md-12">
            		<p class="text-center">&copy; 2014 The Ogden Las Vegas All Rights Reserved.</p>
            	</div>
            </div>
        </div>
        <a href="#contact" id="ogden-request-btn" class="center-block">Request Information</a>
    </footer><!-- End Footer -->
    <?php wp_footer(); ?>

    <script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-18060764-13']);

		setTimeout(function() {
		  var s, ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.defer='defer';
		  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		  s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga,s);
		},5);
	</script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-55502590-1', 'auto');
	  ga('send', 'pageview');
	</script>

	<script type="text/javascript">
		(function() {
		  var didInit = false;
		  function initMunchkin() {
		    if(didInit === false) {
		      didInit = true;
		      Munchkin.init('642-QZN-110');
		    }
		  }
		  var s = document.createElement('script');
		  s.type = 'text/javascript';
		  s.async = true;
		  s.src = '//munchkin.marketo.net/munchkin.js';
		  s.onreadystatechange = function() {
		    if (this.readyState == 'complete' || this.readyState == 'loaded') {
		      initMunchkin();
		    }
		  };
		  s.onload = initMunchkin;
		  document.getElementsByTagName('head')[0].appendChild(s);
		})();
	</script>

	<script type="text/javascript"><!--
		try {
		var tracker = new LassoAnalytics('LAS-866249-31');
		tracker.setTrackingDomain(_ldstJsHost);
		tracker.init();
		tracker.track();
		} catch(error) {}
		-->
	</script>

</body>
</html>
