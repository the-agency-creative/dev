<?php

/*
Template Name: News
*/

get_header();

?>

    <div class="container-fluid">
    	<div class="row">
		    <!-- Page Content -->
		    <div class="container">
		       <div class="row space-above">
		       		<div class="col-md-12 col-lg-9">
		       		<h1>The Ogden News & Press</h1>
		       		<?php $page = (get_query_var('paged')) ? get_query_var('paged') : 1; query_posts("post_type=post&posts_per_page=10&paged=$page"); while ( have_posts() ) : the_post() ?>
		       			<?php if(has_category('press')) { ?>
		       			<article class="clearfix news-item space-above space-below">
		       				<h2><a href="<?php the_field('ogden_press_link'); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
		       				<h3><em>Press</em></h3>
		       				<?php the_excerpt('...'); ?> - <em><?php the_field('ogden_press_source'); ?></em>
		       				<a class="pull-right" href="<?php the_field('ogden_press_link'); ?>">READ ARTICLE &rarr;</a>
		       			</article>
		       			<?php } else { ?>
		       			<article class="clearfix news-item space-above space-below">
		       				<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
		       				<h3><em>News</em></h3>
		       				<?php the_excerpt('...'); ?>
		       				<a class="pull-right" href="<?php the_permalink(); ?>">READ MORE &rarr;</a>
		       			</article>
		       			<?php } ?>
		       		<?php endwhile; ?>
		       		</div>
		       </div>
	       	   <div class="row space-above">
					<div class="container">
						<div class="col-md-12 col-lg-9">
							<nav id="ogden-news">
								<ul class="list-inline">
									<li class="pull-left"><?php previous_posts_link( '&laquo; View Previous Articles', $blog_list->max_num_pages) ?></li>
									<li class="pull-right"><?php next_posts_link( 'View More Articles &raquo;', $blog_list->max_num_pages) ?></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div><!-- End container -->
		 </div>

<?php get_footer(); ?>
