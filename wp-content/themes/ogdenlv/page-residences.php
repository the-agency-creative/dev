<?php

/*
Template Name: Residences
*/

get_header();

?>

    <div class="container-fluid">
    	<div class="row bg-blue-gray">
		    <!-- Page Content -->
		    <div class="container">
		        <div class="row space-above space-below">
		       		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		       		<div class="col-sm-12 col-md-9">
		           		<?php the_content(); ?>
		            </div>
		       		<?php endwhile; else: ?>
		       		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		       		<?php endif; ?>
		        </div><!-- End Row -->
		    </div>
    	</div>
    	<div class="row">
    		<div class="container">
		        <div class="row space-above">
			        <div id="ogden-residences-carousel" class="carousel slide space-below" data-ride="carousel">
				     	<!-- Indicators -->
						<?php
							$ogden_residences_images = get_field('ogden_residences_gallery'); 
							if( $ogden_residences_images ):
							$i=0;
						?>
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<?php foreach( $ogden_residences_images as $ogden_residences_image ): ?>
							<li data-target="#ogden-residences-carousel" data-slide-to="<?php echo $i++; ?>"></li>
							<?php endforeach; ?>
						</ol>
						<div class="carousel-inner">
				        	<?php foreach( $ogden_residences_images as $ogden_residences_image ):
								$ogden_residences_image_size = 'residences-gallery';
								$ogden_residences_image_url = $ogden_residences_image['sizes'][$ogden_residences_image_size];
								$ogden_residences_image_alt = $ogden_residences_image['alt'];
							?>
							<div class="item">
								<img src="<?php echo $ogden_residences_image_url; ?>" alt="<?php the_title(); ?> at The Ogden" class="img-responsive">
							</div>
							<?php endforeach; ?>
				      	</div>
				      	<?php endif; ?>
					  	<a class="left carousel-control" href="#ogden-residences-carousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
					  	<a class="right carousel-control" href="#ogden-residences-carousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
				    </div><!-- /.carousel -->
		        	<div class="table-responsive">
				        <table id="residence-list" class="table table-striped">
				        	<thead>
				        		<th>Residence</th>
				        		<th>Bedrooms</th>
				        		<th>Bathrooms</th>
				        		<th>Floorplan #</th>
				        		<th>Square Feet</th>
				        		<th>Floorplan</th>
				        	</thead>
				        	<tbody>
						        <?php $ogden_residences = new WP_Query("post_type=residences&posts_per_page=99"); while($ogden_residences->have_posts()) : $ogden_residences->the_post();?>	
						        <tr>
						        	<td><?php the_field('ogden_residence_name'); ?></td>
						        	<td><?php the_field('ogden_residence_bedrooms'); ?></td>
						        	<td><?php the_field('ogden_residence_bathrooms'); ?></td>
						        	<td><?php the_field('ogden_residence_unit_number'); ?></td>
						        	<td><?php the_field('ogden_residence_sqft'); ?></td>
						        	<td>
						        	<?php
						        	
						        	if (get_field('ogden_residence_floorplan')) {
						        	
						        	$ogden_floorplan_image_obj = get_field('ogden_residence_floorplan');
						        	$ogden_floorplan_image_src = $ogden_floorplan_image_obj['url'];
									$ogden_floorplan_image_size = 'large';
									$ogden_floorplan_image = $ogden_floorplan_image_obj['sizes'][ $ogden_floorplan_image_size ];
									
									?>
									<a href="<?php echo $ogden_floorplan_image; ?>" rel="lightbox">View</a>
									<?php } else { ?>
						        	<a href="/contact/">Let's Talk</a>
						        	<?php } ?></td>
						        </tr>
						        <?php endwhile; ?>
						        <?php wp_reset_query(); ?>
				        	</tbody>
				        </table>
		        	</div>
		        </div>
			</div><!-- End container -->
		 </div>

<?php get_footer(); ?>
