<?php

/*
Template Name: Schedule Now
*/

?>
<?php get_header() ;?>
<div class="container-fluid">
    	<div class="row">
		    <!-- Page Content -->
		    <div class="container">
		       <div class="row space-above">
		       		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		       		<div class="col-sm-12">
		           		<div class="row">
				    		<div class="container" style="margin:0 !important;">
						        <div class="row">
						        	<div class="col-md-8">
						        		<h1>The New Las Vegas Has Arrived</h1>
										<h2 style="font-style: italic;">And it’s taking shape at The Ogden</h2>
										Discover The Ogden, your sanctuary at the corner of Las Vegas Boulevard and everywhere you want to be. Expansive residences offer modern design touches and refined finishes. Large windows bathe living areas in an abundance of natural light showcasing panoramic vistas of Las Vegas. Just steps outside, an exciting array of dining options, a community grocery market, independent boutiques and more.

										<h3 class="space-above"><em>Condominium Residences from the low $200,000s</em></h3>

										<h4>Design Features:</h4>
										<ul>
											<li>Open floor plans and ample terraces*</li>
											<li>Beautifully upgraded interiors with combinations of hardwood, tile and carpet flooring</li>
											<li>Generous kitchens with granite countertops and large islands*</li>
											<li>Stainless steel GE Profile appliances</li>
											<li>LED recessed and pendant lighting in the kitchen*</li>
											<li>Nest Learning Thermostats</li>
											<li>Distinct and dynamic floor plans</li>
											<li>Blackout shades on all bedroom windows</li>
											<li>Sun shades on all living area windows</li>
											<li>Powder rooms in all one-bedroom homes</li>
											<li>Large walk-in closets</li>
										</ul>
										<em><small>* In select homes</small></em>
										<h1>Open Floor Plans for Every Lifestyle</h1>
										<h2 style="font-style: italic;">Gracious Living Above it All</h2>
										Your home at The Ogden is your personal retreat, your sanctuary towering above an emerging city below. Available one-, two- and three-bedroom plus den residences offer open concept floor plans with kitchens that effortlessly blend into comfortable living areas, and onto private terraces and balconies with panoramic vistas of Las Vegas.<br /><br />
										<div id="ogden-iframe-carousel" class="carousel slide space-above space-below" data-ride="carousel">
									     	<!-- Indicators -->
											<?php
												$ogden_iframe_images = get_field('ogden_iframe_gallery');
												if( $ogden_iframe_images ):
												$i=0;
											?>
											<!-- Indicators -->
											<ol class="carousel-indicators">
												<?php foreach( $ogden_iframe_images as $ogden_iframe_image ): ?>
												<li data-target="#ogden-iframe-carousel" data-slide-to="<?php echo $i++; ?>"></li>
												<?php endforeach; ?>
											</ol>
											<div class="carousel-inner">
									        	<?php foreach( $ogden_iframe_images as $ogden_iframe_image ):
													$ogden_iframe_image_size = 'iframe-gallery';
													$ogden_iframe_image_url = $ogden_iframe_image['sizes'][$ogden_iframe_image_size];
													$ogden_iframe_image_alt = $ogden_iframe_image['alt'];
												?>
												<div class="item">
													<img src="<?php echo $ogden_iframe_image_url; ?>" alt="<?php the_title(); ?> at The Ogden" class="img-responsive">
												</div>
												<?php endforeach; ?>
									      	</div>
									      	<?php endif; ?>
										  	<a class="left carousel-control" href="#ogden-iframe-carousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
										  	<a class="right carousel-control" href="#ogden-iframe-carousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
									    </div><!-- /.carousel -->
							        	<div class="table-responsive space-above">
									        <table id="residence-list" class="table table-striped">
									        	<thead>
									        		<th>Residence</th>
									        		<th>Bedrooms</th>
									        		<th>Bathrooms</th>
									        		<th>Floorplan #</th>
									        		<th>Square Feet</th>
									        		<th>Floorplan</th>
									        	</thead>
									        	<tbody>
											        <?php $ogden_residences = new WP_Query("post_type=residences&posts_per_page=99"); while($ogden_residences->have_posts()) : $ogden_residences->the_post();?>
											        <tr>
											        	<td><?php the_field('ogden_residence_name'); ?></td>
											        	<td><?php the_field('ogden_residence_bedrooms'); ?></td>
											        	<td><?php the_field('ogden_residence_bathrooms'); ?></td>
											        	<td><?php the_field('ogden_residence_unit_number'); ?></td>
											        	<td><?php the_field('ogden_residence_sqft'); ?></td>
											        	<td>
											        	<?php

											        	if (get_field('ogden_residence_floorplan')) {

											        	$ogden_floorplan_image_obj = get_field('ogden_residence_floorplan');
											        	$ogden_floorplan_image_src = $ogden_floorplan_image_obj['url'];
														$ogden_floorplan_image_size = 'large';
														$ogden_floorplan_image = $ogden_floorplan_image_obj['sizes'][ $ogden_floorplan_image_size ];

														?>
														<a href="<?php echo $ogden_floorplan_image; ?>" rel="lightbox">View</a>
														<?php } else { ?>
											        	<a href="/contact/">Let's Talk</a>
											        	<?php } ?></td>
											        </tr>
											        <?php endwhile; ?>
											        <?php wp_reset_query(); ?>
									        	</tbody>
									        </table>
							        	</div>
							        	<h1>In The Life at The Ogden</h1>
										<h2 style="font-style: italic;">Services and Amenities that Reflect your Lifestyle</h2>
										Living at The Ogden is as much about how you live as where you live. Avail yourself of our exceptional services and urban lifestyle amenities including our newly renovated Sky Deck, rooftop pool, 16th Floor Clubhouse and dedicated concierge service.<br /><br />
							        	<div id="ogden-iframe-amenity-carousel" class="carousel slide space-above space-below" data-ride="carousel">
									     	<!-- Indicators -->
											<ol class="carousel-indicators">
												<li data-target="#ogden-iframe-amenity-carousel" data-slide-to="0"></li>
												<li data-target="#ogden-iframe-amenity-carousel" data-slide-to="1"></li>
												<li data-target="#ogden-iframe-amenity-carousel" data-slide-to="2"></li>
												<li data-target="#ogden-iframe-amenity-carousel" data-slide-to="3"></li>
												<li data-target="#ogden-iframe-amenity-carousel" data-slide-to="4"></li>
												<li data-target="#ogden-iframe-amenity-carousel" data-slide-to="5"></li>
												<li data-target="#ogden-iframe-amenity-carousel" data-slide-to="6"></li>
												<li data-target="#ogden-iframe-amenity-carousel" data-slide-to="7"></li>
											</ol>
											<div class="carousel-inner">
												<div class="item active">
													<img src="<?php bloginfo('template_directory'); ?>/images/amenities/ogden-amenities-01.jpg" alt="New Amenities at The Ogden" class="img-responsive">
												</div>
												<div class="item">
													<img src="<?php bloginfo('template_directory'); ?>/images/amenities/ogden-amenities-02.jpg" alt="New Amenities at The Ogden" class="img-responsive">
												</div>
												<div class="item">
													<img src="<?php bloginfo('template_directory'); ?>/images/amenities/ogden-amenities-03.jpg" alt="New Amenities at The Ogden" class="img-responsive">
												</div>
												<div class="item">
													<img src="<?php bloginfo('template_directory'); ?>/images/amenities/ogden-amenities-04.jpg" alt="New Amenities at The Ogden" class="img-responsive">
												</div>
												<div class="item">
													<img src="<?php bloginfo('template_directory'); ?>/images/amenities/ogden-amenities-05.jpg" alt="New Amenities at The Ogden" class="img-responsive">
												</div>
												<div class="item">
													<img src="<?php bloginfo('template_directory'); ?>/images/amenities/ogden-amenities-06.jpg" alt="New Amenities at The Ogden" class="img-responsive">
												</div>
												<div class="item">
													<img src="<?php bloginfo('template_directory'); ?>/images/amenities/ogden-amenities-07.jpg" alt="New Amenities at The Ogden" class="img-responsive">
												</div>
												<div class="item">
													<img src="<?php bloginfo('template_directory'); ?>/images/amenities/ogden-amenities-08.jpg" alt="New Amenities at The Ogden" class="img-responsive">
												</div>
									      	</div>
										  	<a class="left carousel-control" href="#ogden-iframe-amenity-carousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
										  	<a class="right carousel-control" href="#ogden-iframe-amenity-carousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
									    </div><!-- /.carousel -->
						        	</div>
						        </div>
							</div><!-- End container -->
						 </div>
		            </div>
		       		<?php endwhile; else: ?>
		       		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		       		<?php endif; ?>
		        </div><!-- End Row -->
			</div><!-- End container -->
		 </div>
		 <?php get_footer() ;?>
