<?php get_header(); ?>
<?php  $i=0;  if ( have_posts() ) : ?>
				<!-- contain main informative part of the site -->
				<main id="main" role="main">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="txt-area text-center">
									<h1>Residences</h1>
									<p>Pacific Star is a new beacon of luxury in the midst of Beverly Hills, poised in a quiet neighborhood setting within walking distance to everywhere you want to be. Created by developer K Pacific Investments, Gabbay Architects and Interior Architectural Designer, Deb Longua-Zamero, this limited collection of new condominiums, townhomes and penthouses presents an effortless lifestyle of uncompromising luxury and modern simplicity.</p>
									<p><em>Pacific Star is Beverly Hills' first new multi-family development available for ownership south of Wilshire Boulevard since 2009.</em></p>
								</div>
								<nav class="nav text-center">
									<ul class="list-inline">
										<li class="mob">See:</li>
										<?php $residence_types = new WP_Query("post_type=residences"); while($residence_types->have_posts()) : $residence_types->the_post();?>
										<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
										<?php endwhile; ?>
										<?php wp_reset_postdata(); ?>
									</ul>
								</nav>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<!-- article-holder -->
								<div class="article-holder">
									<?php while ( have_posts() ) : the_post();
										$i++;
										if( $i % 2 == 0 ) {
											$align_class = ' align-right';
											$pull_class = ' pull-right';
										} else {
											$align_class = '';
											$pull_class = '';
										}
									?>
									<article class="article<?php echo $align_class; ?>">
										<div class="col-sm-6 col-xs-12 same-height<?php echo $pull_class; ?>">
											<div class="img-box">
												<?php
													$residence_featured_image_obj = get_field('ps_residence_featured_image');
													$residence_featured_image = $residence_featured_image_obj['sizes']['residence-image'];
												?>
												<img class="img-responsive" src="<?php echo $residence_featured_image; ?>" alt="<?php the_title(); ?> - Pacific Star">
											</div>
										</div>
										<div class="col-sm-6 col-xs-12 same-height">
											<div class="text-box">
												<h2><?php the_title(); ?></h2>
												<p><?php the_field('ps_residence_overall_description'); ?></p>
												<div class="btn-holder">
													<a class="btn btn-default" href="<?php the_permalink(); ?>">Learn More</a>
													<a data-toggle="elementscroll" class="btn btn-info" href="<?php the_permalink(); ?>#floorplans">Floorplans</a>
												</div>
											</div>
										</div>
									</article>
									<?php endwhile; ?>
									<div class="article">
										<div class="img-holder">
											<img class="footer-image" src="<?php bloginfo('template_directory'); ?>/images/img-4.jpg" alt="image description">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="text-area">
								<div class="col-lg-5 col-sm-6">
									<h3>interested in owning?</h3>
									<p>To register your interest in Pacific Star and to have a representative contact you please complete the form below. </p>
								</div>
								<div class="col-lg-4 col-lg-push-1 col-sm-6">
									<address>321-327 S. Elm Drive <br> Beverly Hills, California 90212<br> <a href="#">Driving Directions</a> </address>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<!-- Begin MailChimp Signup Form -->
					            <div id="mc_embed_signup">
					                <form action="//pacificstarbh.us9.list-manage.com/subscribe/post?u=579ecb4bb3c3b7a28644271ed&amp;id=185a62a90b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate user-form" target="_blank" novalidate>
						                <fieldset>
						                    <div id="mc_embed_signup_scroll">
							                    <div class="col-holder">
							                        <div class="col">
							                            <div class="mc-field-group form-group">
							                                <input class="form-control" type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="First Name">
							                            </div>
							                        </div>
							                        <div class="col">
							                            <div class="mc-field-group form-group">
							                                <input class="form-control" type="text" value="" name="LNAME" class="" id="mce-LNAME" placeholder="Last Name">
							                            </div>
							                        </div>
							                    </div>
							                    <div class="col-holder">
							                        <div class="col-3">
							                            <div class="mc-field-group form-group">
							                                    <input class="form-control required email" type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Email">
							                            </div>
							                        </div>
							                        <div class="col-3">
							                            <div class="mc-field-group form-group">
							                                <input class="form-control" for type="text" value="" name="MMERGE4" id="mce-MMERGE4" placeholder="Phone">
							                            </div>
							                        </div>
							                        <div class="col-3">
							                            <div class="mc-field-group form-group">
							                                <input class="form-control" for type="text" value="" name="MMERGE3" id="mce-MMERGE3" placeholder="Zip Code">
							                            </div>
							                        </div>
							                    </div>
												<div class="col-holder">
													<div class="col">
														<strong class="title">In what type of home are you interested?</strong>
														<div class="radio-holder">
															<div class="radio-box">
																<input class="form-control" name="house" id="radio-one" type="radio" value="1" name="group[15893][1]">
																<label for="radio-one">Condominium</label>
															</div>
															<div class="radio-box">
																<input class="form-control" name="house" id="radio-two" type="radio" value="2" name="group[15893][2]">
																<label for="radio-two">Townhouse</label>
															</div>
															<div class="radio-box">
																<input class="form-control" name="house" id="radio-three" type="radio" value="4" name="group[15893][4]">
																<label for="radio-three">Penthouse</label>
															</div>
														</div>
													</div>
													<div class="col last">
														<div class="radio-holder">
															<strong class="title">Are you a broker?</strong>
															<div class="radio-area">
																<div class="radio-box">
																	<input class="form-control" name="BROKER" id="radio-four" type="radio">
																	<label for="radio-four">Yes</label>
																</div>
																<div class="radio-box">
																	<input class="form-control" name="BROKER" id="radio-five" type="radio">
																	<label for="radio-five">No</label>
																</div>
															</div>
														</div>
														<strong class="sub-title">If yes, which brokerage?</strong>
														<div class="radio-holder">
															<input class="form-control" type="text" placeholder="Agency Name" name="AGENCYNAME">
														</div>
													</div>
												</div>
						                        <div id="mce-responses" class="clear">
						                                <div class="response" id="mce-error-response" style="display:none"></div>
						                                <div class="response" id="mce-success-response" style="display:none"></div>
						                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						                        <div style="position: absolute; left: -5000px;">
						                            <input type="text" name="b_579ecb4bb3c3b7a28644271ed_185a62a90b" tabindex="-1" value="">
						                        </div>
						                        <div class="clear text-center">
						                                <input class="btn btn-default" type="submit" value="SUBMIT" name="subscribe" id="mc-embedded-subscribe">
						                        </div>
						                    </div>
						                </fieldset>
					                </form>
					            </div>
					            <!--End mc_embed_signup-->
							</div>
						</div>
					</div>
				</main>
<?php else: ?>
<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>
