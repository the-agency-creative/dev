			</div>
		</div>
<!-- footer of the page -->
		<footer id="footer">
			<div class="f1">
				<div class="f2">
					<div class="container">
						<?php if (is_page('team')) { ?>
                        <div class="row home-image">
                        	<div class="col-xs-12">
                       	    	<img src="<?php bloginfo('template_directory'); ?>/images/img-footer-team.jpg" width="1251" height="408" class="img-responsive" alt=""/>
                       	   	</div>
                        </div>
                        <?php } elseif (is_page('contact')) { ?>
                        <div class="row">
                        	<div class="col-xs-12">
                       	    	<img src="<?php bloginfo('template_directory'); ?>/images/img-footer-contact.jpg" width="1251" height="408" class="img-responsive" alt=""/>
                       	    </div>
                        </div>
                        <?php } ?>
						<div class="row">
							<div class="col-sm-12">
								<div class="footer-holder">
									<div class="copy-right">
										<p>Lisa Rubel at <a class="tel" href="tel:4243544887">424.354.4887</a> <a class="email" href="mailto:contact@pacificstarbh.com">contact@pacificstarbh.com</a></p>
									</div>
									<div class="footer-area">
										<a href="http://www.theagencyre.com" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/images/img-6.png" alt="The Agency"></a>
										<p><span>Developer: K Pacific Investments</span> Marketing &amp; Sales exclusively handled by</p>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 text-center">
								<p><small><em>Developer has the right to change square footage, plans and pricing without notice. All illustrations are artist’s concepts only, are not to scale and are subject to change in actual production.</em></small></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<?php wp_footer(); ?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-65839503-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>
