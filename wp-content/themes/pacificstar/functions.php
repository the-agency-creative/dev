<?php

/* register menus */

add_action( 'init', 'register_ps_menu' );
function register_ps_menu() {
	register_nav_menu( 'nav-menu', __( 'Nav Menu' ) );
}

/* kick the tires */

function light_the_fires() {
	wp_deregister_script('jquery');
	wp_enqueue_script( 'jquery', '/wp-includes/js/jquery/jquery.js' , array(), null, true );
	wp_enqueue_script('jquery-custom', get_stylesheet_directory_uri() . '/js/jquery.main.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', false, null, true);
	if(is_page('gallery')) {
	wp_enqueue_script('isotope-js', get_stylesheet_directory_uri() . '/js/gallery/isotope.pkgd.min.js', array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('packery-js', get_stylesheet_directory_uri() . '/js/gallery/min/packery.min.js', array( 'isotope-js' ), '1.0.0', true);
	wp_enqueue_script('magnific-js', get_stylesheet_directory_uri() . '/js/gallery/min/magnific.min.js', array( 'packery-js' ), '1.0.0', true);
	wp_enqueue_script('global-js', get_stylesheet_directory_uri() . '/js/gallery/global.js', array( 'jquery' ), '1.0.0', true);
	}
}
add_action( 'wp_enqueue_scripts', 'light_the_fires' );

/* light the fires */

function add_footer_scripts() { ?>

	<script>
		if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
			var msViewportStyle = document.createElement('style')
			msViewportStyle.appendChild(
			document.createTextNode(
			'@-ms-viewport{width:auto!important}'
			)
			)
			document.querySelector('head').appendChild(msViewportStyle)
		}
	</script>

<?php }
//add to wp_footer
add_action( 'wp_footer', 'add_footer_scripts', 100 );

// set up our sitewide image sizes

add_theme_support( 'post-thumbnails' );
add_image_size('residence-image', 570, 354, true); // residence featured image
add_image_size('floorplan-image', 847, 551, true); // floorplan slider image

//add the page slug to body_class()
function add_slug_body_class( $classes ) {
	global $post;
		if ( isset( $post ) ) {
			$classes[] = $post->post_type . '-' . $post->post_name;
		}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

// let's get some post types set up

function residences_posttype() {
    register_post_type( 'residences',
        array(
            'labels' => array(
                'name' => __( 'Residences' ),
                'singular_name' => __( 'Residence' ),
                'add_new' => __( 'Add New Residence' ),
                'add_new_item' => __( 'Add New Residence' ),
                'edit_item' => __( 'Edit Residence' ),
                'new_item' => __( 'Add New Residence' ),
                'view_item' => __( 'View Residence' ),
                'search_items' => __( 'Search Residences' ),
                'not_found' => __( 'No Residences found' ),
                'not_found_in_trash' => __( 'No Residences found in trash' )
            ),
            'public' => true,
            'supports' => array('title'),
            'capability_type' => 'post',
            'capabilities' => array(
		        'publish_posts' => 'manage_options',
		        'edit_posts' => 'manage_options',
		        'edit_others_posts' => 'manage_options',
		        'delete_posts' => 'manage_options',
		        'delete_others_posts' => 'manage_options',
		        'read_private_posts' => 'manage_options',
		        'edit_post' => 'manage_options',
		        'delete_post' => 'manage_options',
		        'read_post' => 'manage_options',
		    ),
            'rewrite' => array( 'slug' => 'residences'),
            'has_archive' => true,
            'menu_position' => 5
        )
    );
}

add_action( 'init', 'residences_posttype' );

// ps excerpt tweaks

function ps_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'ps_excerpt_length', 999 );

function ps_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'ps_excerpt_more');

// STOP THE MADNESS
function change_css_hide_wpmu_dash()
{
    echo '<style type="text/css"> .wp-admin div.error, #cpt_info_box {display: none !important;}</style>';
}
add_action('all_admin_notices', 'change_css_hide_wpmu_dash');class WPMUDEV_Update_Notifications {}

?>
