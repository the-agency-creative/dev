<!DOCTYPE html>
<html>
<head>
	<!-- set the encoding of your site -->
	<meta charset="utf-8">
	<!-- set the viewport width and initial-scale on mobile devices -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php wp_title(''); ?></title>
	<!-- include custom bootstrap stylesheet -->
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css">
	<!-- include the site stylesheet -->
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/all.css">
	<?php if(is_page('gallery')) { ?>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/gallery/global.css">
	<?php } ?>
	<!-- fonts -->
	<script src="//use.typekit.net/bli3ezs.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<!-- main container of all the page elements -->
	<div id="wrapper">
		<div class="w1">
			<div class="w2">
				<!-- header of the page -->
				<header id="header" class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="header-holder">
								<!-- page logo -->
								<strong class="logo"><a href="/"><img src="<?php bloginfo('template_directory'); ?>/images/logo.jpg" alt="PacificStar"></a></strong>
								<div class="holder">
									<div class="open-close hidden-md hidden-lg">
										<a class="opener btn-info" href="/contact/"><span class="desktop">INQUIRE NOW</span> <span class="mob">close</span></a>
										<div class="slide">
											<p>To register your interest in Pacific Star and to have a representative contact you please complete the form below. </p>
											<!-- Begin MailChimp Signup Form -->
								            <div id="mc_embed_signup">
								                <form action="//pacificstarbh.us9.list-manage.com/subscribe/post?u=579ecb4bb3c3b7a28644271ed&amp;id=185a62a90b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate user-form" target="_blank" novalidate>
									                <fieldset>
									                    <div id="mc_embed_signup_scroll">
										                    <div class="col-holder">
										                        <div class="col">
										                            <div class="mc-field-group form-group">
										                                <input class="form-control" type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="First Name">
										                            </div>
										                        </div>
										                        <div class="col">
										                            <div class="mc-field-group form-group">
										                                <input class="form-control" type="text" value="" name="LNAME" class="" id="mce-LNAME" placeholder="Last Name">
										                            </div>
										                        </div>
										                    </div>
										                    <div class="col-holder">
										                        <div class="col">
										                            <div class="mc-field-group form-group">
										                                    <input class="form-control required email" type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Email">
										                            </div>
										                        </div>
										                        <div class="col">
										                            <div class="mc-field-group form-group">
										                                <input class="form-control" for type="text" value="" name="MMERGE3" id="mce-MMERGE3" placeholder="Zip Code">
										                            </div>
										                        </div>
										                    </div>
															<!--
									                        <div class="col-xs-12 col-sm-12 col-md-6">
									                            <div class="mc-field-group input-group radio-inline">
									                                What is your price range?
									                                <ul>
									                                    <li><input type="radio" value="Under $2.5M" name="PRICERANGE" id="mce-PRICERANGE-0"><label for="mce-PRICERANGE-0"> <span></span> Under $2.5M</label></li>
									                                    <li><input type="radio" value="More than $2.5M" name="PRICERANGE" id="mce-PRICERANGE-1"><label for="mce-PRICERANGE-1"> <span></span> More than $2.5M</label></li>
									                                </ul>
									                            </div>
									                        </div>
															-->
															<div class="col-holder">
																<div class="col">
																	<strong class="title">In what type of home are you interested?</strong>
																	<div class="radio-holder">
																		<div class="radio-box">
																			<input class="form-control" name="house" id="radio-one" type="radio" value="1" name="group[15893][1]">
																			<label for="radio-one">Condominium</label>
																		</div>
																		<div class="radio-box">
																			<input class="form-control" name="house" id="radio-two" type="radio" value="2" name="group[15893][2]">
																			<label for="radio-two">Townhouse</label>
																		</div>
																		<div class="radio-box">
																			<input class="form-control" name="house" id="radio-three" type="radio" value="4" name="group[15893][4]">
																			<label for="radio-three">Penthouse</label>
																		</div>
																	</div>
																</div>
																<div class="col last">
																	<div class="radio-holder">
																		<strong class="title">Are you a broker?</strong>
																		<div class="radio-area">
																			<div class="radio-box">
																				<input class="form-control" name="BROKER" id="radio-four" type="radio">
																				<label for="radio-four">Yes</label>
																			</div>
																			<div class="radio-box">
																				<input class="form-control" name="BROKER" id="radio-five" type="radio">
																				<label for="radio-five">No</label>
																			</div>
																		</div>
																	</div>
																	<strong class="sub-title">If yes, which brokerage?</strong>
																	<div class="radio-holder">
																		<input class="form-control" type="text" placeholder="Agency Name" name="AGENCYNAME">
																	</div>
																</div>
															</div>
									                        <div id="mce-responses" class="clear">
									                                <div class="response" id="mce-error-response" style="display:none"></div>
									                                <div class="response" id="mce-success-response" style="display:none"></div>
									                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
									                        <div style="position: absolute; left: -5000px;">
									                            <input type="text" name="b_579ecb4bb3c3b7a28644271ed_185a62a90b" tabindex="-1" value="">
									                        </div>
									                        <div class="clear text-center">
									                                <input class="btn btn-default" type="submit" value="SUBMIT" name="subscribe" id="mc-embedded-subscribe">
									                        </div>
									                    </div>
									                </fieldset>
								                </form>
								            </div>
								            <!--End mc_embed_signup-->
										</div>
									</div>
									<div class="btn-contact visible-lg visible-md">
										<a class="btn-info" href="/contact/">INQUIRE NOW</a>
									</div>
									<div class="contact hidden-lg">
										Call Lisa Rubel AT <a class="tel" href="tel:4243544887">424.354.4887</a> or <a href="mailto:">Email</a>
									</div>
								</div>
								<div class="nav-holder">
									<a class="nav-opener" href="#">
										<span class="bar-holder">
											<span class="bar"></span>
											<span class="bar"></span>
											<span class="bar"></span>
										</span>
										<div class="nav-txt">
											<em class="show-btn">Menu</em>
											<em class="close-btn">Close</em>
										</div>
									</a>
								</div>
								<div class="drop-holder">
									<div class="drop-area">
										<!-- main navigation of the page -->
										<nav id="nav">
											<ul class="navbar-right list-inline">
												<?php
									    		wp_nav_menu(
									    			array(
									    			      'theme_location' 	=> 'nav-menu',
									    			      'container' => false,
									    			      'menu_class' => 'navbar-right list-inline',
									    			      'menu_id' => '',
									    			      'items_wrap' => '%3$s'
									    			     )
									    			  );
								    			?>
											</ul>
											<address class="hidden-md">
												321 - 327 S. Elm Drive <br> Beverly Hills, California 90212
												<span>Call Lisa Rubel AT <a href="tel:4243544887" class="tel">424.354.4887</a> <em><a href="mailto:contact@pacificstarbh.com">contact@pacificstarbh.com</a></em></span>
											</address>
										</nav>
									</div>
								</div>
							</div>
						</div>
					</div>
				</header>
