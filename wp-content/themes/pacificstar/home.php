<?php get_header(); ?>
				<!-- contain main informative part of the site -->
				<main id="main" role="main" class="mb0">
					<!-- Carousel
				    ================================================== -->
				    <div id="ps-carousel" class="carousel slide carousel-fade" data-ride="carousel">
					    <div class="container">
						    <div class="row">
							    <div class="col-xs-12">
								    <ol class="carousel-indicators text-right">
							        	<li data-target="#ps-carousel" data-slide-to="0" class="active"></li>
										<li data-target="#ps-carousel" data-slide-to="1"></li>
										<li data-target="#ps-carousel" data-slide-to="2"></li>
										<li data-target="#ps-carousel" data-slide-to="3"></li>
							      	</ol>
							    </div>
						    </div>
					    </div>
						<div class="carousel-inner" role="listbox">
				        	<div class="item active">
								<div class="container">
									<div class="row home-image">
				                    	<div class="col-xs-12">
						           	    	<a href="//fast.wistia.net/embed/iframe/qvww4f1p2m?popover=true" class="wistia-popover[height=641,playerColor=3ea9f5,width=1140]"><img src="<?php bloginfo('template_directory'); ?>/images/home-hero-video.jpg" width="1251" height="408" class="img-responsive" alt="" /></a>

						           	    	<script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/popover-v1.js"></script>
						           	   	</div>
				            		</div>
									<div class="row home-text">
										<div class="col-xs-12 col-sm-8 col-sm-offset-2">
											<div class="row text-center">
												<p>A limited collection of<br />
												<strong>30 New Luxury Residences in the Heart of Beverly Hills</strong><br />
												Set Amidst a Charming Tree-Lined Landscape
												</p>
											</div>
											<hr />
											<div class="row home-text">
												<div class="col-xs-12 col-sm-6 cta-text">
													<p>Contact us now for priority<br />
														Phase One pricing and selection.
													</p>
												</div>
												<div class="col-xs-12 col-sm-6 cta-btn">
													<a class="btn btn-default" href="/residences/">View Residences</a>
												</div>
											</div>
										</div>
									</div>
				          		</div>
							</div>
					        <div class="item">
					          	<div class="container">
									<div class="row home-image">
						            	<div class="col-xs-12">
						           	    	<img src="<?php bloginfo('template_directory'); ?>/images/home-hero-4.jpg" width="1251" height="408" class="img-responsive" alt=""/>
						           	   	</div>
					            	</div>
									<div class="row home-text">
										<div class="col-xs-12 col-sm-8 col-sm-offset-2">
											<div class="row text-center">
												<p>A limited collection of<br />
												<strong>30 New Luxury Residences in the Heart of Beverly Hills</strong><br />
												Set Amidst a Charming Tree-Lined Landscape
												</p>
											</div>
											<hr />
											<div class="row home-text">
												<div class="col-xs-12 col-sm-6 cta-text">
													<p>Experience the effortless lifestyle offered<br />
														exclusively at Pacific Star.
													</p>
												</div>
												<div class="col-xs-12 col-sm-6 cta-btn">
													<a class="btn btn-default" href="/amenities/">View Amenities</a>
												</div>
											</div>
										</div>
									</div>
					          	</div>
					        </div>
					        <div class="item">
					          	<div class="container">
									<div class="row home-image">
						            	<div class="col-xs-12">
						           	    	<img src="<?php bloginfo('template_directory'); ?>/images/home-hero-3.jpg" width="1251" height="408" class="img-responsive" alt=""/>
						           	   	</div>
						            </div>
									<div class="row home-text">
										<div class="col-xs-12 col-sm-8 col-sm-offset-2">
											<div class="row text-center">
												<p>A limited collection of<br />
												<strong>30 New Luxury Residences in the Heart of Beverly Hills</strong><br />
												Set Amidst a Charming Tree-Lined Landscape
												</p>
											</div>
											<hr />
											<div class="row home-text">
												<div class="col-xs-12 col-sm-6 cta-text">
													<p>Please join our interest list for more<br />
														information about our residences.
													</p>
												</div>
												<div class="col-xs-12 col-sm-6 cta-btn">
													<a class="btn btn-default" href="/contact/">Contact Us</a>
												</div>
											</div>
										</div>
									</div>
					          	</div>
					        </div>
					        <div class="item">
					          	<div class="container">
									<div class="row home-image">
						            	<div class="col-xs-12">
						           	    	<img src="<?php bloginfo('template_directory'); ?>/images/home-hero-2.jpg" width="1251" height="408" class="img-responsive" alt=""/>
						           	   	</div>
						            </div>
									<div class="row home-text">
										<div class="col-xs-12 col-sm-8 col-sm-offset-2">
											<div class="row text-center">
												<p>A limited collection of<br />
												<strong>30 New Luxury Residences in the Heart of Beverly Hills</strong><br />
												Set Amidst a Charming Tree-Lined Landscape
												</p>
											</div>
											<hr />
											<div class="row home-text">
												<div class="col-xs-12 col-sm-6 cta-text">
													<p>Discover Pacific Star’s exceptional<br />
														lifestyle at the center of it all.
													</p>
												</div>
												<div class="col-xs-12 col-sm-6 cta-btn">
													<a class="btn btn-default" href="/beverly-hills/">What's Nearby</a>
												</div>
											</div>
										</div>
									</div>
					          	</div>
					        </div>
				      	</div>
				    </div><!-- /.carousel -->
				</main>
<?php get_footer(); ?>
