// Global scripts
jQuery(document).ready(function($) {

		// isotope outert container
		var $galleryOuter = $('#hp-gallery');
		// isotope settings
		$galleryOuter.isotope({
			itemSelector: '.hp-gal-item',
			layoutMode: 'packery',
			masonry: {
				columnWidth: ( $galleryOuter.width() / 4 )
			}
		});
		// recalc columnWidth
		$(window).resize( function() {
			$galleryOuter.isotope({
				masonry: { columnWidth: ( $galleryOuter.width() / 4 ) }
			});
		});
		$('.gallery-filters').on( 'click', 'a[data-filter]', function() {
		    var filterBy = $(this).attr('data-filter');

		    $('a[data-filter]').removeClass('active-filter');
		    $(this).addClass('active-filter');

		    $galleryOuter.isotope({
		    	filter: filterBy
		    });
		});

		//magnific popup
		$galleryOuter.magnificPopup({
			delegate: '.hp-gal-item',
			closeOnContentClick : false,
	      closeOnBgClick :true,
	      showCloseBtn : true,
	      enableEscapeKey : false,
				closeMarkup: '<button class="mfp-close" id="hp-gal-close">Close</button>',
			removalDelay: 180,
			gallery: {
				enabled: true,
				arrowMarkup: '<button title="%title%" class="hp-gal-arrow hp-gal-arrow_%dir%"></button>'
			},
			image: {
				markup: '<div class="mfp-figure">'+
            '<div class="mfp-close"></div>'+
            '<div class="mfp-img"></div>'+
            '<div class="mfp-bottom-bar">'+
              '<div class="mfp-title"></div>'+
              '<div class="mfp-counter"></div>'+
            '</div>'+
          '</div>'
			},
			iframe: {
				markup: '<div class="mfp-iframe-scaler">'+
							'<div class="mfp-close"></div>'+
            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
          '</div>'
			},
			callbacks: {
				elementParse: function(item) {
					if (item.type === "iframe") {
						item.src = item.el.data('video-url');
					} else {
						item.src = item.el.data('image-url');
					}
				}
			}
		});
		// slow scroll to anchor with offset

			$(function() {
		        $('[data-toggle="elementscroll"]').click(function() {
				    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
				                || location.hostname == this.hostname) {
				        var hashStr = this.hash.slice(1);
				        var target = $(this.hash);
				        target = target.length ? target : $('[name=' + hashStr +']');

				        if (target.length) {
				            $('html,body').animate({ scrollTop: target.offset().top - 100}, 1000);
				            window.location.hash = hashStr;
				            return false;
				        }
				    }
				});
		    });

			// add active class to carousel pagination

			$(".carousel-indicators li:first").addClass("active");
			$(".carousel-inner .item:first").addClass("active");

});
