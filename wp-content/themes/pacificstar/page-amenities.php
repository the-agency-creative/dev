<?php get_header(); ?>
				<!-- contain main informative part of the site -->
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<main id="main" role="main">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 text-center">
								<h1><?php the_title(); ?></h1>
								<?php the_content(); ?>
							</div>
						</div>
						<hr />
						<div class="row mb60 amenities-list">
							<div class="col-md-6 text-right">
								<?php the_field('ps_amenities_left_list'); ?>
							</div>
							<div class="col-md-6 text-left">
								<?php the_field('ps_amenities_right_list'); ?>
							</div>
						</div>
                       <!-- Amenities Three Images -->
                        <div class="row three-imgs no-edges">
                        	<div class="col-xs-6 col-sm-4">
                       	    	<img src="<?php bloginfo('template_directory'); ?>/images/amenities-1.jpg" width="750" height="510" class="img-responsive" alt=""/>
                            </div>
                            <div class="col-xs-6 col-sm-push-4 col-sm-4">
                       	    	<img src="<?php bloginfo('template_directory'); ?>/images/amenities-3.jpg" width="750" height="510" class="img-responsive" alt=""/>
                            </div>
                            <div class="col-sm-pull-4 col-sm-4">
                       	    	<img src="<?php bloginfo('template_directory'); ?>/images/charging-car.jpg" width="750" height="510" class="img-responsive" alt=""/>
                            </div>
                        </div>
					</div>
				</main>
				<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				<?php endif; ?>
<?php get_footer(); ?>
