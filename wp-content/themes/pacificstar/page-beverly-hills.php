<?php get_header(); ?>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<!-- contain main informative part of the site -->
				<main id="main" role="main">
					<div class="container">
						<div class="row mb60">
							<div class="col-sm-12 text-center">
								<h1><?php the_title(); ?></h1>
								<?php the_content(); ?>
							</div>
						</div>
                        <div class="row three-imgs no-edges mb60">
                        	<div class="col-xs-6 col-sm-4">
                       	    	<img src="<?php bloginfo('template_directory'); ?>/images/bh-1.jpg" width="750" height="510" class="img-responsive" alt=""/>
                            </div>
                            <div class="col-xs-6 col-sm-push-4 col-sm-4">
                       	    	<img src="<?php bloginfo('template_directory'); ?>/images/bh-3.jpg" width="750" height="510" class="img-responsive" alt=""/>
                            </div>
                            <div class="col-sm-pull-4 col-sm-4">
                       	    	<img src="<?php bloginfo('template_directory'); ?>/images/bh-2.jpg" width="750" height="510" class="img-responsive" alt=""/>
                            </div>
                        </div>
							<?php
								// check if the repeater field has rows of data
								if( have_rows('ps_neighborhood_categories') ):
								// loop through the rows of data
								while ( have_rows('ps_neighborhood_categories') ) : the_row();
							?>
							<div class="row bh-list">
								<div class="col-sm-12 col-sm-offset-2">
									<h3><?php the_sub_field('ps_category_title'); ?><h3>
								</div>
								<div class="col-xs-offset-1 col-sm-offset-3 col-sm-9">
	                            	<?php the_sub_field('ps_category_listings'); ?>
								</div>
							</div>
							<?php
								endwhile;
								else :
								// no rows found
								endif;
							?>
						<hr class="margin-top margin-bottom" />
						<div class="row">
							<div class="col-sm-12">
								<?php echo do_shortcode('[gjmaps map_id="1" position="top" zoom="13"]'); ?>
							</div>
						</div>
					</div>
				</main>
				<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				<?php endif; ?>
<?php get_footer(); ?>
