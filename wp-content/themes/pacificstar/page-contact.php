<?php get_header(); ?>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<!-- contain main informative part of the site -->
				<main id="main" role="main" class="mb0">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 text-center mb60">
								<h1><?php the_title();?></h1>
								<h5>Lisa Rubel at <a href="tel:4243544887">424.354.4887</a></h5>
								<a href="mailto:contact@pacificstarbh.com">contact@pacificstarbh.com</a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<!-- Begin MailChimp Signup Form -->
					            <div id="mc_embed_signup">
					                <form action="//pacificstarbh.us9.list-manage.com/subscribe/post?u=579ecb4bb3c3b7a28644271ed&amp;id=185a62a90b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate user-form" target="_blank" novalidate>
						                <fieldset>
						                    <div id="mc_embed_signup_scroll">
							                    <div class="col-holder">
							                        <div class="col">
							                            <div class="mc-field-group form-group">
							                                <input class="form-control" type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="First Name">
							                            </div>
							                        </div>
							                        <div class="col">
							                            <div class="mc-field-group form-group">
							                                <input class="form-control" type="text" value="" name="LNAME" class="" id="mce-LNAME" placeholder="Last Name">
							                            </div>
							                        </div>
							                    </div>
							                    <div class="col-holder">
							                        <div class="col-3">
							                            <div class="mc-field-group form-group">
							                                    <input class="form-control required email" type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Email">
							                            </div>
							                        </div>
							                        <div class="col-3">
							                            <div class="mc-field-group form-group">
							                                <input class="form-control" for type="text" value="" name="MMERGE4" id="mce-MMERGE4" placeholder="Phone">
							                            </div>
							                        </div>
							                        <div class="col-3">
							                            <div class="mc-field-group form-group">
							                                <input class="form-control" for type="text" value="" name="MMERGE3" id="mce-MMERGE3" placeholder="Zip Code">
							                            </div>
							                        </div>
							                    </div>
												<!--
						                        <div class="col-xs-12 col-sm-12 col-md-6">
						                            <div class="mc-field-group input-group radio-inline">
						                                What is your price range?
						                                <ul>
						                                    <li><input type="radio" value="Under $2.5M" name="PRICERANGE" id="mce-PRICERANGE-0"><label for="mce-PRICERANGE-0"> <span></span> Under $2.5M</label></li>
						                                    <li><input type="radio" value="More than $2.5M" name="PRICERANGE" id="mce-PRICERANGE-1"><label for="mce-PRICERANGE-1"> <span></span> More than $2.5M</label></li>
						                                </ul>
						                            </div>
						                        </div>
												-->
												<div class="col-holder">
													<div class="col">
														<strong class="title">In what type of home are you interested?</strong>
														<div class="radio-holder">
															<div class="radio-box">
																<input class="form-control" name="house" id="radio-one" type="radio" value="1" name="group[15893][1]">
																<label for="radio-one">Condominium</label>
															</div>
															<div class="radio-box">
																<input class="form-control" name="house" id="radio-two" type="radio" value="2" name="group[15893][2]">
																<label for="radio-two">Townhouse</label>
															</div>
															<div class="radio-box">
																<input class="form-control" name="house" id="radio-three" type="radio" value="4" name="group[15893][4]">
																<label for="radio-three">Penthouse</label>
															</div>
														</div>
													</div>
													<div class="col last">
														<div class="radio-holder">
															<strong class="title">Are you a broker?</strong>
															<div class="radio-area">
																<div class="radio-box">
																	<input class="form-control" name="BROKER" id="radio-four" type="radio">
																	<label for="radio-four">Yes</label>
																</div>
																<div class="radio-box">
																	<input class="form-control" name="BROKER" id="radio-five" type="radio">
																	<label for="radio-five">No</label>
																</div>
															</div>
														</div>
														<strong class="sub-title">If yes, which brokerage?</strong>
														<div class="radio-holder">
															<input class="form-control" type="text" placeholder="Agency Name" name="AGENCYNAME">
														</div>
													</div>
												</div>
						                        <div id="mce-responses" class="clear">
						                                <div class="response" id="mce-error-response" style="display:none"></div>
						                                <div class="response" id="mce-success-response" style="display:none"></div>
						                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						                        <div style="position: absolute; left: -5000px;">
						                            <input type="text" name="b_579ecb4bb3c3b7a28644271ed_185a62a90b" tabindex="-1" value="">
						                        </div>
						                        <div class="clear text-center">
						                                <input class="btn btn-default" type="submit" value="SUBMIT" name="subscribe" id="mc-embedded-subscribe">
						                        </div>
						                    </div>
						                </fieldset>
					                </form>
					            </div>
					            <!--End mc_embed_signup-->
							</div>
						</div>
					</div>
				</main>
				<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				<?php endif; ?>
<?php get_footer(); ?>
