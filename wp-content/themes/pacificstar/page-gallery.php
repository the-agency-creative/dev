<?php /***** Template Name: Gallery ******/ get_header(); ?>
<!-- contain main informative part of the site -->
<main id="main" role="main">
    <div class="container">
        <div class="row">
            <div class="txt-area text-center">
                <h1><?php the_title(); ?></h1>
                <p>Pacific Star is a new beacon of luxury in the midst of Beverly Hills, poised in a quiet neighborhood setting within walking distance to everywhere you want to be. Created by developer K Pacific, Gabbay Architects and Interior Architectural Designer, Deb Longua-Zamero, this limited collection of new condominiums, townhomes and penthouses presents an effortless lifestyle of uncompromising luxury and modern simplicity.</p>
            </div>
        </div>

        <div class="row">
            <!-- Gallery -->
            <div class="main-cont">

                <div class="gallery-filters text-center">
                    <span>Sort By:</span>
                    <a data-filter="*" class="active-filter" href="#0">All</a>
                    <a data-filter=".gal-int" href="#0">Interior</a>
                    <a data-filter=".gal-ext" href="#0">Exterior</a>
                    <!-- <a data-filter=".hp-gal-video" href="#0">Videos</a> -->
                </div>
                <div id="hp-gallery" class="gallery-items">
                    <div class="gal-sizer"></div>
                    <figure class="hp-gal-item mfp-image hp-gal-img gal-ext hp-gal-half hp-gal-2xh" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-exterior-01.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/square/pacific-star-render-exterior-01.jpg" alt="">
                    </figure>
                    
                    <figure class="hp-gal-item mfp-image hp-gal-img gal-int hp-gal-half" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-interior-07.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/half/pacific-star-render-interior-07.jpg" alt="">
                    </figure>

                    <figure class="hp-gal-item mfp-image hp-gal-img gal-ext hp-gal-quad" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-exterior-02.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/quad/pacific-star-render-exterior-02.jpg" alt="">
                    </figure>
                    <figure class="hp-gal-item mfp-image hp-gal-img gal-int hp-gal-quad" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-interior-02.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/quad/pacific-star-render-interior-02.jpg" alt="">
                    </figure>
                    <figure class="hp-gal-item mfp-image hp-gal-img gal-ext hp-gal-quad" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-exterior-03.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/quad/pacific-star-render-exterior-03.jpg" alt="">
                    </figure>

                     <figure class="hp-gal-item mfp-image hp-gal-img gal-int hp-gal-half hp-gal-2xh" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-interior-08.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/square/pacific-star-render-interior-08.jpg" alt="">
                    </figure>
                    
                    <figure class="hp-gal-item mfp-image hp-gal-img gal-ext hp-gal-quad" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-exterior-04.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/quad/pacific-star-render-exterior-04.jpg" alt="">
                    </figure>
                    <figure class="hp-gal-item mfp-image hp-gal-img gal-int hp-gal-quad" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-interior-04.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/quad/pacific-star-render-interior-04.jpg" alt="">
                    </figure>

                    <figure class="hp-gal-item mfp-image hp-gal-img gal-int hp-gal-quad" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-interior-06.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/quad/pacific-star-render-interior-06.jpg" alt="">
                    </figure>

                    <figure class="hp-gal-item mfp-image hp-gal-img gal-ext hp-gal-half hp-gal-2xh" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-exterior-07.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/square/pacific-star-render-exterior-07.jpg" alt="">
                    </figure>

                    <figure class="hp-gal-item mfp-image hp-gal-img gal-int hp-gal-half hp-gal-quad" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-interior-10.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/quad/pacific-star-render-interior-10.jpg" alt="">
                    </figure>

                    <figure class="hp-gal-item mfp-image hp-gal-img gal-ext hp-gal-half hp-gal-quad" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-exterior-06.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/quad/pacific-star-render-exterior-06.jpg" alt="">
                    </figure>

                    <figure class="hp-gal-item mfp-image hp-gal-img gal-int hp-gal-half" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-interior-05.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/half/pacific-star-render-interior-05.jpg" alt="">
                    </figure>
                    
                    <figure class="hp-gal-item mfp-image hp-gal-img gal-int hp-gal-half" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-interior-01.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/half/pacific-star-render-interior-01.jpg" alt="">
                    </figure>
                   
                    <figure class="hp-gal-item mfp-image hp-gal-img gal-int hp-gal-half hp-gal-2xh" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-interior-03.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/square/pacific-star-render-interior-03.jpg" alt="">
                    </figure>
                    
                    <figure class="hp-gal-item mfp-image hp-gal-img gal-int hp-gal-half hp-gal-quad" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-interior-09.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/quad/pacific-star-render-interior-09.jpg" alt="">
                    </figure>

                    <figure class="hp-gal-item mfp-image hp-gal-img gal-ext hp-gal-quad" data-image-url="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/pacific-star-render-exterior-05.jpg">
                        <img src="<?php bloginfo('template_directory'); ?>/images/gallery/renderings/quad/pacific-star-render-exterior-05.jpg" alt="">
                    </figure>

                </div>
            </div>
            <!--/end main cont-->
        </div>
        <!--/end row-->
    </div>
</main>

<?php get_footer(); ?>
