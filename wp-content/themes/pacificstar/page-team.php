<?php get_header(); ?>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<!-- contain main informative part of the site -->
				<main id="main" role="main" class="mb0">
					<div class="container">
						<div class="row">
							<div class="col-sm-12 text-center">
								<h1><?php the_title(); ?></h1>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 text-center">
								<h5>Lisa Rubel at 424.354.4887</h5>
								<a href="mailto:contact@pacificstarbh.com">contact@pacificstarbh.com</a>
							</div>
							<div class="col-md-6 text-center">
								<h5>Kim Hartley at 424.281.6848</h5>
								<a href="mailto:contact@pacificstarbh.com">contact@pacificstarbh.com</a>
							</div>
						</div>
						<hr />
						<div class="row">
							<div class="col-sm-12">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				</main>
				<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				<?php endif; ?>
<?php get_footer(); ?>
