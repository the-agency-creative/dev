<?php get_header(); ?>
				<!-- contain main informative part of the site -->
				<main id="main" role="main">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="txt-area text-center">
									<h1><?php the_title(); ?></h1>
									<p><?php the_field('ps_residence_overall_description'); ?></p>
								</div>
								<nav class="nav text-center">
									<ul class="list-inline">
										<li class="mob">Also see:</li>
										<li<?php if(is_single('penthouses')) { echo ' class="active desktop"'; } ?>><a href="/residence/penthouses/">Penthouses</a></li>
										<li<?php if(is_single('townhomes')) { echo ' class="active desktop"'; } ?>><a href="/residence/townhomes/">Townhomes</a></li>
										<li<?php if(is_single('condominiums')) { echo ' class="active desktop"'; } ?>><a href="/residence/condominiums/">Condominiums</a></li>
									</ul>
								</nav>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<!-- article-holder -->
								<div class="article-holder">
									<article class="article">
										<div class="col-sm-6 col-xs-12 same-height">
											<div class="img-box">
												<?php
													$residence_featured_image_obj = get_field('ps_residence_featured_image');
													$residence_featured_image = $residence_featured_image_obj['sizes']['residence-image'];
												?>
												<img class="img-responsive" src="<?php echo $residence_featured_image; ?>" alt="<?php the_title(); ?> - Pacific Star">
											</div>
										</div>
										<div class="col-sm-6 col-xs-12 same-height">
											<div class="text-box">

												<?php the_field('ps_residence_detail_description'); ?>
												<!-- <div class="btn-holder inner">
													<a class="btn btn-default" href="#">View Gallery</a>
													<a class="btn btn-info visible-lg" href="#">Floorplans</a>
												</div>  -->

											</div>
										</div>
									</article>
								</div>
								<!-- visual -->
								<a name="floorplans"></a>
								<div class="visual">
									<div id="ps-floorplan-carousel" class="carousel slide" data-ride="carousel">
										<div class="links-holder">
											<h3><?php the_title(); ?> Floorplans</h3>
										</div>
										<?php if( have_rows('ps_residence_floorplans') ): ?>
										<div class="carousel-inner" role="listbox">
											<?php while( have_rows('ps_residence_floorplans') ): the_row();
												// vars
												$floorplan_image_obj = get_sub_field('ps_residence_floorplan_image');
												$floorplan_image = $floorplan_image_obj['sizes']['floorplan-image'];
												$floorplan_pdf = get_sub_field('ps_residence_floorplan_pdf');
											?>
											<div class="item">
												<div class="frame">
													<?php if ($floorplan_pdf) : ?>
													<div class="carousel-caption">
														<a target="_blank" class="btn btn-default" href="<?php echo $floorplan_pdf; ?>">Download PDF</a>
													</div>
													<?php endif; ?>
													<img src="<?php echo $floorplan_image; ?>" class="img-responsive" alt="image description">
												</div>
											</div>
											<?php endwhile; ?>
										</div>
										<a class="left carousel-control" href="#ps-floorplan-carousel" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										</a>
										<a class="right carousel-control" href="#ps-floorplan-carousel" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										</a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="text-area">
								<div class="col-lg-5 col-sm-6">
									<h3>interested in owning?</h3>
									<p>To register your interest in Pacific Star and to have a representative contact you please complete the form below. </p>
								</div>
								<div class="col-lg-4 col-lg-push-1 col-sm-6">
									<address>321-327 Elm Drive <br> Beverly Hills, California 90212<br> <a href="#">Driving Directions</a> </address>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<!-- user form -->
								<form class="user-form" action="#">
									<fieldset>
										<div class="col-holder">
											<div class="col">
												<input class="form-control" type="text" placeholder="First Name">
											</div>
											<div class="col">
												<input class="form-control" type="text" placeholder="Last Name">
											</div>
										</div>
										<div class="col-holder">
					                        <div class="col-3">
					                            <div class="mc-field-group form-group">
					                                    <input class="form-control required email" type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Email">
					                            </div>
					                        </div>
					                        <div class="col-3">
					                            <div class="mc-field-group form-group">
					                                <input class="form-control" for type="text" value="" name="MMERGE4" id="mce-MMERGE4" placeholder="Phone">
					                            </div>
					                        </div>
					                        <div class="col-3">
					                            <div class="mc-field-group form-group">
					                                <input class="form-control" for type="text" value="" name="MMERGE3" id="mce-MMERGE3" placeholder="Zip Code">
					                            </div>
					                        </div>
					                    </div>
										<div class="col-holder">
											<div class="col">
												<strong class="title">In what type of home are you interested?</strong>
												<div class="radio-holder">
													<div class="radio-box">
														<input class="form-control" name="house" id="radio-one" type="radio">
														<label for="radio-one">Condominium</label>
													</div>
													<div class="radio-box">
														<input class="form-control" name="house" id="radio-two" type="radio">
														<label for="radio-two">Townhouse</label>
													</div>
													<div class="radio-box">
														<input class="form-control" name="house" id="radio-three" type="radio">
														<label for="radio-three">Penthouse</label>
													</div>
												</div>
											</div>
											<div class="col last">
												<div class="radio-holder">
													<strong class="title">Are you a broker?</strong>
													<div class="radio-area">
														<div class="radio-box">
															<input class="form-control" name="radio" id="radio-four" type="radio">
															<label for="radio-four">Yes</label>
														</div>
														<div class="radio-box">
															<input class="form-control" name="radio" id="radio-five" type="radio">
															<label for="radio-five">No</label>
														</div>
													</div>
												</div>
												<strong class="sub-title">If yes, which brokerage?</strong>
												<div class="radio-holder">
													<input class="form-control" type="text" placeholder="Agency Name">
												</div>
											</div>
										</div>
										<input class="btn btn-default" type="submit" value="Submit">
									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</main>
<?php get_footer(); ?>
