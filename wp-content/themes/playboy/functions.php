<?php 

show_admin_bar( false );/* kick the tires */

function kick_the_tires() {	
wp_enqueue_style('style', get_template_directory_uri() . '/css/min/style.min.css');    
wp_deregister_script('jquery');	
wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/js/min/jquery.min.js', false, null, true);	
wp_enqueue_script('bootstrap-js', get_stylesheet_directory_uri() . '/js/min/bootstrap.min.js', array( 'jquery' ), '1.0.0', true);	
wp_enqueue_script('jquery-custom', get_stylesheet_directory_uri() . '/js/min/jquery.main-min.js', array( 'jquery' ), '1.0.0', true);	
wp_enqueue_script('modernizr', get_stylesheet_directory_uri() . '/js/vendor/modernizr-2.8.3.min.js', false, null, true);	
wp_enqueue_script('iframe-resize', get_stylesheet_directory_uri() . '/js/min/iframeResizer.min.js', false, '1.0.0', true);	
wp_enqueue_script('iframe-window', get_stylesheet_directory_uri() . '/js/min/iframeResizer.contentWindow.min.js', array( 'iframe-resize' ), '1.0.0', true);	
wp_enqueue_script('jcarousel', get_stylesheet_directory_uri() . '/js/min/jquery.jcarousel.min.js', array( 'jquery' ), '1.0.0', true);	
wp_enqueue_script('jcarousel-responsive', get_stylesheet_directory_uri() . '/js/min/jcarousel.responsive.min.js', array( 'jcarousel' ), '1.0.0', true);
}
add_action( 'wp_enqueue_scripts', 'kick_the_tires' );/* light the fires */

function light_the_fires() { ?><script></script><?php }
add_action( 'wp_footer', 'light_the_fires', 100 );

// set up our sitewide image sizes
add_image_size('slideshow-image', 1920, 1080, false);

add_theme_support( 'post-thumbnails');

if( function_exists('acf_add_options_sub_page') ){	

acf_add_options_sub_page( 'Mansion Slideshow' );	
acf_add_options_sub_page( 'Future Slideshow' );

}
//login css 
function passwd_protect_css() { 
?>    
<link rel="stylesheet" id="custom_wp_admin_css"  href="<?php echo get_bloginfo('stylesheet_directory' ) . '/css/login.css'; ?>" type="text/css" media="all" /><?php } 
add_action( 'login_enqueue_scripts', 'passwd_protect_css' 

);
//add the page slug to body_class()

function add_slug_body_class( $classes ) {	
	global $post;		

	if ( isset( $post ) ) {			
		$classes[] = $post->post_name; 
	}	
	return $classes;
	}
add_filter( 'body_class', 'add_slug_body_class' );?>