<?php

get_header();

if(is_user_logged_in()) {
	get_template_part( 'includes/page', 'landing' );
} else {
	get_template_part( 'includes/page', 'access' );
}

get_footer();

?>
