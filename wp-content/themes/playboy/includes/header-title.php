<header>
	<div class="container-fluid">
		<div class="row">
			<h1 class="proxima pull-left"><a href="/">10236 Charing Cross Road</a></h1>
			<?php if (is_page('history')):
			?>
			<nav class="pb-nav bodoni-italic navbar pull-right">
			  	<div class="navbar-header">
			        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			          	<span class="sr-only">Toggle navigation</span>
					  	<span class="icon-bar"></span>
					  	<span class="icon-bar"></span>
					  	<span class="icon-bar"></span>
			        </button>
			  	</div>
			  	<div id="navbar" class="navbar-collapse collapse">
			    	<ul class="nav navbar-nav">
						<li><a href="<?php echo site_url(); ?>/mansion/">The Mansion.</a></li>
				        <li><a href="<?php echo site_url(); ?>/future/">The Future.</a></li>
			    	</ul>
			  	</div><!--/.nav-collapse -->
			</nav>
		    <?php
				elseif (is_page('future')):
			?>
			<nav class="pb-nav bodoni-italic navbar pull-right">
			  	<div class="navbar-header">
			        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			          	<span class="sr-only">Toggle navigation</span>
					  	<span class="icon-bar"></span>
					  	<span class="icon-bar"></span>
					  	<span class="icon-bar"></span>
			        </button>
			  	</div>
			  	<div id="navbar" class="navbar-collapse collapse">
			    	<ul class="nav navbar-nav">
						<li><a href="<?php echo site_url(); ?>/mansion/">The Mansion.</a></li>
						<li><a href="<?php echo site_url(); ?>/history/">The History.</a></li>
			    	</ul>
			  	</div><!--/.nav-collapse -->
			</nav>
		    <?php
				elseif (is_page('mansion')):
			?>
		    <nav class="pb-nav bodoni-italic navbar pull-right">
			  	<div class="navbar-header">
			        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			          	<span class="sr-only">Toggle navigation</span>
					  	<span class="icon-bar"></span>
					  	<span class="icon-bar"></span>
					  	<span class="icon-bar"></span>
			        </button>
			  	</div>
			  	<div id="navbar" class="navbar-collapse collapse">
			    	<ul class="nav navbar-nav">
						<li><a href="<?php echo site_url(); ?>/history/">The History.</a></li>
						<li><a href="<?php echo site_url(); ?>/future/">The Future.</a></li>
			    	</ul>
			  	</div><!--/.nav-collapse -->
			</nav>
		    <?php else: endif; ?>
		</div>
	</div>
</header>
