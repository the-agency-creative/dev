				<div class="black-bg flex-container">
				    <div class="access-holder flex-item">
					    <div class="logo-holder clearfix">
						    <video class="hidden-xs" loop muted autoplay poster="<?php bloginfo('template_directory'); ?>/image/playboy.jpg">
						        <source src="<?php bloginfo('template_directory'); ?>/video/playboy-wlogo.mp4" type="video/mp4">
						        <source src="<?php bloginfo('template_directory'); ?>/video/playboy-wlogo.webm" type="video/webm">
						        <source src="<?php bloginfo('template_directory'); ?>/video/playboy-wlogo.ogg" type="video/ogg">
						    </video>
						    <img class="visible-xs img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/white-logo.jpg" />
					    </div>
					    <div class="btn-holder">
						    <div class="row">
							    <div class="col-xs-12 text-center">
								    <ul class="text-center list-inline">
										<li class="text-center"><a class="proxima get-up btn btn-default" href="#" data-toggle="modal" data-target="#request">Request a Key</a></li>
										<li class="or bodoni-italic text-center">Or</li>
										<li class="text-center"><a class="proxima get-up btn btn-inverse" href="#" data-toggle="modal" data-target="#login">Enter the Gates</a></li>
								    </ul>
							    </div>
						    </div>
					    </div>
					    <div class="modal fade" id="request" tabindex="-1" role="dialog" aria-labelledby="requestLabel">
					        <div class="modal-dialog modal-wide" role="document">
					            <div class="modal-content">
					                <div class="modal-header">
						                <a class="pull-right" data-dismiss="modal"><i class="fa fa-times"></i></a>
					                    <h2 class="text-center bodoni-italic modal-title" id="requestLabel">Request a Key</h2>
					                </div>
					                <div class="modal-body">
						                <div class="row">
							                <div class="col-xs-12">
												<iframe src="http://go.theagencyre.com/l/75802/2016-01-18/wk3wl" width="100%" height="500" type="text/html" frameborder="0" allowTransparency="true" style="border: 0;height:500px !important;"></iframe>
							                </div>
						                </div>
					                </div>
					            </div>
					        </div>
					    </div>
					    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="loginLabel">
					        <div class="modal-dialog" role="document">
					            <div class="modal-content">
					                <div class="modal-header">
						                <a class="pull-right" data-dismiss="modal"><i class="fa fa-times"></i></a>
					                    <h2 class="text-center bodoni-italic modal-title" id="requestLabel">Enter the Gates</h2>
					                </div>
					                <div class="modal-body">
						                <div class="row">
							                <div class="col-sm-6 col-xs-12 col-sm-offset-3">
							                    <form name="loginform" id="loginform" action="<?php echo site_url(); ?>/wp-login.php" method="post">
													<div class="form-group">
														<input placeholder="Username" type="text" name="log" id="user-login" class="proxima get-up input-lg input form-control" value="" />
													</p>
													<div class="form-group">
														<input placeholder="Password" type="password" name="pwd" id="user-pass" class="proxima get-up input-lg input form-control" value="" />
													</div>
													<div class="form-group text-center">
														<input type="submit" name="wp-submit" id="user-submit" class="input-lg get-up btn btn-inverse proxima" value="Submit" />
														<input type="hidden" name="redirect_to" value="<?php echo site_url(); ?>" />
													</div>
												</form>
							                </div>
						                </div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
				</div>
				
