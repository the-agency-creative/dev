		        <?php get_template_part( 'includes/header', 'title' ); ?>
		        <div class="flex-container">
			        <div class="bg-stretch z-zero">
				        <img src="<?php bloginfo('template_directory'); ?>/images/centerfold.jpg" />
			        </div>
			        <div class="flex-item z-one">
				        <nav class="pb-nav bodoni-italic">
					        <ul class="list-inline">
						        <li><a href="<?php echo site_url(); ?>/history/">The History.</a></li>
						        <li><a href="<?php echo site_url(); ?>/mansion/">The Mansion.</a></li>
						        <li><a href="<?php echo site_url(); ?>/future/">The Future.</a></li>
					        </ul>
				        </nav>
			        </div>
		        </div>
