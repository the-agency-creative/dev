<?php get_header(); ?>
				<?php get_template_part( 'includes/header', 'title' ); ?>
				<?php
				$images = get_field('future_slideshow', 'option');
				if( $images ): ?>
	            <div id="pb-carousel" class="carousel slide" data-ride="carousel">
	                <div class="carousel-inner" role="listbox">
		                <?php
			                foreach( $images as $image ):
		                ?>
	                    <div class="item">
		                    <div>
	                        	<img src="<?php echo $image['sizes']['slideshow-image']; ?>" alt="<?php echo $image['alt']; ?>">
		                    </div>
	                    </div>
	                    <?php endforeach; ?>
	                </div>
	                <a class="left carousel-control" href="#pb-carousel" role="button" data-slide="prev"><i class="fa fa-angle-left left"></i><span class="sr-only">Previous</span></a>
	                <a class="right carousel-control" href="#pb-carousel" role="button" data-slide="next"><i class="fa fa-angle-right right"></i><span class="sr-only">Next</span></a>
	            </div>
	            <?php endif; ?>
<?php get_footer(); ?>
