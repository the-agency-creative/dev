<?php get_header(); ?>
				<?php get_template_part( 'includes/header', 'title' ); ?>
				<div class="hidden-xs flex-container">
					<div class="flex-item">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<div id="timeline">
										<div class="jcarousel-wrapper">
											<div class="jcarousel">
												<ul>
													<li>
														<img class="timeline-image img-responsive" src="<?php bloginfo('template_directory'); ?>/images/timeline1.jpg" class="img-responsive">
														<p class="bodoni-italic timeline-text">Hugh Marston Hefner is born on April 9th in Chicago, Illinois.</p>
														<div class="marker">
															<i class="fa fa-caret-up fa-2x"></i>
														</div>
													</li>
													<li>
														<img class="timeline-image img-responsive" src="<?php bloginfo('template_directory'); ?>/images/timeline2.jpg" class="img-responsive">
														<p class="bodoni-italic timeline-text">Construction begins on 10236 Charing Cross Road in Holmby Hills, CA.</p>
														<div class="marker">
															<i class="fa fa-caret-up fa-2x"></i>
														</div>
													</li>
													<li>
														<img class="timeline-image img-responsive" src="<?php bloginfo('template_directory'); ?>/images/timeline3.jpg" class="img-responsive">
														<p class="bodoni-italic timeline-text">The first issue of Playboy Magazine is published, featuring Marilyn Monroe.</p>
														<div class="marker">
															<i class="fa fa-caret-up fa-2x"></i>
														</div>
													</li>
													<li>
														<img class="timeline-image img-responsive" src="<?php bloginfo('template_directory'); ?>/images/timeline4.jpg" class="img-responsive">
														<p class="bodoni-italic timeline-text">The Playboy Club and Playboy Bunny are born.</p>
														<div class="marker">
															<i class="fa fa-caret-up fa-2x"></i>
														</div>
													</li>
													<li>
														<img class="timeline-image img-responsive" src="<?php bloginfo('template_directory'); ?>/images/timeline5.jpg" class="img-responsive">
														<p class="bodoni-italic timeline-text">Playboy purchases 10236 Charing Cross Road and the Playboy Mansion is born.</p>
														<div class="marker">
															<i class="fa fa-caret-up fa-2x"></i>
														</div>
													</li>
													<li>
														<img class="timeline-image img-responsive" src="<?php bloginfo('template_directory'); ?>/images/timeline6.jpg" class="img-responsive">
														<p class="bodoni-italic timeline-text">The Playboy Mansion throws the first of many parties for Hollywood guests such as Ryan O'Neal, Shel Silverstein, Peter Guber, Russ Meyer and Edy Williams.</p>
														<div class="marker">
															<i class="fa fa-caret-up fa-2x"></i>
														</div>
													</li>
													<li>
														<img class="timeline-image img-responsive" src="<?php bloginfo('template_directory'); ?>/images/timeline7.jpg" class="img-responsive">
														<p class="bodoni-italic timeline-text">The Mansion menagerie is born, becoming a sanctuary for squirrel monkeys, woolly monkeys, pink flamingos, dozens of koi, macaws and more.</p>
														<div class="marker">
															<i class="fa fa-caret-up fa-2x"></i>
														</div>
													</li>
													<li>
														<img class="timeline-image img-responsive" src="<?php bloginfo('template_directory'); ?>/images/timeline8.jpg" class="img-responsive">
														<p class="bodoni-italic timeline-text">Hefner is honored with a star on the Hollywood Walk of Fame and a replica is installed at the Mansion.</p>
														<div class="marker">
															<i class="fa fa-caret-up fa-2x"></i>
														</div>
													</li>
													<li>
														<img class="timeline-image img-responsive" src="<?php bloginfo('template_directory'); ?>/images/timeline9.jpg" class="img-responsive">
														<p class="bodoni-italic timeline-text">A family era begins at the Mansion when Hefner marries Kimberley Conrad in a lavish backyard wedding.</p>
														<div class="marker">
															<i class="fa fa-caret-up fa-2x"></i>
														</div>
													</li>
													<li>
														<img class="timeline-image img-responsive" src="<?php bloginfo('template_directory'); ?>/images/timeline11.jpg" class="img-responsive">
														<p class="bodoni-italic timeline-text">Playboy celebrates its 50th anniversary with a glamorous, star-studded party at the Mansion.</p>
														<div class="marker">
															<i class="fa fa-caret-up fa-2x"></i>
														</div>
													</li>
													<li>
														<img class="timeline-image img-responsive" src="<?php bloginfo('template_directory'); ?>/images/timeline10.jpg" class="img-responsive">
														<p class="bodoni-italic timeline-text">The most famous property in the country hits the market. <a href="/playboy/mansion" style="text-decoration: underline !important; color:black !important;" >Welcome home.</a></p>
														<div class="marker">
															<i class="fa fa-caret-up fa-2x"></i>
														</div>
													</li>
										  		</ul>
											</div>
											<a href="#" class="jcarousel-control-prev"><i class="fa fa-angle-left fa-2x"></i></a>
										  	<a href="#" class="jcarousel-control-next"><i class="fa fa-angle-right fa-2x"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container visible-xs">
					<div id="timeline-mobile" class="row">
						<div class="col-xs-12 text-center timeline-item">
							<img class="timeline-image img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/timeline1.jpg" class="img-responsive center-block">
							<p class="bodoni-italic timeline-text">Hugh Marston Hefner is born on April 9th in Chicago, Illinois.</p>
						</div>
						<div class="col-xs-12 text-center timeline-item">
							<img class="timeline-image img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/timeline2.jpg" class="img-responsive center-block">
							<p class="bodoni-italic timeline-text">Construction begins on 10236 Charing Cross Road in Holmby Hills, CA.</p>
						</div>
						<div class="col-xs-12 text-center timeline-item">
							<img class="timeline-image img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/timeline3.jpg" class="img-responsive center-block">
							<p class="bodoni-italic timeline-text">The first issue of Playboy Magazine is published, featuring Marilyn Monroe.</p>
						</div>
						<div class="col-xs-12 text-center timeline-item">
							<img class="timeline-image img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/timeline4.jpg" class="img-responsive center-block">
							<p class="bodoni-italic timeline-text">The Playboy Club and Playboy Bunny are born.</p>
						</div>
						<div class="col-xs-12 text-center timeline-item">
							<img class="timeline-image img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/timeline5.jpg" class="img-responsive center-block">
							<p class="bodoni-italic timeline-text">Playboy purchases 10236 Charing Cross Road and the Playboy Mansion is born.</p>
						</div>
						<div class="col-xs-12 text-center timeline-item">
							<img class="timeline-image img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/timeline6.jpg" class="img-responsive center-block">
							<p class="bodoni-italic timeline-text">The Playboy Mansion throws the first of many parties for Hollywood guests such as Ryan O'Neal, Shel Silverstein, Peter Guber, Russ Meyer and Edy Williams.</p>
						</div>
						<div class="col-xs-12 text-center timeline-item">
							<img class="timeline-image img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/timeline7.jpg" class="img-responsive center-block">
							<p class="bodoni-italic timeline-text">The Mansion menagerie is born, becoming a sanctuary for squirrel monkeys, woolly monkeys, pink flamingos, dozens of koi, macaws and more</p>
						</div>
						<div class="col-xs-12 text-center timeline-item">
							<img class="timeline-image img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/timeline8.jpg" class="img-responsive center-block">
							<p class="bodoni-italic timeline-text">Hefner is honored with a star on the Hollywood Walk of Fame and a replica is installed at the Mansion.</p>
						</div>
						<div class="col-xs-12 text-center timeline-item">
							<img class="timeline-image img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/timeline9.jpg" class="img-responsive center-block">
							<p class="bodoni-italic timeline-text">A family era begins at the Mansion when Hefner marries Kimberley Conrad in a lavish backyard wedding.</p>
						</div>
						<div class="col-xs-12 text-center timeline-item">
							<img class="timeline-image img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/timeline11.jpg" class="img-responsive center-block">
							<p class="bodoni-italic timeline-text">Playboy celebrates its 50th anniversary with a glamorous, star-studded party at the Mansion.</p>
						</div>
						<div class="col-xs-12 text-center timeline-item">
							<img class="timeline-image img-responsive center-block" src="<?php bloginfo('template_directory'); ?>/images/timeline10.jpg" class="img-responsive center-block">
							<p class="bodoni-italic timeline-text">The most famous property in the country hits the market. Welcome home.</p>
						</div>
					</div>
				</div>
<?php get_footer(); ?>
