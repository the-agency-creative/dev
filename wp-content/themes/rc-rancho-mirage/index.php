<?php
/*
Template Name: Home
*/
?>

<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="The Ritz-Carlton Residences" content="The Ritz-Carlton Residences">
    <meta name="Rancho Mirage" content="Rancho Mirage">
    <!-- <link rel="icon" href="favicon.ico"> -->

    <title>The Ritz-Carlton Residences</title>

    <!-- Core CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

    <!-- Custom styles -->
    <link href="<?php bloginfo('template_directory'); ?>/css/style-custom.min.css" rel="stylesheet">
    <?php wp_head(); ?>
  </head>

  <body cz-shortcut-listen="true">

    <header class="container-fluid hero">
    
    	<!-- Headline Carousel -->
        <div id="top-updates" class="row">

                <div class="text-center show-mobile">
                    <img src="<?php bloginfo('template_directory'); ?>/imgs/logo-rcrm-mobile.png" width="250" height="125" alt="rcrm logo"/>
                </div>

                    <div class="hidden-xs col-sm-6">
                        <div class="logo-container">
                            <img id="logo-rm" src="<?php bloginfo('template_directory'); ?>/imgs/logo-rcrm.png" width="386" height="173" alt="rcrm logo"/>
                        </div>
                    </div>
  
            <div class="col-sm-offset-1 col-sm-5 pr0">
                <!-- Carousel  -->
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active">1</li>
                    <li data-target="#myCarousel" data-slide-to="1">2</li>
                    <li data-target="#myCarousel" data-slide-to="2">3</li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                  
                     <div class="item active">
                      <div class="container-fluid">
                        <div class="carousel-caption">
                          <p class="headline-text"><a href="http://www.theagencyre.com/2015/04/ritz-carlton-residences-rancho-mirage-celebrates-grand-opening/" target="_blank">The Ritz&#8209;Carlton Residences, Rancho Mirage Celebrates Grand Opening&nbsp;&#8209;&nbsp;The Agency</a></p>
                           <p><a href="http://www.theagencyre.com/2015/04/ritz-carlton-residences-rancho-mirage-celebrates-grand-opening/" target="_blank">READ MORE <img src="<?php bloginfo('template_directory'); ?>/imgs/link-arrow-white.png" width="7" height="12" alt="read more"/></a></p>
                        </div>
                      </div>
                    </div>
                    
                     <div class="item">
                      <div class="container-fluid">
                        <div class="carousel-caption">
                          <p class="headline-text"><a href="http://www.theagencyre.com/2015/04/developer-ritz-carlton-residences-rancho-mirage-launches-sales/" target="_blank" >Developer of The Ritz&#8209;Carlton Residences, Rancho Mirage Launches Sales</a></p>
                          <a href="http://www.theagencyre.com/2015/04/developer-ritz-carlton-residences-rancho-mirage-launches-sales/" target="_blank" >READ MORE <img src="<?php bloginfo('template_directory'); ?>/imgs/link-arrow-white.png" width="7" height="12" alt="read more"/></a>
                        </div>
                      </div>
                    </div>
                    
                    
                    <div class="item">
                      <div class="container-fluid">
                        <div class="carousel-caption">
                          <p class="headline-text"><a href="http://www.theagencyre.com/2014/10/agency-named-sales-marketing-partner-ritz-carlton-residences-rancho-mirage/" target="_blank">The Agency Named Sales and Marketing Partner for The Ritz&#8209;Carlton Residences at Rancho Mirage&nbsp;&#8209;&nbsp;The Agency</a></p>
                           <p><a href="http://www.theagencyre.com/2014/10/agency-named-sales-marketing-partner-ritz-carlton-residences-rancho-mirage/" target="_blank">READ MORE <img src="<?php bloginfo('template_directory'); ?>/imgs/link-arrow-white.png" width="7" height="12" alt="read more"/></a></p>
                        </div>
                      </div>
                    </div>
                    
                  </div>
                </div><!-- /.carousel -->
            </div>
        </div>
        
	</header><!-- End container fluid -->

    <div class="container">

    	<div class="row">
    		<div class="col-sm-6">
        		<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/imgs/img1.jpg" width="667" height="392" alt="Bath with a view"/>
    		</div>
            <div class="col-sm-6">
				<h1>16 Exquisite Residences</h1>
                <h2>Where the Desert Meets the Sky</h2>
                <p>Perched 650 feet above the Coachella Valley offering peerless views of the surrounding desert landscape and rugged Santa Rosa Mountains, The Residences will bring a new level of luxury living to the Palm Springs area.</p>
                <p>The Residences are being designed to take full advantage of the area's abundant sun and natural beauty. The desert-inspired design will focus on indoor/outdoor living, and Owners will have access to many of the amenities and services of the adjacent hotel, including an exclusive spa and in-residence dining.</p>

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<?php the_content(); ?>

				<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				<?php endif; ?>
    		</div>
    	</div><!-- End Row -->

        <div class="row txt-schedule">
    		<div class="col-xs-6 col-sm-3 col-sm-push-6 thumbs">
            	<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/imgs/thumb1.jpg" width="321" height="186" alt="view gallery images"/>
            </div>
            <div class="col-xs-6 col-sm-3 col-sm-push-6 thumbs">
            	<img class="img-responsive" src="<?php bloginfo('template_directory'); ?>/imgs/thumb2.jpg" width="321" height="186" alt="view gallery images"/>
            </div>
            <div class="col-sm-6 col-sm-pull-6">
        		<h1>Now Selling</h1>
                <h2>Schedule a Private Presentation</h2>
                <p>To learn more about The Residences please complete the form below, or schedule a presentation with one of our specialists by calling <a href="tel:(760)202-7858">(760) 202&#8209;7858</a>.</p>
    		</div>
        </div>

        <div class="row">
            <!-- Start of Form -->

            <div id="MAform-ffcf7343-025c-49ba-bfcb-58baf344bc89" class="MAform">
				<script type="text/javascript">
                    (function() {var $__MAForm;($__MAForm=function(){if(typeof($__MA)=='undefined'){return window.setTimeout($__MAForm,50);}else{
                            $__MA.addMAForm('ffcf7343-025c-49ba-bfcb-58baf344bc89', 'forms.rcr-ranchomirage.com');
                    }})();})();
                </script>
            </div>



    	</div><!-- End Row -->

        <footer>
        	<div class="row">
            	<!-- Social link
                <div class="col-xs-12 text-center social">
					<img src="<?php bloginfo('template_directory'); ?>/imgs/icon-twitter.png" width="22" height="22" alt="twitter link"/>
                	<img src="<?php bloginfo('template_directory'); ?>/imgs/icon-facebook.png" width="22" height="22" alt="facebook link"/>
                </div>
                -->
               	<div class="col-xs-12 text-center">
					<p class="phone">Tel: <a href="tel:760.202.7858">760.202.7858</a></p>
					<p class="address">68900 Frank Sinatra Drive, Rancho Mirage, CA 92270</p>
                    <p class="disclaimer">The Ritz-Carlton Residences, Rancho Mirage are not owned, developed or sold by The Ritz-Carlton Hotel Company, L.L.C. or its affiliates (“Ritz-Carlton”). New Age Rancho Mirage, LLC uses The Ritz-Carlton marks under a license from Ritz-Carlton, which has not confirmed the accuracy of any of the statements or representations made herein.</p>
              </div>
    		</div><!-- End Row -->
        </footer>

	</div><!-- End container fluid -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <script id="__maSrc" type="text/javascript" data-pid="10360">
(function () {
    var d=document,t='script',c=d.createElement(t),s=(d.URL.indexOf('https:')==0?'s':''),p;
    c.type = 'text/java'+t;
    c.src = 'http'+s+'://'+s+'c.cdnma.com/apps/capture.js';
    p=d.getElementsByTagName(t)[0];p.parentNode.insertBefore(c,p);
}());
</script>
<?php wp_footer(); ?>
</body>
</html>
