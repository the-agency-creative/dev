		<div id="spanish-location" class="row room-below room-above<?php if(is_page('mobile')) {} else { echo ' space-above'; } ?> bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="get-up">The Location</h3>
                        <h4 class="lora">Las Vegas</h4>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <p>Spanish Palms places you in the heart of the vibrant Spanish Trails neighborhood.</p>
                    </div>
                    <div id="footer-hours" class="col-xs-12 col-sm-offset-0 col-sm-3 col-md-offset-1">
                        <p><strong>Mon - Tues</strong> Closed<br />
	                    <strong>Mon - Fri</strong> 10am – 6pm<br>
                        <strong>Sat</strong> 10am - 5pm<br />
                        <strong>Sun</strong> 12pm - 5pm<br />
                        Tel: <a href="tel:702.719.6100">702.719.6100</a><br />
                        Fax: 702.895.9977</p>
                    </div>
                    <div id="footer-address" class="col-xs-12 col-sm-4 col-md-3">
                            <p><strong>Spanish Palms</strong> <a href="https://www.google.com/maps/place/5250+S+Rainbow+Blvd,+Las+Vegas,+NV+89118/data=!4m2!3m1!1s0x80c8c702319d1c51:0x8acde0eb8b5b2432?sa=X&ei=R0YLVbbvKJCxogSmooLIDA&ved=0CB4Q8gEwAA" target="_blank">directions</a><br>
                                5250 South Rainbow Blvd. #1055<br>
                                Las Vegas, NV 89118</p>
                    </div>
                </div>
			</div>
         </div>
		 <div class="row">
            <!-- map -->
			<div class="map">
				<div class="m1">
					<div id="map" class="m2">
						<?php echo do_shortcode('[put_wpgm id=1]'); ?>
					</div>
				</div>
			</div>
         </div>
         <div id="spanish-contact" class="row room-above space-above room-below">
           <div class="container">
               <div class="row">
               		<div class="col-xs-12 col-sm-8">
	               		<a id="own" name="own"></a>
                        <h3 class="get-up">Schedule a presentation</h3>
                        <h4 class="lora">Interested in owning?</h4>
                        <p>To learn more about our residences please fill out the form below, or schedule a presentation with one of our condominium specialists by calling <a href="tel:702.719.6100">702.719.6100</a>.</p>
					</div>
               </div>
               <div class="row space-above">
					<div class="col-md-12">
						<form accept-charset="utf-8" name="62234a01-b2fa-4c9d-9443-ca42bf0ab5d0" class="maForm" method="POST" action="https://apps.net-results.com/data/public/IncomingForm/Ma/submit">

    <div class="row">
    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
       <label for="FirstName" id="FirstNameLabel" class="">First Name<span class="formRequiredLabel">*</span>

        </label>
        <input placeholder="First name*" type="text" name="FirstName" class="formText formRequired" value="" />
    </div>

    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
        <label for="LastName" id="LastNameLabel" class="">Last Name<span class="formRequiredLabel">*</span>

        </label>
           <input placeholder="Last name*" type="text" name="LastName" class="formText formRequired" value="" />

    </div>
    </div>

    <div class="row">
    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
       <label for="Phone" id="PhoneLabel" class="">Phone<span class="formRequiredLabel">*</span>

        </label>
<input  placeholder="Phone*"type="text" name="Phone" class="formText formRequired" value="" />

    </div>
    <div class="maFormElement col-lg-6 col-md-6 col-sm-6">
        <label for="Email" id="EmailLabel" class="">Email Address<span class="formRequiredLabel">*</span>

        </label>
           <input placeholder="Email*" type="text" name="Email" class="formText formEmail formRequired" value="" />

    </div>
    </div>

    <div class="row">
	    <div class="maFormElement col-lg-4 col-md-4 col-sm-4">
	      <label for="City" id="CityLabel" class="">City<span class="formRequiredLabel">*</span>
	        </label>

	            <input type="text" placeholder="City*" name="City" class="formText formRequired" value="" />

	    </div>

	     <div class="maFormElement col-lg-4 col-md-4 col-sm-4">
	        <label for="State" id="StateLabel" class="">State<span class="formRequiredLabel">*</span>

	        </label>
	          <input type="text" placeholder="State*" name="State" class="formText formRequired" value="" />
	    </div>

	    <div class="maFormElement col-lg-4 col-md-4 col-sm-4">
	       <label for="ZipCode" id="ZipCodeLabel" class="">Zip Code<span class="formRequiredLabel">*</span>

	        </label>
	         <input type="text" placeholder="Zip code*" name="ZipCode" class="formText formRequired" value="" />
	    </div>
    </div>
    <div class="row">
	    <div class="maFormElement col-lg-4 col-md-4 col-sm-4">
	       <label for="How_did_you_hear_about_us_" id="How_did_you_hear_about_us_Label" class="">How did you hear about us?<span class="formRequiredLabel">*</span></label>
	       <input type="text" placeholder="How did you hear about us?*" name="How_did_you_hear_about_us_" class="formText formRequired" value="" />
	    </div>
	    <div class="maFormElement radio-buttons col-lg-4 col-md-4 col-sm-4 text-right" style="margin-bottom:2px;">
		    <ul class="list-inline">
			    <li><p>Are you a broker?*</p><label for="are_you_broker" id="are_you_brokerLabel" class="radio-inline">Are you a broker?<span class="formRequiredLabel">*</span></label></li>
			    <li><span class="lbl">Yes</span> <input id="are_you_broker-0" class="formRadio formRequired radio-inline" type="radio" value="Yes" name="are_you_broker"> <label class="formRadioLabel radio-inline">Yes</label></li>
			    <li><span class="lbl">No</span> <input id="are_you_broker-1" class="formRadio formRequired radio-inline" type="radio" value="No" name="are_you_broker"> <label class="formRadioLabel radio-inline">No</label></li>
		    </ul>
	    </div>

	    <div class="maFormElement col-lg-4 col-md-4 col-sm-4" style="margin-bottom:2px;">
	        <label for="which-brokerage" id="which-brokerageLabel" class="">If so, which brokerage?</label><input type="text" name="which-brokerage" class="formText " value="" placeholder="If so, which brokerage?">
	    </div>
    </div>
    <div class="row">
	    <div class="maFormElement col-lg-6 col-md-6 col-sm-6" style="margin-bottom:15px;">
	         <label for="Desired_Floor_Plan" id="Desired_Floor_PlanLabel" class="">Desired Floor Plan<span class="formRequiredLabel">*</span>

	        </label>
	        <select name="Desired_Floor_Plan" class="formSelect formRequired">
	                <option value="">Desired floor plan*</option>
	                <option value="1  Bedroom">1 Bedroom</option>
	                <option value="2  Bedroom">2 Bedroom</option>
	                <option value="3 Bedroom">3 Bedroom</option>
	            </select>
	    </div>
	    <div class="maFormElement col-lg-6 col-md-6 col-sm-6" style="margin-bottom:2px;">
	        <label for="Desired_Pricing" id="Desired_PricingLabel" class="">Desired Pricing<span class="formRequiredLabel">*</span>

	        </label>
	           <select name="Desired_Pricing" class="formSelect formRequired">
	               <option value="">Desired pricing*</option>
	                <option value="$115,000 - $130.000">$115,000 - $130.000</option>
	                <option value="$130,000 - $160.000">$130,000 - $160.000</option>
	                <option value="$160,000 - $200.000">$160,000 - $200.000</option>
	            </select>
	    </div>
    </div>
    <div class="maFormElement submitAlign">
        <button name="Submit" class="formSubmit " value="submit" type="submit">Submit</button>
    </div>
    <input type="hidden" name="form_id" value="62234a01-b2fa-4c9d-9443-ca42bf0ab5d0" />
    <input type="hidden" name="form_access_id" value="" />
    <input type="hidden" name="__mauuid" value="" />
</form>
					</div>
				</div><!-- End Row -->
            </div>
         </div><!-- End Row -->
	</div><!-- End Container Fluid -->
	<footer class="new-section-p space-above">
    	<div class="container room-above border-top">
        	<div class="row">
            	<div class="col-md-2 pull-left">
            		<ul class="list-inline">
	                	<li><a href="https://twitter.com/spanishpalmslv"><i class="fa fa-twitter"></i></a></li>
	                	<li><a href="https://www.facebook.com/pages/Spanish-Palms/110173549072688?fref=ts"><i class="fa fa-facebook"></i></a></li>
	                	<!-- <li><a href="http://instagram.com/ogdenlv_/"><i class="fa fa-instagram"></i></a></li> -->
            		</ul>
                </div>
                <div class="col-md-8 text-center">
                	<p class="footer-line1">5250 South Rainbow Blvd. #1055, Las Vegas, NV 89118 | Tel <a href="tel:702.719.6100">702.719.6100</a></p>
                    <p class="footer-line2">&copy; 2015 SPANISH PALMS Las Vegas, All Rights Reserved.</p>
                </div>
                <div class="col-md-2 pull-right">
                	<!-- <img src="<?php bloginfo('template_directory'); ?>/images/icon-footer.png" width="25" height="34" alt="logo icon"/> -->
                	<p class="credit">Site by <a href="http://wickedta.com" rel="nofollow">Wicked + The Agency</a></p>
                </div>
            </div><!-- End Row -->
            <div class="row">
            	<div class="col-xs-12">
            		&nbsp;
            	</div>
            </div>
        </div>
	<?php if(is_page('home')) { } else { ?><a href="#spanish-contact" id="spanish-request-btn" class="center-block">Request Information</a><?php } ?>
    </footer><!-- End Footer -->
    <?php wp_footer(); ?>
    <?php the_field('sp_footer_scripts', 'option'); ?>

    <script id="__maSrc" type="text/javascript" data-pid="11778">
(function () {
    var d=document,t='script',c=d.createElement(t),s=(d.URL.indexOf('https:')==0?'s':''),p;
    c.type = 'text/java'+t;
    c.src = 'http'+s+'://'+s+'c.cdnma.com/apps/capture.js';
    p=d.getElementsByTagName(t)[0];p.parentNode.insertBefore(c,p);
}());
</script>
</body>
</html>
