<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="http://www.spanishpalmslv.com/favicon.ico?v=2">

    <title><?php wp_title(''); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,400italic' rel='stylesheet' type='text/css'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles -->
    <link href="<?php bloginfo('template_directory'); ?>/css/custom.css" rel="stylesheet">

	<?php wp_head(); ?>
	<?php the_field('sp_header_scripts', 'option'); ?>
  </head>

  <body <?php body_class(); ?>>

    <!-- Navigation -->
    <nav id="spanish-nav" class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#spanish-nav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-left">
                	<a class="navbar-brand page-scroll" href="/"><img src="<?php bloginfo('template_directory'); ?>/images/logo-sp.png" class="img-responsive" width="245" height="24" alt="logo mark"/></a>
                </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="spanish-nav-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a href="/residences/">RESIDENCES</a>
                    </li>
                    <li>
                        <a href="/in-the-life/">IN THE LIFE</a>
                    </li>
                    <li>
                        <a href="/neighborhood/">NEIGHBORHOOD</a>
                    </li>
                    <li>
                        <a href="/news/">NEWS &amp; UPDATES</a>
                    </li>
                    <li>
                        <a href="/team/">TEAM</a>
                    </li>
                    <li>
                        <a href="/contact/">CONTACT</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <?php
    if(get_field('spanish_page_header_image')) {
	    $spanish_page_header_image_obj = get_field('spanish_page_header_image');
		// vars
		$spanish_page_header_image_src = $spanish_page_header_image_obj['url'];
		$spanish_page_header_image_size = 'page-header-image';
		$spanish_page_header_image = $spanish_page_header_image_obj['sizes'][ $spanish_page_header_image_size ];
    ?>
    <header style="background-image: url('<?php echo $spanish_page_header_image; ?>');">
        <div class="container hero-hundred-h">
            <div class="row hero-hundred-h">
            	<div class="col-xs-6 hero-hundred-h">
                	<div id="hero-text">
                		<span class="mock3">	Condominium Residences</span><br>
                    	<span class="mock2">from the $120,000s</span>
                    </div>
            	</div>
                	<div class="col-xs-6 hero-hundred-h">
                        <div id="hero-text-r">
                        <span class="mock2">Interested in<br>Owning?</span>
                        <a href="/contact/" class="btn-default btn">LET&rsquo;S TALK</a>
                    </div>
            	</div>
            </div>
        </div>
    </header>
    <?php } else { ?>
    <header style="background-image: url('<?php bloginfo('template_directory'); ?>/images/header-image01.jpg');">
        <div class="container hero-hundred-h">
            <div class="row hero-hundred-h">
            	<div class="col-xs-6 hero-hundred-h">
                	<div id="hero-text">
                		<h3>	Condominium Residences</h3>
                    	<h5>from the $120,000s</h5>
                    </div>
            	</div>
                	<div class="col-xs-6 hero-hundred-h">
                        <div id="hero-text-r">
                        <h5>Interested<br>in Owning?</h5>
                        <a href="/contact/" class="btn-default btn">LET&rsquo;S TALK</a>
                    </div>
            	</div>
            </div>
        </div>
    </header>
    <?php } ?>
