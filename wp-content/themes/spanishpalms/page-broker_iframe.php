<?php

/*
Template Name: Broker iFrame
*/

?>

<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="http://www.spanishpalmslv.com/favicon.ico">

    <title><?php wp_title(''); ?></title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300" rel="stylesheet" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles -->
    <link href="<?php bloginfo('template_directory'); ?>/css/custom.css" rel="stylesheet">

	<?php wp_head(); ?>

  </head>
  	<body <?php body_class(); ?>>
    	<div class="row">
    		<div class="container" style="margin:0 !important;">
		        <div class="row">
		        	<div class="col-md-8">
		        		<img src="http://spanishpalmslv.com/wp-content/uploads/2015/05/sp-iframe-hero_v2.jpg" class="img-responsive space-below" />
		        		<h3>BEAUTIFULLY APPOINTED, SPACIOUS HOMES</h3>
						<h1>A Gated Oasis Minutes from the Heart of Las Vegas</h1>
						Our residences have been thoughtfully refreshed with modern design highlights and refined finishes. Large windows bathe living areas in an abundance of natural light, while offering unobstructed views of Downtown Las Vegas, The Strip and surrounding mountains, including Red Rock Canyon. Generous kitchens feature gourmet appliances and granite countertops with hardwood and ceramic tile floors running throughout.
						<h4 class="space-above gold"><strong>Limited introductory pricing from the $120,000s</strong></h4>
						<h4>Design Features:</h4>
						<ul>
							<li>Modern open floor plans with private balconies and patios</li>
							<li>Beautifully upgraded interiors</li>
							<li>Generous kitchens with granite countertops</li>
							<li>Gas-burning fireplaces with travertine tile surround and floating mantelpiece</li>
							<li>Large walk-in closets</li>
							<li>Attached private one- and two-car garages with ample storage space</li>
						</ul>
						<h3 class="space-above">RESIDENCES FOR EVERY LIFESTYLE</h3>
						<h1>Resort-Style Living Offering Convenience and Comfort</h1>
						Spanish Palms is your personal retreat, offering a relaxed style of desert resort living with the conveniences of city life within close reach. Nestled amongst the shady palms, blocks from the Spanish Trail Country Club, Spanish Palms offers one-, two- and three-bedroom residences designed to fit any lifestyle.

						<div id="spanish-iframe-carousel" class="carousel slide space-above space-below" data-ride="carousel">
					     	<!-- Indicators -->
							<?php
								$spanish_iframe_images = get_field('spanish_iframe_gallery');
								if( $spanish_iframe_images ):
								$i=0;
							?>
							<!-- Indicators -->
							<ol class="carousel-indicators">
								<?php foreach( $spanish_iframe_images as $spanish_iframe_image ): ?>
								<li data-target="#spanish-iframe-carousel" data-slide-to="<?php echo $i++; ?>"></li>
								<?php endforeach; ?>
							</ol>
							<div class="carousel-inner">
					        	<?php foreach( $spanish_iframe_images as $spanish_iframe_image ):
									$spanish_iframe_image_size = 'iframe-gallery';
									$spanish_iframe_image_url = $spanish_iframe_image['sizes'][$spanish_iframe_image_size];
									$spanish_iframe_image_alt = $spanish_iframe_image['alt'];
								?>
								<div class="item">
									<img src="<?php echo $spanish_iframe_image_url; ?>" alt="<?php the_title(); ?> at The Ogden" class="img-responsive">
								</div>
								<?php endforeach; ?>
					      	</div>
					      	<?php endif; ?>
						  	<a class="left carousel-control" href="#spanish-iframe-carousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
						  	<a class="right carousel-control" href="#spanish-iframe-carousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
					    </div><!-- /.carousel -->
			        	<div class="table-responsive space-above">
					        <table id="residence-list" class="table table-striped">
					        	<thead>
					        		<th>Residence</th>
					        		<th>Bedrooms</th>
					        		<th>Bathrooms</th>
					        		<th>Floorplan #</th>
					        		<th>Square Feet</th>
					        		<th>Floorplan</th>
					        	</thead>
					        	<tbody>
							        <?php $spanish_residences = new WP_Query("post_type=residences&posts_per_page=99"); while($spanish_residences->have_posts()) : $spanish_residences->the_post();?>
							        <tr>
							        	<td><?php the_field('spanish_residence_name'); ?></td>
							        	<td><?php the_field('spanish_residence_bedrooms'); ?></td>
							        	<td><?php the_field('spanish_residence_bathrooms'); ?></td>
							        	<td><?php the_field('spanish_residence_unit_number'); ?></td>
							        	<td><?php the_field('spanish_residence_sqft'); ?></td>
							        	<td>
							        	<?php

							        	if (get_field('spanish_residence_floorplan')) {

							        	$spanish_floorplan_image_obj = get_field('spanish_residence_floorplan');
							        	$spanish_floorplan_image_src = $spanish_floorplan_image_obj['url'];
										$spanish_floorplan_image_size = 'large';
										$spanish_floorplan_image = $spanish_floorplan_image_obj['sizes'][ $spanish_floorplan_image_size ];

										?>
										<a href="<?php echo $spanish_floorplan_image; ?>" rel="lightbox">View</a>
										<?php } else { ?>
							        	<a href="/contact/">Let's Talk</a>
							        	<?php } ?></td>
							        </tr>
							        <?php endwhile; ?>
							        <?php wp_reset_query(); ?>
					        	</tbody>
					        </table>
			        	</div>
		        	</div>
		        </div>
			</div><!-- End container -->
		 </div>
<?php wp_footer(); ?>
	</body>
</html>
