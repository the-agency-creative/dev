<?php

/*
Template Name: Home
*/

get_header();

?>
    <div class="container-fluid">
	    <?php $i=0; $spanish_messages = new WP_Query("post_type=message&posts_per_page=3"); if($spanish_messages->have_posts()) : ?>
    	<div class="row bg-light-gray">
       	  <div id="sp-carousel" class="carousel slide" data-interval="5000" data-ride="carousel">
          	<!-- Indicators -->
            <ol class="carousel-indicators">
	          <?php while($spanish_messages->have_posts()) : $spanish_messages->the_post();?>
              <li data-target="#sp-carousel" data-slide-to="<?php echo $i++; ?>"></li>
              <?php endwhile; ?>
	          <?php wp_reset_postdata(); ?>
            </ol>

            <!-- Carousel items -->
              <div class="carousel-inner" role="listbox">
	            <?php while($spanish_messages->have_posts()) : $spanish_messages->the_post();?>
	            <div class="item">
                  <div class="container">
                    <div class="carousel-caption">
	                  <?php if(get_field('spanish_message_heading')) { ?>
                      <h3 class="get-up"><?php the_field('spanish_message_heading'); ?></h3>
                      <?php } ?>
                      <?php if(get_field('spanish_message_subheading')) { ?>
                      <h2 class="lora"><?php the_field('spanish_message_subheading'); ?></h2>
                      <?php } ?>
                      <?php if(get_field('spanish_message_content')) { ?>
                      <p><?php the_field('spanish_message_content'); ?></p>
                      <?php } ?>
                      <?php
					  if( have_rows('spanish_message_buttons') ):
					  while ( have_rows('spanish_message_buttons') ) : the_row();
					  ?>
					  <a href="<?php the_sub_field('spanish_message_button_link'); ?>" class="btn btn-lg btn-primary get-up" role="button"><?php the_sub_field('spanish_message_button_text'); ?></a>
					  <?php endwhile; endif; ?>
                    </div>
                  </div>
                </div>
	            <?php endwhile; ?>
	            <?php wp_reset_postdata(); ?>
              </div>

              <!-- Carousel nav -->
                <a class="carousel-control left hidden-xs" href="#sp-carousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                </a>
                <a class="carousel-control right hidden-xs" href="#sp-carousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                </a>
    		</div>
        </div><!-- END Carousel Row -->
        <?php endif; ?>
    	<div class="row">
		    <!-- Page Content -->
		    <div id="home-tiles" class="container">
		       <div class="row">

		       		<?php
		       		$home_tiles = new WP_Query("post_type=tiles&posts_per_page=12"); while($home_tiles->have_posts()) : $home_tiles->the_post();

		       		if(get_field('spanish_tile_text_image') == 'Text') {

		       		?>

		       		<div class="col-xs-12 col-sm-4 text-center tile">
		            	<div class="text-box bg-gray">
		                    <h3><?php the_field('spanish_tile_text_subheader'); ?></h3>
                            <h2 class="gold"><?php the_field('spanish_tile_text_headline'); ?></h2>
		                    <p class="home-grid-text"><?php the_field('spanish_tile_text_blurb'); ?></p>
		                    <a target="_blank" href="<?php the_field('spanish_tile_link'); ?>" class="btn-default btn btn-wire"><?php the_field('spanish_tile_text_button'); ?></a>
		                </div>
		            </div>

		            <?php } else {

			        	if(get_field('spanish_tile_single_double') == 'Single') {

				        $tile_image_obj = get_field('spanish_tile_image');
					    $tile_image_size = 'home-singlewide';
						$tile_image_url = $tile_image_obj['sizes'][$tile_image_size];
						$tile_image_title = $tile_image_obj['title'];
						$tile_image_alt = $tile_image_obj['alt'];

		            ?>

		            <div class="col-xs-12 col-sm-4 tile">
		            	<img class="img-responsive" src="<?php echo $tile_image_url; ?>" alt="<?php echo $tile_image_alt; ?>"/>
		            </div>

		            <?php

		            	} else {

		            	$tile_image_obj = get_field('spanish_tile_image');
					    $tile_image_size = 'home-doublewide';
						$tile_image_url = $tile_image_obj['sizes'][$tile_image_size];
						$tile_image_title = $tile_image_obj['title'];
						$tile_image_alt = $tile_image_obj['alt'];
					?>

		            <div class="col-xs-12 col-sm-8 tile">
		       	    	<img class="img-responsive" src="<?php echo $tile_image_url; ?>" alt="<?php echo $tile_image_alt; ?>"/>
		            </div>

		       		<?php
		       			}

		       		}
		       		?>
		       		<?php endwhile; ?>
		       		<?php wp_reset_query(); ?>

				</div><!-- End Row -->
			</div><!-- End container -->
		 </div>

<?php get_footer(); ?>
