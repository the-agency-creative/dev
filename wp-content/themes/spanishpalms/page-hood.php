<?php

/*
Template Name: Neighborhood
*/

get_header();

?>

    <div class="container-fluid">
    	<div class="row">
		    <!-- Page Content -->
		    <div class="container">
		        <div class="row space-above space-below">
		       		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		       		<div class="col-sm-12 col-md-9">
		           		<?php the_content(); ?>
		            </div>
		       		<?php endwhile; else: ?>
		       		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		       		<?php endif; ?>
		        </div><!-- End Row -->
		    </div>
    	</div>

<?php get_footer(); ?>
