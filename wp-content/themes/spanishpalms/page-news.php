<?php

/*
Template Name: News
*/

get_header();

?>

    <div class="container-fluid">
    	<div class="row">
		    <!-- Page Content -->
		    <div class="container">
		       <div class="row space-above">
		       		<div class="col-md-12">
		       		<h1><span><?php bloginfo('title'); ?></span><br>News &amp; Press</h1>
		       		<?php $page = (get_query_var('paged')) ? get_query_var('paged') : 1; query_posts("post_type=post&posts_per_page=10&paged=$page"); while ( have_posts() ) : the_post() ?>
		       			<?php if(has_category('press')) { ?>
		       			<article class="clearfix news-item space-above space-below">
		       				<h3>Press</h3>
                            <h2><a href="<?php the_field('spanish_press_link'); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
		       				<?php the_excerpt('...'); ?> - <em><?php the_field('spanish_press_source'); ?></em>
		       				<a class="pull-right read-more" href="<?php the_field('spanish_press_link'); ?>">READ ARTICLE &rarr;</a>
		       			</article>
		       			<?php } else { ?>
		       			<article class="clearfix news-item space-above space-below">
                            <h3>News</h3>
                            <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
		       				<?php the_excerpt('...'); ?>
		       				<a class="pull-right read-more" href="<?php the_permalink(); ?>">READ MORE &rarr;</a>
		       			</article>
		       			<?php } ?>
		       		<?php endwhile; ?>
		            </div>
		        </div><!-- End Row -->
			</div><!-- End container -->
		 </div>

<?php get_footer(); ?>
