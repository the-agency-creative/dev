<?php

/*
Template Name: Residences
*/

get_header();

?>

    <div class="container-fluid">
    	<div class="row bg-gray">
		    <!-- Page Content -->
		    <div class="container">
		        <div class="row space-above space-below">
		       		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		       		<div class="col-sm-12 col-md-9">
		           		<?php the_content(); ?>
		            </div>
		       		<?php endwhile; else: ?>
		       		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		       		<?php endif; ?>
		        </div><!-- End Row -->
		    </div>
    	</div>
    	<a id="gallery" name="gallery"></a>
    	<div class="row">
    		<div class="container">
		        <div class="row space-above">
			        <div id="spanish-residences-carousel" class="carousel slide space-below" data-ride="carousel">
				     	<!-- Indicators -->
						<?php
							$spanish_residences_images = get_field('spanish_residences_gallery');
							if( $spanish_residences_images ):
							$i=0;
						?>
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<?php foreach( $spanish_residences_images as $spanish_residences_image ): ?>
							<li data-target="#spanish-residences-carousel" data-slide-to="<?php echo $i++; ?>"></li>
							<?php endforeach; ?>
						</ol>
						<div class="carousel-inner">
				        	<?php foreach( $spanish_residences_images as $spanish_residences_image ):
								$spanish_residences_image_size = 'residences-gallery';
								$spanish_residences_image_url = $spanish_residences_image['sizes'][$spanish_residences_image_size];
								$spanish_residences_image_alt = $spanish_residences_image['alt'];
							?>
							<div class="item">
								<img src="<?php echo $spanish_residences_image_url; ?>" alt="<?php the_title(); ?> at The Ogden" class="img-responsive">
							</div>
							<?php endforeach; ?>
				      	</div>
				      	<?php endif; ?>
				      	<a class="left carousel-control" href="#spanish-residences-carousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
						<a class="right carousel-control" href="#spanish-residences-carousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
				    </div><!-- /.carousel -->
		        	<div class="table-responsive">
				        <table id="residence-list" class="table table-striped">
				        	<thead>
				        		<th>Residence</th>
				        		<th>Bedrooms</th>
				        		<th>Bathrooms</th>
				        		<th>Square Feet</th>
				        		<th>Floorplan</th>
				        	</thead>
				        	<tbody>
						        <?php $spanish_residences = new WP_Query("post_type=residences&posts_per_page=99"); while($spanish_residences->have_posts()) : $spanish_residences->the_post();?>
						        <tr>
						        	<td><?php the_field('spanish_residence_name'); ?></td>
						        	<td><?php the_field('spanish_residence_bedrooms'); ?></td>
						        	<td><?php the_field('spanish_residence_bathrooms'); ?></td>
						        	<td><?php the_field('spanish_residence_sqft'); ?></td>
						        	<td>
						        	<?php

						        	if (get_field('spanish_residence_floorplan')) {

						        	$spanish_floorplan_image_obj = get_field('spanish_residence_floorplan');
						        	$spanish_floorplan_image_src = $spanish_floorplan_image_obj['url'];
									$spanish_floorplan_image_size = 'large';
									$spanish_floorplan_image = $spanish_floorplan_image_obj['sizes'][ $spanish_floorplan_image_size ];

									?>
									<a href="<?php echo $spanish_floorplan_image; ?>" rel="lightbox">View</a>
									<?php } else { ?>
						        	<a href="/contact/">Let's Talk</a>
						        	<?php } ?></td>
						        </tr>
						        <?php endwhile; ?>
						        <?php wp_reset_query(); ?>
				        	</tbody>
				        </table>
		        	</div>
		        </div>
			</div><!-- End container -->
		 </div>

<?php get_footer(); ?>
