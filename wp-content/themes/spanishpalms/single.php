<?php get_header(); ?>

    <div class="container-fluid">
    	<div class="row">
		    <!-- Page Content -->
		    <div class="container">
		       <div class="row space-above">
		       		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		       		<div class="col-md-12">
			       		<a id="learn" name="learn"></a>
		       			<h1 class="get-up text-center space-above space-below"><?php the_title(); ?></h1>
		           		<?php the_content(); ?>
		            </div>
		       		<?php endwhile; else: ?>
		       		<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		       		<?php endif; ?>
		        </div><!-- End Row -->
			</div><!-- End container -->
		 </div>

<?php get_footer(); ?>
