<?php /* Template Name: Banners */ ?>

<?php get_header(); ?>

<div class="page-container">

    <div id="hero-bloc" class="bloc bgc-2 d-bloc" style="margin-top:10px;">
        <div class="container-fluid">
            <div class="col-sm-6 col-sm-offset-3 text-center">
               <a href="/digital-work">
                  <img src="<?php bloginfo('template_directory'); ?>/img/tac-logo.png" width="210" height="253" class="center-block img-responsive" alt="The Agency Creative logo" /></a>
                <h2 class="text-center">Digital Banners</h2>
                <h3 class="text-center">Passionate about developing luxury and lifestyle brands through artfully persuasive storytelling, design and communications.</h3>
            </div>
        </div>
    </div>
    <!-- Hero Bloc END -->


    <!-- page bloc -->
    <div class="bloc l-bloc">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel-group" id="accordion">
                       <!-- Insert Banner Groups -->
                       
                       
                       
                       
                       
                       <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><i class="fa fa-arrow-right arrow-pad"></i> The Ogden - Life Reimagined</a>
                </h4>
                            </div>
                            <div id="collapseSix" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="text-center">

                                        <!-- Nav tabs -->

                                        <div class="tab-center">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#160x600-Ogden-Life" aria-controls="160x600-Ogden-Life" role="tab" data-toggle="tab">160x600</a>
                                                </li>
                                                <li role="presentation"><a href="#300x250-Ogden-Life" aria-controls="300x250-Ogden-Life" role="tab" data-toggle="tab">300x250</a>
                                                </li>
                                                <li role="presentation"><a href="#300x600-Ogden-Life" aria-controls="300x600-Ogden-Life" role="tab" data-toggle="tab">300x600</a>
                                                </li>
                                                <li role="presentation"><a href="#728x90-Ogden-Life" aria-controls="728x90-Ogden-Life" role="tab" data-toggle="tab">728x90</a>
                                                </li>
                                            </ul>
                                        </div>



                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="160x600-Ogden-Life">

                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="160" height="600" id="160x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden-life/160x600.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden-life/160x600.swf" width="160" height="600">
                                                            <param name="movie" value="160x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>

                                            </div>

                                           
                                            <div role="tabpanel" class="tab-pane fade" id="300x250-Ogden-Life">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="250" id="300x250" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden-life/300x250.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden-life/300x250.swf" width="300" height="250">
                                                            <param name="movie" value="300x250.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            
                                            <div role="tabpanel" class="tab-pane fade" id="300x600-Ogden-Life">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="600" id="300x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden-life/300x600.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden-life/300x600.swf" width="300" height="600">
                                                            <param name="movie" value="300x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            
                                             <div role="tabpanel" class="tab-pane fade" id="728x90-Ogden-Life">

                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="728" height="90" id="728x90" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden-life/728x90.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden-life/728x90.swf" width="728" height="90">
                                                            <param name="movie" value="728x90.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>


                                            <p>Animated banners created for The Ogden in several sizes, optimized for high web conversions.</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                       
                       
                       
                       <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><i class="fa fa-arrow-right arrow-pad"></i> The Ogden - Everywhere You Want To Be</a>
                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="text-center">

                                        <!-- Nav tabs -->

                                        <div class="tab-center">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#160x600-Ogden-Interior" aria-controls="160x600-Ogden-Interior" role="tab" data-toggle="tab">160x600</a>
                                                </li>
                                                <li role="presentation"><a href="#300x250-Ogden-Interior" aria-controls="300x250-Ogden-Interior" role="tab" data-toggle="tab">300x250</a>
                                                </li>
                                                <li role="presentation"><a href="#300x600-Ogden-Interior" aria-controls="300x600-Ogden-Interior" role="tab" data-toggle="tab">300x600</a>
                                                </li>
                                                <li role="presentation"><a href="#728x90-Ogden-Interior" aria-controls="728x90-Ogden-Interior" role="tab" data-toggle="tab">728x90</a>
                                                </li>
                                            </ul>
                                        </div>



                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="160x600-Ogden-Interior">

                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="160" height="600" id="160x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden-interior/160x600.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden-interior/160x600.swf" width="160" height="600">
                                                            <param name="movie" value="160x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>

                                            </div>

                                           
                                            <div role="tabpanel" class="tab-pane fade" id="300x250-Ogden-Interior">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="250" id="300x250" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden-interior/300x250.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden-interior/300x250.swf" width="300" height="250">
                                                            <param name="movie" value="300x250.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            
                                            <div role="tabpanel" class="tab-pane fade" id="300x600-Ogden-Interior">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="600" id="300x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden-interior/300x600.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden-interior/300x600.swf" width="300" height="600">
                                                            <param name="movie" value="300x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            
                                             <div role="tabpanel" class="tab-pane fade" id="728x90-Ogden-Interior">

                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="728" height="90" id="728x90" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden-interior/728x90.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden-interior/728x90.swf" width="728" height="90">
                                                            <param name="movie" value="728x90.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>


                                            <p>Animated banners created for The Ogden in several sizes, optimized for high web conversions.</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        
                       
                       
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="fa fa-arrow-right arrow-pad"></i> The Ogden - Owning Is The New Renting</a>
                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="text-center">

                                        <!-- Nav tabs -->

                                        <div class="tab-center">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#160x600-Ogden" aria-controls="160x600-Ogden" role="tab" data-toggle="tab">160x600</a>
                                                </li>
                                                <li role="presentation"><a href="#300x250-Ogden" aria-controls="300x250-Ogden" role="tab" data-toggle="tab">300x250</a>
                                                </li>
                                                <li role="presentation"><a href="#300x600-Ogden" aria-controls="300x600-Ogden" role="tab" data-toggle="tab">300x600</a>
                                                </li>
                                                <li role="presentation"><a href="#728x90-Ogden" aria-controls="728x90-Ogden" role="tab" data-toggle="tab">728x90</a>
                                                </li>
                                            </ul>
                                        </div>



                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="160x600-Ogden">

                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="160" height="600" id="160x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden/160x600.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden/160x600.swf" width="160" height="600">
                                                            <param name="movie" value="160x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>

                                            </div>

                                          
                                            <div role="tabpanel" class="tab-pane fade" id="300x250-Ogden">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="250" id="300x250" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden/300x250.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden/300x250.swf" width="300" height="250">
                                                            <param name="movie" value="300x250.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            
                                            
                                            <div role="tabpanel" class="tab-pane fade" id="300x600-Ogden">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="600" id="300x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden/300x600.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden/300x600.swf" width="300" height="600">
                                                            <param name="movie" value="300x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            
                                            
                                            <div role="tabpanel" class="tab-pane fade" id="728x90-Ogden-Interior">

                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="728" height="90" id="728x90" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/ogden-interior/728x90.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/ogden-interior/728x90.swf" width="728" height="90">
                                                            <param name="movie" value="728x90.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>


                                            <p>Animated banners created for The Ogden in several sizes, optimized for high web conversions.</p>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><i class="fa fa-arrow-right arrow-pad"></i> Victory Ranch - Adventure Is Out There</a>
                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                               
                               
                                <div class="panel-body">
                                    <div class="text-center">

                                        <!-- Nav tabs -->

                                        <div class="tab-center">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#160x600-VR" aria-controls="160x600-VR" role="tab" data-toggle="tab">160x600</a>
                                                </li>
                                                <li role="presentation"><a href="#300x250-VR" aria-controls="300x250-VR" role="tab" data-toggle="tab">300x250</a>
                                                </li>
                                                <li role="presentation"><a href="#300x600-VR" aria-controls="300x600-VR" role="tab" data-toggle="tab">300x600</a>
                                                </li>
                                                <li role="presentation"><a href="#728x90-VR" aria-controls="728x90-VR" role="tab" data-toggle="tab">728x90</a>
                                                </li>
                                            </ul>
                                        </div>
                                        
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="160x600-VR">

                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="160" height="600" id="160x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/vr/160x600.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/vr/160x600.swf" width="160" height="600">
                                                            <param name="movie" value="160x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>

                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="300x250-VR">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="250" id="300x250" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/vr/300x250.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/vr/300x250.swf" width="300" height="250">
                                                            <param name="movie" value="300x250.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="300x600-VR">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="600" id="300x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/vr/300x600.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/vr/300x600.swf" width="300" height="600">
                                                            <param name="movie" value="300x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="728x90-VR">

                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="728" height="90" id="728x90" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/vr/728x90.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/vr/728x90.swf" width="728" height="90">
                                                            <param name="movie" value="728x90.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>


                                            <p>Animated banners created for Victory Ranch in several sizes, optimized for high web conversions.</p>
                                        </div>
                                        
                                    </div>    
                                </div>
                                
                                
                            </div>
                        </div>
                        
                        
                        
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><i class="fa fa-arrow-right arrow-pad"></i> Espiritu - A Life Unlike Any Other</a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                               <div class="panel-body">
                                    <div class="text-center">

                                        <!-- Nav tabs -->

                                        <div class="tab-center">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#160x600-Espiritu" aria-controls="160x600-Espiritu" role="tab" data-toggle="tab">160x600</a>
                                                </li>
                                                <li role="presentation"><a href="#300x250-Espiritu" aria-controls="300x250-Espiritu" role="tab" data-toggle="tab">300x250</a>
                                                </li>
                                                <li role="presentation"><a href="#300x600-Espiritu" aria-controls="300x600-Espiritu" role="tab" data-toggle="tab">300x600</a>
                                                </li>
                                                <li role="presentation"><a href="#728x90-Espiritu" aria-controls="728x90-Espiritu" role="tab" data-toggle="tab">728x90</a>
                                                </li>
                                            </ul>
                                        </div>
                                        
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="160x600-Espiritu">

                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="160" height="600" id="160x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/espiritu/Espiritu160x600-34k.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/espiritu/Espiritu160x600-34k.swf" width="160" height="600">
                                                            <param name="movie" value="160x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>

                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="300x250-Espiritu">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="250" id="300x250" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/espiritu/Espiritu300x250-40k.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/espiritu/Espiritu300x250-40k.swf" width="300" height="250">
                                                            <param name="movie" value="300x250.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="300x600-Espiritu">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="600" id="300x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/espiritu/Espiritu300x600-40k.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/espiritu/Espiritu300x600-40k.swf" width="300" height="600">
                                                            <param name="movie" value="300x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="728x90-Espiritu">

                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="728" height="90" id="728x90" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/espiritu/Espiritu728x90-29k.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/espiritu/Espiritu728x90-29k.swf" width="728" height="90">
                                                            <param name="movie" value="728x90.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>


                                            <p>Animated banners created for Espiritu in several sizes, optimized for high web conversions.</p>
                                        </div>
                                        
                                    </div>    
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><i class="fa fa-arrow-right arrow-pad"></i> Spanish Palms - Las Vegas' Most In-Demand Condos</a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse">
                               <div class="panel-body">
                                    <div class="text-center">

                                        <!-- Nav tabs -->

                                        <div class="tab-center">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#160x600-SpanishDemand" aria-controls="160x600-SpanishDemand" role="tab" data-toggle="tab">160x600</a>
                                                </li>
                                                 <li role="presentation"><a href="#180x150-SpanishDemand" aria-controls="180x150-SpanishDemand" role="tab" data-toggle="tab">180x150</a>
                                                </li>
                                                <li role="presentation"><a href="#300x250-SpanishDemand" aria-controls="300x250-SpanishDemand" role="tab" data-toggle="tab">300x250</a>
                                                </li>
                                                <li role="presentation"><a href="#300x600-SpanishDemand" aria-controls="300x600-SpanishDemand" role="tab" data-toggle="tab">300x600</a>
                                                </li>
                                                <li role="presentation"><a href="#728x90-SpanishDemand" aria-controls="728x90-SpanishDemand" role="tab" data-toggle="tab">728x90</a>
                                                </li>
                                            </ul>
                                        </div>
                                        
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="160x600-SpanishDemand">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="160" height="600" id="160x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/sp-most-in-demand/sp-In-Demand-160x600-40k.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/sp-most-in-demand/sp-In-Demand-160x600-40k.swf" width="160" height="600">
                                                            <param name="movie" value="160x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>

                                            </div>
                                            
                                            
                                            
                                             <div role="tabpanel" class="tab-pane fade" id="180x150-SpanishDemand">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="180" height="150" id="180x150" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/sp-most-in-demand/sp-In-Demand-180x150-40k.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/sp-most-in-demand/sp-In-Demand-180x150-40k.swf" width="180" height="150">
                                                            <param name="movie" value="180x150.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>

                                            </div>
                                            
                                            
                                            
                                            
                                            
                                            <div role="tabpanel" class="tab-pane fade" id="300x250-SpanishDemand">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="250" id="300x250" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/sp-most-in-demand/sp-In-Demand-300x250-40k.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/sp-most-in-demand/sp-In-Demand-300x250-40k.swf" width="300" height="250">
                                                            <param name="movie" value="300x250.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            
                                            
                                            <div role="tabpanel" class="tab-pane fade" id="300x600-SpanishDemand">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="600" id="300x600" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/sp-most-in-demand/sp-In-Demand-300x600-40k.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#ffffff" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/sp-most-in-demand/sp-In-Demand-300x600-40k.swf" width="300" height="600">
                                                            <param name="movie" value="300x600.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#ffffff" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            
                                            
                                            <div role="tabpanel" class="tab-pane fade" id="728x90-SpanishDemand">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="728" height="90" id="728x90" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/sp-most-in-demand/sp-In-Demand-728x90-40k.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/sp-most-in-demand/sp-In-Demand-728x90-40k.swf" width="728" height="90">
                                                            <param name="movie" value="728x90.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>


                                            <p>Animated banners created for Spanish Palms in several sizes, optimized for high web conversions.</p>
                                        </div>
                                        
                                    </div>    
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><i class="fa fa-arrow-right arrow-pad"></i> Terranea - The Villas at Terranea</a>
                                </h4>
                            </div>
                            <div id="collapseSeven" class="panel-collapse collapse">
                               <div class="panel-body">
                                    <div class="text-center">

                                        <!-- Nav tabs -->

                                        <div class="tab-center">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#300x250-Terranea" aria-controls="300x250-Terranea" role="tab" data-toggle="tab">300x250</a>
                                                </li>
                                                <li role="presentation"><a href="#728x90-Terranea" aria-controls="728x90-Terranea" role="tab" data-toggle="tab">728x90</a>
                                                </li>
                                            </ul>
                                        </div>
                                        
                                        <!-- Tab panes -->
                                            
                                   
                                    <div class="tab-content"> 
                                            <div role="tabpanel" class="tab-pane active" id="300x250-Terranea">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="300" height="250" id="300x250" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/terranea/300x250.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/terranea/300x250.swf" width="300" height="250">
                                                            <param name="movie" value="300x250.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            <div role="tabpanel" class="tab-pane fade" id="728x90-Terranea">
                                                <div id="flashContent" align="center" class="flash-contain">
                                                    <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="728" height="90" id="728x90" align="middle">
                                                        <param name="movie" value="<?php bloginfo('template_directory'); ?>/banners/terranea/728x90.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="bgcolor" value="#000000" />
                                                        <param name="play" value="true" />
                                                        <param name="loop" value="true" />
                                                        <param name="wmode" value="window" />
                                                        <param name="scale" value="showall" />
                                                        <param name="menu" value="true" />
                                                        <param name="devicefont" value="false" />
                                                        <param name="salign" value="" />
                                                        <param name="allowScriptAccess" value="sameDomain" />
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="<?php bloginfo('template_directory'); ?>/banners/terranea/728x90.swf" width="728" height="90">
                                                            <param name="movie" value="728x90.swf" />
                                                            <param name="quality" value="high" />
                                                            <param name="bgcolor" value="#000000" />
                                                            <param name="play" value="true" />
                                                            <param name="loop" value="true" />
                                                            <param name="wmode" value="window" />
                                                            <param name="scale" value="showall" />
                                                            <param name="menu" value="true" />
                                                            <param name="devicefont" value="false" />
                                                            <param name="salign" value="" />
                                                            <param name="allowScriptAccess" value="sameDomain" />
                                                            <!--<![endif]-->
                                                            <a href="http://www.adobe.com/go/getflash">
                                                                <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
                                                            </a>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </div>
                                            </div>


                                            <p>Animated banners created for Terranea in several sizes, optimized for high web conversions.</p>
                                        </div>
                                        
                                    </div>    
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        <!-- Close End of Banner Groups -->
                    </div>

                   
                </div>
            </div>
        </div>
    </div>
    <!-- page-bloc END -->


    <!-- ScrollToTop Button -->
    <a class="bloc-button btn scrollToTop" onclick="scrollToTarget('1')"><img src="<?php bloginfo('template_directory'); ?>/img/arrow-up-gray.png" height="52" width="22" alt="scroll to top"></a>
    <!-- ScrollToTop Button END-->
    <!-- Footer - footer -->

    <div class="bloc l-bloc " id="footer">
        <div class="container bloc-lg">
            <div class="row">
                <div class="col-sm-12">
                    <div class="divider-h">
                        <hr />
                    </div>
                </div>
            </div>

            <div class="row voffset">
                <div class="col-xs-12 col-sm-3 col-sm-offset-3"><img src="<?php bloginfo('template_directory'); ?>/img/tac-logo.jpg" width="100" alt='TAC logo'>
                </div>

                <div class="col-xs-12 col-sm-3">
                    <h5 class="mg-md"><strong>CONTACT</strong></h5>

                    <p><a href="mailto:info@theagencycreates.com">info@theagencycreates.com</a>
                    </p>
                    <!-- <a href="tel:310.480.9100">310.480.9100</a> -->
                </div>

                <div class="col-xs-12 col-sm-3">
                    <h5 class="mg-md"><strong>OFFICE</strong></h5>

                    <p>The Agency Creative
                        <br /> 331 Foothill Road, Suite 100
                        <br /> Beverly Hills, CA 90210</p>
                </div>

                <!-- <div class="col-xs-6 col-sm-3">
                        <h5 class="mg-md"><strong>SOCIAL</strong></h5>
						<ul class="social list-inline">
							<li><a href="#">Facebook</a></li>
							<li><a href="#">Linkedin</a></li>
							<li><a href="#">Twitter</a></li>
							<li><a href="#">Instagram</a></li>
						</ul>
                    </div> -->
            </div>
        </div>
    </div>
    <!-- Footer - footer END -->

</div>

<?php get_footer(); ?>
