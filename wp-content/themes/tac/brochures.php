<?php /* Template Name: Brochures */ ?>


<div id="hero-bloc" class="bloc bgc-2 d-bloc" style="margin-top:10px;">
    <div class="container-fluid">
        <div class="col-sm-6 col-sm-offset-3 text-center">
            <a href="/digital-work"><img src="<?php bloginfo('template_directory'); ?>/img/tac-logo.png" width="210" height="253" class="center-block img-responsive" alt="The Agency Creative logo" /></a>
            <h2 class="text-center">Brochures</h2>
            <h3 class="text-center">Passionate about developing luxury and lifestyle brands through artfully persuasive storytelling, design and communications.</h3>
        </div>
    </div>
</div>
<!-- Hero Bloc END -->

<?php get_header(); ?>

<div class="page-container">
    <!-- page bloc -->
    <div class="bloc l-bloc">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4"><?php echo do_shortcode('[real3dflipbook id="1"]'); ?></div>
                            <div class="col-md-4"><?php echo do_shortcode('[real3dflipbook id="2"]'); ?></div>
                            <div class="col-md-4"><?php echo do_shortcode('[real3dflipbook id="4"]'); ?></div>
                            <div class="col-md-4"><?php echo do_shortcode('[real3dflipbook id="6"]'); ?></div>
                            <div class="col-md-4"><?php echo do_shortcode('[real3dflipbook id="7"]'); ?></div>
                            <div class="col-md-4"><?php echo do_shortcode('[real3dflipbook id="8"]'); ?></div>
                            <div class="col-md-4"><?php echo do_shortcode('[real3dflipbook id="9"]'); ?></div>
                            <div class="col-md-4"><?php echo do_shortcode('[real3dflipbook id="10"]'); ?></div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- page-bloc END -->
</div>




<!-- ScrollToTop Button -->
<a class="bloc-button btn scrollToTop" onclick="scrollToTarget('1')"><img src="<?php bloginfo('template_directory'); ?>/img/arrow-up-gray.png" height="52" width="22" alt="scroll to top"></a>
<!-- ScrollToTop Button END-->
<!-- Footer - footer -->

<div class="bloc l-bloc " id="footer">
    <div class="container bloc-lg">
        <div class="row">
            <div class="col-sm-12">
                <div class="divider-h">
                    <hr />
                </div>
            </div>
        </div>

        <div class="row voffset">
            <div class="col-xs-12 col-sm-3 col-sm-offset-3"><img src="<?php bloginfo('template_directory'); ?>/img/tac-logo.jpg" width="100" alt='TAC logo'>
            </div>

            <div class="col-xs-12 col-sm-3">
                <h5 class="mg-md"><strong>CONTACT</strong></h5>

                <p><a href="mailto:info@theagencycreates.com">info@theagencycreates.com</a>
                </p>
                <!-- <a href="tel:310.480.9100">310.480.9100</a> -->
            </div>

            <div class="col-xs-12 col-sm-3">
                <h5 class="mg-md"><strong>OFFICE</strong></h5>

                <p>The Agency Creative
                    <br /> 331 Foothill Road, Suite 100
                    <br /> Beverly Hills, CA 90210</p>
            </div>

            <!-- <div class="col-xs-6 col-sm-3">
                        <h5 class="mg-md"><strong>SOCIAL</strong></h5>
						<ul class="social list-inline">
							<li><a href="#">Facebook</a></li>
							<li><a href="#">Linkedin</a></li>
							<li><a href="#">Twitter</a></li>
							<li><a href="#">Instagram</a></li>
						</ul>
                    </div> -->
        </div>
    </div>
</div>
<!-- Footer - footer END -->

<?php get_footer(); ?>
