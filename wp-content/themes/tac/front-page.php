<?php get_header(); ?>
    <nav class="navbar navbar-fixed-top">
		<div class="container-fluid bloc-sm hero-nav">
			<div class="navbar-header">
				<button id="nav-toggle" type="button" class="ui-navbar-toggle navbar-toggle" data-toggle="collapse" data-target=".navbar-1">
					<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse navbar-1">
				<ul class="site-navigation nav navbar-nav navbar-right">
					<li>
						<a href="#who">WHO</a>
					</li>
                    <li>
						<a href="#what">WHAT</a>
					</li>
                    <li>
						<a href="#team">TEAM</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- Main container -->

    <div class="page-container">
        <!-- Hero Bloc -->

        <div id="hero-bloc" class="hero bgc-2 d-bloc clearfix">
	        <div class="fullscreen-bg">
		        <video loop muted autoplay poster="<?php bloginfo('template_directory'); ?>/img/bg-lion.png" class="fullscreen-bg-video">
			        <source src="<?php bloginfo('template_directory'); ?>/video/lion.webm" type="video/webm">
			        <source src="<?php bloginfo('template_directory'); ?>/video/lion.mp4" type="video/mp4">
			        <source src="<?php bloginfo('template_directory'); ?>/video/lion.ogv" type="video/ogg">
			    </video>
	        </div>
            <div class="v-center text-center">
                <div class="vc-content">
                    <div class="container-fluid">
	                    <div class="col-sm-4 col-sm-offset-8 text-center">
							<a class="hero-image" href="/"><img src="<?php bloginfo('template_directory'); ?>/img/tac-logo.png" width="250" height="301" class="center-block img-responsive" alt="The Agency Creative logo"/></a>
                        	<h2 class="text-center">We are a<span class="mobile-break"></span>creative agency</h2>
							<h3 class="text-center">With an insatiable hunger to command attention through masterful design and powerful storytelling.</h3>
                    	</div>
                    </div><a id="scroll-hero" class="btn-dwn">&darr;</a>
                </div>
            </div>
        </div><!-- Hero Bloc END -->
        <!-- bloc-2 -->

        <div class="bloc l-bloc clearfix" id="who">
            <div class="container bloc-lg">
                <div class="row">
                    <div class="col-sm-2 col-xs-12">
                        <h3 class="mg-md"><strong>WHO</strong><span class="mobile-break"></span>WE&nbsp;ARE</h3>
                    </div>

                    <div class="col-sm-offset-1 col-sm-9 col-xs-12">
                        <p>The Agency Creative is the in-house creative, communications and public relations division of The Agency, the global real estate brokerage and lifestyle company based in Beverly Hills. We specialize in the art of developing luxury and lifestyle brands, offering a strategically driven, integrated approach to marketing, branding, and design.<br />
                        <br />
                        Like a bespoke three-piece suit, our marketing strategies are tailored to meet the needs of our individual clients. From traditional advertising to innovative digital design, we create brand defining experiences for a wide range of clients, including real estate ventures, luxury resorts, fashion brands, celebrities and red carpet affairs.<br /></p>
                    </div>
                </div>
            </div>
        </div><!-- bloc-2 END -->
        <!-- bloc-3 -->

        <div class="bloc l-bloc " id="what">
            <div class="container bloc-lg">
                <div class="row">
                    <div class="col-sm-2 col-xs-12">
                        <h3 class="mg-md"><strong>WHAT</strong><span class="mobile-break"></span>WE DO</h3>
                    </div>

                    <div class="col-xs-8 col-xs-offset-2 col-sm-offset-1 col-sm-9 pl0 pr0">
                        <div id="what-do" class="row">
                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Advertising Creative</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Brand Identity &amp; Development</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Collateral Material Development</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Content Creation</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Visual Storytelling</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Website Design &amp; Development</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Customer Acquisition</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Digital Marketing</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Direct Marketing</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Event &amp; Experiential Marketing</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Social Media</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Marketing Strategies &amp; Positioning</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Psychographics</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Public Relations</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Research &amp; Analytics</h5>
                            </div>

                            <div class="col-xs-6 col-sm-4 do-box">
                                <h5 class="text-center mg-md">Search Marketing</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- bloc-3 END -->
        <!-- bloc-8 -->

        <div class="bloc l-bloc " id="team">
            <div class="container bloc-lg">
                <div class="row">
                    <div class="col-sm-2 col-xs-12">
                        <h3>THE<span class="mobile-break"></span><strong>TEAM</strong></h3>
                    </div>
                    <div class="col-sm-9 col-sm-offset-1">
                        <h3 class="text-left">Mike Leipart</h3>

                        <p class="text-left"><strong>Founder // Partner</strong></p>

                        <p class="text-left">As co-founder of The Agency Creative, Mike also serves as the Managing Partner of The Agency’s New Development Division. With years of experience coordinating strategic partnerships, an in-house creative team at his disposal, and the finest real estate agents in the business, Mike is in a unique position to guide prospective developers of luxury properties through the entire development process, allowing for a “one stop shop” that caters to every need. Previously, Mike worked for ST Residential, where he served as Senior Vice President, National Marketing Director. In that role, Mike created the national brand strategy for ST, as well as launching nearly 50 multi-family properties nationally (a $3Billion portfolio). This included all aspects of sales from brochures to website to digital strategy. He planned, purchased and measured a yearly media budget of $10 Million.</p>
                    </div>
                </div>
                <div class="row mg-md">
                    <div class="col-sm-9 col-sm-offset-3">
                        <h3 class="text-left">Carolina Celeboglu</h3>

                        <p class="text-left"><strong>Creative Director</strong></p>

                        <p class="text-left">With a career that spans over 10 years and a host of international clientele to her credit, Carolina Celeboglu now serves as Creative Director for the marketing department at The Agency, overseeing the design, copywriting, branding strategy and other means of exposure for a particular brand. Her strength is not only in managing the details, but in articulating a client’s vision from concept to execution, a process that involves developing the voice, look, and feel of a marketing campaign and all its collateral across all platforms. One of her most significant contributions was helping to start the ID at The Agency program, an integrated package offering specific tailored marketing, PR and creative campaign by listing. Carolina is very comfortable managing a fleet of designers, coordinators and copywriters and loves the collaborative process.</p>
                    </div>
                </div>
                <div class="row mg-md">
                    <div class="col-sm-9 col-sm-offset-3">
                        <h3 class="text-left">Charles Simmons</h3>

                        <p class="text-left"><strong>Director of Digital Engagement</strong></p>

                        <p class="text-left">As Director of Digital Engagement for The Agency Creative, Charles Simmons leads the creation of compelling, innovative and impactful marketing campaigns across all digital channels. He originates and executes websites with a keen understanding of the nuanced elements that go into developing a brand’s online presence. Charles excels at creating engaging user experiences and leveraging new media and technologies to develop fully integrated digital campaigns. He manages all aspects of a client’s online marketing including web development, digital advertising, search engine marketing, web analytics, and email marketing.</p>
                    </div>
                </div>
                <div class="row mg-md">
                    <div class="col-sm-9 col-sm-offset-3">
                        <h3 class="text-left">Kate Schillace</h3>

                        <p class="text-left"><strong>Art Director</strong></p>

                        <p class="text-left">As Art Director at The Agency Creative, Kate has led the visual development of multiple brands including Bodytraffic, a renowned LA-based dance company, The Agency, Espiritu, a luxury residential resort in Los Cabos, and a collection of Northcap properties including The Ogden and Spanish Palms in Las Vegas. A Los Angeles native, Kate attended Art Center College of Design, with a focus in corporate branding.</p>
                    </div>
                </div>
                <div class="row mg-md">
                    <div class="col-sm-9 col-sm-offset-3">
                        <h3 class="text-left">Meg Kollar</h3>

                        <p class="text-left"><strong>Public Relations Director</strong></p>

                        <p class="text-left">Meg Kollar is the Public Relations Director for The Agency Creative where she oversees the firm’s communications activities. Meg manages media relations, internal communications, content marketing efforts and serves as a liaison with the company’s brand partnerships. Additionally, Meg executed several sponsorships at events for The Agency and their clients. Meg has directed PR in connection with high-profile clients, real estate listings / trends, new development and resort projects, and corporate events. She manages and contributes to the company blog and several social media feeds. Meg’s media contacts extend from television to national, local and international print and digital journalists.</p>
                    </div>
                </div>
                <div class="row mg-md">
                    <div class="col-sm-9 col-sm-offset-3">
                        <h3 class="text-left">Michelle Jenkins</h3>

                        <p class="text-left"><strong>Account Director</strong></p>

                        <p class="text-left">As Account Director for The Agency Creative, Michelle Jenkins leads marketing efforts for the New Development Division. With experience guiding a range of projects, including condominiums, apartments and resort developments, Michelle manages the creative process from brand ideation through to final launch. By planning and managing the creative process, Michelle ensures projects are delivered on time and exceed project goals.</p>
                    </div>
                </div>
				<div class="row mg-md">
                    <div class="col-sm-9 col-sm-offset-3">
                        <h3 class="text-left">Nicole Montgomery</h3>

                        <p class="text-left"><strong>Senior Copywriter</strong></p>

                        <p class="text-left">Nicole Montgomery is a Senior Copywriter and resident storyteller at The Agency Creative.  Nicole leads content development and brand positioning by telling relatable stories that resonate across all media platforms. Prior to joining The Agency Creative, Nicole co-directed marketing efforts and created the voice for the luxury residential resort development, Villas Del Mar in Los Cabos, Mexico. With a strong journalism background, Nicole was a writer and producer for the CBS News radio affiliates in New York City and Washington D.C.</p>
                    </div>
                </div>
                <div id="who-are" class="row mg-md pt20">
				    <div class="col-sm-offset-3 col-sm-9">
				        <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">BILLY ROSE</h4>
				            <p class="text-center">Co-Chairman//Founder</p>
				        </div>
				         <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">CAILAN ROBINSON</h4>
				            <p class="text-center">Account Director</p>
				        </div>
				        <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">CARLOS BUITRON</h4>
				            <p class="text-center">Developer</p>
				        </div>
				        <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">EMILY GREEN</h4>
				            <p class="text-center">Account Manager</p>
				        </div>
				        <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">EMILY JOHNSTON</h4>
				            <p class="text-center">Senior Designer</p>
				        </div>
				        <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">HELEN TRUONG</h4>
				            <p class="text-center">Graphic Designer</p>
				        </div>
				        <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">JESSICA DRUXMAN</h4>
				            <p class="text-center">Production Designer</p>
				        </div>
						<div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">JULIE MARZOUK</h4>
				            <p class="text-center">Email Marketing Associate</p>
				        </div>
				        <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">KIT LANDRY</h4>
				            <p class="text-center">Studio Coordinator</p>
				        </div>
				        <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">KYLE LANGAN</h4>
				            <p class="text-center">Social Media Manager</p>
				        </div>
				        <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">KYLEE BROUGHTON</h4>
				            <p class="text-center">Content Writer</p>
				        </div>
						<div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">KRISTIN VIOLET</h4>
				            <p class="text-center">Graphic Designer</p>
				        </div>
				        <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">MAURICIO UMANSKY</h4>
				            <p class="text-center">Co-Chairman//Founder</p>
				        </div>
						<div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">SAM GREEN</h4>
				            <p class="text-center">Account Manager</p>
				        </div>
				        <div class="col-xs-6 col-sm-4 who-box">
				            <h4 class="text-center mt20">YURI HUXLEY</h4>
				            <p class="text-center">Copywriter</p>
				        </div>
				    </div>
				</div>
            </div>
        </div><!-- bloc-8 END -->
        <!-- ScrollToTop Button -->
        <a class="bloc-button btn scrollToTop" onclick="scrollToTarget('1')"><img src="<?php bloginfo('template_directory'); ?>/img/arrow-up-gray.png" height="52" width="22" alt="scroll to top"></a> <!-- ScrollToTop Button END-->
         <!-- Footer - footer -->

        <div class="bloc l-bloc " id="footer">
            <div class="container bloc-lg">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="divider-h"><hr /></div>
                    </div>
                </div>

                <div class="row voffset">
                    <div class="col-xs-12 col-sm-3 col-sm-offset-3"><img src="<?php bloginfo('template_directory'); ?>/img/tac-logo.jpg" width="100" alt='TAC logo'></div>

                    <div class="col-xs-12 col-sm-3">
                        <h5 class="mg-md"><strong>CONTACT</strong></h5>

                        <p><a href="mailto:info@theagencycreates.com">info@theagencycreates.com</a></p>
                        <!-- <a href="tel:310.480.9100">310.480.9100</a> -->
                    </div>

                    <div class="col-xs-12 col-sm-3">
                        <h5 class="mg-md"><strong>OFFICE</strong></h5>

                        <p>The Agency Creative<br />
                        331 Foothill Road, Suite 100<br />
                        Beverly Hills, CA 90210</p>
                    </div>

                    <!-- <div class="col-xs-6 col-sm-3">
                        <h5 class="mg-md"><strong>SOCIAL</strong></h5>
						<ul class="social list-inline">
							<li><a href="#">Facebook</a></li>
							<li><a href="#">Linkedin</a></li>
							<li><a href="#">Twitter</a></li>
							<li><a href="#">Instagram</a></li>
						</ul>
                    </div> -->
                </div>
            </div>
        </div><!-- Footer - footer END -->
    </div><!-- Main container END -->
<?php get_footer(); ?>
