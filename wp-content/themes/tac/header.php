<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>The Agency Creative - A Creative Agency in Beverly Hills</title>
    <meta name="description" content="The Agency Creative - A creative agency developing luxury lifestyle brands through potently artful design communications. Beverly Hills.">
    <link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css">
    <link rel="stylesheet" id="ppstyle" type="text/css" href="<?php bloginfo('template_directory'); ?>/style.css">
    <link rel='stylesheet' href='<?php bloginfo('template_directory'); ?>/css/animate.css' type="text/css">
    <link rel='stylesheet' href='<?php bloginfo('template_directory'); ?>/css/font-awesome.min.css' type="text/css">
    <script src="//use.typekit.net/ogq2jef.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	<?php wp_head(); ?>
	<script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/blocs.js" type="text/javascript"></script>
</head>
<body data-spy="scroll" data-target=".navbar" <?php body_class(); ?>>
