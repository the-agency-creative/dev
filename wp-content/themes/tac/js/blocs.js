// page init
jQuery(function(){
	initSameHeight();
});
// Startup Scripts
jQuery(document).ready(function($) {
	$('.hero').css('height', ($(window).height() - $('header').outerHeight()) + 'px'); // Set hero to fill page height

	$('#scroll-hero').click(function()
	{
		$('html,body').animate({scrollTop: $("#hero-bloc").height()}, 'slow');
	});
	$(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 50) {
            $(".navbar").addClass("solid-bg");
        } else {
	        $(".navbar").removeClass("solid-bg");
        }
     });
     animateWhenVisable();  // Activate animation when visable
     // Window resize
	$(window).resize(function() {
		$('.hero').css('height', ($(window).height() - $('header').outerHeight()) + 'px'); // Refresh hero height
	});
	// Initial tooltips
	$(function()
	{
	  $('[data-toggle="tooltip"]').tooltip()
	})
	// Scroll to target
	function scrollToTarget(D)
	{
		if(D == 1) // Top of page
		{
			D = 0;
		}
		else if(D == 2) // Bottom of page
		{
			D = $(document).height();
		}
		else // Specific Bloc
		{
			D = $(D).offset().top;
		}

		$('html,body').animate({scrollTop:D}, 'slow');
	}

	// Animate when visable
	function animateWhenVisable()
	{
		$('.animated').removeClass('animated').addClass('hideMe'); // replace animated with hide

		inViewCheck(); // Initail check on page load

		$(window).scroll(function()
		{
			inViewCheck(); // Check object visability on page scroll
			scrollToTopView(); // ScrollToTop button visability toggle
		});
	};

	// Check if object is inView
	function inViewCheck()
	{
		$($(".hideMe").get().reverse()).each(function(i)
		{
			var target = jQuery(this);

			a = target.offset().top + target.height();
			b = $(window).scrollTop() + $(window).height();

			if (a < b)
			{

				var objectClass = target.attr('class').replace('hideMe' , 'animated');

				if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1) // If Firefox
				{
					target.css('visibility','hidden').removeAttr('class');
					setTimeout(function(){target.attr('class',objectClass).css('visibility','visable');},0.1);
				}
				else
				{
					target.attr('class',objectClass)
				}
			}
		});
	};

	// ScrollToTop button toggle
	function scrollToTopView()
	{
		if($(window).scrollTop() > $(window).height()/3)
		{
			if(!$('.scrollToTop').hasClass('showScrollTop'))
			{
				$('.scrollToTop').addClass('showScrollTop');
			}
		}
		else
		{
			$('.scrollToTop').removeClass('showScrollTop');
		}
	};
});

/*
 * jQuery SameHeight plugin
 */
;(function($){
	$.fn.sameHeight = function(opt) {
		var options = $.extend({
			skipClass: 'same-height-ignore',
			leftEdgeClass: 'same-height-left',
			rightEdgeClass: 'same-height-right',
			elements: '>*',
			flexible: false,
			multiLine: false,
			useMinHeight: false,
			biggestHeight: false
		},opt);
		return this.each(function(){
			var holder = $(this), postResizeTimer, ignoreResize;
			var elements = holder.find(options.elements).not('.' + options.skipClass);
			if(!elements.length) return;

			// resize handler
			function doResize() {
				elements.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', '');
				if(options.multiLine) {
					// resize elements row by row
					resizeElementsByRows(elements, options);
				} else {
					// resize elements by holder
					resizeElements(elements, holder, options);
				}
			}
			doResize();

			// handle flexible layout / font resize
			var delayedResizeHandler = function() {
				if(!ignoreResize) {
					ignoreResize = true;
					doResize();
					clearTimeout(postResizeTimer);
					postResizeTimer = setTimeout(function() {
						doResize();
						setTimeout(function(){
							ignoreResize = false;
						}, 10);
					}, 100);
				}
			};

			// handle flexible/responsive layout
			if(options.flexible) {
				$(window).bind('resize orientationchange fontresize', delayedResizeHandler);
			}

			// handle complete page load including images and fonts
			$(window).bind('load', delayedResizeHandler);
		});
	};

	// detect css min-height support
	var supportMinHeight = typeof document.documentElement.style.maxHeight !== 'undefined';

	// get elements by rows
	function resizeElementsByRows(boxes, options) {
		var currentRow = $(), maxHeight, maxCalcHeight = 0, firstOffset = boxes.eq(0).offset().top;
		boxes.each(function(ind){
			var curItem = $(this);
			if(curItem.offset().top === firstOffset) {
				currentRow = currentRow.add(this);
			} else {
				maxHeight = getMaxHeight(currentRow);
				maxCalcHeight = Math.max(maxCalcHeight, resizeElements(currentRow, maxHeight, options));
				currentRow = curItem;
				firstOffset = curItem.offset().top;
			}
		});
		if(currentRow.length) {
			maxHeight = getMaxHeight(currentRow);
			maxCalcHeight = Math.max(maxCalcHeight, resizeElements(currentRow, maxHeight, options));
		}
		if(options.biggestHeight) {
			boxes.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', maxCalcHeight);
		}
	}

	// calculate max element height
	function getMaxHeight(boxes) {
		var maxHeight = 0;
		boxes.each(function(){
			maxHeight = Math.max(maxHeight, $(this).outerHeight());
		});
		return maxHeight;
	}

	// resize helper function
	function resizeElements(boxes, parent, options) {
		var calcHeight;
		var parentHeight = typeof parent === 'number' ? parent : parent.height();
		boxes.removeClass(options.leftEdgeClass).removeClass(options.rightEdgeClass).each(function(i){
			var element = $(this);
			var depthDiffHeight = 0;
			var isBorderBox = element.css('boxSizing') === 'border-box' || element.css('-moz-box-sizing') === 'border-box' || element.css('-webkit-box-sizing') === 'border-box';

			if(typeof parent !== 'number') {
				element.parents().each(function(){
					var tmpParent = $(this);
					if(parent.is(this)) {
						return false;
					} else {
						depthDiffHeight += tmpParent.outerHeight() - tmpParent.height();
					}
				});
			}
			calcHeight = parentHeight - depthDiffHeight;
			calcHeight -= isBorderBox ? 0 : element.outerHeight() - element.height();

			if(calcHeight > 0) {
				element.css(options.useMinHeight && supportMinHeight ? 'minHeight' : 'height', calcHeight);
			}
		});
		boxes.filter(':first').addClass(options.leftEdgeClass);
		boxes.filter(':last').addClass(options.rightEdgeClass);
		return calcHeight;
	}
}(jQuery));
// align blocks height
function initSameHeight() {
	jQuery('#what-do').sameHeight({
		elements: '.do-box',
		flexible: true,
		multiLine: true
	});
	jQuery('#who-are').sameHeight({
		elements: '.who-box',
		flexible: true,
		multiLine: true
	});
	jQuery('#client-logos').sameHeight({
		elements: '.logo-box',
		flexible: true,
		multiLine: true
	});
}
