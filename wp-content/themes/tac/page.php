<?php
/*
Template Name: Name
*/
?>

<?php get_header(); ?>

	<div class="page-container">
        <!-- page bloc -->
        <div class="bloc l-bloc">
            <div class="container bloc-lg">
                <div class="row">
                    <div class="col-md-12">
	                    <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div><!-- page-bloc END -->
	</div>

<?php get_footer(); ?>
