        <footer>
        	<div class="logo">
        		<a href="http://theagencyre.com/"><img src="<?php bloginfo('template_directory'); ?>/images/agency-footer.png" /></a>
        	</div>
        	<div class="credits">
        		<div class="no-break">Serving the Luxury Real Estate Market Worldwide <span class="hide">|</span></div> <div class="break"><a href="http://theagencyre.com/luxury-real-estate/">View more properties &rarr;</a></div>
        	</div>
        </footer>
        <script src="<?php bloginfo('template_directory'); ?>/js/vendor/modernizr-2.6.1.min.js"></script>
		</script>
        <script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-35272498-4']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
  		</script>
  		<?php wp_footer(); ?>
  		<script src="<?php bloginfo('template_directory'); ?>/js/nav/jquery.meanmenu.js"></script>
        <script>
  			jQuery(document).ready(function () {
			    jQuery('#nav').meanmenu();
			});
  		</script>
    </body>
</html>